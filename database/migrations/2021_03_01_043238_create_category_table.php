<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->String('name');
            $table->String('image')->nullable();
            $table->Text('banner')->nullable();
            $table->String('background')->nullable();
            $table->timestamps();
        });
        DB::table('category')->insert([
            [
                'name' => "Món ăn",
            ],
            [
                'name' => "Cafe",
            ],
            [
                'name' => "Trà sữa",
            ],
            [
                'name' => "Ăn vặt",
            ],
            [
                'name' => "Hoa tươi",
            ],
            [
                'name' => "Tạp hóa"
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
