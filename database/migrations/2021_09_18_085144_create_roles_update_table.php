<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRolesUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('roles')->insert([
            [
                'name' => "Tài khoản chuyển nhượng",
            ],
            [
                'name' => "Nhân viên tài khoản chuyển nhượng",
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
