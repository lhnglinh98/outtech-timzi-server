<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFoodUpdate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_food', function($table){
            $table->DateTime('time_check')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
