<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeWebsiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_website', function (Blueprint $table) {
            $table->increments('id');
            $table->String('title');
            $table->text('content');
            $table->timestamps();
        });
        DB::table('home_website')->insert([
            'title' => "Đặt món ăn, đồ uống & Hơn thế nữa",
            'content' => "Chúng tôi tạo ra ứng dụng giúp bạn giải quyết vấn đề đặt món ăn tại quá. Ngoài việc kiểm soát thực đơn menu, bạn có thể gọi món dễ dàng và hơn hết bạn biết được những món ngon nhất tại đó."
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_website');
    }
}
