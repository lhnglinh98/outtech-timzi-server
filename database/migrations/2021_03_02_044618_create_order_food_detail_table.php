<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFoodDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_food_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_food_id');
            $table->foreign('order_food_id')->references('id')->on('order_food');
            $table->unsignedInteger('card_food_id');
            $table->foreign('card_food_id')->references('id')->on('card_food');
            $table->unsignedInteger('combo_food_id')->nullable();
            $table->foreign('combo_food_id')->references('id')->on('combo_food');
            $table->unsignedInteger('food_id')->nullable();
            $table->foreign('food_id')->references('id')->on('food');
            $table->unsignedInteger('store_id');
            $table->foreign('store_id')->references('id')->on('stores');
            $table->integer('price')->default(0);
            $table->decimal('star', 2, 1)->nullable();
            $table->String('content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_food_detail');
    }
}
