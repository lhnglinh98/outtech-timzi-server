<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_delivery', function (Blueprint $table) {
            $table->increments('id');
            $table->String('title');
            $table->String('code');
            $table->text('description');
            $table->float('percent');
            $table->integer('type')->default(1);
            $table->integer('proviso')->default(0);
            $table->integer('money')->default(0);
            $table->integer('type_percent_or_money')->default(1);
            $table->integer('quantity')->default(0);
            $table->integer('quantity_used')->default(0);
            $table->integer('quantity_use_with_user_new')->default(0);
            $table->date('time_open')->nullable();
            $table->date('time_close')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_delivery');
    }
}
