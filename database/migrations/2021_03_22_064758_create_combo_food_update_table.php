<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComboFoodUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('combo_food', function($table){
            $table->Text('content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
