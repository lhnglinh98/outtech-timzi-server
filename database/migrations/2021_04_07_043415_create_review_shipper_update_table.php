<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewShipperUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('review_shipper', function($table){
            $table->unsignedInteger('order_food_id');
            $table->foreign('order_food_id')->references('id')->on('order_food');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
