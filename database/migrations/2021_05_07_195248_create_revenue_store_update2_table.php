<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevenueStoreUpdate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('revenue_store', function($table){
            $table->date('date_check')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
