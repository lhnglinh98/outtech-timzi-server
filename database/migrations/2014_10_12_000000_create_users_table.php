<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->String('phone')->unique();
            $table->string('password');
            $table->datetime('date_check_code')->nullable();
            $table->string('otp')->nullable();
            $table->unsignedInteger('role_id')->default(5);
            $table->string('device_id')->nullable();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->string('avatar')->nullable();
            $table->string('address')->nullable();
            $table->date('birthday')->nullable();
            $table->integer('gender')->default(1);
            $table->integer('coin')->default(0);
            $table->integer('is_notify')->default(1);
            $table->integer('is_main_shipper')->default(0);
            $table->String('career')->nullable();
            $table->String('driver_license_image')->nullable();
            $table->String('vehicle_registration_image')->nullable();
            $table->String('image_cmnd_front')->nullable();
            $table->String('image_cmnd_back')->nullable();
            $table->integer('status_shipper')->default(0);
            $table->integer('status_staff')->default(0);
            $table->integer('status_system_owner')->default(0);
            $table->integer('status_owner')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
        DB::table('users')->insert([
            [
                'code' => rand(10000000, 99999999),
                'name' => "Huy Nguyễn",
                'phone' => "0329714645",
                'role_id' => 1,
                'status' => 1,
                'password' => Hash::make("123456")
            ],
            [
                'code' => rand(10000000, 99999999),
                'name' => "Huy Nguyễn",
                'phone' => "0329714646",
                'role_id' => 2,
                'status' => 1,
                'password' => Hash::make("123456")
            ],
            [
                'code' => rand(10000000, 99999999),
                'name' => "Huy Nguyễn",
                'phone' => "0329714647",
                'role_id' => 3,
                'status' => 1,
                'password' => Hash::make("123456")
            ],
            [
                'code' => rand(10000000, 99999999),
                'name' => "Huy Nguyễn",
                'phone' => "0329714648",
                'role_id' => 4,
                'status' => 1,
                'password' => Hash::make("123456")
            ],
            [
                'code' => rand(10000000, 99999999),
                'name' => "Huy Nguyễn",
                'phone' => "0329714649",
                'role_id' => 5,
                'status' => 1,
                'password' => Hash::make("123456")
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
