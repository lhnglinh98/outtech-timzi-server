<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMergeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_merge', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('book_table_id');
            $table->foreign('book_table_id')->references('id')->on('book_table');
            $table->unsignedInteger('table_store_id');
            $table->foreign('table_store_id')->references('id')->on('table_store');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_merge');
    }
}
