<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodUpdate5Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('food', function($table){
            $table->integer('is_group_food')->default(0);
            $table->integer('quantity_food')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
