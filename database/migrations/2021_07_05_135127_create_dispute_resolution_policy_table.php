<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisputeResolutionPolicyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispute_resolution_policy', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('content');
        });
        DB::table('dispute_resolution_policy')->insert([
            'content' => "Chính sách giải quyết tranh chấp"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispute_resolution_policy');
    }
}
