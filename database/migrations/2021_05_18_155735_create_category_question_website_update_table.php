<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryQuestionWebsiteUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_question_website', function($table){
            $table->String('answer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
