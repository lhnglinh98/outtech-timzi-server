<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_food', function (Blueprint $table) {
            $table->increments('id');
            $table->String('code');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->String('user_name');
            $table->String('phone');
            $table->String('address');
            $table->String('latitude')->nullable();
            $table->String('longtidue')->nullable();
            $table->DateTime('date_time');
            $table->unsignedInteger('store_id');
            $table->foreign('store_id')->references('id')->on('stores');
            $table->integer('fee_ship')->default(0);
            $table->integer('discount')->default(0);
            $table->integer('total_money')->default(0);
            $table->integer('coin')->default(0);
            $table->text('note')->nullable();
            $table->unsignedInteger('payment_method_id')->nullable();
            $table->foreign('payment_method_id')->references('id')->on('payment_method');
            $table->unsignedInteger('shipper_id')->nullable();
            $table->foreign('shipper_id')->references('id')->on('users');
            $table->decimal('star', 2, 1)->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_food');
    }
}
