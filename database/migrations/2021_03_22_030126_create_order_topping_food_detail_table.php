<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderToppingFoodDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_topping_food_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_food_detail_id');
            $table->foreign('order_food_detail_id')->references('id')->on('order_food_detail');
            $table->unsignedInteger('topping_food_id');
            $table->foreign('topping_food_id')->references('id')->on('topping_food');
            $table->String('category_topping_food_name');
            $table->String('topping_food_name');
            $table->String('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_topping_food_detail');
    }
}
