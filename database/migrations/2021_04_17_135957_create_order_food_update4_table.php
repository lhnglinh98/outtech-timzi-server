<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFoodUpdate4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_food', function($table){
            $table->integer('money_voucher')->default(0);
            $table->integer('type_voucher')->default(0);
            $table->unsignedInteger('voucher_id')->nullable();
            $table->foreign('voucher_id')->references('id')->on('voucher');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_food');
    }
}
