<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramStoreDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_store_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('program_store_id');
            $table->foreign('program_store_id')->references('id')->on('program_store');
            $table->unsignedInteger('food_id');
            $table->foreign('food_id')->references('id')->on('food');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_store_detail');
    }
}
