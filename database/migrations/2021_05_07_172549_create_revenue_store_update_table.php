<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevenueStoreUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('revenue_store', function($table){
            $table->integer('late_day')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
