<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetupWithdrawMoneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_withdraw_money', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('money')->default(0);
            $table->timestamps();
        });
        DB::table('setup_withdraw_money')->insert([
            'money' => "50000",
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_withdraw_money');
    }
}
