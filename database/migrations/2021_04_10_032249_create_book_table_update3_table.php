<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookTableUpdate3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_table', function($table){
            $table->integer('coin')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
