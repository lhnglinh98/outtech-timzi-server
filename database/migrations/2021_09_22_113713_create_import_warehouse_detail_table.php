<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportWarehouseDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_warehouse_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('import_warehouse_id');
            $table->foreign('import_warehouse_id')->references('id')->on('import_warehouse');
            $table->unsignedInteger('material_id');
            $table->foreign('material_id')->references('id')->on('material');
            $table->integer('quantity')->default(0);
            $table->integer('money')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_warehouse_detail');
    }
}
