<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevenueStoreUpdate3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('revenue_store', function($table){
            $table->String('code')->nullable()->default(rand(1000000000, 9999999999));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
