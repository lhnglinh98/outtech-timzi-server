<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_food', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('combo_food_id')->nullable();
            $table->foreign('combo_food_id')->references('id')->on('combo_food');
            $table->unsignedInteger('food_id')->nullable();
            $table->foreign('food_id')->references('id')->on('food');
            $table->String('size')->nullable();
            $table->integer('quantity')->default(1);
            $table->integer('price')->default(0);
            $table->unsignedInteger('store_id');
            $table->foreign('store_id')->references('id')->on('stores');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_food');
    }
}
