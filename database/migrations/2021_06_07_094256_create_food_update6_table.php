<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodUpdate6Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('food', function($table){
            $table->unsignedInteger('type_food_id')->nullable();
            $table->foreign('type_food_id')->references('id')->on('type_food');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
