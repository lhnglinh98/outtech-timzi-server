<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookTableUpdate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_table', function($table){
            $table->integer('right_to_order')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
