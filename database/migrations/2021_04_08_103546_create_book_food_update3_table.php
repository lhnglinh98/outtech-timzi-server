<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookFoodUpdate3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_food', function($table){
            $table->unsignedInteger('combo_food_id')->nullable();
            $table->foreign('combo_food_id')->references('id')->on('combo_food');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
