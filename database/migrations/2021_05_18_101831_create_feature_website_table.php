<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeatureWebsiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_website', function (Blueprint $table) {
            $table->increments('id');
            $table->String('image')->nullable();
            $table->String('title');
            $table->text('content');
            $table->timestamps();
        });
        DB::table('feature_website')->insert([
            [
                'title' => "Tìm kiếm dễ dàng",
                'content' => "Chung ta sẽ tìm kiếm món ăn theo sở thích và đặt chúng 1 cách dễ dàng"
            ],
            [
                'title' => "Tìm kiếm dễ dàng",
                'content' => "Chung ta sẽ tìm kiếm món ăn theo sở thích và đặt chúng 1 cách dễ dàng"
            ],
            [
                'title' => "Tìm kiếm dễ dàng",
                'content' => "Chung ta sẽ tìm kiếm món ăn theo sở thích và đặt chúng 1 cách dễ dàng"
            ],
            [
                'title' => "Tìm kiếm dễ dàng",
                'content' => "Chung ta sẽ tìm kiếm món ăn theo sở thích và đặt chúng 1 cách dễ dàng"
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_website');
    }
}
