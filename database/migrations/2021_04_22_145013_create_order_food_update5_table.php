<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFoodUpdate5Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_food', function($table){
            $table->longText('polyline')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
