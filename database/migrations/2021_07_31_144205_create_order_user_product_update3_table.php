<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderUserProductUpdate3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_user_product', function($table){
            $table->text('image_send_product')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
