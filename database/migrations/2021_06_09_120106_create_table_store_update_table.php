<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableStoreUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('table_store', function($table){
            $table->String('number_table')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
