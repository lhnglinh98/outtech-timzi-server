<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderUserProductUpdate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_user_product', function($table){
            $table->float('from_weight')->default(0);
            $table->float('to_weight')->default(0);
            $table->integer('money_with_product_weight')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
