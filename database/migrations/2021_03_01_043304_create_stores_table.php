<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->String('name');
            $table->String('image');
            $table->String('address');
            $table->unsignedInteger('province_id')->nullable();
            $table->foreign('province_id')->references('id')->on('province');
            $table->unsignedInteger('district_id')->nullable();
            $table->foreign('district_id')->references('id')->on('district');
            $table->unsignedInteger('ward_id')->nullable();
            $table->foreign('ward_id')->references('id')->on('ward');
            $table->String('latitude')->nullable();
            $table->String('longtidue')->nullable();
            $table->decimal('star', 2, 1)->default(5);
            $table->integer('average_price')->default(0);
            $table->String('open_hours')->nullable();
            $table->String('close_hours')->nullable();
            $table->String('hotline')->nullable();
            $table->integer('is_owner')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
