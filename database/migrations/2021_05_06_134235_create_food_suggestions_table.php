<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodSuggestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_suggestions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('food_id');
            $table->foreign('food_id')->references('id')->on('food');
            $table->unsignedInteger('food_suggestions_id');
            $table->foreign('food_suggestions_id')->references('id')->on('food');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_suggestions');
    }
}
