<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryRechargeAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_recharge_account', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('money')->default(0);
            $table->integer('money_current')->default(0);
            $table->unsignedInteger('shipper_id');
            $table->foreign('shipper_id')->references('id')->on('users');
            $table->String('content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_recharge_account');
    }
}
