<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetupPercentFeeStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_percent_fee_store', function (Blueprint $table) {
            $table->increments('id');
            $table->float('percent');
            $table->timestamps();
        });
        DB::table('setup_percent_fee_store')->insert([
            [
                'percent' => 5,
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_percent_fee_store');
    }
}
