<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryQuestionWebsiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_question_website', function (Blueprint $table) {
            $table->increments('id');
            $table->String('name');
            $table->timestamps();
        });
        DB::table('category_question_website')->insert([
            [
                'name' => "TÔI NÊN LÀM CÁI GÌ TRONG ỨNG DỤNG NÀY?",
            ],
            [
                'name' => "ỨNG DỤNG NÀY LÀM VIỆC NHƯ THẾ NÀO?",
            ],
            [
                'name' => "ỨNG DỤNG NÀY CÓ MIỄN PHÍ HAY PHÍ NHƯ THẾ NÀO?",
            ],
            [
                'name' => "TÔI CÓ THỂ CÀI ĐẶT ỨNG DỤNG NÀY Ở ĐÂU?",
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_question_website');
    }
}
