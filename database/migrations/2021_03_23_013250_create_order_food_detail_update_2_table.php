<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFoodDetailUpdate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_food_detail', function($table){
            $table->integer('money_receive_shipper')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
