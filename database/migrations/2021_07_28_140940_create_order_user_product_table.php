<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderUserProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_user_product', function (Blueprint $table) {
            $table->increments('id');
            $table->String('code');
            $table->unsignedInteger('sender_id');
            $table->foreign('sender_id')->references('id')->on('users');
            $table->String('sender_name');
            $table->String('sender_phone');
            $table->String('sender_address');
            $table->String('latitude_sender')->nullable();
            $table->String('longtidue_sender')->nullable();
            $table->String('latitude_receiver')->nullable();
            $table->String('longtidue_receiver')->nullable();
            $table->unsignedInteger('receiver_id')->nullable();
            $table->foreign('receiver_id')->references('id')->on('users');
            $table->String('receiver_name');
            $table->String('receiver_phone');
            $table->String('receiver_address');
            $table->String('product_name');
            $table->text('image_product')->nullable();
            $table->unsignedInteger('shipper_id')->nullable();
            $table->foreign('shipper_id')->references('id')->on('users');
            $table->text('image_receive_product')->nullable();
            $table->integer('fee_ship')->default(0);
            $table->integer('money')->default(0);
            $table->integer('total_money')->default(0);
            $table->text('note')->nullable();
            $table->integer('status')->default(0);
            $table->integer('quantity_push')->default(0);
            $table->DateTime('time_check')->nullable();
            $table->longText('polyline')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_user_product');
    }
}
