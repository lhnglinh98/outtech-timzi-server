<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOderBuyMeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oder_buy_me', function (Blueprint $table) {
            $table->increments('id');
            $table->String('code');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('distance_id');
            $table->foreign('distance_id')->references('id')->on('distances');
            $table->unsignedInteger('category_service_id');
            $table->foreign('category_service_id')->references('id')->on('category_service');
            $table->unsignedInteger('notebook_id');
            $table->foreign('notebook_id')->references('id')->on('notebook');
            $table->String('name');
            $table->String('phone');
            $table->String('address');
            $table->integer('price_discount');
            $table->integer('price_service');
            $table->integer('money_surcharge')->nullable();
            $table->text('note_surcharge')->nullable();
            $table->integer('status')->default(1);
            $table->integer('option_pay');
            $table->integer('total_money');
            $table->text('all_note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oder_buy_me');
    }
}
