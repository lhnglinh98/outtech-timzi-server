<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFoodUpdate8Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_food', function($table){
            $table->integer('total_money_food')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
