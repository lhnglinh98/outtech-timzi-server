<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryMoneyShipperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_money_shipper', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('money')->default(0);
            $table->unsignedInteger('shipper_id');
            $table->foreign('shipper_id')->references('id')->on('users');
            $table->integer('type');
            $table->String('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_money_shipper');
    }
}
