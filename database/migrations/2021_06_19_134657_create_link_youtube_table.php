<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkYoutubeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_youtube', function (Blueprint $table) {
            $table->increments('id');
            $table->text('link_app_customer');
            $table->text('link_app_shop');
            $table->timestamps();
        });
        DB::table('link_youtube')->insert([
            'link_app_customer' => "https://www.youtube.com/watch?v=VlCcKo1o8qM",
            'link_app_shop' => "https://www.youtube.com/watch?v=VlCcKo1o8qM"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_youtube');
    }
}
