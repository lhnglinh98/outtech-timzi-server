<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table){
            $table->String('latitude')->nullable();
            $table->String('longtidue')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
