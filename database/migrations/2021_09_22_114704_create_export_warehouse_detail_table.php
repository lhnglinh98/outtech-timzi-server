<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExportWarehouseDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_warehouse_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('export_warehouse_id');
            $table->foreign('export_warehouse_id')->references('id')->on('export_warehouse');
            $table->unsignedInteger('material_id');
            $table->foreign('material_id')->references('id')->on('material');
            $table->integer('quantity')->default(0);
            $table->integer('money')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_warehouse_detail');
    }
}
