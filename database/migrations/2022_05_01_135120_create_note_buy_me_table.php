<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoteBuyMeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('note_buy_me', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notebook_id');
            $table->foreign('notebook_id')->references('id')->on('notebook');
            $table->String('title');
            $table->String('content');
            $table->integer('status')->default(1);;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('note_buy_me');
    }
}
