<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function($table){
            $table->float('percent_discount_revenue')->default(5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
