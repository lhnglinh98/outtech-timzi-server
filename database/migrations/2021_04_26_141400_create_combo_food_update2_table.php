<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComboFoodUpdate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('combo_food', function($table){
            $table->integer('is_new')->default(0);
            $table->integer('is_specialties')->default(0);
            $table->integer('is_out_of_food')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
