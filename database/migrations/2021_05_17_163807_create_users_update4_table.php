<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersUpdate4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table){
            $table->unsignedInteger('province_id')->nullable();
            $table->foreign('province_id')->references('id')->on('province');
            $table->unsignedInteger('district_id')->nullable();
            $table->foreign('district_id')->references('id')->on('district');
            $table->unsignedInteger('manage_province_id')->nullable();
            $table->foreign('manage_province_id')->references('id')->on('province');
            $table->unsignedInteger('manage_district_id')->nullable();
            $table->foreign('manage_district_id')->references('id')->on('district');
        });
        DB::table('roles')->insert([
            [
                'name' => "Quản lý khách hàng",
            ],
            [
                'name' => "Quản lý người giao hàng",
            ],
            [
                'name' => "Quản lý cửa hàng",
            ],
            [
                'name' => "Quản lý doanh số",
            ],
            [
                'name' => "Quản lý khu vực",
            ],
            [
                'name' => "Quản lý danh mục",
            ],
        ]);
        DB::table('users')->insert([
            [
                'code' => rand(10000000, 99999999),
                'name' => "Lê Thanh Trường",
                'phone' => "0901945179",
                'role_id' => 1,
                'status' => 1,
                'password' => Hash::make("123456")
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
