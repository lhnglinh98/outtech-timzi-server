<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRechargeShipperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recharge_shipper', function (Blueprint $table) {
            $table->increments('id');
            $table->String('code');
            $table->unsignedInteger('shipper_id');
            $table->foreign('shipper_id')->references('id')->on('users');
            $table->BigInteger('money')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_shipper');
    }
}
