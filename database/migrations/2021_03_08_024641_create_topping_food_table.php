<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToppingFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topping_food', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_topping_food_id');
            $table->foreign('category_topping_food_id')->references('id')->on('category_topping_food');
            $table->String('name');
            $table->integer('price');
            $table->integer('check')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topping_food');
    }
}
