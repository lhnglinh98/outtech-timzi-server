<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsedSolutionUpdate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('used_solution', function($table){
            $table->renameColumn('link', 'content');
            $table->String('title')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
