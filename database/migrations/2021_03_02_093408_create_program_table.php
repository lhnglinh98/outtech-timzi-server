<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program', function (Blueprint $table) {
            $table->increments('id');
            $table->String('name');
            $table->String('image');
            $table->unsignedInteger('category_food_id');
            $table->foreign('category_food_id')->references('id')->on('category_food');
            $table->float('percent')->default(0);
            $table->Text('content')->nullable();
            $table->Date('time_open');
            $table->Date('time_close');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program');
    }
}
