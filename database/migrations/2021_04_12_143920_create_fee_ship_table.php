<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeeShipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_ship', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('money')->default(0);
            $table->float('from_range')->default(0);
            $table->float('to_range')->nullable();
            $table->timestamps();
        });
        DB::table('fee_ship')->insert([
            [
                'money' => 15000,
                'from_range' => 0,
                'to_range' => 5
            ],
            [
                'money' => 30000,
                'from_range' => 5,
                'to_range' => 10
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_ship');
    }
}
