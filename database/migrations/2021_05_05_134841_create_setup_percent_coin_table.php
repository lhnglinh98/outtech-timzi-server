<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetupPercentCoinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_percent_coin', function (Blueprint $table) {
            $table->increments('id');
            $table->float('percent');
            $table->timestamps();
        });
        DB::table('setup_percent_coin')->insert([
            [
                'percent' => 1,
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_percent_coin');
    }
}
