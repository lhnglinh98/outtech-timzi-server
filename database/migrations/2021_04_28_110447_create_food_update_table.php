<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('food', function($table){
            $table->unsignedInteger('category_store_food_id')->nullable();
            $table->foreign('category_store_food_id')->references('id')->on('category_store_food');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
