<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramStoreUpdate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_store', function($table){
            $table->integer('check_close')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
