<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryMoneyShipperUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_money_shipper', function($table){
            $table->integer('type_order')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
