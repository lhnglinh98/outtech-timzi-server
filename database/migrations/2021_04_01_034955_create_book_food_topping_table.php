<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookFoodToppingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_food_topping', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('book_food_id');
            $table->foreign('book_food_id')->references('id')->on('book_food');
            $table->unsignedInteger('topping_food_id');
            $table->foreign('topping_food_id')->references('id')->on('topping_food');
            $table->String('category_topping_food_name');
            $table->String('topping_food_name');
            $table->String('money');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_food_topping');
    }
}
