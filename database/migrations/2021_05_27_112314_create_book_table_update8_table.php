<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookTableUpdate8Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_table', function($table){
            $table->String('staff_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
