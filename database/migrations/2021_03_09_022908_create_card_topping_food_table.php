<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardToppingFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_topping_food', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('card_food_id');
            $table->foreign('card_food_id')->references('id')->on('card_food');
            $table->unsignedInteger('topping_food_id');
            $table->foreign('topping_food_id')->references('id')->on('topping_food');
            $table->String('category_topping_food_name');
            $table->String('topping_food_name');
            $table->String('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_topping_food');
    }
}
