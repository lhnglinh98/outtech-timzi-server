<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramUpdate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program', function($table){
            $table->integer('money')->default(0);
            $table->integer('type_percent_or_money')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
