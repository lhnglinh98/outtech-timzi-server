<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentTowardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_towards', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content_app_customer');
            $table->text('content_app_ship');
            $table->text('content_app_shop');
            $table->timestamps();
        });
        DB::table('content_towards')->insert([
            'content_app_customer' => 'Những Timzier-Member ơi! Bạn có thể đăng ký sử dụng ngay ứng dụng "Tìm Zì" để trải nghiệm các tính năng ưu việt của nó. Bạn sẽ không phải mất quá nhiều thời gian để làm quen nó bởi với "Tìm Zì" luôn cố gắng tạo ra một hệ sinh thái dành cho mọi lứa tuổi có thể trải nghiệm.',
            'content_app_ship' => 'Bạn là một người có nhiều thời gian rảnh, bạn có thể đăng ký làm nhân viên giao hàng của hệ thống "Tìm Zì". Việc kết nối giữa cửa hàng với người sử dụng là rất quan trọng. Vì thế bạn là một phần không thể thiếu của hệ thống chúng tôi. Hãy đăng ký để trở thành Timzier-Shipper ngay nhé!',
            'content_app_shop' => 'Chúng tôi đã tạo ra "Tìm Zì" để giúp các Timzier-Shop có thể tương tác với người sử dụng dịch vụ một cách đơn giản chỉ với một chiếc smart-phone. Bạn là doanh nghiệp, chủ cửa hàng hay chỉ đơn giản là một cửa hàng nhỏ bạn đều có thể trải nghiệm và sử dụng "Tìm Zì". Hãy liên hệ với chúng tôi nếu bạn cần nhé!',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_towards');
    }
}
