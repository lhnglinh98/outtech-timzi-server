<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_food', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('book_table_id');
            $table->foreign('book_table_id')->references('id')->on('book_table');
            $table->unsignedInteger('food_id')->nullable();
            $table->foreign('food_id')->references('id')->on('food');
            $table->String('size')->nullable();
            $table->integer('quantity')->default(1);
            $table->integer('money')->default(0);
            $table->integer('money_discount')->default(0);
            $table->integer('total_money')->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_food');
    }
}
