<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvinceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->char('code', 20);
            $table->integer('is_hot')->default(0);
            $table->string('image')->nullable();
            $table->timestamps();
        });
        DB::table('province')->insert([
            [
                'name' => 'Hồ Chí Minh',
                'code' => 'SG',
                'image' => 'e4e39e45af1c34f7867e628a1fa60c27.jpg',
                
            ],
            [
                'name' => 'Hà Nội',
                'code' => 'HN',
                'image' => 'ab2675dad24ca8890be854a99dd1accb.jpg',
                
            ],
            [
                'name' => 'Đà Nẵng',
                'code' => 'DDN',
                'image' => '1db3b08af16bcfded58a147ef0ed3d0f.jpg',
                
            ],
            [
                'name' => 'Bình Dương',
                'code' => 'BD',
                'image' => '',
                
            ],
            [
                'name' => 'Đồng Nai',
                'code' => 'DNA',
                'image' => '',
                
            ],
            [
                'name' => 'Khánh Hòa',
                'code' => 'KH',
                'image' => '',
                
            ],
            [
                'name' => 'Hải Phòng',
                'code' => 'HP',
                'image' => 'hoa-phuong-1.jpg',
                
            ],
            [
                'name' => 'Long An',
                'code' => 'LA',
                'image' => '',
                
            ],
            [
                'name' => 'Quảng Nam',
                'code' => 'QNA',
                'image' => '',
                
            ],
            [
                'name' => 'Bà Rịa Vũng Tàu',
                'code' => 'VT',
                'image' => '',
                
            ],
            [
                'name' => 'Đắk Lắk',
                'code' => 'DDL',
                'image' => '',
                
            ],
            [
                'name' => 'Cần Thơ',
                'code' => 'CT',
                'image' => '',
                
            ],
            [
                'name' => 'Bình Thuận',
                'code' => 'BTH',
                'image' => '',
                
            ],
            [
                'name' => 'Lâm Đồng',
                'code' => 'LDD',
                'image' => '',
                
            ],
            [
                'name' => 'Thừa Thiên Huế',
                'code' => 'TTH',
                'image' => '',
                
            ],
            [
                'name' => 'Kiên Giang',
                'code' => 'KG',
                'image' => '',
                
            ],
            [
                'name' => 'Bắc Ninh',
                'code' => 'BN',
                'image' => '',
                
            ],
            [
                'name' => 'Quảng Ninh',
                'code' => 'QNI',
                'image' => 'noimage4.jpg',
                
            ],
            [
                'name' => 'Thanh Hóa',
                'code' => 'TH',
                'image' => '',
                
            ],
            [
                'name' => 'Nghệ An',
                'code' => 'NA',
                'image' => '',
                
            ],
            [
                'name' => 'Hải Dương',
                'code' => 'HD',
                'image' => '',
                
            ],
            [
                'name' => 'Gia Lai',
                'code' => 'GL',
                'image' => '',
                
            ],
            [
                'name' => 'Bình Phước',
                'code' => 'BP',
                'image' => '',
                
            ],
            [
                'name' => 'Hưng Yên',
                'code' => 'HY',
                'image' => '',
                
            ],
            [
                'name' => 'Bình Định',
                'code' => 'BDD',
                'image' => '',
                
            ],
            [
                'name' => 'Tiền Giang',
                'code' => 'TG',
                'image' => '',
                
            ],
            [
                'name' => 'Thái Bình',
                'code' => 'TB',
                'image' => '',
                
            ],
            [
                'name' => 'Bắc Giang',
                'code' => 'BG',
                'image' => '',
                
            ],
            [
                'name' => 'Hòa Bình',
                'code' => 'HB',
                'image' => '',
                
            ],
            [
                'name' => 'An Giang',
                'code' => 'AG',
                'image' => '',
                
            ],
            [
                'name' => 'Vĩnh Phúc',
                'code' => 'VP',
                'image' => '',
                
            ],
            [
                'name' => 'Tây Ninh',
                'code' => 'TNI',
                'image' => '',
                
            ],
            [
                'name' => 'Thái Nguyên',
                'code' => 'TN',
                'image' => '',
                
            ],
            [
                'name' => 'Lào Cai',
                'code' => 'LCA',
                'image' => '',
                
            ],
            [
                'name' => 'Nam Định',
                'code' => 'NDD',
                'image' => '',
                
            ],
            [
                'name' => 'Quảng Ngãi',
                'code' => 'QNG',
                'image' => '',
                
            ],
            [
                'name' => 'Bến Tre',
                'code' => 'BTR',
                'image' => '',
                
            ],
            [
                'name' => 'Đắk Nông',
                'code' => 'DNO',
                'image' => '',
                
            ],
            [
                'name' => 'Cà Mau',
                'code' => 'CM',
                'image' => '',
                
            ],
            [
                'name' => 'Vĩnh Long',
                'code' => 'VL',
                'image' => '',
                
            ],
            [
                'name' => 'Ninh Bình',
                'code' => 'NB',
                'image' => '',
                
            ],
            [
                'name' => 'Phú Thọ',
                'code' => 'PT',
                'image' => '',
                
            ],
            [
                'name' => 'Ninh Thuận',
                'code' => 'NT',
                'image' => '',
                
            ],
            [
                'name' => 'Phú Yên',
                'code' => 'PY',
                'image' => '',
                
            ],
            [
                'name' => 'Hà Nam',
                'code' => 'HNA',
                'image' => '',
                
            ],
            [
                'name' => 'Hà Tĩnh',
                'code' => 'HT',
                'image' => '',
                
            ],
            [
                'name' => 'Đồng Tháp',
                'code' => 'DDT',
                'image' => '',
                
            ],
            [
                'name' => 'Sóc Trăng',
                'code' => 'ST',
                'image' => '',
                
            ],
            [
                'name' => 'Kon Tum',
                'code' => 'KT',
                'image' => '',
                
            ],
            [
                'name' => 'Quảng Bình',
                'code' => 'QB',
                'image' => '',
                
            ],
            [
                'name' => 'Quảng Trị',
                'code' => 'QT',
                'image' => '',
                
            ],
            [
                'name' => 'Trà Vinh',
                'code' => 'TV',
                'image' => '',
                
            ],
            [
                'name' => 'Hậu Giang',
                'code' => 'HGI',
                'image' => '',
                
            ],
            [
                'name' => 'Sơn La',
                'code' => 'SL',
                'image' => '',
                
            ],
            [
                'name' => 'Bạc Liêu',
                'code' => 'BL',
                'image' => '',
                
            ],
            [
                'name' => 'Yên Bái',
                'code' => 'YB',
                'image' => '',
                
            ],
            [
                'name' => 'Tuyên Quang',
                'code' => 'TQ',
                'image' => '',
                
            ],
            [
                'name' => 'Điện Biên',
                'code' => 'DDB',
                'image' => '',
                
            ],
            [
                'name' => 'Lai Châu',
                'code' => 'LCH',
                'image' => '',
                
            ],
            [
                'name' => 'Lạng Sơn',
                'code' => 'LS',
                'image' => '',
                
            ],
            [
                'name' => 'Hà Giang',
                'code' => 'HG',
                'image' => '',
                
            ],
            [
                'name' => 'Bắc Kạn',
                'code' => 'BK',
                'image' => '',
                
            ],
            [
                'name' => 'Cao Bằng',
                'code' => 'CB',
                'image' => '',
                
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('province');
    }
}
