<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        DB::table('roles')->insert([
            [
                'name' => "Admin",
            ],
            [
                'name' => "Chủ chuỗi cửa hàng",
            ],
            [
                'name' => "Nhân viên",
            ],
            [
                'name' => "Shipper",
            ],
            [
                'name' => "Khách hàng",
            ],
            [
                'name' => "Chủ cửa hàng",
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
