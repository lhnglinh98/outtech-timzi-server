<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookFoodUpdate4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_food', function($table){
            $table->integer('quantity_minus_group_food')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
