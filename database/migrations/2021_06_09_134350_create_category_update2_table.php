<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryUpdate2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category', function($table){
            $table->integer('is_eat_store')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
