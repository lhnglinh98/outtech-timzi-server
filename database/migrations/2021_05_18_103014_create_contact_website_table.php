<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactWebsiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_website', function (Blueprint $table) {
            $table->increments('id');
            $table->String('name')->nullable();
            $table->String('email')->nullable();
            $table->text('message')->nullable();
            $table->unsignedInteger('question_website_id')->nullable();
            $table->foreign('question_website_id')->references('id')->on('question_website');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_website');
    }
}
