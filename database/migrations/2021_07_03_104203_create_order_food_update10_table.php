<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFoodUpdate10Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_food', function($table){
            $table->integer('status_payment_vnpay')->default(0);
            $table->integer('status_payment_success')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
