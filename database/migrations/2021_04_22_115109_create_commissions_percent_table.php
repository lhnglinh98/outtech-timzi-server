<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionsPercentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions_percent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_user')->default(1);
            $table->integer('percent')->default(0);
            $table->timestamps();
        });
        DB::table('commissions_percent')->insert([
            [
                'type_user' => 1,
                'percent' => 20,
            ],
            [
                'type_user' => 2,
                'percent' => 20,
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions_percent');
    }
}
