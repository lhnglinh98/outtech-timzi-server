<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRadiusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('radius', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('radius');
            $table->timestamps();
        });
        DB::table('radius')->insert([
            [
                'radius' => 2
            ],
            [
                'radius' => 5
            ],
            [
                'radius' => 7
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('radius');
    }
}
