<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramStoreUpdate4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_store', function($table){
            $table->integer('quantity_bought')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
