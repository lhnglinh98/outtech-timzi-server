<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComboFoodDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combo_food_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('combo_food_id');
            $table->foreign('combo_food_id')->references('id')->on('combo_food');
            $table->unsignedInteger('food_id');
            $table->foreign('food_id')->references('id')->on('food');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combo_food_detail');
    }
}
