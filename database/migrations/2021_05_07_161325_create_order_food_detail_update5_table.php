<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFoodDetailUpdate5Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_food_detail', function($table){
            $table->dropForeign('order_food_detail_card_food_id_foreign');
            $table->dropColumn('card_food_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
