<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/export-voucher-order/{id}', 'Admin\VoucherController@exportVoucherOrder');
Route::namespace ('Client')->group(function () {
    Route::get('/tinh-tien', 'UserController@tinhTien');
    Route::Post('/login-user', 'UserController@loginUser');
    Route::Post('/login-shipper', 'UserController@loginShipper');
    Route::Post('/login-staff', 'UserController@loginStaff');
    Route::Post('/register-customer', 'UserController@registerCustomer');
    Route::Post('/register-shipper', 'UserController@registerShipper');
    Route::Post('/register-staff', 'UserController@registerStaff');
    Route::put('/check-otp-register', 'UserController@checkOtpRegister');
    Route::put('/forgot-password', 'UserController@forgotPassword');
    Route::put('/check-otp-verify', 'UserController@checkOTPVerify');
    Route::put('/update-new-password', 'UserController@updateNewPassword');
    Route::get('/list-province', 'AddressUserController@listProvince');
    Route::get('/list-district', 'AddressUserController@listDistrict');
    Route::get('/list-ward', 'AddressUserController@listWard');
    Route::get('/list-store-with-category/{category_id}', 'StoreController@listStoreWithCategory');
    Route::get('/store-detail-with-customer/{id}', 'StoreController@storeDetailWithCustomer');
    Route::get('/store-detail/{id}', 'StoreController@storeDetail');
    Route::get('/list-table-store/{id}', 'StoreController@listTableStore');
    Route::get('/list-combo-food-store/{id}', 'StoreController@listComboFoodStore');
    Route::get('/list-program-store-with-customer/{id}', 'StoreController@listProgramStoreWithCustomer');
    Route::get('/list-category-food-store/{id}', 'StoreController@listCategoryFoodStore');
    Route::get('/list-category-food-with-client', 'StoreController@listCategoryFoodWithClient');
    Route::get('/list-category-store-food-with-store/{id}', 'StoreController@listCategoryStoreFoodStore');
    Route::get('/list-card-with-store/{id}', 'StoreController@listCardWithStore');
    Route::get('/book-table-with-store/{id}', 'StoreController@bookTableWithStore');
    Route::get('/list-store-with-customer', 'StoreController@listStoreWithCustomer');
    Route::get('/food-detail/{id}', 'FoodController@foodDetail');
    Route::get('/list-category', 'StoreController@listCategory');
    Route::put('/confirm-store/{id}', 'StoreController@confirmStore');
    Route::get('/list-program-store', 'StoreController@listProgramStore');
    Route::put('/confirm-voucher/{id}', 'VoucherController@confirmVoucher');
    Route::put('/confirm-combo-food/{id}', 'ComboFoodController@confirmComboFood');
    Route::put('/confirm-program-store/{id}', 'ProgramStoreController@confirmProgramStore');
    Route::get('/check-fee-ship', 'OrderFoodController@checkFeeShip');
    Route::get('/list-voucher', 'VoucherController@listVoucher');
    Route::get('/voucher-detail/{id}', 'VoucherController@voucherDetail');
    Route::get('/list-store-recently', 'StoreController@listStoreRecently');
    Route::get('/list-store-new', 'StoreController@listStoreNew');
    Route::get('/list-store-partner', 'StoreController@listStorePartner');
    Route::get('/list-store-many-order', 'StoreController@listStoreManyOrders');
    Route::get('/list-store-in-home', 'StoreController@listStoreInHome');
    Route::get('/list-category-with-store', 'StoreController@listCategoryWithStore');
    Route::get('store/store-detail-with-book-table/{id}', 'StoreController@storeDetailWithBookTable');
    Route::get('list-store-website', 'StoreController@listStoreWebsite');
    Route::get('store-detail-website/{id}', 'StoreController@storeDetailWebsite');
    Route::put('confirm-program-system/{id}', 'ProgramController@confirmProgramSystem');
    Route::get('/link-youtube-detail', 'WebsiteController@linkYoutubeDetail');
    Route::get('/content-toward-detail', 'WebsiteController@contentTowardDetail');
    Route::get('/home-website-detail', 'WebsiteController@homeWebSiteDetail');
    Route::get('list-feature-website', 'WebsiteController@listFeatureWebsite');
    Route::get('list-leadership-team', 'WebsiteController@listLeadershipTeam');
    Route::get('list-tutorial-document', 'WebsiteController@listTutorialDocument');
    Route::get('list-used-solution', 'WebsiteController@listUsedSolution');
    Route::get('list-category-question-website', 'WebsiteController@listCategoryQuestionWebsite');
    Route::post('create-contact', 'WebsiteController@createContact');
    Route::put('update-total-money-food', 'OrderFoodController@updateTotalMoneyFood');
    Route::get('list-suggestion', 'StoreController@listSuggestion');
    Route::get('search-store', 'StoreController@searchStore');
    Route::get('call-google-api-geocode', 'UserController@callGoogleApiGeocode');
    Route::get('call-google-api-place', 'UserController@callGoogleApiPlace');
    Route::get('call-google-api-auto-complete', 'UserController@callGoogleApiAutoComplete');
    Route::get('vnpay-ipn-recharge-order', 'OrderFoodController@vnpayIPNRechargeOrder');
    Route::get('vnpay-ipn-recharge-shipper', 'RechargeShipperController@vnpayIPNRechargeShipper');
    Route::get('vnpay-ipn-payment-fee-revenue', 'StoreController@vnpayIPNPaymentFeeRevenue');
    Route::get('vnpay-ipn-recharge-order-live', 'OrderFoodController@vnpayIPNRechargeOrderLive');
    Route::get('vnpay-ipn-recharge-shipper-live', 'RechargeShipperController@vnpayIPNRechargeShipperLive');
    Route::get('vnpay-ipn-payment-fee-revenue-live', 'StoreController@vnpayIPNPaymentFeeRevenueLive');
    Route::prefix('policy-app')->group(function () {
        Route::get('/payment-guide', 'PolicyAppController@paymentGuide');
        Route::get('/privacy-policy', 'PolicyAppController@privacyPolicy');
        Route::get('/regulation', 'PolicyAppController@regulation');
        Route::get('/term-of-use', 'PolicyAppController@termOfUse');
        Route::get('/dispute-resolution-policy', 'PolicyAppController@disputeResolutionPolicy');
    });
    Route::group(['middleware' => ['auth.jwt']], function () {
        Route::get('/logout', 'UserController@logout');
        Route::get('/get-user-infomation', 'UserController@getUserInfo');
        Route::put('/update-device-id', 'UserController@updateDeviceId');
        Route::put('/update-user', 'UserController@updateUser');
        Route::put('/change-password', 'UserController@changePassword');
        Route::get('/list-coin-history', 'UserController@listCoinHistory');
        Route::post('/receive-voucher-with-customer/{id}', 'VoucherController@receiveVoucherWithCustomer');
        Route::get('list-voucher-user', 'VoucherController@listVoucherUser');
        Route::get('list-voucher-in-order/{store_id}', 'VoucherController@listVoucherInOrder');
        Route::prefix('shipper')->group(function () {
            Route::put('/upload-image-infomation', 'UserController@uploadImageInfomation');
        });
        Route::prefix('order-user-product')->group(function () {
            Route::get('/list-voucher-delivery', 'OrderUserProductController@listVoucherDelivery');
            Route::post('/create', 'OrderUserProductController@create');
            Route::get('/fee-ship', 'OrderUserProductController@feeShip');
            Route::get('/list-product-weight', 'OrderUserProductController@listProductWeight');
            Route::get('/list-order-with-user', 'OrderUserProductController@listOrderWithUser');
            Route::get('/history-order', 'OrderUserProductController@historyOrder');
            Route::get('/order-detail-with-user/{id}', 'OrderUserProductController@orderDetailWithUser');
            Route::get('/list-order-pending', 'OrderUserProductController@listOrderPending');
            Route::get('/order-detail-with-shipper/{id}', 'OrderUserProductController@orderDetailWithShipper');
            Route::put('/receive-order-with-shipper/{id}', 'OrderUserProductController@receiveOrderWithShipper');
            Route::put('/arrived-sender-order-with-shipper/{id}', 'OrderUserProductController@arrivedSenderOrderWithShipper');
            Route::put('/pickup-sender-order-with-shipper', 'OrderUserProductController@pickupSenderOrderWithShipper');
            Route::put('/arrived-receiver-order-with-shipper/{id}', 'OrderUserProductController@arrivedReceiverOrderWithShipper');
            Route::put('/finish-order-with-shipper', 'OrderUserProductController@finishOrderWithShipper');
            Route::put('/cancel-order-with-shipper/{id}', 'OrderUserProductController@cancelOrderWithShipper');
            Route::put('/sender-cancel-order-with-shipper/{id}', 'OrderUserProductController@senderCancelOrderWithShipper');
            Route::put('/receiver-cancel-order-with-shipper/{id}', 'OrderUserProductController@receiverCancelOrderWithShipper');
            Route::put('/cancel-order-with-sender/{id}', 'OrderUserProductController@cancelOrderWithSender');
            Route::get('/list-order-with-receiver', 'OrderUserProductController@listOrderWithReceiver');
            Route::get('/order-detail-with-receiver/{id}', 'OrderUserProductController@orderDetailWithReceiver');
            Route::get('/list-order-success', 'OrderUserProductController@listOrderSuccess');
        });
        Route::prefix('notify')->group(function () {
            Route::get('/list-notify', 'NotifyController@listNotify');
            Route::put('/confirm-view-notify', 'NotifyController@confirmViewNotify');
        });
        Route::prefix('store')->group(function () {
            Route::get('/list-category-in-create-store', 'StoreController@listCategoryInCreateStore');
            Route::get('/store-detail-in-web/{id}', 'StoreController@storeDetailInWeb');
            Route::get('/discount-this-month-store/{store_id}', 'StoreController@discountThisMonthStore');
            Route::get('/list-discount-store/{store_id}', 'StoreController@listDiscountStore');
            Route::get('/list-revenue-store/{store_id}', 'StoreController@listRevenueStore');
            Route::get('/list-revenue-food-store/{store_id}', 'StoreController@listRevenueFoodStore');
            // Route::put('/confirm-payment-fee-revenue-store/{id}', 'StoreController@confirmPaymentFeeRevenueStore');
            Route::get('/list-store-with-owner', 'StoreController@listStoreWithOwner');
            Route::get('/store-detail-with-owner/{id}', 'StoreController@storeDetailWithOwner');
            Route::post('/create-store', 'StoreController@createStore');
            Route::put('/update-store/{id}', 'StoreController@updateStore');
            Route::get('/store-staff/{status}', 'StoreController@storeStaff');
            Route::get('/search-store-with-staff', 'StoreController@searchStoreWithStaff');
            Route::post('/choose-store/{store_id}', 'StoreController@chooseStore');
            Route::delete('/delete-choose-store/{store_id}', 'StoreController@deleteChooseStore');
            Route::get('/list-store-confirm-with-owner', 'StoreController@listStoreConfirmWithOwner');
            Route::put('/confirm-store-with-owner', 'StoreController@confirmStoreWithOwner');
            Route::get('/list-staff-in-store', 'StoreController@listStaffInStore');
            Route::delete('/delete-staff', 'StoreController@deleteStaff');
            Route::put('/confirm-staff', 'StoreController@confirmStaff');
            Route::put('/stop-staff', 'StoreController@stopStaff');
            Route::put('/active-staff', 'StoreController@activeStaff');
            Route::get('list-food-with-category', 'StoreController@listFoodWithCategory');
            Route::get('list-store-owner/{store_id}', 'StoreController@listStoreOwner');
            Route::put('confirm-store-owner', 'StoreController@confirmStoreOwner');
            Route::delete('delete-store-owner', 'StoreController@deleteStoreOwner');
            Route::delete('delete-store/{id}', 'StoreController@deleteStore');
            Route::get('list-table-empty/{store_id}', 'StoreController@listTableEmpty');
            Route::get('search-google-map', 'StoreController@searchGoogleMap');
            Route::post('confirm-do-store-owner', 'StoreController@confirmDoStoreOwner');
            Route::delete('cancel-do-store-owner', 'StoreController@cancelDoStoreOwner');
            Route::get('list-all-combo-food-store/{store_id}', 'StoreController@listAllComboFoodStore');
            Route::get('list-food-in-program-store/{id}', 'StoreController@listFoodInProgramStore');
            Route::get('list-food-in-program-store-limit/{id}', 'StoreController@listFoodInProgramStoreLimit');
            Route::get('list-food-with-category-in-order', 'StoreController@listFoodWithCategoryInOrder');
            Route::get('list-food-with-category-in-order-limit', 'StoreController@listFoodWithCategoryInOrderLimit');
            Route::get('check-food-in-category/{food_id}', 'StoreController@checkFoodInCategory');
            Route::put('open-store/{store_id}', 'StoreController@openStore');
            Route::get('dashboard-store', 'StoreController@dashboardStore');
        });
        Route::prefix('food')->group(function () {
            Route::get('/list-food-in-store/{store_id}', 'FoodController@listFoodInstore');
            Route::get('/list-type-food', 'FoodController@listTypeFood');
            Route::post('/create-food', 'FoodController@createFood');
            Route::put('/update-food/{id}', 'FoodController@updateFood');
            Route::put('/update-size-food/{id}', 'FoodController@updateSizeFood');
            Route::get('/list-category-topping-food/{food_id}', 'FoodController@listCategoryToppingFood');
            Route::put('/update-category-topping-food/{id}', 'FoodController@updateCategoryToppingFood');
            Route::put('/update-topping-food/{id}', 'FoodController@updateToppingFood');
            Route::delete('/delete-food/{id}', 'FoodController@deleteFood');
            Route::post('/add-size-food', 'FoodController@addSizeFood');
            Route::put('/update-size-food-web/{id}', 'FoodController@updateSizeFoodWeb');
            Route::delete('/delete-size-food/{id}', 'FoodController@deleteSizeFood');
            Route::post('/add-topping-food', 'FoodController@addToppingFood');
            Route::put('/update-topping-food-web/{id}', 'FoodController@updateToppingFoodWeb');
            Route::delete('/delete-topping-food/{id}', 'FoodController@deleteToppingFood');
            Route::post('/add-category-topping-food', 'FoodController@addCategoryToppingFood');
            Route::put('/update-category-topping-food-web/{id}', 'FoodController@updateCategoryToppingFoodWeb');
            Route::delete('/delete-category-topping-food/{id}', 'FoodController@deleteCategoryToppingFood');
        });
        Route::prefix('program-store')->group(function () {
            Route::get('/list-program-store', 'ProgramStoreController@listProgramStore');
            Route::get('/program-store-detail/{id}', 'ProgramStoreController@programStoreDetail');
            Route::post('/create-program-store', 'ProgramStoreController@createProgramStore');
            Route::put('/update-program-store/{id}', 'ProgramStoreController@updateProgramStore');
            Route::put('/stop-program-store/{id}', 'ProgramStoreController@stopProgramStore');
            Route::put('/active-program-store/{id}', 'ProgramStoreController@activeProgramStore');
            Route::delete('/delete-program-store/{id}', 'ProgramStoreController@deleteProgramStore');
        });
        Route::prefix('combo-food')->group(function () {
            Route::get('/list-combo-food/{store_id}', 'ComboFoodController@listComboFood');
            Route::get('/combo-food-detail/{id}', 'ComboFoodController@comboFoodDetail');
            Route::post('/create-combo-food', 'ComboFoodController@createComboFood');
            Route::put('/update-combo-food/{id}', 'ComboFoodController@updateComboFood');
            Route::put('/active-combo-food/{id}', 'ComboFoodController@activeComboFood');
            Route::delete('/delete-combo-food/{id}', 'ComboFoodController@deleteComboFood');
        });
        Route::prefix('address')->group(function () {
            Route::get('/list-address-user', 'AddressUserController@listAddressUser');
            Route::get('/address-user-detail/{id}', 'AddressUserController@addressUserDetail');
            Route::post('/create-address-user', 'AddressUserController@createAddressUser');
            Route::put('/update-address-user/{id}', 'AddressUserController@updateAddressUser');
            Route::delete('/delete-address-user/{id}', 'AddressUserController@deleteAddressUser');
            Route::put('/update-address-default/{id}', 'AddressUserController@updateAddressDefault');
        });
        Route::prefix('category')->group(function () {
            Route::get('/list-category-store-food', 'CategoryStoreFoodController@listCategoryStoreFood');
            Route::get('/category-store-food-detail/{id}', 'CategoryStoreFoodController@categoryStoreFoodDetail');
            Route::post('/create-category-store-food', 'CategoryStoreFoodController@createCategoryStoreFood');
            Route::put('/update-category-store-food/{id}', 'CategoryStoreFoodController@updateCategoryStoreFood');
            Route::delete('/delete-category-store-food/{id}', 'CategoryStoreFoodController@deleteCategoryStoreFood');
        });
        Route::prefix('table-store')->group(function () {
            Route::get('/table-store-detail/{id}', 'TableStoreController@tableStoreDetail');
            Route::post('/create-table-store', 'TableStoreController@createTableStore');
            Route::put('/update-table-store/{id}', 'TableStoreController@updateTableStore');
            Route::delete('/delete-table-store/{id}', 'TableStoreController@deleteTableStore');
            Route::post('/gen-image-qr-code', 'TableStoreController@genImageQRCode');
        });
        Route::prefix('card-food')->group(function () {
            Route::post('/add-card-food', 'OrderFoodController@addCardFood');
            Route::post('/add-card-combo-food', 'OrderFoodController@addCardComboFood');
            Route::get('/list-card-with-store/{store_id}', 'OrderFoodController@listCardwithStore');
            Route::get('/list-card-with-food/{food_id}', 'OrderFoodController@listCardWithFood');
            Route::get('/card-detail/{id}', 'OrderFoodController@cardDetail');
            Route::delete('/delete-card/{id}', 'OrderFoodController@deleteCard');
            Route::put('/subtract-quantity-food', 'OrderFoodController@subtractQuantityFood');
        });
        Route::prefix('order')->group(function () {
            Route::post('/order-food', 'OrderFoodController@orderFood');
            Route::get('/list-order-with-owner', 'OrderFoodController@listOrderWithOwner');
            Route::get('/order-detail-with-owner/{id}', 'OrderFoodController@orderDetailWithOwner');
            Route::get('/list-order-with-user', 'OrderFoodController@listOrderWithUser');
            Route::get('/list-order-draft-with-user', 'OrderFoodController@listOrderDraftWithUser');
            Route::get('/order-detail-with-user/{id}', 'OrderFoodController@orderDetailWithUser');
            Route::delete('/delete-order-draft-with-user', 'OrderFoodController@deleteOrderDraftWithUser');
            Route::put('/confirm-order-with-owner/{order_id}', 'OrderFoodController@confirmOrderWithOwner');
            Route::put('/cancel-order-with-owner/{order_id}', 'OrderFoodController@cancelOrderWithOwner');
            Route::put('/shipper-received-order-with-owner/{order_id}', 'OrderFoodController@shipperReceivedOrderWithOwner');
            Route::get('/list-order-pending-with-shipper', 'OrderFoodController@listOrderPendingWithShipper');
            Route::get('/order-detail-with-shipper/{order_id}', 'OrderFoodController@orderDetailWithShipper');
            Route::get('/order-detail-success-with-shipper/{order_id}', 'OrderFoodController@orderDetailSuccessWithShipper');
            Route::put('/receive-order-with-shipper/{order_id}', 'OrderFoodController@receiveOrderWithShipper');
            Route::put('/arrived-store-order-with-shipper/{order_id}', 'OrderFoodController@arrivedStoreOrderWithShipper');
            Route::put('/arrived-customer-order-with-shipper/{order_id}', 'OrderFoodController@arrivedCustomerOrderWithShipper');
            Route::put('/store-cancel-order-with-shipper/{order_id}', 'OrderFoodController@storeCancelOrderWithShipper');
            Route::put('/customer-cancel-order-with-shipper/{order_id}', 'OrderFoodController@customerCancelOrderWithShipper');
            Route::put('/cancel-order-with-shipper/{order_id}', 'OrderFoodController@cancelOrderWithShipper');
            Route::put('/pickup-store-order-with-shipper/{order_id}', 'OrderFoodController@pickupStoreOrderWithShipper');
            Route::put('/received-order-with-customer/{order_id}', 'OrderFoodController@receivedOrderWithCustomer');
            Route::put('/cancel-order-with-customer/{order_id}', 'OrderFoodController@cancelOrderWithCustomer');
            Route::put('/finish-order-with-shipper/{order_id}', 'OrderFoodController@finishOrderWithShipper');
            Route::post('/evaluate-order', 'OrderFoodController@evaluateOrder');
            Route::post('/evaluate-food', 'OrderFoodController@evaluateFood');
            Route::post('/evaluate-shipper', 'OrderFoodController@evaluateShipper');
            Route::get('/list-evaluate-with-store/{store_id}', 'OrderFoodController@listEvaluateWithStore');
            Route::get('/list-evaluate-with-shipper', 'OrderFoodController@listEvaluateWithShipper');
            Route::get('/list-order-success-with-shipper', 'OrderFoodController@listOrderSuccessWithShipper');
            Route::get('/current-order-with-shipper', 'OrderFoodController@currentOrderWithShipper');
            Route::get('/test-radius', 'OrderFoodController@testRadius');
            Route::put('/update-location', 'OrderFoodController@updateLocation');
        });
        Route::prefix('store-shipper')->group(function () {
            Route::get('/list-shipper', 'StoreShipperController@listShipper');
            Route::get('/list-shipper-in-web', 'StoreShipperController@listShipperInWeb');
            Route::post('/choose-shipper', 'StoreShipperController@chooseShipper');
            Route::get('/list-shipper-with-store', 'StoreShipperController@listShipperWithStore');
            Route::delete('/cancel-shipper-with-store', 'StoreShipperController@cancelShipperWithStore');
        });
        Route::prefix('book-table')->group(function () {
            Route::post('/book-table-with-customer', 'BookTableController@booktableWithCustomer');
            Route::post('/book-table-with-barcode', 'BookTableController@booktableWithBarcode');
            Route::get('/list-book-table-store-with-owner', 'BookTableController@listBookTableStoreWithOwner');
            Route::get('/book-table-store-detail-with-owner/{id}', 'BookTableController@bookTableStoreDetailWithOwner');
            Route::put('/confirm-book-table-store/{id}', 'BookTableController@confirmBookTableStore');
            Route::put('/cancel-book-table-store/{id}', 'BookTableController@cancelBookTableStore');
            Route::put('/open-table-store-with-owner/{id}', 'BookTableController@openTableStoreWithOwner');
            Route::put('/close-table-store-with-owner/{id}', 'BookTableController@closeTableStoreWithOwner');
            Route::get('/list-table-store/{store_id}', 'BookTableController@listTableStore');
            Route::get('/list-book-table-with-customer', 'BookTableController@listBookTableWithCustomer');
            Route::post('/order-food-with-book-table', 'BookTableController@orderFoodWithBooktable');
            Route::post('/order-combo-with-book-table', 'BookTableController@orderComboWithBooktable');
            Route::put('/subtract-quantity-book-food', 'BookTableController@subtractQuantityBookFood');
            Route::put('/return-book-food', 'BookTableController@returnBookFood');
            Route::put('/right-to-order-with-owner/{id}', 'BookTableController@rightToOrderWithOwner');
            Route::put('/submit-order-food-with-customer/{id}', 'BookTableController@submitOrderFoodWithCustomer');
            Route::delete('/cancel-book-food-with-customer', 'BookTableController@cancelBookFoodWithCustomer');
            Route::get('/book-table-detail-with-customer/{id}', 'BookTableController@bookingTableDetailWithCustomer');
            Route::put('/confirm-food-on-the-table/{id}', 'BookTableController@confirmFoodOnTheTable');
            Route::put('/payment-book-food', 'BookTableController@paymentBookFood');
            Route::put('/confirm-payment-book-food-with-owner/{id}', 'BookTableController@confirmPaymentBookFoodWithOwner');
            Route::post('/delete-book-table-with-customer', 'BookTableController@deleteBookTableWithCustomer');
            Route::get('/book-table-detail-in-history/{id}', 'BookTableController@bookTableDetailInHistory');
            Route::put('/call-staff/{id}', 'BookTableController@CallStaff');
            Route::post('/book-table-with-staff', 'BookTableInStoreController@booktableWithStaff');
            Route::post('/book-table-qr-code-with-staff', 'BookTableInStoreController@booktableQRCodeWithStaff');
            Route::post('/order-food-with-book-table-in-store', 'BookTableInStoreController@orderFoodWithBooktableInStore');
            Route::post('/order-combo-with-book-table-in-store', 'BookTableInStoreController@orderComboWithBooktableInStore');
            Route::put('/subtract-quantity-book-food-in-store', 'BookTableInStoreController@subtractQuantityBookFoodInStore');
            Route::delete('/cancel-book-food-with-staff/{id}', 'BookTableInStoreController@cancelBookFoodWithStaff');
            Route::put('/submit-order-food-with-staff/{id}', 'BookTableInStoreController@submitOrderFoodWithStaff');
            Route::put('/payment-book-food-with-staff', 'BookTableInStoreController@paymentBookFoodWithStaff');
            Route::get('/store-detail-with-book-table-in-store', 'BookTableInStoreController@storeDetailWithBookTableInStore');
            Route::post('/table-merge-with-owner', 'BookTableController@tableMergeWithOwner');
            Route::put('/change-table-with-staff', 'BookTableInStoreController@changeTableWithStaff');
            Route::get('/list-combo-food-store-with-owner/{id}', 'BookTableInStoreController@listComboFoodStoreWithOwner');
            Route::get('/list-all-combo-food-store-with-owner/{id}', 'BookTableInStoreController@listAllComboFoodStoreWithOwner');
            Route::get('/list-program-store-with-owner/{id}', 'BookTableInStoreController@listProgramStoreWithOwner');
            Route::get('/list-food-in-program-store-with-owner/{id}', 'BookTableInStoreController@listFoodInProgramStoreWithOwner');
            Route::get('/list-food-in-program-store-limit-with-owner/{id}', 'BookTableInStoreController@listFoodInProgramStoreLimitWithOwner');
            Route::get('/list-category-food-store-with-owner/{id}', 'BookTableInStoreController@listCategoryFoodStoreWithOwner');
            Route::get('/list-category-store-food-store-with-owner/{id}', 'BookTableInStoreController@listCategoryStoreFoodStoreWithOwner');
            Route::get('/list-food-with-category-in-store', 'BookTableInStoreController@listFoodWithCategoryInStore');
            Route::get('/list-food-with-category-in-store-limit', 'BookTableInStoreController@listFoodWithCategoryInStoreLimit');
            Route::get('/book-table-with-store-with-owner/{id}', 'BookTableInStoreController@bookTableWithStoreWithOwner');
        });
        Route::prefix('shipper')->group(function () {
            Route::get('/list-history-money', 'ShipperController@listHistoryMoney');
            Route::get('/list-store-shipper-confirm', 'ShipperController@listStoreShipperConfirm');
            Route::get('/list-store-shipper', 'ShipperController@listStoreShipper');
            Route::put('/confirm-store-with-shipper/{id}', 'ShipperController@confirmStoreWithShipper');
            Route::post('/recharge-shipper', 'RechargeShipperController@rechargeShipper');
            Route::delete('/cancel-store-with-shipper/{id}', 'ShipperController@cancelStoreWithShipper');
            Route::post('/withdraw-money', 'ShipperController@withdrawMoney');
        });
        Route::prefix('program')->group(function () {
            Route::get('/list-program-system', 'ProgramController@listProgramSystem');
            Route::get('/program-system-detail/{id}', 'ProgramController@programSystemDetail');
            Route::post('/receive-program-system', 'ProgramController@receiveProgramSystem');
            Route::get('/list-program-system-with-store/{id}', 'ProgramController@listProgramSystemWithStore');
            Route::delete('/stop-program-system-with-store', 'ProgramController@stopProgramSystemWithStore');
        });
        Route::prefix('supplier')->group(function () {
            Route::get('/list-supplier/{id}', 'SupplierController@listSupplier');
            Route::get('/supplier-detail/{id}', 'SupplierController@supplierDetail');
            Route::post('/create-supplier', 'SupplierController@createSupplier');
            Route::put('/update-supplier/{id}', 'SupplierController@updateSupplier');
            Route::delete('/delete-supplier/{id}', 'SupplierController@deleteSupplier');
        });
        Route::prefix('type-material')->group(function () {
            Route::get('/list-type-material/{id}', 'TypeMaterialController@listTypeMaterial');
            Route::get('/type-material-detail/{id}', 'TypeMaterialController@typeMaterialDetail');
            Route::post('/create-type-material', 'TypeMaterialController@createTypeMaterial');
            Route::put('/update-type-material/{id}', 'TypeMaterialController@updateTypeMaterial');
            Route::delete('/delete-type-material/{id}', 'TypeMaterialController@deleteTypeMaterial');
        });
        Route::prefix('material')->group(function () {
            Route::get('/list-material/{id}', 'MaterialController@listMaterial');
            Route::get('/material-detail/{id}', 'MaterialController@materialDetail');
            Route::post('/create-material', 'MaterialController@createMaterial');
            Route::put('/update-material/{id}', 'MaterialController@updateMaterial');
            Route::delete('/delete-material/{id}', 'MaterialController@deleteMaterial');
        });
        Route::prefix('warehouse')->group(function () {
            Route::post('/import-warehouse', 'WarehouseController@importWarehouse');
            Route::post('/export-warehouse', 'WarehouseController@exportWarehouse');
            Route::get('/list-import-warehouse/{id}', 'WarehouseController@listImportWarehouse');
            Route::get('/list-export-warehouse/{id}', 'WarehouseController@listExportWarehouse');
            Route::get('/list-material-warehouse/{id}', 'WarehouseController@listMaterialWarehouse');
        });
        Route::prefix('distances')->group(function () {
            Route::post('/create-distances', 'DistancesController@createDistances');
            Route::get('/list-distances', 'DistancesController@listdataDistances');
            Route::get('/list-distances/{status}', 'DistancesController@listdataDistancesStatus');
            Route::put('/update-distances/{id}', 'DistancesController@updateDistances');
            Route::delete('/delete-distances/{id}', 'DistancesController@deleteDistances');
        });
        Route::prefix('notebook')->group(function () {
            Route::get('/notebookdetail/{id}', 'NotebookController@notebookDetail');
            Route::post('/createNoteBook', 'NotebookController@createNoteBook');
            Route::get('/checkIdNotebook', 'NotebookController@checkIdNoteBook');
            Route::put('/update-notebook/{id}', 'NotebookController@updateNoteBook');
            Route::post('/create-note-buy-me', 'NotebookController@createNoteBuyMe');
            Route::get('/list-note-buy-me/{id}', 'NotebookController@listNoteBuyMe');
            Route::get('/list-note-buy-me-status', 'NotebookController@listNoteBuyMeStatus');
            Route::put('/update-note-buy-me/{id}', 'NotebookController@updateNoteBuyMe');
            Route::delete('/delete-note-buy-me/{id}', 'NotebookController@deleteNoteBuyMe');
        });
        Route::prefix('oderbuyme')->group(function () {
            Route::post('/create-oderbuyme', 'OderBuyMeController@createOderBuyMe');
            Route::get('/list-oder-buy-me', 'OderBuyMeController@listOderBuyMe');
            Route::get('/list-oder-buy-me-user', 'OderBuyMeController@listOderBuyMeUser');
            Route::get('/detail-oder-buy-me/{id}', 'OderBuyMeController@detailOderBuyMe');
            Route::put('/update-oder-buy-me/{id}', 'OderBuyMeController@updateOderBuyMe');
        });
        Route::prefix('category-services')->group(function () {
            Route::post('/create-category-services', 'CategorySerivceController@createCategoryServices');
            Route::get('/list-category-services', 'CategorySerivceController@listdataCategoryService');
            Route::get('/list-category-services/{status}', 'CategorySerivceController@listdataCategoryServiceStatus');
            Route::delete('/delete-category-services/{id}', 'CategorySerivceController@deleteCategoryService');
            Route::put('/update-category-services/{id}', 'CategorySerivceController@updateCategoryServices');
        });
        Route::prefix('bannerr')->group(function () {
            Route::post('/create-banner-buy-me', 'BannerBuyMController@createBannerBuyMe');
            Route::get('/list-banner-buy-mee', 'BannerBuyMController@getBannerBuyMe');
            Route::delete('/delete-banner-buy-me/{id}', 'BannerBuyMController@deleteBannerBuyMe');
            Route::put('/update-banner-buy-me/{id}', 'BannerBuyMController@updateBannerBuyMe');
        });
    });
});
Route::prefix('admin')->group(function () {
    Route::namespace ('Admin')->group(function () {
        Route::post('/login-admin', 'UserController@loginAdmin');
        Route::put('/check-otp-verify', 'UserController@checkOTPVerify');
        Route::group(['middleware' => ['auth.jwt']], function () {
            Route::get('/get-user-information', 'UserController@getUserInfo');
            Route::put('/update-user', 'UserController@updateUser');
            Route::put('/change-password', 'UserController@changePassword');
            Route::post('/push-all-user', 'NotifyController@pushAllUser');
            Route::post('/push-all-user-with-transfer', 'NotifyController@pushAllUserWithTransfer');
            Route::get('/list-notify-with-transfer', 'NotifyController@listNotifyWithTransfer');
            Route::get('/list-notify-transfer-with-admin', 'NotifyController@listNotifyTransferWithAdmin');
            Route::put('/confirm-notify-transfer/{id}', 'NotifyController@confirmNotifyTransfer');
            Route::put('/cancel-notify-transfer/{id}', 'NotifyController@cancelNotifyTransfer');
            Route::get('/delete-notify', 'NotifyController@deleteNotify');
            Route::prefix('account')->group(function () {
                Route::get('/list-chain-owner', 'AccountController@listChainOwner');
                Route::get('/list-account', 'AccountController@listAccount');
                Route::get('/list-shipper', 'AccountController@listShipper');
                Route::get('/list-history-money-shipper/{id}', 'AccountController@listHistoryMoneyShipper');
                Route::get('/list-history-recharge-account/{id}', 'AccountController@listHistoryRechargeAccount');
                Route::get('/total-recharge-account-transfer', 'AccountController@totalRechargeAccountTransfer');
                Route::get('/list-staff-store', 'AccountController@listStaffStore');
                Route::get('/account-detail/{id}', 'AccountController@accountDetail');
                Route::delete('/delete-account/{id}', 'AccountController@deleteAccount');
                Route::delete('/delete-account-phone/{phone}', 'AccountController@deleteAccountPhone');
                Route::put('/lock-account/{id}', 'AccountController@lockAccount');
                Route::put('/unlock-account/{id}', 'AccountController@unlockAccount');
                Route::post('/create-account', 'AccountController@createAccount');
                Route::put('/update-account/{id}', 'AccountController@updateAccount');
                Route::post('/recharge-account', 'AccountController@rechargeAccount');
                Route::put('/reset-password/{id}', 'AccountController@resetPassword');
            });
            Route::prefix('category')->group(function () {
                Route::get('/list-category', 'CategoryController@listCategory');
                Route::get('/category-detail/{id}', 'CategoryController@categoryDetail');
                Route::post('/create-category', 'CategoryController@createCategory');
                Route::put('/update-category/{id}', 'CategoryController@updateCategory');
                Route::delete('/delete-category/{id}', 'CategoryController@deleteCategory');
            });
            Route::prefix('category-permanent')->group(function () {
                Route::get('/list-category-permanent/{category_id}', 'CategoryPermanentController@listCategoryPermanent');
                Route::get('/category-permanent-detail/{id}', 'CategoryPermanentController@categoryPermanentDetail');
                Route::post('/create-category-permanent', 'CategoryPermanentController@createCategoryPermanent');
                Route::put('/update-category-permanent/{id}', 'CategoryPermanentController@updateCategoryPermanent');
                Route::delete('/delete-category-permanent/{id}', 'CategoryPermanentController@deleteCategoryPermanent');
            });
            Route::prefix('category-food')->group(function () {
                Route::get('/list-category-food', 'CategoryFoodController@listCategoryFood');
                Route::get('/category-food-detail/{id}', 'CategoryFoodController@categoryFoodDetail');
                Route::post('/create-category-food', 'CategoryFoodController@createCategoryFood');
                Route::put('/update-category-food/{id}', 'CategoryFoodController@updateCategoryFood');
                Route::delete('/delete-category-food/{id}', 'CategoryFoodController@deleteCategoryFood');
                Route::put('/update-status-category-food/{id}', 'CategoryFoodController@updateStatusCategoryFood');
            });
            Route::prefix('store')->group(function () {
                Route::get('/list-store-with-push-notify', 'StoreController@listStoreWithPushNotify');
                Route::get('/list-store', 'StoreController@listStore');
                Route::get('/store-detail/{id}', 'StoreController@storeDetail');
                Route::post('/create-store', 'StoreController@createStore');
                Route::put('/update-store/{id}', 'StoreController@updateStore');
                Route::put('/confirm-store/{id}', 'StoreController@confirmStore');
                Route::put('/cancel-store/{id}', 'StoreController@cancelStore');
                Route::put('/lock-store/{id}', 'StoreController@lockStore');
                Route::put('/unlock-store/{id}', 'StoreController@unlockStore');
                Route::delete('delete-store/{id}', 'StoreController@deleteStore');
            });
            Route::prefix('fee-ship')->group(function () {
                Route::get('/list-fee-ship', 'FeeShipController@listFeeShip');
                Route::get('/fee-ship-detail/{id}', 'FeeShipController@feeShipDetail');
                Route::post('/create-fee-ship', 'FeeShipController@createFeeShip');
                Route::put('/update-fee-ship/{id}', 'FeeShipController@updateFeeShip');
                Route::delete('/delete-fee-ship/{id}', 'FeeShipController@deleteFeeShip');
            });
            Route::prefix('voucher')->group(function () {
                Route::get('/list-voucher', 'VoucherController@listVoucher');
                Route::get('/voucher-detail/{id}', 'VoucherController@voucherDetail');
                Route::post('/create-voucher', 'VoucherController@createVoucher');
                Route::put('/update-voucher/{id}', 'VoucherController@updateVoucher');
                Route::delete('/delete-voucher/{id}', 'VoucherController@deleteVoucher');
            });
            Route::prefix('voucher-delivery')->group(function () {
                Route::get('/list-voucher', 'VoucherDeliveryController@listVoucher');
                Route::get('/voucher-detail/{id}', 'VoucherDeliveryController@voucherDetail');
                Route::post('/create-voucher', 'VoucherDeliveryController@createVoucher');
                Route::put('/update-voucher/{id}', 'VoucherDeliveryController@updateVoucher');
                Route::delete('/delete-voucher/{id}', 'VoucherDeliveryController@deleteVoucher');
            });
            Route::prefix('program')->group(function () {
                Route::get('/list-program', 'ProgramController@listProgram');
                Route::get('/program-detail/{id}', 'ProgramController@programDetail');
                Route::post('/create-program', 'ProgramController@createProgram');
                Route::put('/update-program/{id}', 'ProgramController@updateProgram');
                Route::delete('/delete-program/{id}', 'ProgramController@deleteProgram');
                Route::get('/list-program-store', 'ProgramController@listProgramStore');
                Route::get('/program-store-detail/{id}', 'ProgramController@programStoreDetail');
            });
            Route::prefix('banner')->group(function () {
                Route::get('/list-banner', 'BannerController@listBanner');
                Route::get('/banner-detail/{id}', 'BannerController@bannerDetail');
                Route::post('/create-banner', 'BannerController@createBanner');
                Route::put('/update-banner/{id}', 'BannerController@updateBanner');
                Route::delete('/delete-banner/{id}', 'BannerController@deleteBanner');
            });
            Route::prefix('percent')->group(function () {
                Route::get('/percent-coin-detail', 'SetupPercentController@percentCoinDetail');
                Route::get('/percent-fee-shipper-detail', 'SetupPercentController@percentFeeShipperDetail');
                Route::put('/update-percent-fee-shipper/{id}', 'SetupPercentController@updatePercentFeeShipper');
                Route::get('/percent-fee-store-detail', 'SetupPercentController@percentFeeStoreDetail');
                Route::put('/update-percent-fee-store/{id}', 'SetupPercentController@updatePercentFeeStore');
            });
            Route::prefix('percent-coin')->group(function () {
                Route::put('/update-percent-coin/{id}', 'SetupPercentController@updatePercentCoin');
            });
            Route::prefix('revenue')->group(function () {
                Route::get('/list-revenue-store', 'RevenueController@listRevenueStore');
                Route::get('/list-revenue-shipper', 'RevenueController@listRevenueShipper');
                Route::get('/revenue-store-detail/{id}', 'RevenueController@revenueStoreDetail');
                Route::get('/revenue-shipper-detail/{id}', 'RevenueController@revenueShipperDetail');
            });
            Route::prefix('staff')->group(function () {
                Route::get('/list-role-staff', 'StaffController@listRoleStaff');
                Route::get('/list-staff', 'StaffController@listStaff');
                Route::get('/staff-detail/{id}', 'StaffController@staffDetail');
                Route::delete('/delete-staff/{id}', 'StaffController@deleteStaff');
                Route::put('/lock-staff/{id}', 'StaffController@lockStaff');
                Route::put('/unlock-staff/{id}', 'StaffController@unlockStaff');
                Route::post('/create-staff', 'StaffController@createStaff');
                Route::put('/update-staff/{id}', 'StaffController@updateStaff');
            });
            Route::prefix('transfer-account')->group(function () {
                Route::get('/list', 'TransferAccountController@listTransferAccount');
                Route::get('/detail/{id}', 'TransferAccountController@transferAccountDetail');
                Route::delete('/delete/{id}', 'TransferAccountController@deleteTransferAccount');
                Route::put('/lock/{id}', 'TransferAccountController@lockTransferAccount');
                Route::put('/unlock/{id}', 'TransferAccountController@unlockTransferAccount');
                Route::post('/create', 'TransferAccountController@createTransferAccount');
                Route::put('/update/{id}', 'TransferAccountController@updateTransferAccount');
            });
            Route::prefix('transfer-staff-account')->group(function () {
                Route::get('/list', 'TransferStaffAccountController@listTransferStaffAccount');
                Route::get('/detail/{id}', 'TransferStaffAccountController@transferStaffAccountDetail');
                Route::delete('/delete/{id}', 'TransferStaffAccountController@deleteTransferStaffAccount');
                Route::put('/lock/{id}', 'TransferStaffAccountController@lockTransferStaffAccount');
                Route::put('/unlock/{id}', 'TransferStaffAccountController@unlockTransferStaffAccount');
                Route::post('/create', 'TransferStaffAccountController@createTransferStaffAccount');
                Route::put('/update/{id}', 'TransferStaffAccountController@updateTransferStaffAccount');
            });
            Route::prefix('website')->group(function () {
                Route::get('/home-website-detail', 'WebsiteController@homeWebSiteDetail');
                Route::put('/update-home-website/{id}', 'WebsiteController@updateHomeWebSite');
                Route::get('/list-feature-website', 'WebsiteController@listFeatureWebsite');
                Route::get('/feature-website-detail/{id}', 'WebsiteController@FeatureWebsiteDetail');
                Route::post('/create-feature-website', 'WebsiteController@createFeatureWebsite');
                Route::put('/update-feature-website/{id}', 'WebsiteController@updateFeatureWebsite');
                Route::delete('/delete-feature-website/{id}', 'WebsiteController@deleteFeatureWebsite');
                Route::get('/list-category-question-website', 'WebsiteController@listCategoryQuestionWebsite');
                Route::get('/category-question-website-detail/{id}', 'WebsiteController@categoryQuestionWebsiteDetail');
                Route::post('/create-category-question-website', 'WebsiteController@createCategoryQuestionWebsite');
                Route::put('/update-category-question-website/{id}', 'WebsiteController@updateCategoryQuestionWebsite');
                Route::delete('/delete-category-question-website/{id}', 'WebsiteController@deleteCategoryQuestionWebsite');
                Route::get('/list-question-website', 'WebsiteController@listQuestionWebsite');
                Route::get('/question-website-detail/{id}', 'WebsiteController@questionWebsiteDetail');
                Route::post('/create-question-website', 'WebsiteController@createQuestionWebsite');
                Route::put('/update-question-website/{id}', 'WebsiteController@updateQuestionWebsite');
                Route::delete('/delete-question-website/{id}', 'WebsiteController@deleteQuestionWebsite');
                Route::get('/list-contact-website', 'WebsiteController@listContactWebsite');
                Route::get('/link-youtube', 'WebsiteController@linkYoutube');
                Route::put('/update-link-youtube/{id}', 'WebsiteController@updateLinkYoutube');
                Route::get('/content-toward', 'WebsiteController@contentToward');
                Route::put('/update-content-toward/{id}', 'WebsiteController@updateContentToward');
            });
            Route::prefix('leadership-team')->group(function () {
                Route::get('/list-leadership-team', 'LeadershipTeamController@listLeadershipTeam');
                Route::get('/leadership-team-detail/{id}', 'LeadershipTeamController@leadershipTeamDetail');
                Route::post('/create-leadership-team', 'LeadershipTeamController@createLeadershipTeam');
                Route::put('/update-leadership-team/{id}', 'LeadershipTeamController@updateLeadershipTeam');
                Route::delete('/delete-leadership-team/{id}', 'LeadershipTeamController@deleteLeadershipTeam');
            });
            Route::prefix('tutorial-document')->group(function () {
                Route::get('/list-tutorial-document', 'TutorialDocumentController@listTutorialDocument');
                Route::get('/tutorial-document-detail/{id}', 'TutorialDocumentController@tutorialDocumentDetail');
                Route::post('/create-tutorial-document', 'TutorialDocumentController@createTutorialDocument');
                Route::put('/update-tutorial-document/{id}', 'TutorialDocumentController@updateTutorialDocument');
                Route::delete('/delete-tutorial-document/{id}', 'TutorialDocumentController@deleteTutorialDocument');
            });
            Route::prefix('used-solution')->group(function () {
                Route::get('/list-used-solution', 'UsedSolutionController@listUsedSolution');
                Route::get('/used-solution-detail/{id}', 'UsedSolutionController@usedSolutionDetail');
                Route::post('/create-used-solution', 'UsedSolutionController@createUsedSolution');
                Route::put('/update-used-solution/{id}', 'UsedSolutionController@updateUsedSolution');
                Route::delete('/delete-used-solution/{id}', 'UsedSolutionController@deleteUsedSolution');
            });
            Route::prefix('suggestion')->group(function () {
                Route::get('/list-suggestion', 'SuggestionController@listSuggestion');
                Route::get('/suggestion-detail/{id}', 'SuggestionController@suggestionDetail');
                Route::post('/create-suggestion', 'SuggestionController@createSuggestion');
                Route::put('/update-suggestion/{id}', 'SuggestionController@updateSuggestion');
                Route::delete('/delete-suggestion/{id}', 'SuggestionController@deleteSuggestion');
            });
            Route::prefix('type-food')->group(function () {
                Route::get('/list-type-food', 'TypeFoodController@listTypeFood');
                Route::get('/type-food-detail/{id}', 'TypeFoodController@typeFoodDetail');
                Route::post('/create-type-food', 'TypeFoodController@createTypeFood');
                Route::put('/update-type-food/{id}', 'TypeFoodController@updateTypeFood');
                Route::delete('/delete-type-food/{id}', 'TypeFoodController@deleteTypeFood');
            });
            Route::prefix('policy-app')->group(function () {
                Route::get('/payment-guide', 'PolicyAppController@paymentGuide');
                Route::put('/update-payment-guide/{id}', 'PolicyAppController@updatePaymentGuide');
                Route::get('/privacy-policy', 'PolicyAppController@privacyPolicy');
                Route::put('/update-privacy-policy/{id}', 'PolicyAppController@updatePrivacyPolicy');
                Route::get('/regulation', 'PolicyAppController@regulation');
                Route::put('/update-regulation/{id}', 'PolicyAppController@updateRegulation');
                Route::get('/term-of-use', 'PolicyAppController@termOfUse');
                Route::put('/update-term-of-use/{id}', 'PolicyAppController@updateTermOfUse');
                Route::get('/dispute-resolution-policy', 'PolicyAppController@disputeResolutionPolicy');
                Route::put('/update-dispute-resolution-policy/{id}', 'PolicyAppController@updateDisputeResolutionPolicy');
            });
            Route::prefix('order-food')->group(function () {
                Route::get('/list-order-food', 'OrderFoodController@listOrderFood');
                Route::get('/order-detail/{id}', 'OrderFoodController@orderDetail');
                Route::put('/cancel-order/{id}', 'OrderFoodController@cancelOrder');
                Route::get('/list-order-user-product', 'OrderFoodController@listOrderUserProduct');
                Route::get('/order-user-product-detail/{id}', 'OrderFoodController@orderUserProductDetail');
                Route::put('/cancel-order-user-product/{id}', 'OrderFoodController@cancelOrderUserProduct');
            });
            Route::prefix('product-weight')->group(function () {
                Route::get('/list-product-weight', 'ProductWeightController@listProductWeight');
                Route::get('/product-weight-detail/{id}', 'ProductWeightController@productWeightDetail');
                Route::post('/create-product-weight', 'ProductWeightController@createProductWeight');
                Route::put('/update-product-weight/{id}', 'ProductWeightController@updateProductWeight');
                Route::delete('/delete-product-weight/{id}', 'ProductWeightController@deleteProductWeight');
            });
            Route::prefix('withdraw-money')->group(function () {
                Route::get('/list-withdraw-money', 'WithdrawMoneyController@listWithdrawMoney');
                Route::put('/confirm-withdraw-money/{id}', 'WithdrawMoneyController@confirmWithdrawMoney');
                Route::put('/cancel-withdraw-money/{id}', 'WithdrawMoneyController@cancelWithdrawMoney');
            });
            Route::prefix('setup-withdraw-money')->group(function () {
                Route::get('/detail', 'SetupWithdrawMoneyController@setupWithdrawMoney');
                Route::put('/update/{id}', 'SetupWithdrawMoneyController@updateSetupWithdrawMoney');
            });
        });
    });
});
