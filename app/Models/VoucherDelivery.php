<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherDelivery extends Model
{
    protected $table = 'voucher_delivery';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title',
        'code',
        'description',
        'percent',
        'money',
        'type',
        'quantity',
        'quantity_used',
        'quantity_use_with_user_new',
        'type_percent_or_money',
        'proviso',
        'time_open',
        'time_close',
        'status',
    ];
}
