<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ToppingFood extends Model
{
    //
    protected $table = 'topping_food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'category_topping_food_id',
        'name',
        'price',
        'status'
    ];
    public function categoryToppingFood()
    {
        return $this->belongsTo(CategoryToppingFood::class, 'category_topping_food_id');
    }
}
