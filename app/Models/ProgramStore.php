<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class ProgramStore extends Model
{
    //
    protected $table = 'program_store';
    protected $primaryKey = 'id';
    protected $fillable = [
        'store_id',
        'name',
        'name_public',
        'image',
        'percent',
        'time_open',
        'time_close',
        'content',
        'status',
        'type_percent_or_money',
        'money',
        'quantity',
        'check_close',
        'quantity_bought',
        'is_active'
    ];
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function food()
    {
        return $this->belongsToMany(Food::class, 'program_store_detail');
    }
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/image_store_default.png');
        }
        return asset($image);
    }
}
