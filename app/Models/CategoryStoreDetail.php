<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class CategoryStoreDetail extends Model
{
    //
    protected $table = 'category_store_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'store_id',
        'category_name',
    ];
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
