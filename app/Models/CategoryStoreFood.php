<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryStoreFood extends Model
{
    //
    protected $table = 'category_store_food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'store_id',
        'name',
    ];
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function food()
    {
        return $this->HasMany(Food::class, 'category_store_food_id');
    }
}
