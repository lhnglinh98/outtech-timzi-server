<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class UserStore extends Model
{
    //
    protected $table = 'user_store';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'store_id',
        'status',
        'is_owner'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
