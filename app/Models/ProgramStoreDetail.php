<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramStoreDetail extends Model
{
    //
    protected $table = 'program_store_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'program_store_id',
        'food_id',
        'status'
    ];
}
