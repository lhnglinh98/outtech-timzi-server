<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class TableStore extends Model
{
    //
    protected $table = 'table_store';
    protected $primaryKey = 'id';
    protected $fillable = [
        'store_id',
        'code',
        'number_table',
        'number_people_min',
        'number_people_max',
        'number_floor',
        'status',
        'pass_wifi'
    ];
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
