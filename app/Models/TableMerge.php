<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TableMerge extends Model
{
    //
    protected $table = 'table_merge';
    protected $primaryKey = 'id';
    protected $fillable = [
        'book_table_id',
        'table_store_id',
    ];
}
