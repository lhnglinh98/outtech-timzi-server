<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryPermanent extends Model
{
    //
    protected $table = 'category_permanent';
    protected $primaryKey = 'id';
    protected $fillable = [
        'category_id',
        'name',
    ];
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
