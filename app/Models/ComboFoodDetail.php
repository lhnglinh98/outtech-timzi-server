<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class ComboFoodDetail extends Model
{
    //
    protected $table = 'combo_food_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'combo_food_id',
        'food_id',
        'status'
    ];
}
