<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionPercent extends Model
{
    //
    protected $table = 'commissions_percent';
    protected $primaryKey = 'id';
    protected $fillable = [
        'type_user', 'percent'
    ];
}
