<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class BookFood extends Model
{
    //
    protected $table = 'book_food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'book_table_id',
        'combo_food_id',
        'food_id',
        'size',
        'quantity',
        'price',
        'status',
        'quantity_minus_group_food'
    ];
    public function bookTable()
    {
        return $this->belongsTo(BookTable::class, 'book_table_id');
    }
    public function food()
    {
        return $this->belongsTo(Food::class, 'food_id');
    }
    public function comboFood()
    {
        return $this->belongsTo(ComboFood::class, 'combo_food_id');
    }
    public function toppingFood()
    {
        return $this->hasMany(BookFoodTopping::class, 'book_food_id');
    }
}
