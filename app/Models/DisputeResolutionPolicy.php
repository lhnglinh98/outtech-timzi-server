<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DisputeResolutionPolicy extends Model
{
    protected $table = 'dispute_resolution_policy';
    protected $primaryKey = 'id';
    protected $fillable = [
        'content'
    ];
}
