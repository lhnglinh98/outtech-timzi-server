<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class HistoryMoneyShipper extends Model
{
    //
    protected $table = 'history_money_shipper';
    protected $primaryKey = 'id';
    protected $fillable = [
        'shipper_id',
        'money',
        'type',
        'content',
        'type_order'
    ];
    public function shipper()
    {
        return $this->belongsTo(User::class, 'shipper_id');
    }
}
