<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class OrderFood extends Model
{
    //
    protected $table = 'order_food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code',
        'user_id',
        'user_name',
        'phone',
        'address',
        'latitude',
        'longtidue',
        'date_time',
        'store_id',
        'fee_ship',
        'discount',
        'total_money',
        'coin',
        'note',
        'payment_method_id',
        'shipper_id',
        'star',
        'content',
        'status',
        'is_review_shipper',
        'time_check',
        'quantity_push',
        'money_voucher',
        'type_voucher',
        'polyline',
        'voucher_id',
        'is_relatives',
        'is_payment_store',
        'total_money_food',
        'status_payment_vnpay',
        'status_payment_success',
        'is_push_store',
        'staff_timzi_id',
        'updated_at'
    ];
    public function getPolylineAttribute($polyline)
    {
        return json_decode($polyline);
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function staffTimzi()
    {
        return $this->belongsTo(User::class, 'staff_timzi_id');
    }
    public function shipper()
    {
        return $this->belongsTo(User::class, 'shipper_id');
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id');
    }
    public function orderFoodDetail()
    {
        return $this->HasMany(OrderFoodDetail::class, 'order_food_id');
    }
    public function reviewShipper()
    {
        return $this->HasOne(ReviewShipper::class, 'order_food_id');
    }
    public function voucher()
    {
        return $this->belongsTo(Voucher::class, 'voucher_id');
    }
    public function getUpdatedAtAttribute($updated_at)
    {
        if($updated_at == null){
            return null;
        }
        return date('Y-m-d H:i:s', strtotime($updated_at));
    }
}
