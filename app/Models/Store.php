<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Store extends Model
{
    //
    protected $table = 'stores';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'name',
        'image',
        'province_id',
        'district_id',
        'ward_id',
        'address',
        'latitude',
        'longtidue',
        'star',
        'average_price',
        'open_hours',
        'close_hours',
        'hotline',
        'is_owner',
        'count_order',
        'status',
        'percent_discount_revenue',
        'is_open',
        'banner'
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/image_store_default.png');
        }
        return asset($image);
    }
    public function getBannerAttribute($banner)
    {
        if ($banner == null) {
            if($this->image == null){
                return asset('/image_default/image_store_default.png');
            }else{
                return asset($this->image);
            }
        }
        return asset($banner);
    }
    public function category()
    {
        return $this->belongsToMany(Category::class, 'category_store');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function staff()
    {
        return $this->belongsToMany(User::class, 'user_store');
    }
    public function categoryStoreDetail()
    {
        return $this->HasMany(CategoryStoreDetail::class, 'store_id');
    }
    public function categoryStoreFood()
    {
        return $this->HasMany(CategoryStoreFood::class, 'store_id');
    }
    public function programStore()
    {
        return $this->HasMany(ProgramStore::class, 'store_id');
    }
    public function comboFood()
    {
        return $this->HasMany(ComboFood::class, 'store_id');
    }
    public function food()
    {
        return $this->HasMany(Food::class, 'store_id');
    }
    public function tableStore()
    {
        return $this->HasMany(TableStore::class, 'store_id');
    }
    public function program()
    {
        return $this->belongsToMany(Program::class, 'program_detail');
    }
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }
}
