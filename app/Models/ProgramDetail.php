<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class ProgramDetail extends Model
{
    //
    protected $table = 'program_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'program_id',
        'store_id',
        'status'
    ];
}
