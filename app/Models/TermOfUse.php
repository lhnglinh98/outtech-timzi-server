<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TermOfUse extends Model
{
    protected $table = 'terms_of_use';
    protected $primaryKey = 'id';
    protected $fillable = [
        'content'
    ];
}
