<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TutorialDocument extends Model
{
    //
    protected $table = 'tutorial_document';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'image', 'file', 'created_at', 'updated_at'
    ];
    public function getImageAttribute($image)
    {
        if($image == null){
            return asset('/image_system/avatar_default.jpg');
        }
        return asset($image);
    }
    public function getFileAttribute($file)
    {
        if($file == null){
            return null;
        }
        return asset($file);
    }
}
