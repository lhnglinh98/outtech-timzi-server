<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoteBuyMe extends Model
{
    protected $table = 'note_buy_me';
    protected $primaryKey = 'id';
    protected $fillable = [
        'notebook_id',
        'title',
        'content',
        'status'
    ];
    public function NoteBook()
    {
        return $this->belongsTo(NoteBook::class, 'notebook_id');
    }

}
