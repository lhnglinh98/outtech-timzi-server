<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'supplier';
    protected $primaryKey = 'id';
    protected $fillable = [
        'store_id',
        'name',
        'phone',
        'description',
        'status',
    ];
    public function material()
    {
        return $this->belongsToMany(Material::class, 'material_supplier')->where('material.status', 1);
    }
}
