<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SetupPercentCoin extends Model
{
    //
    protected $table = 'setup_percent_coin';
    protected $primaryKey = 'id';
    protected $fillable = [
        'percent'
    ];
}
