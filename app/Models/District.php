<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class District extends Model
{
    //
    protected $table = 'district';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'prefix', 'province_id'
    ];
}
