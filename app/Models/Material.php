<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'material';
    protected $primaryKey = 'id';
    protected $fillable = [
        'store_id',
        'type_material_id',
        'name',
        'description',
        'status',
    ];
    public function supplier()
    {
        return $this->belongsToMany(Supplier::class, 'material_supplier');
    }
    public function type()
    {
        return $this->belongsTo(TypeMaterial::class, 'type_material_id');
    }
}
