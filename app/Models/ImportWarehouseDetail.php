<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportWarehouseDetail extends Model
{
    protected $table = 'import_warehouse_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'import_warehouse_id',
        'material_id',
        'quantity',
        'money',
    ];
    public function importWarehouse()
    {
        return $this->belongsTo(ImportWarehouse::class, 'import_warehouse_id');
    }
    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id');
    }
}
