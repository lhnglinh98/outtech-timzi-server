<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class CategoryService extends Model
{
    //
    protected $table = 'category_service';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title',
        'content',
        'status',
        'price_service'
    ];
}
