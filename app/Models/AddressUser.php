<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class AddressUser extends Model
{
    //
    protected $table = 'address_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'user_name',
        'phone',
        'address',
        'latitude',
        'longtidue',
        'type'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }
}
