<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Province extends Model
{
    //
    protected $table = 'province';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'image', 'code'
    ];
}
