<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Voucher extends Model
{
    //
    protected $table = 'voucher';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title',
        'code',
        'description',
        'percent',
        'money',
        'type',
        'quantity',
        'type_percent_or_money',
        'proviso',
        'store_id',
        'time_open',
        'time_close',
        'status',
        'is_transfer',
        'transfer_id',
    ];
    public function user()
    {
        return $this->belongsToMany(User::class, 'voucher_user');
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
