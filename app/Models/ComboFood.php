<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class ComboFood extends Model
{
    //
    protected $table = 'combo_food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'store_id',
        'image',
        'name',
        'price',
        'quantity',
        'content',
        'status',
        'time_open',
        'time_close',
        'is_new',
        'is_specialties',
        'is_out_of_food',
        'quantity_bought',
        'is_active',
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/image_store_default.png');
        }
        return asset($image);
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function food()
    {
        return $this->belongsToMany(Food::class, 'combo_food_detail');
    }
    public function card()
    {
        return $this->HasMany(CardFood::class, 'combo_food_id')->where('card_food.status', 0);
    }
}
