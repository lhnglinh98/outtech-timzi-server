<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    //
    protected $table = 'food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'image',
        'image',
        'code',
        'name',
        'description',
        'price',
        'status',
        'store_id',
        'category_food_id',
        'is_sale',
        'price_discount',
        'price_discount_with_program',
        'is_new',
        'is_specialties',
        'is_out_of_food',
        'category_store_food_id',
        'is_group_food',
        'quantity_food',
        'type_food_id'
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/image_store_default.png');
        }
        return asset($image);
    }
    public function categoryFood()
    {
        return $this->belongsTo(CategoryFood::class, 'category_food_id');
    }
    public function typeFood()
    {
        return $this->belongsTo(TypeFood::class, 'type_food_id');
    }
    public function categoryStoreFood()
    {
        return $this->belongsTo(CategoryStoreFood::class, 'category_store_food_id');
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function size()
    {
        return $this->HasMany(FoodSize::class, 'food_id');
    }
    public function card()
    {
        return $this->HasMany(CardFood::class, 'food_id')->where('card_food.status', 0);
    }
    public function categoryToppingFood()
    {
        return $this->HasMany(CategoryToppingFood::class, 'food_id')->where('category_topping_food.status', 1);
    }
    public function orderFoodDetail()
    {
        return $this->HasMany(OrderFoodDetail::class, 'food_id')->where('order_food_detail.star', '!=', null);
    }
}
