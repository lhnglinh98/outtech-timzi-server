<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class VoucherUser extends Model
{
    //
    protected $table = 'voucher_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'voucher_id',
        'status'
    ];
}
