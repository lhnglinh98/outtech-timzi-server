<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Program extends Model
{
    //
    protected $table = 'program';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'image',
        'category_food_id',
        'percent',
        'content',
        'time_open',
        'time_close',
        'status',
        'type_percent_or_money',
        'money',
        'check_close'
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/image_store_default.png');
        }
        return asset($image);
    }
    public function categoryFood()
    {
        return $this->belongsTo(CategoryFood::class, 'category_food_id');
    }
    public function store()
    {
        return $this->belongsToMany(Store::class, 'program_detail');
    }
}
