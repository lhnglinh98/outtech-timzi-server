<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoteBook extends Model
{
    protected $table = 'notebook';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'title',
        'description'
    ];
    public function NoteBookMe()
    {
        return $this->HasMany(NoteBuyMe::class, 'notebook_id');
    }

}
