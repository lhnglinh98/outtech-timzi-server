<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class CardFood extends Model
{
    //
    protected $table = 'card_food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'combo_food_id',
        'food_id',
        'size',
        'quantity',
        'price',
        'store_id',
        'status'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function food()
    {
        return $this->belongsTo(Food::class, 'food_id');
    }
    public function comboFood()
    {
        return $this->belongsTo(ComboFood::class, 'combo_food_id');
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function toppingFood()
    {
        return $this->hasMany(CardToppingFood::class, 'card_food_id');
    }
}
