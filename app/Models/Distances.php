<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Distances extends Model
{
    protected $table = 'distances';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title',
        'price_discount',
        'status'
    ];

}
