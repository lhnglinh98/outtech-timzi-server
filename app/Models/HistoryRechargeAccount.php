<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class HistoryRechargeAccount extends Model
{
    protected $table = 'history_recharge_account';
    protected $primaryKey = 'id';
    protected $fillable = [
        'shipper_id',
        'money',
        'money_current',
        'content',
    ];
    public function shipper()
    {
        return $this->belongsTo(User::class, 'shipper_id');
    }
    public function getCreatedAtAttribute($created_at)
    {
        if($created_at == null){
            return null;
        }
        return date('Y-m-d H:i:s', strtotime($created_at));
    }
}
