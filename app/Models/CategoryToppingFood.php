<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryToppingFood extends Model
{
    //
    protected $table = 'category_topping_food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'food_id',
        'name',
        'limit',
        'status'
    ];
    public function food()
    {
        return $this->belongsTo(Food::class, 'food_id');
    }
    public function toppingFood()
    {
        return $this->HasMany(ToppingFood::class, 'category_topping_food_id')->where('topping_food.status', 1);
    }
}
