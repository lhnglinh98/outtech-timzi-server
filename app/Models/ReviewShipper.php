<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class ReviewShipper extends Model
{
    //
    protected $table = 'review_shipper';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'shipper_id',
        'star',
        'content',
        'order_food_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function shipper()
    {
        return $this->belongsTo(User::class, 'shipper_id');
    }
}
