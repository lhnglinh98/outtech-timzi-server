<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryQuestionWebsite extends Model
{
    //
    protected $table = 'category_question_website';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'answer'
    ];
    public function questionWebsite()
    {
        return $this->hasMany(QuestionWebsite::class, 'category_id');
    }
}
