<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsedSolution extends Model
{
    //
    protected $table = 'used_solution';
    protected $primaryKey = 'id';
    protected $fillable = [
        'image', 'title', 'content'
    ];
    public function getImageAttribute($image)
    {
        if($image == null){
            return asset('/image_system/avatar_default.jpg');
        }
        return asset($image);
    }
}
