<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipperStore extends Model
{
    //
    protected $table = 'shipper_store';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'store_id',
        'status'
    ];
}
