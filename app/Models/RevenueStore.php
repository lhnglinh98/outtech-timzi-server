<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RevenueStore extends Model
{
    //
    protected $table = 'revenue_store';
    protected $primaryKey = 'id';
    protected $fillable = [
        'store_id',
        'money',
        'fee_money',
        'month',
        'late_day',
        'date_check',
        'status',
        'code'
    ];
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
