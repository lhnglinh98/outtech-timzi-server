<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    protected $table = 'banner';
    protected $primaryKey = 'id';
    protected $fillable = [
        'image', 'store_id'
    ];
    public function getImageAttribute($image)
    {
        if($image == null){
            return asset('/image_default/image_store_default.png');
        }
        return asset($image);
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
