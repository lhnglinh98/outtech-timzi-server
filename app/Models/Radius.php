<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Radius extends Model
{
    //
    protected $table = 'radius';
    protected $primaryKey = 'id';
    protected $fillable = [
        'radius'
    ];
}
