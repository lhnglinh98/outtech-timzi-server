<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class PaymentMethod extends Model
{
    //
    protected $table = 'payment_method';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name'
    ];
}
