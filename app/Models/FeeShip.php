<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeeShip extends Model
{
    //
    protected $table = 'fee_ship';
    protected $primaryKey = 'id';
    protected $fillable = [
        'money', 'from_range', 'to_range'
    ];
}
