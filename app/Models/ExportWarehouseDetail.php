<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExportWarehouseDetail extends Model
{
    protected $table = 'export_warehouse_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'export_warehouse_id',
        'material_id',
        'quantity',
        'money',
    ];
    public function exportWarehouse()
    {
        return $this->belongsTo(ExportWarehouse::class, 'export_warehouse_id');
    }
    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id');
    }
}
