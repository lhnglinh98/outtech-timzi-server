<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class OrderFoodDetail extends Model
{
    //
    protected $table = 'order_food_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'order_food_id',
        'food_id',
        'combo_food_id',
        'user_id',
        'store_id',
        'quantity',
        'money_receive_shipper',
        'star',
        'content',
    ];
    public function orderFood()
    {
        return $this->belongsTo(OrderFood::class, 'order_food_id');
    }
    public function food()
    {
        return $this->belongsTo(Food::class, 'food_id');
    }
    public function comboFood()
    {
        return $this->belongsTo(ComboFood::class, 'combo_food_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function orderToppingFoodDetail()
    {
        return $this->HasMany(OrderToppingFoodDetail::class, 'order_food_detail_id');
    }
}
