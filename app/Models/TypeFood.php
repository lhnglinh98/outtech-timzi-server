<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeFood extends Model
{
    //
    protected $table = 'type_food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'status', 'created_at', 'updated_at'
    ];
}
