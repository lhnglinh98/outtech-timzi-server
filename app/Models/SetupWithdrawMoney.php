<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SetupWithdrawMoney extends Model
{
    protected $table = 'setup_withdraw_money';
    protected $primaryKey = 'id';
    protected $fillable = [
        'money'
    ];
}
