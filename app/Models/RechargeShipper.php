<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RechargeShipper extends Model
{
    //
    protected $table = 'recharge_shipper';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code', 'shipper_id', 'money', 'status'
    ];
}
