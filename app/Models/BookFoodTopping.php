<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookFoodTopping extends Model
{
    //
    protected $table = 'book_food_topping';
    protected $primaryKey = 'id';
    protected $fillable = [
        'book_food_id',
        'topping_food_id',
        'category_topping_food_name',
        'topping_food_name',
        'price',
    ];
    public function toppingFood()
    {
        return $this->belongsTo(ToppingFood::class, 'topping_food_id');
    }
}
