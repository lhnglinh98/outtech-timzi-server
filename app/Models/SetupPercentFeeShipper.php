<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SetupPercentFeeShipper extends Model
{
    //
    protected $table = 'setup_percent_fee_shipper';
    protected $primaryKey = 'id';
    protected $fillable = [
        'percent'
    ];
}
