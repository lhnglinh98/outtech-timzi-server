<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderToppingFoodDetail extends Model
{
    //
    protected $table = 'order_topping_food_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
        'order_food_detail_id',
        'topping_food_id',
        'category_topping_food_name',
        'topping_food_name',
        'price',
    ];
    public function toppingFood()
    {
        return $this->belongsTo(ToppingFood::class, 'topping_food_id');
    }
    
}
