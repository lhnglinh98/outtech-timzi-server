<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionWebsite extends Model
{
    //
    protected $table = 'question_website';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'category_id'
    ];
    public function categoryQuestionWebsite()
    {
        return $this->belongsTo(CategoryQuestionWebsite::class, 'category_id');
    }
}
