<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
    //
    protected $table = 'notification';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'device_id', 'content', 'object_id', 'type', 'is_view', 'status', 'created_at',
    ];
    public function getCreatedAtAttribute($created_at)
    {
        if ($created_at != null) {
            // return date('H:i d-m-Y', strtotime($created_at));
            return $this->getdateFacebook($created_at);
        }
        return null;
    }
    public static function getdateFacebook($date)
    {
        $date = date('Y-m-d H:i:s', strtotime($date));
        $date_facebook = '';
        if (!empty($date)) {
            //lay giờ theo giống facebook
            Carbon::setLocale('vi'); // hiển thị ngôn ngữ tiếng việt.
            $date = date_create($date);
            $date_fb = Carbon::create((date_format($date, "Y")),
                (date_format($date, "m")),
                (date_format($date, "d")),
                (date_format($date, "H")),
                (date_format($date, "i")),
                (date_format($date, "s")));
            $now = Carbon::now();
            $date_facebook = $date_fb->diffForHumans($now); //1 giờ trước
        }
        return $date_facebook;
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
