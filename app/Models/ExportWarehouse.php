<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExportWarehouse extends Model
{
    protected $table = 'export_warehouse';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code',
        'store_id',
        'quantity',
        'total_money',
        'user_export_name',
        'date_export',
        'note',
        'status',
    ];
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function exportWarehouseDetail()
    {
        return $this->HasMany(ExportWarehouseDetail::class, 'export_warehouse_id');
    }
}
