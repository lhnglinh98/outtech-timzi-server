<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardToppingFood extends Model
{
    //
    protected $table = 'card_topping_food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'card_food_id',
        'topping_food_id',
        'category_topping_food_name',
        'topping_food_name',
        'price',
    ];
    public function toppingFood()
    {
        return $this->belongsTo(ToppingFood::class, 'topping_food_id');
    }
}
