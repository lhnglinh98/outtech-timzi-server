<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactWebsite extends Model
{
    //
    protected $table = 'contact_website';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'email', 'message', 'question_website_id'
    ];
    public function questionWebsite()
    {
        return $this->belongsTo(QuestionWebsite::class, 'question_website_id');
    }
}
