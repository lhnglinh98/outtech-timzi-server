<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentGuide extends Model
{
    //
    protected $table = 'payment_guide';
    protected $primaryKey = 'id';
    protected $fillable = [
        'content'
    ];
}
