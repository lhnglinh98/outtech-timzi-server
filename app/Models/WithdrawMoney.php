<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class WithdrawMoney extends Model
{
    protected $table = 'withdraw_money';
    protected $primaryKey = 'id';
    protected $fillable = [
        'shipper_id',
        'money',
        'status',
    ];
    public function shipper()
    {
        return $this->belongsTo(User::class, 'shipper_id');
    }
}
