<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TransferNotify extends Model
{
    protected $table = 'transfer_notify';
    protected $primaryKey = 'id';
    protected $fillable = [
        'store_id',
        'transfer_id',
        'type',
        'content',
        'status',
    ];
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function transfer()
    {
        return $this->belongsTo(User::class, 'transfer_id');
    }
}
