<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportWarehouse extends Model
{
    protected $table = 'import_warehouse';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code',
        'store_id',
        'supplier_id',
        'quantity',
        'total_money',
        'user_import_name',
        'date_import',
        'note',
        'status',
    ];
    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function importWarehouseDetail()
    {
        return $this->HasMany(ImportWarehouseDetail::class, 'import_warehouse_id');
    }
}
