<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentToward extends Model
{
    //
    protected $table = 'content_towards';
    protected $primaryKey = 'id';
    protected $fillable = [
        'content_app_customer', 'content_app_ship', 'content_app_shop'
    ];
}
