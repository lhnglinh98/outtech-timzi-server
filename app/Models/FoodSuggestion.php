<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodSuggestion extends Model
{
    //
    protected $table = 'food_suggestions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'food_id',
        'food_suggestions_id'
    ];
    public function food()
    {
        return $this->belongsTo(Food::class, 'food_id');
    }
    public function foodSuggestion()
    {
        return $this->belongsTo(Food::class, 'food_suggestions_id');
    }
}
