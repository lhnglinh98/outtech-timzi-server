<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class OrderUserProduct extends Model
{
    protected $table = 'order_user_product';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code',
        'sender_id',
        'sender_name',
        'sender_phone',
        'sender_address',
        'latitude_sender',
        'longtidue_sender',
        'latitude_receiver',
        'longtidue_receiver',
        'receiver_id',
        'receiver_name',
        'receiver_phone',
        'receiver_address',
        'product_name',
        'image_product',
        'shipper_id',
        'image_receive_product',
        'fee_ship',
        'money',
        'total_money',
        'note',
        'status',
        'polyline',
        'staff_timzi_id',
        'money_with_product_weight',
        'from_weight',
        'to_weight',
        'image_send_product',
        'is_two_way',
        'voucher_id',
        'discount_voucher',
        'type_voucher',
        'created_at',
        'updated_at'
    ];
    public function getPolylineAttribute($polyline)
    {
        return json_decode($polyline);
    }
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }
    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }
    public function shipper()
    {
        return $this->belongsTo(User::class, 'shipper_id');
    }
    public function staffTimzi()
    {
        return $this->belongsTo(User::class, 'staff_timzi_id');
    }
    public function voucher()
    {
        return $this->belongsTo(VoucherDelivery::class, 'voucher_id');
    }
    public function getImageProductAttribute($image_product)
    {
        $array = [];
        if($image_product != null){
            $data = json_decode($image_product);
            foreach($data as $item){
                $array[] = asset($item);
            }
        }
        return $array;
    }
    public function getImageReceiveProductAttribute($image_receive_product)
    {
        $array = [];
        if($image_receive_product != null){
            $data = json_decode($image_receive_product);
            foreach($data as $item){
                $array[] = asset($item);
            }
        }
        return $array;
    }
    public function getImageSendProductAttribute($image_send_product)
    {
        $array = [];
        if($image_send_product != null){
            $data = json_decode($image_send_product);
            foreach($data as $item){
                $array[] = asset($item);
            }
        }
        return $array;
    }
    public function getCreatedAtAttribute($created_at)
    {
        if($created_at == null){
            return null;
        }
        return date('Y-m-d H:i:s', strtotime($created_at));
    }
    public function getUpdatedAtAttribute($updated_at)
    {
        if($updated_at == null){
            return null;
        }
        return date('Y-m-d H:i:s', strtotime($updated_at));
    }
}
