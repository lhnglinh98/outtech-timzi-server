<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerBuyMe extends Model
{
    //
    protected $table = 'banner_buy_me';
    protected $primaryKey = 'id';
    protected $fillable = [
        'image'
    ];
    public function getImageAttribute($image)
    {
        if($image == null){
            return asset('/image_default/image_store_default.png');
        }
        return asset($image);
    }
}
