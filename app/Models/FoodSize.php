<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class FoodSize extends Model
{
    //
    protected $table = 'food_size';
    protected $primaryKey = 'id';
    protected $fillable = [
        'food_id',
        'name',
        'price',
        'check'
    ];
    public function food()
    {
        return $this->belongsTo(Food::class, 'food_id');
    }
}
