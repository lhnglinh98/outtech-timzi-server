<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadershipTeam extends Model
{
    //
    protected $table = 'leadership_team';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'image', 'position', 'created_at', 'updated_at', 'status'
    ];
    public function getImageAttribute($image)
    {
        if($image == null){
            return asset('/image_system/avatar_default.jpg');
        }
        return asset($image);
    }
}
