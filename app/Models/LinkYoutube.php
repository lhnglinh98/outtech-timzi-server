<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinkYoutube extends Model
{
    //
    protected $table = 'link_youtube';
    protected $primaryKey = 'id';
    protected $fillable = [
        'link_app_customer', 'link_app_shop'
    ];
}
