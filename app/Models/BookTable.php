<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class BookTable extends Model
{
    //
    protected $table = 'book_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'code',
        'store_id',
        'table_store_id',
        'name',
        'phone',
        'time_booking',
        'number_people',
        'note',
        'coin',
        'total_money',
        'status',
        'right_to_order',
        'payment_method_id',
        'is_shop_book',
        'staff_name',
        'staff_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function staff()
    {
        return $this->belongsTo(User::class, 'staff_id');
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function tableStore()
    {
        return $this->belongsTo(TableStore::class, 'table_store_id');
    }
    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id');
    }
    public function bookFood()
    {
        return $this->HasMany(BookFood::class, 'book_table_id');
    }
    public function tableMerge()
    {
        return $this->belongsToMany(TableStore::class, 'table_merge');
    }
}
