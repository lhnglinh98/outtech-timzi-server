<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SetupPercentFeeStore extends Model
{
    //
    protected $table = 'setup_percent_fee_store';
    protected $primaryKey = 'id';
    protected $fillable = [
        'percent'
    ];
}
