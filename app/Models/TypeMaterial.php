<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeMaterial extends Model
{
    protected $table = 'type_material';
    protected $primaryKey = 'id';
    protected $fillable = [
        'store_id',
        'name',
        'status'
    ];
}
