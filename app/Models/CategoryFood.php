<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class CategoryFood extends Model
{
    //
    protected $table = 'category_food';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'status'
    ];
    public function food()
    {
        return $this->HasMany(Food::class, 'category_food_id');
    }
}
