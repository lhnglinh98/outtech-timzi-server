<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeatureWebsite extends Model
{
    //
    protected $table = 'feature_website';
    protected $primaryKey = 'id';
    protected $fillable = [
        'image', 'title', 'content'
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/image_feature_website_default.png');
        }
        return asset($image);
    }
}
