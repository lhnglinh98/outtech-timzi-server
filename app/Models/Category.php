<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Category extends Model
{
    //
    protected $table = 'category';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'image',
        'banner',
        'background',
        'is_eat_store',
        'status'
    ];
    public function store()
    {
        return $this->belongsToMany(Store::class, 'category_store');
    }
    public function categoryPermanent()
    {
        return $this->HasMany(CategoryPermanent::class, 'category_id');
    }
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/image_store_default.png');
        }
        return asset($image);
    }
    public function getBannerAttribute($banner)
    {
        if ($banner == null) {
            return asset('/image_default/image_store_default.png');
        }
        return asset($banner);
    }
    public function getBackgroundAttribute($background)
    {
        if ($background == null) {
            return asset('/image_default/image_store_default.png');
        }
        return asset($background);
    }
}
