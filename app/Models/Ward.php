<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Ward extends Model
{
    //
    protected $table = 'ward';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'prefix', 'province_id', 'district_id'
    ];
}
