<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherDeliveryUser extends Model
{
    protected $table = 'voucher_delivery_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'voucher_id',
        'quantity',
        'status'
    ];
}
