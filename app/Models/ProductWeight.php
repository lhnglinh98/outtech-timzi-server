<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductWeight extends Model
{
    protected $table = 'product_weight';
    protected $primaryKey = 'id';
    protected $fillable = [
        'from_weight',
        'to_weight',
        'money',
    ];
}
