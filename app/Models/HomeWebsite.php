<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeWebsite extends Model
{
    //
    protected $table = 'home_website';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title', 'content'
    ];
}
