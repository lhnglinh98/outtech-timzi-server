<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use GuzzleHttp\Client;
trait GoogleApi
{

    /**
     * Parse response format
     *
     * @param  array $data
     * @param  string $statusCode
     * @return JsonResponse
     */
    public function googleApi($latitude, $longtidue, $latitude_store, $longtidue_store)
    {
        try {
            // AIzaSyCR2s4FVEi38-3U4az5ykzCl6mWGiBT61w
            // $client = new Client();
            // $data1 = $client->get("https://maps.googleapis.com/maps/api/directions/json?origin=$latitude, $longtidue&destination=$latitude_store, $longtidue_store&key=AIzaSyAgxaD412H4b24diNaw4rmum-qQBnfDxhU");
            // $response = json_decode($data1->getBody()->getContents(), true);
            // return $response['routes'][0]['legs'][0]['distance'];
            $latitude1 = $latitude;
            $longitude1 = $longtidue;
            $latitude2 = $latitude_store;
            $longitude2 = $longtidue_store;

            //Converting to radians
            $longi1 = deg2rad($longitude1);
            $longi2 = deg2rad($longitude2);
            $lati1 = deg2rad($latitude1);
            $lati2 = deg2rad($latitude2);

            //Haversine Formula
            $difflong = $longi2 - $longi1;
            $difflat = $lati2 - $lati1;

            $val = pow(sin($difflat / 2), 2) + cos($lati1) * cos($lati2) * pow(sin($difflong / 2), 2);

            $res1 = 3936 * (2 * asin(sqrt($val))); //for miles
            $res2 = 6378.8 * (2 * asin(sqrt($val))); //for kilometers
            $distance = round($res2 * 1000);
            if ($res2 < 0.1) {
                $data = array(
                    'text' => $distance . ' m',
                    'value' => $distance,
                );
            } else {
                $distance_km = round($res2, 1);
                $data = array(
                    'text' => $distance_km . ' km',
                    'value' => $distance,
                );
            }
            //display distance in miles
            return $data;
        } catch (\Throwable $th) {
            return null;
        }

    }
    public function searchMap($key)
    {
        $client = new Client();
        $data1 = $client->get("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=45 $key&key=AIzaSyCR2s4FVEi38-3U4az5ykzCl6mWGiBT61w&language=vi&components=country:vn");
        $response = json_decode($data1->getBody()->getContents(), true);
        return $response;
    }
}
