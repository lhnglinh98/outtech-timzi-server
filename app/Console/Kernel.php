<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\CheckOrder',
        'App\Console\Commands\CheckComboFood',
        'App\Console\Commands\CheckProgramStore',
        'App\Console\Commands\CheckBookTable',
        'App\Console\Commands\CheckVoucher',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('CheckOrder')
            ->everyMinute()
            // ->withoutOverlapping()
            ->appendOutputTo('/var/www/deploy/log.txt');
        $schedule->command('CheckProgramStore')
            ->hourly()
            ->appendOutputTo('/var/www/deploy/log.txt');
        $schedule->command('CheckComboFood')
            ->hourly()
            ->appendOutputTo('/var/www/deploy/log.txt');
        $schedule->command('CheckBookTable')
            ->everyFiveMinutes()
            ->appendOutputTo('/var/www/deploy/log.txt');
        $schedule->command('CheckVoucher')
            ->everyFiveMinutes()
            ->appendOutputTo('/var/www/deploy/log.txt');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
