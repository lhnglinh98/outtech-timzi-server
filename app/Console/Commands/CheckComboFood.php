<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ComboFood;
class CheckComboFood extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:combo_food';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date_check = date('Y-m-d', time());
        $check_order_food_open = ComboFood::where([['status', 0], ['time_open', '<=', $date_check], ['is_active', 1]])->update([
            'status' => 1,
            'updated_at' => date('Y-m-d H:i:s', time()),
        ]);
        $date_check_close = date('Y-m-d', time());
        $check_order_food = ComboFood::where([['status', 1], ['time_close', '<', $date_check]])->update([
            'status' => 2,
            'updated_at' => date('Y-m-d H:i:s', time()),
        ]);
        $this->info('Cron check commissions success!');
    }
}
