<?php

namespace App\Console\Commands;

use App\Models\Food;
use App\Models\Program;
use App\Models\ProgramDetail;
use App\Models\ProgramStore;
use Illuminate\Console\Command;

class CheckProgramStore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:program_store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date_check = date('Y-m-d', time());
        $check_program_open = Program::where([['status', 0], ['time_open', '<=', $date_check]])->update([
            'status' => 1,
            'updated_at' => date('Y-m-d H:i:s', time()),
        ]);
        $check_program_close = Program::where([['status', 1], ['time_close', '<', $date_check]])->update([
            'status' => 2,
            'updated_at' => date('Y-m-d H:i:s', time()),
        ]);
        $ls_store_id = ProgramDetail::Join('program as p', 'p.id', 'program_detail.program_id')
            ->where('p.status', 1)->pluck('program_detail.store_id')->toArray();
        $check_program_store_open = ProgramStore::where([['status', 0], ['time_open', '<=', $date_check], ['is_active', 1]])->update([
            'status' => 1,
            'updated_at' => date('Y-m-d H:i:s', time()),
        ]);
        $date_check_close = date('Y-m-d', time());
        $check_program_store_close = ProgramStore::where([['status', 1], ['time_close', '<', $date_check]])->update([
            'status' => 2,
            'updated_at' => date('Y-m-d H:i:s', time()),
        ]);
        $check_program_store_close = ProgramStore::where([['status', 1], ['quantity', '<=', 0]])->update([
            'status' => 2,
            'updated_at' => date('Y-m-d H:i:s', time()),
        ]);
        $data_stop = ProgramStore::where([['status', 3], ['is_active', 1]])->with('food')->get();
        foreach ($data_stop as $c1) {
            if (count($c1->food) > 0) {
                foreach ($c1->food as $f3) {
                    $update = Food::where('id', $f3->id)->update([
                        'price_discount_with_program' => 0,
                    ]);
                }
            }
        }
        // $data_stop1 = ProgramStore::where([['status', 1], ['is_active', 0]])->with('food')->get();
        // foreach ($data_stop1 as $c2) {
        //     if (count($c2->food) > 0) {
        //         foreach ($c2->food as $f4) {
        //             $update = Food::where('id', $f4->id)->update([
        //                 'price_discount_with_program' => 0,
        //             ]);
        //         }
        //     }
        // }
        $ls_program = ProgramDetail::select('program_detail.*')->Join('program as p', 'p.id', 'program_detail.program_id')
            ->where('p.status', 1)->get();
        foreach ($ls_program as $p) {
            $program = Program::where('id', $p->program_id)->with('store', 'categoryFood')->first();
            if ($program) {
                $food = Food::where([['store_id', $p->store_id], ['category_food_id', $program->category_food_id]])->get();
                foreach ($food as $f) {
                    if ($program->type_percent_or_money == 1) {
                        $price = $f->price - $f->price * $program->percent / 100;
                        if ($f->price_discount > 0) {
                            $price = $f->price_discount - $f->price_discount * $program->percent / 100;
                        }
                    } else {
                        $price = $f->price - $program->money;
                        if ($f->price_discount > 0) {
                            $price = $f->price_discount - $program->money;
                        }
                    }
                    $update = Food::where('id', $f->id)->update([
                        'price_discount_with_program' => $price,
                    ]);
                }
            }
        }
        $data_open = ProgramStore::where([['status', 1], ['is_active', 1]])->whereNotIn('store_id', $ls_store_id)->with('food')->get();
        foreach ($data_open as $d) {
            if (count($d->food) > 0) {
                foreach ($d->food as $f) {
                    if ($d->type_percent_or_money == 1) {
                        $price = $f->price - $f->price * $d->percent / 100;
                        if ($f->price_discount > 0) {
                            $price = $f->price_discount - $f->price_discount * $d->percent / 100;
                        }
                    } else {
                        $price = $f->price - $d->money;
                        if ($f->price_discount > 0) {
                            $price = $f->price_discount - $d->money;
                        }
                    }
                    $this->info('id: ' . $f->id . ' price: ' . $price);
                    $update = Food::where('id', $f->id)->update([
                        'price_discount_with_program' => $price,
                    ]);
                }
            }
        }
        
        $data_close = ProgramStore::where([['status', 2], ['check_close', 0], ['is_active', 1]])->whereNotIn('store_id', $ls_store_id)->with('food')->get();
        foreach ($data_close as $c) {
            if (count($c->food) > 0) {
                foreach ($c->food as $f2) {
                    $update = Food::where('id', $f2->id)->update([
                        'price_discount_with_program' => 0,
                    ]);
                }
                ProgramStore::where('id', $c->id)->update([
                    'check_close' => 1,
                ]);
            }
        }
        $ls_store_id_close = ProgramDetail::Join('program as p', 'p.id', 'program_detail.program_id')
            ->where([['p.status', 2], ['p.check_close', 0]])->pluck('program_detail.store_id')->toArray();

        $update = ProgramStore::where([['status', 3], ['time_open', '<=', $date_check], ['time_close', '>=', $date_check], ['is_active', 1]])
            ->whereIn('store_id', $ls_store_id_close)
            ->update([
                'status' => 1,
            ]);
        $update = ProgramStore::where([['status', 3], ['time_close', '<', $date_check], ['is_active', 1]])
            ->whereIn('store_id', $ls_store_id_close)
            ->update([
                'status' => 2,
            ]);
        Program::where([['status', 2], ['check_close', 0], ['is_active', 1]])->update([
            'check_close' => 1,
        ]);
        $this->info('Cron check commissions success!');
    }
}
