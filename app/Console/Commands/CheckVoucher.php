<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Voucher;
use App\Models\VoucherDelivery;
use App\Models\VoucherUser;
class CheckVoucher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:voucher';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date_time = date('Y-m-d', time());
        $check_voucher_open = Voucher::where([['status', 1], ['time_open', '<=', $date_time]])->update([
            'status' => 2
        ]);
        $check_voucher_close = Voucher::where([['status', 2], ['time_close', '<', $date_time]])->update([
            'status' => 3
        ]);
        $check_voucher_delivery_open = VoucherDelivery::where([['status', 1], ['type', 1], ['time_open', '<=', $date_time]])->update([
            'status' => 2
        ]);
        $check_voucher_delivery_close = VoucherDelivery::where([['status', 2], ['type', 1], ['time_close', '<', $date_time]])->update([
            'status' => 3
        ]);
        $list_id_voucher_close = Voucher::where([['status', 3]])->pluck('id')->toArray();
        $delere = VoucherUser::whereIn('voucher_id', $list_id_voucher_close)->delete();
        $this->info('Cron check commissions success!');
    }
}
