<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Utils\SystemParam;
use App\Models\BookFood;
use App\Models\BookFoodTopping;
use App\Models\BookTable;
use App\Models\TableStore;
use App\Models\TableMerge;
use App\Models\Notify;
use App\User;
use GuzzleHttp\Client;
class CheckBookTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:book_table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info(date('Y-m-d H:i:s', time() - 15 * 60));
        $check_book_table = BookTable::where([['status', SystemParam::status_booking_table_confirmed], ['right_to_order', 0], ['time_booking', '<', date('Y-m-d H:i:s', time() - 15 * 60)]])->with('tableStore')->get();
        foreach($check_book_table as $c){
            $c->update([
                'status' => SystemParam::status_booking_table_time_out_with_customer
            ]);
            $this->pushNotify([$c->user_id], 'Lần đặt bàn có mã ' . $c->code . ' đã bị hủy vì thời gian đặt bàn của bạn đã hết hạn', $c->id, SystemParam::type_order, $c);
            $update_table = TableStore::where('id', $c->table_store_id)->update([
                'status' => SystemParam::status_table_store_empty
            ]);
        }
        $check_user = User::where([['status', 0]])->get();
        foreach($check_user as $c){
            if ($c->date_check_code == null || strtotime($c->date_check_code) + 20 * 60 < time()) {
                User::where('id', $c->id)->delete();
            }
        }
        $array_status = [SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment];
        $check = BookTable::whereIn('status', $array_status)->where('time_booking', '<', date('Y-m-d H:i:s', time() - 12 * 60 *60))->get();
        foreach($check as $c1){
            BookTable::where('id', $c1->id)->update([
                'status' => SystemParam::status_booking_table_time_out_with_customer
            ]);
            TableStore::where('id', $c1->table_store_id)->update([
                'status' => SystemParam::status_table_store_empty
            ]);
            $check_table_merge = TableMerge::where('book_table_id', $c1->id)->pluck('table_store_id')->toArray();
            TableStore::whereIn('id', $check_table_merge)->update([
                'status' => SystemParam::status_table_store_empty
            ]);
        }
        // $check = BookTable::whereIn('status', $array_status)->where('time_booking', '<', date('Y-m-d H:i:s', time() - 12 * 60 *60))->update([
        //     'status' => SystemParam::status_booking_table_time_out_with_customer
        // ]);
        $this->info('Cron check commissions success! book table');
    }
    public function pushNotify($user_id, $content, $object_id, $type, $data)
    {
        try {
            $ls_device_id = [];
            $date_now = date('Y-m-d H:i:s', time());
            $ls_noti = [];
            $role_id = null;
            foreach ($user_id as $id) {
                if ($user = User::find($id)) {
                    $role_id = $user->role_id;
                    $noti_id = Notify::insertGetId([
                        'user_id' => $id,
                        'device_id' => $user->device_id,
                        'created_at' => $date_now,
                        'content' => $content,
                        'type' => $type,
                        'object_id' => $object_id,
                        'is_view' => 0,
                    ]);
                    if ($user->device_id != null && $user->device_id != "undefined" && $user->device_id != "null") {
                        if($type == 3){
                            $book_table = BookTable::where('id', $object_id)->first();
                            if($book_table){
                                $ls_noti[] = array(
                                    'notify_id' => $noti_id,
                                    'user_id' => $id,
                                    'type' => $type,
                                    'object_id' => $object_id,
                                    'store_id' => $book_table->store_id
                                );
                            }else{
                                $ls_noti[] = array(
                                    'notify_id' => $noti_id,
                                    'user_id' => $id,
                                    'type' => $type,
                                    'object_id' => $object_id,
                                    'store_id' => null
                                );
                            }
                        }else{
                            $ls_noti[] = array(
                                'notify_id' => $noti_id,
                                'user_id' => $id,
                                'type' => $type,
                                'object_id' => $object_id,
                                'store_id' => null
                            );
                        }
                        $ls_device_id[] = $user->device_id;
                    }
                }
            }
            if ($ls_device_id != null && count($ls_device_id) > 0) {
                $contents = array(
                    "en" => $content,
                );
                // $data2 = array(
                //     "foo" => $data
                // );
                $headings = array(
                    "en" => "Bạn có thông báo mới",
                );
                $array = array_chunk($ls_device_id, 99);
                $array2 = array_chunk($ls_noti, 99);
                $dem = 0;
                if($role_id == 5){
                    $app_id = "ddeba80d-f236-451c-9929-a62e2cc9d922";
                    $android_channel_id = "8a36af32-022e-48c1-b828-7335a5da5f9d";
                    $REST_API_KEY = "Y2FjY2MzNTItZDkxZC00ZDE2LTg0NjctYTgzYmJkOTQzOTg2";
                }else if ($role_id == 4) {
                    $app_id = "2b125f9c-7a1c-42bb-87a1-82bf757c6403";
                    if ($type == SystemParam::type_order_with_shipper) {
                        $android_channel_id = "cd1bea2b-937c-4e86-9a7a-e6929cec1760";
                    } else {
                        $android_channel_id = "4193d919-2ce7-4fc2-9a7d-db48f3d75fbf";
                    }
                    $REST_API_KEY = "MmM5ZjIyYWYtYTA0Zi00MTM1LWI3ODQtNTA1YmQ5MWFjZTcw";
                }else if ($role_id == 2 || $role_id == 3 || $role_id == 6) {
                    $app_id = "570a33bf-ddd5-414e-9470-a027dfd28faa";
                    if ($type == SystemParam::type_order_with_store) {
                        $android_channel_id = "b7d593c6-c8ca-4729-a049-971bba7f1a89";
                    } else {
                        $android_channel_id = "7a720715-b986-4b20-87ca-841e72876864";
                    }
                    $REST_API_KEY = "MjM0YTllMmMtN2ZlYy00ZjUxLWE4NmItMjNmNTY1ODM4YjA1";
                }else {
                    $app_id = "389afe0d-332d-4727-84b9-b30d7f2eb0f8";
                    $android_channel_id = "7a720715-b986-4b20-87ca-841e72876864";
                    $REST_API_KEY = "ODY4YTc4MGYtMzU5MC00MTliLTliMmUtYjg4NzMxYzA1M2Q2";
                }
                foreach ($array as $device_id) {
                    $check_d = array(
                        'ls_notify_id' => $array2[$dem],
                    );
                    $data3 = (object) array_merge(
                        (array) $data,
                        (array) $check_d
                    );
                    $data2 = array(
                        "foo" => $data3,
                    );

                    if ($type == SystemParam::type_order_with_shipper) {
                        $body = [
                            "app_id" => $app_id,
                            "contents" => $contents,
                            "headings" => $headings,
                            "data" => $data2,
                            "ios_badgeType" => "None",
                            "large_icon" => "ic_stat_onesignal_default",
                            "android_accent_color" => "#E9333A",
                            "ios_sound" => "noti_default3.wav",
                            "android_channel_id" => $android_channel_id,
                            "include_player_ids" => $device_id,
                        ];
                    } else {
                        $body = [
                            "app_id" => $app_id,
                            "contents" => $contents,
                            "headings" => $headings,
                            "data" => $data2,
                            "ios_badgeType" => "None",
                            "large_icon" => "ic_stat_onesignal_default",
                            "android_accent_color" => "#E9333A",
                            "android_channel_id" => $android_channel_id,
                            "include_player_ids" => $device_id,
                        ];
                    }
                    $data1 = json_encode($body);
                    $url = 'https://onesignal.com/api/v1/notifications';
                    $client = new Client([
                        'headers' => [
                            'cache-control' => 'no-cache',
                            'Connection' => 'keep-alive',
                            'Cookie' => '__cfduid=d1725b36e818f28c2b8eb54dbdd780f2e1573645836',
                            'Content-Length' => '219',
                            'Accept-Encoding' => 'gzip, deflate',
                            'Host' => 'onesignal.com',
                            'Postman-Token' => 'c142bc3e-ae52-4b01-8818-ced791f4e659,807c2c04-e012-43fa-afd0-60814633502d',
                            'Cache-Control' => 'no-cache',
                            'Accept' => '*/*',
                            'User-Agent' => 'PostmanRuntime/7.19.0',
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Basic :' . $REST_API_KEY,
                            'https' => '//onesignal.com/api/v1/notifications',
                        ],
                    ]);
                    $req = $client->post(
                        $url,
                        ['body' => $data1]
                    );
                    $dem++;
                }
            }
        } catch (\Exception $ex) {
            return 0;
        }
    }
}
