<?php

namespace App\Console\Commands;

use App\Http\Utils\SystemParam;
use App\Models\Notify;
use App\Models\OrderFood;
use App\Models\OrderFoodDetail;
use App\Models\ProgramStoreDetail;
use App\Models\OrderUserProduct;
use App\Models\ProgramStore;
use App\Models\ComboFood;
use App\Models\Radius;
use App\Models\RevenueStore;
use App\Models\ShipperStore;
use App\Models\BookTable;
use App\Models\Store;
use App\Models\UserStore;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use App\Traits\GoogleApi;
class CheckOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date_check = date('Y-m-d H:i', time() + 30 * 60);
        $date_check_push_store = date('Y-m-d H:i', time() + 45 * 60);
        $this->info($date_check);
        $id_order_food_push_store = OrderFood::where([['status', 0], ['date_time', '<=', $date_check_push_store], ['is_push_store', 0]])->pluck('id')->toArray();
        $check_order_food_push_store = OrderFood::whereIn('id', $id_order_food_push_store)->update([
            'is_push_store' => 1
        ]);
        $ls_order_food_push_store = OrderFood::whereIn('id', $id_order_food_push_store)->with('user', 'store')->get();
        foreach($ls_order_food_push_store as $l2){
            $id2 = UserStore::where([['status', 1], ['store_id', $l2->store_id]])->pluck('user_id')->toArray();
            $id2 = [$l2->store->user_id];
            $ls_shipper_id = ShipperStore::where([['store_id', $l2->store_id], ['status', 1]])->pluck('user_id')->toArray();
            $this->pushNotify($id2, 'Khách hàng ' . $l2->user->name . ' đã đặt 1 đơn hàng mới', $l2->id, SystemParam::type_order_with_store, $l2, $l2->user->name);
        }
        $id_order_food = OrderFood::where([['status', 0], ['date_time', '<=', $date_check]])->pluck('id')->toArray();
        $check_order_food = OrderFood::whereIn('id', $id_order_food)->update([
            'status' => 2,
            'updated_at' => date('Y-m-d H:i:s', time()),
        ]);
        $ls_order_food = OrderFood::whereIn('id', $id_order_food)->with('user', 'store')->get();
        foreach($ls_order_food as $l){
            $id = UserStore::where([['status', 1], ['store_id', $l->store_id]])->pluck('user_id')->toArray();
            $id = [$l->store->user_id];
            $ls_shipper_id = ShipperStore::where([['store_id', $l->store_id], ['status', 1]])->pluck('user_id')->toArray();
            $this->pushNotify($ls_shipper_id, 'Có 1 đơn hàng mới của cửa hàng tên ' . $l->store->name . ' cần bạn giao đến khách hàng ' . $l->user->name, $l->id, SystemParam::type_order_with_shipper, $l, $l->user->name);
            $this->pushNotify($id, 'Khách hàng ' . $l->user->name . ' đã đặt 1 đơn hàng mới', $l->id, SystemParam::type_order_with_store, $l, $l->user->name);
        }
        // $list_wait_shop_confirm = OrderFood::where([['status', 1], ['date_time', '<', date('Y-m-d H:i:s', time() - 30 * 60)]])->get();
        // foreach ($list_wait_shop_confirm as $l) {
        //     $this->pushNotify([$l->user_id], 'Đơn hàng có mã ' . $l->code . ' của bạn đã bị hủy vì cửa hàng không xác nhận', $l->id, SystemParam::type_order, $l);
        // }
        $list_id_time_out = OrderFood::where([['status', SystemParam::status_order_confirmed], ['date_time', '<', date('Y-m-d H:i:s', time() - 40 * 60)]])->pluck('user_id')->toArray();
        // foreach($list_id_time_out as $lt){
            $check_order_detail = OrderFoodDetail::whereIn('order_food_id', $list_id_time_out)->get();
                foreach ($check_order_detail as $c2) {
                    if ($c2->food_id) {
                        $ls_program = ProgramStoreDetail::where('food_id', $c2->food_id)->pluck('program_store_id')->toArray();
                        $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                        if (count($check_program) > 0) {
                            foreach ($check_program as $p) {
                                ProgramStore::where('id', $p->id)->update([
                                    'quantity' => $p->quantity + $c2->quantity,
                                ]);
                            }
                        }
                    }else if ($c2->combo_food_id) {
                        $check_combo = ComboFood::find($c2->combo_food_id);
                        if ($check_combo) {
                            $check_combo->update([
                                'quantity' => $check_combo->quantity + $c2->quantity,
                            ]);
                        }
                    }
                }
        // }
        $update_list_wait_shop_confirm = OrderFood::whereIn('id', $list_id_time_out)->update([
            'status' => SystemParam::status_order_time_out_with_shipper,
        ]);
        $update_list_wait_shop_confirm = OrderFood::where([['created_at', '<', date('Y-m-d H:i:s', time() - 24 * 60 * 60)]])
                                        ->whereNotIn('status', [SystemParam::status_order_finish, SystemParam::status_order_store_cancel, SystemParam::status_order_customer_cancel, SystemParam::status_order_time_out_with_store, SystemParam::status_order_shipper_cancel, SystemParam::status_order_time_out_with_shipper, SystemParam::status_order_time_out_with_24h])
                                        ->update([
                                            'status' => SystemParam::status_order_time_out_with_24h,
                                        ]);
        $list_wait_shipper = OrderFood::where([['status', 2], ['time_check', '<', date('Y-m-d H:i:s', time() - 15 * 60)]])->with('store', 'user')->get();
        foreach ($list_wait_shipper as $s) {
            $check_shipper_store = ShipperStore::where([['store_id', $s->id], ['status', 1]])->pluck('user_id')->toArray();
            $list_shipper = User::whereNotIn('id', $check_shipper_store)->where([['role_id', 4], ['status', 1]])->get();
            $data = [];
            if ($s->quantity_push < 3) {
                $check_radius1 = Radius::where('id', $s->quantity_push)->first();
                $check_radius2 = Radius::where('id', $s->quantity_push + 1)->first();
                $latitude_store = $s->store->latitude;
                $longtidue_store = $s->store->longtidue;
                foreach ($list_shipper as $ls) {
                    $client = new Client();
                    $data1 = $this->googleApi($ls->latitude, $ls->longtidue, $latitude_store, $longtidue_store);
                    if (isset($data1)) {
                        $distance_value = $data1['value'] / 1000;
                        if ($check_radius1) {
                            if ($distance_value > $check_radius1->radius && $distance_value <= $check_radius2->radius) {
                                $data[] = $ls->id;
                            }
                        } else {
                            if ($distance_value <= $check_radius2->radius) {
                                $data[] = $ls->id;
                            }
                        }
                    }
                }
                $update_wait_shipper = OrderFood::where('id', $s->id)->update([
                    'time_check' => date('Y-m-d H"i"s', time()),
                    'quantity_push' => $s->quantity_push + 1,
                ]);
                $this->pushNotify($data, 'Có 1 đơn hàng mới của cửa hàng tên ' . $s->store->name . ' cần bạn giao đến khách hàng ' . $s->user->name, $s->id, SystemParam::type_order_with_shipper, $s, $s->user->name);
            } else {
                $check_order_detail = OrderFoodDetail::where('order_food_id', $s->id)->get();
                foreach ($check_order_detail as $c1) {
                    if ($c1->food_id) {
                        $ls_program = ProgramStoreDetail::where('food_id', $c1->food_id)->pluck('program_store_id')->toArray();
                        $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                        if (count($check_program) > 0) {
                            foreach ($check_program as $p) {
                                ProgramStore::where('id', $p->id)->update([
                                    'quantity' => $p->quantity + $c1->quantity,
                                ]);
                            }
                        }
                    }else if ($c1->combo_food_id) {
                        $check_combo = ComboFood::find($c1->combo_food_id);
                        if ($check_combo) {
                            $check_combo->update([
                                'quantity' => $check_combo->quantity + $c1->quantity,
                            ]);
                        }
                    }
                }
                $update = OrderFood::where('id', $s->id)->update([
                    'status' => SystemParam::status_order_time_out_with_shipper,
                ]);
                $this->pushNotify([$s->store->user_id], 'Đơn hàng ' . $s->code . ' không tìm thấy người giao hàng', $s->id, SystemParam::type_order, $s, 'Timzi');
            }
        }
        $list_wait_shipper_order_user_product = OrderUserProduct::where([['status', SystemParam::status_order_user_product_pending], ['time_check', '<', date('Y-m-d H:i:s', time() - 15 * 60)]])->get();
        foreach ($list_wait_shipper_order_user_product as $s2) {
            $list_shipper = User::where([['role_id', 4], ['status', 1]])->get();
            $data2 = [];
            if ($s->quantity_push < 3) {
                $check_radius1 = Radius::where('id', $s2->quantity_push)->first();
                $check_radius2 = Radius::where('id', $s2->quantity_push + 1)->first();
                $latitude_store = $s2->latitude_sender;
                $longtidue_store = $s2->longtidue_sender;
                foreach ($list_shipper as $ls1) {
                    // $client = new Client();
                    $data3 = $this->googleApi($ls1->latitude, $ls1->longtidue, $latitude_store, $longtidue_store);
                    if (isset($data3)) {
                        $distance_value = $data3['value'] / 1000;
                        if ($check_radius1) {
                            if ($distance_value > $check_radius1->radius && $distance_value <= $check_radius2->radius) {
                                $data2[] = $ls1->id;
                            }
                        } else {
                            if ($distance_value <= $check_radius2->radius) {
                                $data2[] = $ls1->id;
                            }
                        }
                    }
                }
                OrderUserProduct::where('id', $s2->id)->update([
                    'time_check' => date('Y-m-d H"i"s', time()),
                    'quantity_push' => $s2->quantity_push + 1,
                ]);
                $this->pushNotify($data2, 'Khách hàng tên ' . $s2->sender_name . ' cần bạn giao hàng đến khách hàng ' . $s2->receiver_name, $s2->id, SystemParam::type_order_user_product_with_shipper, $s2, $s2->sender_name);
            } else {
                // $check_order_detail = OrderFoodDetail::where('order_food_id', $s->id)->get();
                // foreach ($check_order_detail as $c1) {
                //     if ($c1->food_id) {
                //         $ls_program = ProgramStoreDetail::where('food_id', $c1->food_id)->pluck('program_store_id')->toArray();
                //         $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                //         if (count($check_program) > 0) {
                //             foreach ($check_program as $p) {
                //                 ProgramStore::where('id', $p->id)->update([
                //                     'quantity' => $p->quantity + $c1->quantity,
                //                 ]);
                //             }
                //         }
                //     }else if ($c1->combo_food_id) {
                //         $check_combo = ComboFood::find($c1->combo_food_id);
                //         if ($check_combo) {
                //             $check_combo->update([
                //                 'quantity' => $check_combo->quantity + $c1->quantity,
                //             ]);
                //         }
                //     }
                // }
                OrderUserProduct::where('id', $s2->id)->update([
                    'status' => SystemParam::status_order_user_product_time_out_shipper,
                ]);
                $this->pushNotify([$s2->sender_id], 'Đơn hàng giao hộ có mã ' . $s2->code . ' của bạn không tìm thấy người giao hàng.', $s2->id, SystemParam::type_order_user_product_with_shipper, $s2, 'Timzi');
                // $this->pushNotify([$s->store->user_id], 'Đơn hàng ' . $s->code . ' không tìm thấy người giao hàng', $s->id, SystemParam::type_order, $s);
            }
        }
        $date_check = date('Y-m-d', time());
        $date_month_front = strtotime(date("Y-m-d", strtotime($date_check)) . " -1 month");
        $date_month_front = date("Y-m", $date_month_front);
        // $this->info($date_month_front);
        $date_check_revenue = date('Y-m-d', time());
        $day_now = date('d', time());
        // $this->info($day_now);
        if ($day_now == 1) {
            $data_revenue = RevenueStore::where([['status', 0], ['date_check', '!=', $date_check]])->orWhere([['status', 0], ['date_check', null]])->get();
            // $this->info($data_revenue);
            foreach ($data_revenue as $d) {
                $month = date('Y-m', strtotime($d->month));
                if ($month == $date_month_front) {
                    // $this->info($d->id);
                    RevenueStore::where('id', $d->id)->update([
                        'status' => 2,
                        'date_check' => $date_check
                    ]);
                    $check_store_revenue = Store::find($d->store_id);
                    if($check_store_revenue){
                        $id2 = UserStore::where([['status', 1], ['store_id', $d->store_id], ['is_owner', 1]])->pluck('user_id')->toArray();
                        if (count($id2) > 0) {
                            $id2[] = $check_store_revenue->user_id;
                        }else{
                            $id2 = [$check_store_revenue->user_id];
                        }
                    }

                    $this->pushNotify($id2, 'Cửa hàng ' . $check_store_revenue->name . ' của bạn đã đến hạn đóng phí chiết khấu.', $d->store_id, SystemParam::type_revenue_store, $check_store_revenue, 'Timzi');
                }
            }
        }
        if ($day_now > 1) {
            $data_revenue = RevenueStore::where([['status', 2], ['date_check', '!=', $date_check]])->orWhere([['status', 2], ['date_check', null]])->orwhere([['status', 3], ['date_check', '!=', $date_check]])->get();
            // $this->info($data_revenue);
            foreach ($data_revenue as $d) {
                $month = date('Y-m', strtotime($d->month));
                if ($month == $date_month_front) {
                    // $this->info($d->id);
                    RevenueStore::where('id', $d->id)->update([
                        'status' => 3,
                        'late_day' => $d->late_day + 1,
                        'date_check' => $date_check
                    ]);
                    $check_store_revenue = Store::find($d->store_id);
                    if($check_store_revenue){
                        $id2 = UserStore::where([['status', 1], ['store_id', $d->store_id], ['is_owner', 1]])->pluck('user_id')->toArray();
                        if (count($id2) > 0) {
                            $id2[] = $check_store_revenue->user_id;
                        }else{
                            $id2 = [$check_store_revenue->user_id];
                        }
                    }
                    $this->pushNotify($id2, 'Cửa hàng ' . $check_store_revenue->name . ' của bạn đã chậm đóng phí chiết khấu ' . ($d->late_day + 1) . ' ngày.', $d->store_id, SystemParam::type_revenue_store, $check_store_revenue, 'Timzi');
                }
            }
            $data_revenue_wait = RevenueStore::where([['status', 0], ['date_check', '!=', $date_check]])->orWhere([['status', 0], ['date_check', null]])->get();
            foreach ($data_revenue_wait as $d2) {
                $month2 = date('Y-m', strtotime($d2->month));
                $date_month_back = strtotime(date("Y-m", strtotime($date_check)) . " +1 month");
                $date_month_back = date("Y-m-d", $date_month_back);
                $date_month_back_minus_3 = strtotime(date("Y-m-d", strtotime($date_month_back)) . " -3 day");
                $date_month_back_minus_3 = date("Y-m-d", $date_month_back_minus_3);
                if ($date_check == $date_month_back_minus_3) {
                    RevenueStore::where('id', $d->id)->update([
                        'date_check' => $date_check
                    ]);
                    $id = UserStore::where([['status', 1], ['is_owner', 1], ['store_id', $d->store_id]])->pluck('user_id')->toArray();
                    $check_store = Store::where('id', $d->store_id)->first();
                    if ($check_store) {
                        $id[] = $check_store->user_id;
                        $this->pushNotify($id, 'Cửa hàng ' . $check_store->name . ' của bạn sắp đến hạn đóng phí chiết khấu.', $d->store_id, SystemParam::type_revenue_store, $check_store, 'Timzi');
                    }
                }
            }
        }
        $this->info('Cron check commissions success!');
    }
    public function pushNotify($user_id, $content, $object_id, $type, $data, $title)
    {
        try {
            $ls_device_id = [];
            $date_now = date('Y-m-d H:i:s', time());
            $ls_noti = [];
            $role_id = null;
            foreach ($user_id as $id) {
                if ($user = User::find($id)) {
                    $role_id = $user->role_id;
                    $noti_id = Notify::insertGetId([
                        'user_id' => $id,
                        'device_id' => $user->device_id,
                        'created_at' => $date_now,
                        'content' => $content,
                        'type' => $type,
                        'object_id' => $object_id,
                        'is_view' => 0,
                    ]);
                    if ($user->device_id != null && $user->device_id != "undefined" && $user->device_id != "null") {
                        if($type == 3){
                            $book_table = BookTable::where('id', $object_id)->first();
                            if($book_table){
                                $ls_noti[] = array(
                                    'notify_id' => $noti_id,
                                    'user_id' => $id,
                                    'type' => $type,
                                    'object_id' => $object_id,
                                    'store_id' => $book_table->store_id
                                );
                            }else{
                                $ls_noti[] = array(
                                    'notify_id' => $noti_id,
                                    'user_id' => $id,
                                    'type' => $type,
                                    'object_id' => $object_id,
                                    'store_id' => null
                                );
                            }
                        }else{
                            $ls_noti[] = array(
                                'notify_id' => $noti_id,
                                'user_id' => $id,
                                'type' => $type,
                                'object_id' => $object_id,
                                'store_id' => null
                            );
                        }
                        $ls_device_id[] = $user->device_id;
                    }
                }
            }
            if ($ls_device_id != null && count($ls_device_id) > 0) {
                $contents = array(
                    "en" => $content,
                );
                // $data2 = array(
                //     "foo" => $data
                // );
                $headings = array(
                    "en" => $title,
                );
                $array = array_chunk($ls_device_id, 99);
                $array2 = array_chunk($ls_noti, 99);
                $dem = 0;
                if($role_id == 5){
                    $app_id = "ddeba80d-f236-451c-9929-a62e2cc9d922";
                    $android_channel_id = "8a36af32-022e-48c1-b828-7335a5da5f9d";
                    $REST_API_KEY = "Y2FjY2MzNTItZDkxZC00ZDE2LTg0NjctYTgzYmJkOTQzOTg2";
                }else if ($role_id == 4) {
                    $app_id = "2b125f9c-7a1c-42bb-87a1-82bf757c6403";
                    if ($type == SystemParam::type_order_with_shipper) {
                        $android_channel_id = "cd1bea2b-937c-4e86-9a7a-e6929cec1760";
                    } else {
                        $android_channel_id = "4193d919-2ce7-4fc2-9a7d-db48f3d75fbf";
                    }
                    $REST_API_KEY = "MmM5ZjIyYWYtYTA0Zi00MTM1LWI3ODQtNTA1YmQ5MWFjZTcw";
                }else if ($role_id == 2 || $role_id == 3 || $role_id == 6) {
                    $app_id = "570a33bf-ddd5-414e-9470-a027dfd28faa";
                    if ($type == SystemParam::type_order_with_store) {
                        $android_channel_id = "b7d593c6-c8ca-4729-a049-971bba7f1a89";
                    } else {
                        $android_channel_id = "7a720715-b986-4b20-87ca-841e72876864";
                    }
                    $REST_API_KEY = "MjM0YTllMmMtN2ZlYy00ZjUxLWE4NmItMjNmNTY1ODM4YjA1";
                }else {
                    $app_id = "389afe0d-332d-4727-84b9-b30d7f2eb0f8";
                    $android_channel_id = "7a720715-b986-4b20-87ca-841e72876864";
                    $REST_API_KEY = "ODY4YTc4MGYtMzU5MC00MTliLTliMmUtYjg4NzMxYzA1M2Q2";
                }
                foreach ($array as $device_id) {
                    $check_d = array(
                        'ls_notify_id' => $array2[$dem],
                    );
                    $data3 = (object) array_merge(
                        (array) $data,
                        (array) $check_d
                    );
                    $data2 = array(
                        "foo" => $data3,
                    );
                    if ($type == SystemParam::type_order_with_shipper) {
                        $body = [
                            "app_id" => $app_id,
                            "contents" => $contents,
                            "headings" => $headings,
                            "data" => $data2,
                            "ios_badgeType" => "None",
                            "large_icon" => "ic_stat_onesignal_default",
                            "android_accent_color" => "#E9333A",
                            "ios_sound" => "noti_default3.wav",
                            "android_channel_id" => $android_channel_id,
                            "include_player_ids" => $device_id,
                        ];
                    } else {
                        $body = [
                            "app_id" => $app_id,
                            "contents" => $contents,
                            "headings" => $headings,
                            "data" => $data2,
                            "ios_badgeType" => "None",
                            "large_icon" => "ic_stat_onesignal_default",
                            "android_accent_color" => "#E9333A",
                            "android_channel_id" => $android_channel_id,
                            "include_player_ids" => $device_id,
                        ];
                    }
                    $data1 = json_encode($body);
                    $url = 'https://onesignal.com/api/v1/notifications';
                    $client = new Client([
                        'headers' => [
                            'cache-control' => 'no-cache',
                            'Connection' => 'keep-alive',
                            'Cookie' => '__cfduid=d1725b36e818f28c2b8eb54dbdd780f2e1573645836',
                            'Content-Length' => '219',
                            'Accept-Encoding' => 'gzip, deflate',
                            'Host' => 'onesignal.com',
                            'Postman-Token' => 'c142bc3e-ae52-4b01-8818-ced791f4e659,807c2c04-e012-43fa-afd0-60814633502d',
                            'Cache-Control' => 'no-cache',
                            'Accept' => '*/*',
                            'User-Agent' => 'PostmanRuntime/7.19.0',
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Basic :' . $REST_API_KEY,
                            'https' => '//onesignal.com/api/v1/notifications',
                        ],
                    ]);
                    $req = $client->post(
                        $url,
                        ['body' => $data1]
                    );
                    $dem++;
                }
            }
        } catch (\Exception $ex) {
            return 0;
        }
    }
}
