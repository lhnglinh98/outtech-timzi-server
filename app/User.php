<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Role;
use App\Models\Voucher;
use App\Models\AddressUser;
use App\Models\Province;
use App\Models\District;
use App\Models\Store;
use Illuminate\Support\Facades\Hash;
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'email',
        'phone',
        'password',
        'date_check_code',
        'otp',
        'role_id',
        'device_id',
        'avatar',
        'address',
        'birthday',
        'gender',
        'coin',
        'is_notify',
        'is_main_shipper',
        'career',
        'driver_license_image',
        'vehicle_registration_image',
        'image_cmnd_front',
        'image_cmnd_back',
        'status_shipper',
        'status_staff',
        'status_system_owner',
        'status_owner',
        'latitude',
        'longtidue',
        'status',
        'money',
        'province_id',
        'district_id',
        'manage_province_id',
        'manage_district_id',
        'is_store_owner_and_chain_owner',
        'token',
        'store_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function setPasswordAttribute($password)
    {
        if ($password) {
            $this->attributes['password'] = Hash::make($password);
        }
    }
    public function getAvatarAttribute($avatar)
    {
        if ($avatar == null) {
            return asset('/image_default/avatar_default.jpg');
        }
        return asset($avatar);
    }
    public function getDriverLicenseImageAttribute($driver_license_image)
    {
        if ($driver_license_image == null) {
            return null;
        }
        return asset($driver_license_image);
    }
    public function getVehicleRegistrationImageAttribute($vehicle_registration_image)
    {
        if ($vehicle_registration_image == null) {
            return null;
        }
        return asset($vehicle_registration_image);
    }
    public function getImageCmndFrontAttribute($image_cmnd_front)
    {
        if ($image_cmnd_front == null) {
            return null;
        }
        return asset($image_cmnd_front);
    }
    public function getImageCmndBackAttribute($image_cmnd_back)
    {
        if ($image_cmnd_back == null) {
            return null;
        }
        return asset($image_cmnd_back);
    }
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
    public function addressUser()
    {
        return $this->HasMany(AddressUser::class, 'user_id');
    }
    public function voucher()
    {
        return $this->belongsToMany(Voucher::class, 'voucher_user');
    }
    public function staffStore()
    {
        return $this->belongsToMany(Store::class, 'user_store');
    }
    public function store()
    {
        return $this->HasOne(Store::class, 'user_id');
    }
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
    public function manageProvince()
    {
        return $this->belongsTo(Province::class, 'manage_province_id');
    }
    public function manageDistrict()
    {
        return $this->belongsTo(District::class, 'manage_district_id');
    }
}
