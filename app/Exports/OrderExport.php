<?php

namespace App\Exports;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithStyles;
class OrderExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithCustomStartCell, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }
    public function collection()
    {
        // $array = [];
        // $array[] = [
        //     'store_name' => "1",
        //     'date' => "1",
        //     'total_order' => "1",
        //     'sum_money' => "1",
        //     'sum_money_shop_return' => "1",
        //     'sum_money_timzi_return' => "1"
        // ];
        return collect($this->data['list_data']);
        // return $this->data['list_data'];
    }
    public function startCell(): string
    {
        return 'B2';
    }
    public function styles(Worksheet $sheet)
    {
        return [
            'B2'    => ['font' => ['bold' => true]],
            'B3'    => ['font' => ['bold' => true]],
            'D2'    => ['font' => ['bold' => true]],
            'D3'    => ['font' => ['bold' => true]],
            'B4'    => ['font' => ['bold' => true]],
            'B5'    => ['font' => ['bold' => true]],
            'B6'    => ['font' => ['bold' => true]],
            'B7'    => ['font' => ['bold' => true]],
            'B8'    => ['font' => ['bold' => true]],
            'C8'    => ['font' => ['bold' => true]],
            'D8'    => ['font' => ['bold' => true]],
            'E8'    => ['font' => ['bold' => true]],
            'F8'    => ['font' => ['bold' => true]],
            'G8'    => ['font' => ['bold' => true]],
            'H8'    => ['font' => ['bold' => true]],
        ];
    }
    public function headings(): array {
        return [
            [
                'Tên chương trình:',
                $this->data['title'],
                'Ghi chú:',
                $this->data['note'],
                ""
            ],
            [
                'Từ ngày:',
                $this->data['time_open'],
                'Đến ngày:',
                $this->data['time_close'],
                ""
            ],
            [
                'Tên quán',
                $this->data['store_name'],
                '',
                '',
                ""
            ],
            [
                'Tổng tiền',
                $this->data['total_money'],
                '',
                "",
                ""
            ],
            [
                'Tổng tiền chủ quán trả',
                $this->data['total_money_shop_return'],
                '',
                "",
                ""
            ],
            [
                'Tổng tiền timzi trả',
                $this->data['total_money_timzi_return'],
                '',
                "",
                ""
            ],
            [
                'Tên quán',
                'Tên shipper',
                "Ngày nhận đơn",
                'Tổng số đơn',
                "Tổng tiền",
                "Số tiền chủ quán trả",
                "Số tiền timzi trả"
            ]

        ];
    }

    public function map($data): array {
        return [
            $data['store_name'],
            $data['shipper_name'],
            $data['date'],
            $data['total_order'],
            $data['sum_money'],
            $data['sum_money_shop_return'],
            $data['sum_money_timzi_return']
        ];
    }

}
