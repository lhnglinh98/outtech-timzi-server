<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithStyles;
class VoucherShipperExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithCustomStartCell, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }
    public function collection()
    {
        return collect($this->data['list_data']);
    }
    public function startCell(): string
    {
        return 'B2';
    }
    public function styles(Worksheet $sheet)
    {
        return [
            'B2'    => ['font' => ['bold' => true]],
            'B3'    => ['font' => ['bold' => true]],
            'D2'    => ['font' => ['bold' => true]],
            'D3'    => ['font' => ['bold' => true]],
            'B5'    => ['font' => ['bold' => true]],
            'C5'    => ['font' => ['bold' => true]],
            'D5'    => ['font' => ['bold' => true]],
            'E5'    => ['font' => ['bold' => true]],
            'F5'    => ['font' => ['bold' => true]],
        ];
    }
    public function headings(): array {
        return [
            [
                'Tên chương trình:',
                $this->data['title'],
                'Ghi chú:',    
                $this->data['note'],
                ""
            ],
            [
                'Từ ngày:',
                $this->data['time_open'],
                'Đến ngày:',
                $this->data['time_close'],
                ""
            ],
            [
                'Tổng tiền trả',
                $this->data['total_money'],
                '',
                "",
                ""
            ],
            [
                'Tên shipper',
                'Ngày nhận đơn',
                "Tên quán",
                "Tổng số đơn",
                "Tổng tiền"
            ]
            
        ];
    }
 
    public function map($data): array {
        return [
            $data['shipper_name'],
            $data['date'],
            $data['store_name'],
            $data['total_order'],
            $data['sum_money']
        ];
    }
}
