<?php


namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\CategoryService;
use App\Http\Utils\SystemParam;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategorySerivceController extends Controller
{
    public function createCategoryServices(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'price_service' => 'required',
        ], [
            'title.required' => 'Vui lòng nhập danh mục dịch vụ',
            'price_service.required' => 'Vui lòng nhập giá dịch vụ ',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }

        $req = $request->all();
        $req['content']='';
        $data_category_services = CategoryService::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::code_success, $data_category_services);
    }
    public function updateCategoryServices($id, Request $request)
    {
        $check = CategoryService::findorfail($id);
        $req = $request->all();
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);

    }
    public function listdataCategoryService()
    {
        $dataCategoryService = CategoryService::select('category_service.*')->orderby('id', 'desc')->paginate(20);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $dataCategoryService);
    }
    public function listdataCategoryServiceStatus($status)
    {
        $dataCategoryService = CategoryService::select('category_service.*')->where('status',$status)->orderby('id', 'desc')->paginate(20);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $dataCategoryService);
    }
    public function deleteCategoryService($id)
    {
        CategoryService::where([['id', $id]])->firstorfail()->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, '');
    }
}
