<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\PaymentGuide;
use App\Models\DisputeResolutionPolicy;
use App\Models\PrivacyPolicy;
use App\Models\Regulation;
use App\Models\TermOfUse;
use App\Http\Utils\SystemParam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PolicyAppController extends Controller
{
    public function paymentGuide(){
        $data = PaymentGuide::first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function privacyPolicy(){
        $data = PrivacyPolicy::first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function regulation(){
        $data = Regulation::first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function termOfUse(){
        $data = TermOfUse::first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function disputeResolutionPolicy(){
        $data = DisputeResolutionPolicy::first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
}
