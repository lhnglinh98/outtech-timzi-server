<?php


namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\NoteBook;
use App\Http\Utils\SystemParam;
use App\Models\NoteBuyMe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NotebookController extends Controller
{
    public function createNoteBook(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if (NoteBook::where([['user_id', $user->id]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::user_already_exist, null);
        }
        $req = $request->all();
        $req['user_id'] = $user->id;
        $notebook = NoteBook::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $notebook);
    }
    public function checkIdNoteBook()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $idNoteBook = NoteBook::where([['user_id',$user->id]])->first();
        if($idNoteBook){
            $id_noteBook= [
                'id_noteBook'=>$idNoteBook->id
            ];
        }else{
            $id_noteBook= [
                'id_noteBook'=> 0
            ];
        };

        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::status_success, $id_noteBook);
    }
    public function updateNoteBook($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check = NoteBook::findorfail($id);
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ], [
            'title.required' => 'Vui lòng nhập tên Sổ tay',
            'description.required' => 'Vui lòng nhập mô tả sổ tay',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);

    }
    public function notebookDetail($id)
    {
        $dataNotebook = NoteBook::find($id)->get()->first();
        $dataNoteBuyMe = NoteBuyMe::select('note_buy_me.*')
            ->where([['note_buy_me.notebook_id', $id]])
            ->get();
        $dataNotebook['notebuyme']=$dataNoteBuyMe;
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $dataNotebook);
    }
    public function createNoteBuyMe(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'notebook_id' => 'required',
            'title' => 'required',
            'content' => 'required',
        ], [
            'notebook_id.required' => 'Hệ thống lỗi về notebook_id không chọn được',
            'title.required' => 'Vui lòng nhập tiêu đề ghi chú',
            'content.required' => 'Vui lòng nhập mô tả ghi chú',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }

        $req = $request->all();
        $notebook = NoteBuyMe::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $notebook);
    }
    public function updateNoteBuyMe($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check = NoteBuyMe::findorfail($id);
        $validator = Validator::make($request->all(), [
            'notebook_id' => 'required',
        ], [
            'notebook_id.required' => 'Hệ thống lỗi về notebook_id không chọn được',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);

    }
    public function deleteNoteBuyMe($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check_note_buy_me = NoteBuyMe::where([['id', $id]])->firstorfail();
        $check_note_buy_me->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check_note_buy_me);
    }
    public function listNoteBuyMe($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = NoteBuyMe::where('notebook_id',$id)->orderby('id', 'desc')->paginate(20);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listNoteBuyMeStatus(){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $notebook_id = NoteBook::Where('user_id',$user->id)->firstorfail()->id;
        $data = NoteBuyMe::where([['notebook_id',$notebook_id],['status',1]])->orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
}
