<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\BookFood;
use App\Models\BookFoodTopping;
use App\Models\BookTable;
use App\Models\ComboFood;
use App\Models\Food;
use App\Models\FoodSize;
use App\Models\PaymentMethod;
use App\Models\ProgramStore;
use App\Models\ProgramStoreDetail;
use App\Models\RevenueStore;
use App\Models\SetupPercentCoin;
use App\Models\Store;
use App\Models\TableMerge;
use App\Models\TableStore;
use App\Models\ToppingFood;
use App\Models\UserStore;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookTableController extends Controller
{
    //
    protected $notifycontroller;
    public function __construct()
    {
        $this->notifycontroller = new NotifyController();
    }
    public function booktableWithCustomer(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'table_store_id' => 'required',
            'time_booking' => 'required|after_or_equal:now',
            'name' => 'required',
            'phone' => 'required',
            'number_people' => 'required',
            'note' => 'required',
        ], [
            'table_store_id.required' => 'Vui lòng chọn bàn cần đặt',
            'time_booking.required' => 'Vui lòng chọn thời gian',
            'name.required' => 'Vui lòng nhập tên',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'number_people.required' => 'Vui lòng nhập số người',
            'note.required' => 'Vui lòng nhập ghi chú',
            'time_booking.date' => 'Vui lòng nhập đúng định dạng',
            'time_booking.after_or_equal' => 'Thời gian đặt lịch không được nhỏ hơn hiện tại',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = TableStore::findorfail($request->table_store_id);
        if ($check->status != SystemParam::status_table_store_empty) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_table_store_not_empty, null);
        }
        if ($check->number_people_min > $request->number_people || $check->number_people_max < $request->number_people) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Số lượng người chỉ được phép trong khoảng từ " . $check->number_people_min . " đến " . $check->number_people_max . " người", null);
        }
        $check_book_table = BookTable::where([['user_id', $user->id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
        if ($check_book_table) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_book_table_already_exist, null);
        }
        $check_store = Store::find($check->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $req = $request->all();
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (BookTable::where('code', $req['code'])->exists());
        $req['time_booking'] = date('Y-m-d H:i:s', strtotime($request->time_booking));
        $req['store_id'] = $check->store_id;
        $req['user_id'] = $user->id;
        $create = BookTable::create($req);
        $check->update([
            'status' => SystemParam::status_table_store_booked,
        ]);
        $id = UserStore::where([['status', 1], ['store_id', $check->store_id]])->pluck('user_id')->toArray();
        $id = [$check_store->user_id];
        $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' đã gửi yêu cầu đặt bàn số ' . $check->number_table . ' với ' . $request->number_people . ' người.', $create->id, SystemParam::type_book_table, $create, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::booking_success_wait_confirm, $create);
    }
    public function changeTableWithCustomer(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'table_store_id' => 'required',
            'book_table_id' => 'required',
        ], [
            'table_store_id.required' => 'Vui lòng chọn bàn muốn đổi',
            'book_table_id.required' => 'Vui lòng chọn bàn đã đặt trước đó',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_table = TableStore::findorfail($request->table_store_id);
        $check_book = BookTable::where('id', $request->book_table_id)->firstorfail()->load('tableStore');
        if ($check_book->status != SystemParam::status_booking_table_wait_confirm || $check_book->status != SystemParam::status_booking_table_confirmed) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_book_status, null);
        }
        if ($check_book->number_people != null && $check_book->number_people > 0) {
            if ($check_table->number_people_min > $check_book->number_people || $check_table->number_people_max < $check_book->number_people) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_table_store_number_people_change, null);
            }
        }
        $check_store = Store::find($check_table->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        TableStore::where('id', $check_book->table_store_id)->update([
            'status' => SystemParam::status_table_store_empty,
        ]);
        $check_book->update([
            'table_store_id' => $request->table_store_id,
        ]);
        TableStore::where('id', $request->table_store_id)->update([
            'status' => SystemParam::status_table_store_booked,
        ]);
        $id = UserStore::where([['status', 1], ['store_id', $check_table->store_id]])->pluck('user_id')->toArray();
        $id = [$check_store->user_id];

        $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' đã gửi yêu cầu thay đổi bàn số ' . $check_book->tableStore->number_table . ' sang bàn số ' . $check_table->number_table, $check_book->id, SystemParam::type_book_table, $check_book, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::change_success, $check_book);
    }
    public function booktableWithBarcode(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApiNew(SystemParam::status_error, SystemParam::status_success, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'name' => 'required',
            // 'phone' => 'required',
            // 'number_people' => 'required',
        ], [
            'code.required' => 'Vui lòng chọn bàn cần đặt',
            'name.required' => 'Vui lòng nhập tên',
            // 'phone.required' => 'Vui lòng nhập số điện thoại',
            // 'number_people.required' => 'Vui lòng nhập số người',
        ]);
        if ($validator->fails()) {
            return $this->responseApiNew(SystemParam::status_error, SystemParam::status_success, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = TableStore::where('code', $request->code)->firstorfail();
        if ($check->status != SystemParam::status_table_store_empty) {
            return $this->responseApiNew(SystemParam::status_error, SystemParam::status_success, SystemParam::code_not_found, SystemParam::error_table_store_not_empty, null);
        }
        $check_store_food = Store::find($check->store_id);
        if($check_store_food && $check_store_food->is_open == 0){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_store_not_open, null);
        }
        // if ($check->number_people_min > $request->number_people || $check->number_people_max < $request->number_people) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Số lượng người chỉ được phép trong khoảng từ " . $check->number_people_min . " đến " . $check->number_people_max . " người", null);
        // }
        $check_book_table = BookTable::where([['user_id', $user->id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
        if ($check_book_table) {
            return $this->responseApiNew(SystemParam::status_error, SystemParam::status_success, SystemParam::code_error_server, SystemParam::error_check_book_table_already_exist, null);
        }
        $check_store = Store::find($check->store_id);
        if (!$check_store) {
            return $this->responseApiNew(SystemParam::status_error, SystemParam::status_success, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        if ($check_store->open_hours && $check_store->close_hours) {
            $time_open_hours = date('H', strtotime($check_store->open_hours)) * 3600 + date('i', strtotime($check_store->open_hours)) * 60;
            $time_close_hours = date('H', strtotime($check_store->close_hours)) * 3600 + date('i', strtotime($check_store->close_hours)) * 60;
            $time_order = date('H', time()) * 3600 + date('i', time()) * 60;
            if ($time_open_hours > $time_order || $time_close_hours < $time_order) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_time_book_table, null);
            }
        }
        $req = $request->all();
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (BookTable::where('code', $req['code'])->exists());
        $req['time_booking'] = date('Y-m-d H:i:s', time());
        $req['store_id'] = $check->store_id;
        $req['table_store_id'] = $check->id;
        $req['user_id'] = $user->id;
        $req['right_to_order'] = 1;
        $req['status'] = SystemParam::status_booking_table_confirmed;
        $create = BookTable::create($req);
        $check->update([
            'status' => SystemParam::status_table_store_booked,
        ]);
        $id = UserStore::where([['status', 1], ['store_id', $check->store_id]])->pluck('user_id')->toArray();
        $id = [$check_store->user_id];
        $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' đã đến quán đặt bàn số ' . $check->number_table . ' với ' . $request->number_people . ' người.', $create->id, SystemParam::type_book_table, $create, $user->name);
        return $this->responseApiNew(SystemParam::status_success, SystemParam::status_success, SystemParam::code_success, SystemParam::booking_success, $create);
    }
    public function listBookTableStoreWithOwner(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'status' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'status.required' => 'Vui lòng chọn loại đơn đặt bàn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = UserStore::where([['store_id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        // $where = [];
        // if ($request->status != null) {
        //     $where[] = ['status', $request->status];
        // }
        $array_status = [];
        if ($request->status == 1) {
            $array_status = [SystemParam::status_booking_table_wait_confirm];
        } else if ($request->status == 2) {
            $array_status = [SystemParam::status_booking_table_confirmed];
        } else if ($request->status == 3) {
            $array_status = [SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment];
        } else if ($request->status == 4) {
            $array_status = [SystemParam::status_booking_table_has_served];
        } else {
            $array_status = [SystemParam::status_booking_table_cancel_with_customer, SystemParam::status_booking_table_cancel_with_store, SystemParam::status_booking_table_time_out_with_customer];
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['created_at', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['created_at', '<=', $to_date];
        }
        $data = BookTable::where('store_id', $request->store_id)->where($where)->whereIn('status', $array_status)->with('user', 'staff', 'store', 'tableStore', 'bookFood', 'tableMerge')->orderby('id', 'desc')->paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function bookTableStoreDetailWithOwner($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_book = BookTable::findorfail($id);
        $check = UserStore::where([['store_id', $check_book->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $check_book->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_book->load('user', 'staff', 'store', 'tableStore', 'bookFood.food', 'bookFood.comboFood', 'bookFood.toppingFood', 'tableMerge');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $check_book);
    }
    public function confirmBookTableStore($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_book = BookTable::where([['id', $id], ['status', SystemParam::status_booking_table_wait_confirm]])->firstorfail();
        $check_book->update([
            'status' => SystemParam::status_booking_table_confirmed,
        ]);
        $this->notifycontroller->pushNotify([$check_book->user_id], 'Lần đặt bàn có mã ' . $check_book->code . ' của bạn đã được duyệt.', $check_book->id, SystemParam::type_book_table, $check_book, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $check_book);
    }
    public function cancelBookTableStore($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_book = BookTable::where([['id', $id]])->firstorfail();
        $check_book->update([
            'status' => SystemParam::status_booking_table_cancel_with_store,
        ]);
        $update_table = TableStore::where('id', $check_book->table_store_id)->update([
            'status' => SystemParam::status_table_store_empty,
        ]);
        $table_merge = TableMerge::where('book_table_id', $id)->pluck('table_store_id')->toArray();
        if(count($table_merge) > 0){
            TableStore::whereIn('id', $table_merge)->update([
                'status' => SystemParam::status_table_store_empty,
            ]);
        }
        $this->notifycontroller->pushNotify([$check_book->user_id], 'Lần đặt bàn có mã ' . $check_book->code . ' của bạn đã bị hủy.', $check_book->id, SystemParam::type_book_table, $check_book, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check_book);
    }
    public function listTableStore($store_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = TableStore::where('store_id', $store_id)->paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function openTableStoreWithOwner($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_table = TableStore::findorfail($id);
        $check = UserStore::where([['store_id', $check_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $check_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_table->update([
            'status' => SystemParam::status_table_store_empty,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::open_table_store_success, $check_table);
    }
    public function closeTableStoreWithOwner($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_table = TableStore::findorfail($id);
        $check = UserStore::where([['store_id', $check_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $check_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_table->update([
            'status' => SystemParam::status_table_store_booked,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::close_table_store_success, $check_table);
    }
    public function orderFoodWithBooktable(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'food_id' => 'required',
            'quantity' => 'required',
        ], [
            'food_id.required' => 'Vui lòng chọn món ăn',
            'quantity.required' => 'Vui lòng chọn số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $food = Food::find($request->food_id);
        if (!$food) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_food_not_found, null);
        }
        if ($food->status != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_food_status, null);
        }
        $check_store_food = Store::find($food->store_id);
        if($check_store_food && $check_store_food->is_open == 0){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_store_not_open, null);
        }
        $status = [SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service];
        $check_book_table = BookTable::where([['user_id', $user->id]])->whereIn('status', $status)->first();
        if (!$check_book_table) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_book_table_not_found, null);
        }
        $req = $request->all();
        $req['user_id'] = $user->id;
        $req['book_table_id'] = $check_book_table->id;
        $price = $food->price * $request->quantity;
        if ($food->price_discount == 0 && $food->price_discount_with_program == 0) {
            $price = $food->price * $request->quantity;
        } else if ($food->price_discount != 0 && $food->price_discount_with_program == 0) {
            $price = $food->price_discount * $request->quantity;
        } else if ($food->price_discount_with_program != 0) {
            $price = $food->price_discount_with_program * $request->quantity;
        }
        if ($request->size_id) {
            $size = FoodSize::find($request->size_id);
            if (!$size) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_size_not_found, null);
            }
            $req['size'] = $size->name;
            $price = $price + $size->price * $request->quantity;
        }
        if ($request->topping_food_id && is_array($request->topping_food_id)) {
            $ls_topping_food_id = array_unique($request->topping_food_id);
            $topping_food = ToppingFood::whereIn('id', $ls_topping_food_id)->with('categoryToppingFood')->get();
            if (count($topping_food) == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_topping_food_id_not_found, null);
            }
            if (isset($req['size'])) {
                $check = BookFood::where([['book_table_id', $check_book_table->id], ['food_id', $request->food_id], ['size', $req['size']], ['status', SystemParam::status_booking_food_wait_submit]])->get();
            } else {
                $check = BookFood::where([['book_table_id', $check_book_table->id], ['food_id', $request->food_id], ['status', SystemParam::status_booking_food_wait_submit]])->get();
            }
            $book_food_id = null;
            if (count($check) > 0) {
                foreach ($check as $c) {
                    $book_food_topping_id = BookFoodTopping::where('book_food_id', $c->id)->pluck('topping_food_id')->toArray();
                    $check_array = array_intersect($book_food_topping_id, $ls_topping_food_id);
                    if (count($check_array) == count($book_food_topping_id) && count($check_array) == count($ls_topping_food_id)) {
                        $book_food_id = $c->id;
                    }
                }
            }
            $price = $price + $topping_food->SUM('price') * $request->quantity;
            if ($book_food_id != null) {
                $check_book = BookFood::find($book_food_id);
                $ls_program = ProgramStoreDetail::where('food_id', $request->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    if ($check_book->quantity + $request->quantity > $check_program->SUM('quantity')) {
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình được phép đặt chỉ còn " . $check_program->SUM('quantity') . " số món.", null);
                    }
                }
                $check_book->update([
                    'quantity' => $check_book->quantity + $request->quantity,
                    'price' => $check_book->price + $price,
                ]);
            } else {
                $req['price'] = $price;
                $ls_program = ProgramStoreDetail::where('food_id', $request->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    if ($request->quantity > $check_program->SUM('quantity')) {
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình được phép đặt chỉ còn " . $check_program->SUM('quantity') . " số món.", null);
                    }
                }
                $check_book = BookFood::create($req);
                foreach ($topping_food as $t) {
                    $create = BookFoodTopping::insert([
                        'book_food_id' => $check_book->id,
                        'topping_food_id' => $t->id,
                        'category_topping_food_name' => $t->categoryToppingFood->name,
                        'topping_food_name' => $t->name,
                        'price' => $t->price,
                    ]);
                }
            }
        } else {
            $req['price'] = $price;
            if (isset($req['size'])) {
                $check_book = BookFood::where([['book_table_id', $check_book_table->id], ['food_id', $request->food_id], ['size', $req['size']], ['status', SystemParam::status_booking_food_wait_submit]])->first();
            } else {
                $check_book = BookFood::where([['book_table_id', $check_book_table->id], ['food_id', $request->food_id], ['status', SystemParam::status_booking_food_wait_submit]])->first();
            }
            if ($check_book) {
                $ls_program = ProgramStoreDetail::where('food_id', $request->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    if ($check_book->quantity + $request->quantity > $check_program->SUM('quantity')) {
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình được phép đặt chỉ còn " . $check_program->SUM('quantity') . " số món.", null);
                    }
                }
                $check_book->update([
                    'quantity' => $check_book->quantity + $request->quantity,
                    'price' => $check_book->price + $req['price'],
                ]);
            } else {
                $ls_program = ProgramStoreDetail::where('food_id', $request->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    if ($request->quantity > $check_program->SUM('quantity')) {
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình được phép đặt chỉ còn " . $check_program->SUM('quantity') . " số món.", null);
                    }
                }
                $check_book = BookFood::create($req);
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::add_book_food_success, $check_book->load('toppingFood'));
    }
    public function orderComboWithBooktable(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'combo_food_id' => 'required',
            'quantity' => 'required',
        ], [
            'combo_food_id.required' => 'Vui lòng chọn combo',
            'quantity.required' => 'Vui lòng chọn số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $combo_food = ComboFood::where([['id', $request->combo_food_id], ['status', 1]])->firstorfail();
        if (!$combo_food) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_combo_food_not_found, null);
        }
        $check_store_food = Store::find($combo_food->store_id);
        if($check_store_food && $check_store_food->is_open == 0){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_store_not_open, null);
        }
        // if ($combo_food->quantity < $request->quantity) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_id_combo_food_quantity, null);
        // }
        $status = [SystemParam::status_booking_food_wait_submit, SystemParam::status_booking_food_not_on_the_table_yet];
        $check_book_table = BookTable::where([['user_id', $user->id]])->whereIn('status', $status)->first();
        if (!$check_book_table) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_book_table_not_found, null);
        }
        $card_2 = BookFood::where([['combo_food_id', $request->combo_food_id], ['book_table_id', $check_book_table->id], ['status', '<=', 2]])->get();
        if ($card_2->SUM('quantity') + $request->quantity > $combo_food->quantity) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Combo được phép đặt chỉ còn " . $combo_food->quantity . " số món.", null);
        }
        $req = $request->all();
        $req['user_id'] = $user->id;
        $req['book_table_id'] = $check_book_table->id;
        $price = $combo_food->price * $request->quantity;
        $req['price'] = $price;
        $check_book = BookFood::where([['book_table_id', $check_book_table->id], ['combo_food_id', $request->combo_food_id], ['status', SystemParam::status_booking_food_wait_submit]])->first();
        if ($check_book) {
            $check_book->update([
                'quantity' => $check_book->quantity + $request->quantity,
                'price' => $check_book->price + $req['price'],
            ]);
        } else {
            $check_book = BookFood::create($req);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::add_book_food_success, $check_book->load('toppingFood'));
    }
    public function subtractQuantityBookFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'book_food_id' => 'required',
            'quantity' => 'required',
        ], [
            'book_food_id.required' => 'Vui lòng chọn sản phẩm',
            'quantity.required' => 'Vui lòng chọn số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $data = BookFood::where([['id', $request->book_food_id]])->firstorfail();
        $check_book_table = BookTable::where([['id', $data->book_table_id], ['user_id', $user->id]])->firstorfail();
        if ($request->quantity <= 0) {
            $delete_topping = BookFoodTopping::where('book_food_id', $data->id)->delete();
            $data->delete();
        } else {
            $price_1_product = $data->price / $data->quantity;
            if ($data->food_id) {
                $ls_program = ProgramStoreDetail::where('food_id', $data->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    $card_2 = BookFood::where([['food_id', $data->food_id], ['book_table_id', $check_book_table->id]])->get();
                    if ($card_2->SUM('quantity') + $request->quantity > $check_program->SUM('quantity')) {
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình được phép đặt chỉ còn " . $check_program->SUM('quantity') . " số món.", null);
                    }
                }
            }
            $data->update([
                'quantity' => $request->quantity,
                'price' => $price_1_product * $request->quantity,
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $data);
    }
    public function returnBookFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'book_food_id' => 'required',
            'quantity' => 'required',
        ], [
            'book_food_id.required' => 'Vui lòng chọn sản phẩm',
            'quantity.required' => 'Vui lòng chọn số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $data = BookFood::where([['id', $request->book_food_id], ['status', SystemParam::status_booking_food_was_on_the_table]])->with('food')->firstorfail();
        $check_book_table = BookTable::where([['id', $data->book_table_id]])->firstorfail();
        if ($data->food->is_group_food == 0) {
            if ($request->quantity <= 0 || $request->quantity > $data->quantity) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_quantity_book_food, null);
            }
            if ($request->quantity == $data->quantity) {
                $delete_topping = BookFoodTopping::where('book_food_id', $data->id)->delete();
                $data->delete();
            } else {
                $price_1_product = $data->price / $data->quantity;
                $data->update([
                    'quantity_minus_group_food' => $data->quantity_minus_group_food + $request->quantity,
                    'quantity' => $data->quantity - $request->quantity,
                    'price' => $data->price - $price_1_product * $request->quantity,
                ]);
            }
        } else {
            if ($request->quantity <= 0 || ($request->quantity + $data->quantity_minus_group_food) > $data->quantity * $data->food->quantity_food) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_quantity_book_food, null);
            }
            if ($request->quantity == $data->quantity * $data->food->quantity_food) {
                $delete_topping = BookFoodTopping::where('book_food_id', $data->id)->delete();
                $data->delete();
            } else {
                $price_1_product = $data->price / ($data->quantity * $data->food->quantity_food);
                $data->update([
                    'quantity_minus_group_food' => $data->quantity_minus_group_food + $request->quantity,
                    'price' => $data->price - $price_1_product * $request->quantity,
                ]);
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::return_food_success, $data);
    }
    public function rightToOrderWithOwner($book_table_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::where([['id', $book_table_id], ['status', SystemParam::status_booking_table_confirmed], ['right_to_order', 0]])->firstorfail();
        $check->update([
            'right_to_order' => 1,
            'status' => SystemParam::status_booking_table_in_service,
        ]);
        $this->notifycontroller->pushNotify([$check->user_id], 'Lần đặt bàn có mã ' . $check->code . ' của bạn đã được cho phép gọi món.', $check->id, SystemParam::type_book_table, $check, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function tableMergeWithOwner(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'book_table_id' => 'required',
            'table_store_id' => 'required',
        ], [
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
            'table_store_id.required' => 'Vui lòng chọn bàn muốn gộp',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_book = BookTable::where([['id', $request->book_table_id]])->firstorfail();
        if (!is_array($request->table_store_id)) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_array_table_store, null);
        }
        $ls_id = TableMerge::where('book_table_id', $request->book_table_id)->pluck('table_store_id')->toArray();
        TableStore::whereIn('id', $ls_id)->update([
            'status' => SystemParam::status_table_store_empty,
        ]);
        TableMerge::where('book_table_id', $request->book_table_id)->delete();
        if (count($request->table_store_id) > 0) {
            foreach ($request->table_store_id as $t) {
                $check_table = TableStore::where([['id', $t], ['status', SystemParam::status_table_store_empty]])->first();
                if ($check_table) {
                    TableMerge::insert([
                        'book_table_id' => $request->book_table_id,
                        'table_store_id' => $t,
                    ]);
                    $check_table->update([
                        'status' => SystemParam::status_table_store_booked,
                    ]);
                }
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::table_merge_success, $check_book->load('tableMerge'));
    }
    public function submitOrderFoodWithCustomer($book_table_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check = BookTable::where([['id', $book_table_id]])->with('tableStore')->firstorfail();
        if ($check->status == SystemParam::status_booking_table_wait_confirm) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_not_confirm_book_table, null);
        }
        if ($check->right_to_order == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_right_to_order_with_customer, null);
        }
        $check_store = Store::find($check->tableStore->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check->update([
            'status' => SystemParam::status_booking_table_in_service,
        ]);
        $update = BookFood::where([['book_table_id', $book_table_id], ['status', SystemParam::status_booking_food_wait_submit]])->update([
            'status' => SystemParam::status_booking_food_not_on_the_table_yet,
        ]);
        $id = UserStore::where([['status', 1], ['store_id', $check->tableStore->store_id]])->pluck('user_id')->toArray();
        $id = [$check_store->user_id];
        $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' tại bàn số ' . $check->tableStore->number_table . ' có yêu cầu gọi món.', $check->id, SystemParam::type_book_table, $check, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::order_food_success, $check);
    }
    public function cancelBookFoodWithCustomer(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'book_table_id' => 'required',
        ], [
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = BookTable::findorfail($request->book_table_id);
        if ($request->book_food_id) {
            $check_book_food = BookFood::find($request->book_food_id);
            if (!$check_book_food) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_check_book_food_not_found, null);
            }
            $delete_topping = BookFoodTopping::where('book_food_id', $request->book_food_id)->delete();
            $check_book_food->delete();
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check_book_food);
        } else {
            $check_book_food = BookFood::where([['book_table_id', $request->book_table_id]])->get();
            foreach ($check_book_food as $c) {
                $delete_topping = BookFoodTopping::where('book_food_id', $c->id)->delete();
            }
            $delete_book_food = BookFood::where([['book_table_id', $request->book_table_id]])->delete();
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $delete_book_food);
        }
    }
    public function bookingTableDetailWithCustomer($book_table_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check_book_table = BookTable::find($book_table_id);
        if(!$check_book_table){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_book_table_check_not_found, null);
        }
        if ($request->type == 1) {
            $data = BookTable::findorfail($book_table_id)->load('user', 'store', 'tableStore', 'paymentMethod', 'tableMerge')->load(['bookFood' => function ($query) {
                $query->where('book_food.status', SystemParam::status_booking_food_wait_submit)->with('food', 'comboFood', 'toppingFood')->get();
            }]);
            $data['total_quantity'] = BookFood::where([['book_table_id', $book_table_id]])->get()->SUM('quantity');
        } else if ($request->type == 2) {
            $array_status = [SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_has_served, SystemParam::status_booking_table_wait_payment];
            $data = BookTable::where('id', $book_table_id)->whereIn('status', $array_status)->first();
            if (!$data) {
                $data = BookTable::where('id', $book_table_id)->firstorfail()->load('user', 'store', 'tableStore');
                $data['book_food'] = [];
                $data['total_quantity'] = BookFood::where([['book_table_id', $book_table_id]])->get()->SUM('quantity');
            } else {
                $data->load('user', 'store', 'tableStore', 'paymentMethod', 'tableMerge')->load(['bookFood' => function ($query) {
                    $query->where('book_food.status', SystemParam::status_booking_food_not_on_the_table_yet)->with('food', 'comboFood', 'toppingFood')->get();
                }]);
                $data['total_quantity'] = BookFood::where([['book_table_id', $book_table_id]])->get()->SUM('quantity');
            }
        } else if ($request->type == 3) {
            $array_status = [SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_has_served, SystemParam::status_booking_table_wait_payment];
            $data = BookTable::where('id', $book_table_id)->whereIn('status', $array_status)->first();
            if (!$data) {
                $data = BookTable::where('id', $book_table_id)->firstorfail()->load('user', 'store', 'tableStore');
                $data['book_food'] = [];
                $data['total_quantity'] = BookFood::where([['book_table_id', $book_table_id]])->get()->SUM('quantity');
            } else {
                $data->load('user', 'store', 'tableStore', 'paymentMethod', 'tableMerge')->load(['bookFood' => function ($query) {
                    $query->where('book_food.status', SystemParam::status_booking_food_was_on_the_table)->with('food', 'comboFood', 'toppingFood')->get();
                }]);
                $data['total_quantity'] = BookFood::where([['book_table_id', $book_table_id]])->get()->SUM('quantity');
            }
        } else {
            $data = BookTable::where('id', $book_table_id)->first();
            if (!$data) {
                $data = BookTable::where('id', $book_table_id)->firstorfail()->load('user', 'store', 'tableStore');
                $data['book_food'] = [];
                $data['total_quantity'] = BookFood::where([['book_table_id', $book_table_id]])->get()->SUM('quantity');
            } else {
                $data->load('user', 'store', 'tableStore', 'paymentMethod', 'bookFood.food', 'bookFood.comboFood', 'bookFood.toppingFood', 'tableMerge');
                $data['total_quantity'] = BookFood::where([['book_table_id', $book_table_id]])->get()->SUM('quantity');
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function confirmFoodOnTheTable($book_food_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookFood::findorfail($book_food_id);
        if ($check->food_id) {
            $ls_program = ProgramStoreDetail::where('food_id', $check->food_id)->pluck('program_store_id')->toArray();
            $check_program = ProgramStore::whereIn('id', $ls_program)->where([['status', 1]])->get();
            if (count($check_program) > 0) {
                $card = BookFood::where([['book_table_id', $check->book_table_id], ['food_id', $check->food_id]])->get();
                foreach ($check_program as $p) {
                    if ($p->quantity < $card->SUM('quantity')) {
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình không đủ để lên bàn.", null);
                    }
                }
                foreach ($check_program as $p) {
                    if ($p->quantity >= $card->SUM('quantity')) {
                        ProgramStore::where('id', $p->id)->update([
                            'quantity' => $p->quantity - $check->quantity,
                            'quantity_bought' => $p->quantity_bought + $check->quantity,
                        ]);
                    } else {
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình không đủ để lên bàn.", null);
                    }
                }
            }
        }
        $check->update([
            'status' => SystemParam::status_booking_food_was_on_the_table,
        ]);
        $check->load('bookTable');
        $this->notifycontroller->pushNotify([$check->bookTable->user_id], 'Lần đặt bàn có mã ' . $check->bookTable->code . ' của bạn đã được cho phép gọi món.', $check->bookTable->id, SystemParam::type_book_table, $check, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function paymentBookFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'book_table_id' => 'required',
            'total_money' => 'required',
            'payment_method_id' => 'required',
        ], [
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
            'total_money.required' => 'Vui lòng chọn lần đặt bàn',
            'payment_method_id.required' => 'Vui lòng chọn phương thức thanh toán',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = BookTable::where([['user_id', $user->id], ['id', $request->book_table_id], ['status', SystemParam::status_booking_table_in_service]])->firstorfail();
        $check_payment_method = PaymentMethod::find($request->payment_method_id);
        if (!$check_payment_method) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_check_payment_method_not_found, null);
        }
        $check_store = Store::find($check->tableStore->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        if ($request->coin) {
            if ($user->coin < $request->coin) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_coin_user_not_enough, null);
            }
            $check->update([
                'status' => SystemParam::status_booking_table_wait_payment,
                'total_money' => $request->total_money,
                'coin' => $request->coin,
                'payment_method_id' => $request->payment_method_id,
            ]);
        } else {
            $check->update([
                'status' => SystemParam::status_booking_table_wait_payment,
                'total_money' => $request->total_money,
                'payment_method_id' => $request->payment_method_id,
            ]);
        }
        $id = UserStore::where([['status', 1], ['store_id', $check->tableStore->store_id]])->pluck('user_id')->toArray();
        $id = [$check_store->user_id];
        $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' tại bàn số ' . $check->tableStore->number_table . ' có yêu cầu thanh toán.', $check->id, SystemParam::type_book_table, $check, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::request_payment_success, $check);
    }
    public function confirmPaymentBookFoodWithOwner($book_table_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_book_table = BookTable::find($book_table_id);
        if(!$check_book_table){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_book_table_check_not_found, null);
        }
        $check_book_table = BookTable::where([['id', $book_table_id], ['status', SystemParam::status_booking_table_wait_payment]])->firstorfail();
        $check = UserStore::where([['store_id', $check_book_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store2 = Store::where([['id', $check_book_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store2) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_store = Store::find($check_book_table->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check_book_table->update([
            'status' => SystemParam::status_booking_table_has_served,
            'staff_id' => $user->id,
        ]);
        $update_book_food = BookFood::where([['book_table_id', $check_book_table->id], ['status', SystemParam::status_booking_food_was_on_the_table]])->update([
            'status' => SystemParam::status_booking_food_finish,
        ]);
        $update_table = TableStore::where('id', $check_book_table->table_store_id)->update([
            'status' => SystemParam::status_table_store_empty,
        ]);
        $check_merge = TableMerge::where('book_table_id', $book_table_id)->get();
        if (count($check_merge) > 0) {
            foreach ($check_merge as $c) {
                TableStore::where('id', $c->table_store_id)->update([
                    'status' => SystemParam::status_table_store_empty,
                ]);
            }
        }
        $customer = User::where('id', $check_book_table->user_id)->first();
        // if ($customer && $check_book_table->coin > 0) {
        //     User::where('id', $check_book_table->user_id)->update([
        //         'coin' => $customer->coin - $check_book_table->coin,
        //     ]);
        //     CoinHistory::insert([
        //         'user_id' => $customer->id,
        //         'content' => "Bạn đã sử dụng " . $check_book_table->coin . " xu để thanh toán lần ăn tại quán có mã là " . $check_book_table->code,
        //         'coin' => $check_book_table->coin,
        //         'type' => 2,
        //         'created_at' => date('Y-m-d H:i:s', time()),
        //     ]);
        // }
        $check_percent_coin = SetupPercentCoin::first();
        $customer2 = User::where('id', $check_book_table->user_id)->first();
        // if ($customer2) {
        //     if ($check_percent_coin && $check_percent_coin->percent > 0 && $check_book_table->total_money > 0) {
        //         $coin = ($check_book_table->total_money) * $check_percent_coin->percent / 100;
        //         User::where('id', $check_book_table->user_id)->update([
        //             'coin' => $customer2->coin + $coin,
        //         ]);
        //         CoinHistory::insert([
        //             'user_id' => $customer2->id,
        //             'content' => "Bạn đã được tặng " . $coin . " xu khi hoàn thành lần ăn tại quán có mã là " . $check_book_table->code,
        //             'coin' => $coin,
        //             'type' => 1,
        //             'created_at' => date('Y-m-d H:i:s', time()),
        //         ]);
        //         $this->notifycontroller->pushNotify([$customer2->id], "Bạn đã được tặng " . $coin . " xu khi hoàn thành lần ăn tại quán có mã là " . $check_book_table->code . ".", $check_book_table->id, SystemParam::type_book_table, $check_book_table);
        //     }
        // }
        $month = date('Y-m', time());
        $check_revenue = RevenueStore::where([['store_id', $check_book_table->store_id], ['month', 'LIKE', '%' . $month . '%'], ['status', 0]])->first();
        if ($check_revenue) {
            $check_revenue->update([
                'money' => $check_revenue->money + $check_book_table->total_money,
                'fee_money' => $check_revenue->fee_money + ($check_book_table->total_money) * ($check_store->percent_discount_revenue / 100),
            ]);
        } else {
            do {
                $code_revenue = 'TZ' . $this->makeCodeRandom(8);
            } while (RevenueStore::where('code', $code_revenue)->exists());
            RevenueStore::insert([
                'code' => $code_revenue,
                'store_id' => $check_book_table->store_id,
                'money' => $check_book_table->total_money,
                'fee_money' => ($check_book_table->total_money) * ($check_store->percent_discount_revenue / 100),
                'month' => date('Y-m-d', time()),
            ]);
        }
        $this->notifycontroller->pushNotify([$check_book_table->user_id], 'Lần đặt bàn có mã ' . $check_book_table->code . ' của bạn đã được duyệt thanh toán.', $check_book_table->id, SystemParam::type_book_table, $check_book_table, $check_store->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_payment_success, $check_book_table);
    }
    public function listBookTableWithCustomer(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'type.required' => 'Vui lòng chọn loại đơn hàng',
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        if ($request->type == 1) {
            $array_status = [SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment];
            $data = BookTable::where([['user_id', $user->id]])->whereIn('status', $array_status)->with('store', 'tableStore', 'bookFood', 'tableMerge')->paginate();
        } else if ($request->type == 2) {
            $array_status = [SystemParam::status_booking_table_has_served, SystemParam::status_booking_table_cancel_with_store, SystemParam::status_booking_table_cancel_with_customer, SystemParam::status_booking_table_time_out_with_customer];
            $data = BookTable::where([['user_id', $user->id]])->whereIn('status', $array_status)->with('store', 'tableStore', 'bookFood', 'tableMerge')->paginate();
        } else {
            $data = BookTable::where([['user_id', $user->id], ['status', SystemParam::status_booking_table_wait_confirm]])->with('store', 'tableStore', 'bookFood', 'tableMerge')->paginate();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function deleteBookTableWithCustomer(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $array_status = [SystemParam::status_booking_table_has_served, SystemParam::status_booking_table_cancel_with_store, SystemParam::status_booking_table_cancel_with_customer, SystemParam::status_booking_table_time_out_with_customer];
        if ($request->book_table_id) {
            $data = BookTable::where([['user_id', $user->id], ['id', $request->book_table_id]])->whereNotIn('status', $array_status)->firstorfail();
            $check_book_food = BookFood::where('book_table_id', $data->id)->get();
            foreach ($check_book_food as $f) {
                $delete_topping = BookFoodTopping::where('book_food_id', $f->id)->delete();
            }
            $delete_book_food = BookFood::where('book_table_id', $data->id)->delete();
            $update_table = TableStore::where('id', $data->table_store_id)->update([
                'status' => SystemParam::status_table_store_empty,
            ]);
            $data->delete();
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $data);
        } else {
            $datas = BookTable::where([['user_id', $user->id], ['status', SystemParam::status_booking_table_wait_confirm]])->get();
            foreach ($datas as $data) {
                $check_book_food = BookFood::where('book_table_id', $data->id)->get();
                foreach ($check_book_food as $f) {
                    $delete_topping = BookFoodTopping::where('book_food_id', $f->id)->delete();
                }
                $delete_book_food = BookFood::where('book_table_id', $data->id)->delete();
                $update_table = TableStore::where('id', $data->table_store_id)->update([
                    'status' => SystemParam::status_table_store_empty,
                ]);
                $delete = BookTable::where('id', $data->id)->delete();
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, null);
        }
    }
    public function bookTableDetailInHistory($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = BookTable::findorfail($id)->load('user', 'store', 'tableStore', 'paymentMethod', 'bookFood.food', 'bookFood.comboFood', 'bookFood.toppingFood', 'tableMerge');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function CallStaff($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = BookTable::findorfail($id)->load('tableStore');
        $id = UserStore::where([['status', 1], ['store_id', $data->store_id], ['is_owner', 0]])->pluck('user_id')->toArray();
        $id = UserStore::where([['status', 1], ['store_id', $data->store_id], ['is_owner', 1]])->pluck('user_id')->toArray();
        $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' tại bàn số ' . $data->tableStore->number_table . ' có yêu cầu gọi nhân viên', $data->id, SystemParam::type_call_staff_book_table, $data, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::call_staff_success, $data);
    }
}
