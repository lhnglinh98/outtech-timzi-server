<?php


namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\NoteBook;
use App\Http\Utils\SystemParam;
use App\Models\NoteBuyMe;
use App\Models\Distances;
use App\Models\CategoryService;
use App\Models\OderBuyMeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OderBuyMeController extends Controller
{
    public function createOderBuyMe(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'distance_id' => 'required',
            'category_service_id' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'option_pay' => 'required',
            'all_note' => 'required',
        ], [
            'user_id.required' => 'Phiên đăng nhập hết ',
            'distance_id.required' => 'Bạn chưa chọn khoảng cách',
            'category_service_id.required' => 'Vui lòng chọn dịch vụ',
            'name.required' => 'Vui lòng nhập tên người tạo ',
            'phone.required' => 'Vui lòng nhập số điện thoại người tạo đơn',
            'option_pay.required' => 'Vui lòng chọn loại dịch vụ',
            'address.required' => 'Vui lòng chọn địa chỉ',
            'all_note.required' => 'Vui lòng tạo ghi chú trong sổ tay trước khi tạo đơn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $notebook_id = NoteBook::select('notebook.id')->join('users', 'users.id', 'notebook.user_id')->where('notebook.user_id', $user->id)->first()->id;
        $distance_id = $request->distance_id;
        $price_discount = (Distances::find($distance_id)->price_discount) ? (Distances::find($distance_id)->price_discount) : (0);
        $category_service_id = $request->category_service_id;
        $price_service = (CategoryService::find($category_service_id)->price_service) ? (CategoryService::find($category_service_id)->price_service) : (0);
        $req = $request->all();
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (OderBuyMeModel::where('code', $req['code'])->exists());

        $req['user_id'] = $user->id;
        $req['notebook_id'] = (int)$notebook_id;
        $req['price_discount'] = $price_discount;
        $req['price_service'] = $price_service;
        $req['total_money'] = $price_service + $price_discount;
        $req['status'] = 1;
        $req['money_surcharge'] = 0;
        $req['note_surcharge'] = ' ';
        $oderbuyme = OderBuyMeModel::create($req);
        // if($oderbuyme){
        //     $notebuyme_id = NoteBook::join('note_buy_me as n', 'n.notebook_id', 'notebook.id')
        //     ->where('notebook_id',$oderbuyme->notebook_id)->where('n.status', 1)->pluck('n.id')->toArray();
        //     foreach ($notebuyme_id as $i) {
        //         NoteBuyMe::findorfail($i)->update([
        //             'status' => 3,
        //         ]);
        //     }
        // }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $oderbuyme);
    }
    public function updateOderBuyMe($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // $validator = Validator::make($request->all(), [
        //     'money_surcharge'=>'required',
        //     'note_surcharge'=>'required'
        // ], [
        //     'money_surcharge.required' => 'Vui lòng nhập số tiền phụ thu thêm',
        //     'note_surcharge.required' => 'Vui lòng nhập nội dung và lý do thu thêm phí'
        // ]);
        // if ($validator->fails()) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        // }
        $check = OderBuyMeModel::findorfail($id);

        $price_discount = ($request->price_discount) ? ($request->price_discount) : ($check->price_discount);
        $price_service = ($request->price_service) ? ($request->price_service) : ($check->price_service);
        $money_surcharge = ($request->money_surcharge) ? ($request->money_surcharge) : ($check->money_surcharge);
        $req = $request->all();

        if ($request->status) {
            $req['status'] = $request->status;
        }
        $req['price_discount'] = $price_discount;
        $req['price_service'] = $price_service;
        $req['total_money'] = $price_service + $price_discount + $money_surcharge;
        $check->update($req);

        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::code_success, $check);
    }
    public function listOderBuyMe(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['oder_buy_me.created_at', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['oder_buy_me.created_at', '<=', $to_date];
        }
        if ($request->code) {
            $where[] = ['code', $request->code];
        }
        if ($request->status) {
            $where[] = ['status', $request->status];
        }
        if ($request->user_id) {
            $where[] = ['user_id', $request->user_id];
        }
        if ($request->name) {
            $where[] = ['name', 'like', '%' . $request->customer_name . '%'];
        }
        if ($request->limit) {
            $data = OderBuyMeModel::where($where)->orderby('id', 'desc')->limit(5)->get();
        } else {
            $data = OderBuyMeModel::where($where)->orderby('id', 'desc')->paginate(20);
        }
        $dataList = array(
            'countOder' => count($data),
            'coutMoney' => $data->SUM('total_money'),
            'data' => $data
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $dataList);
    }
    public function listOderBuyMeUser()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $where = [];
        if ($user->id) {
            $where[] = ['user_id', $user->id];
        }
        $data = OderBuyMeModel::where($where)->orderby('id', 'desc')->paginate(20);
        $dataList = array(
            'countOder' => count($data),
            'coutMoney' => $data->SUM('total_money'),
            'data' => $data
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $dataList);
    }
    public function detailOderBuyMe($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $oderbuyme = OderBuyMeModel::findorfail($id);
        $dataDistances = Distances::findorfail($oderbuyme->distance_id);
        $dataCategoryService = CategoryService::findorfail($oderbuyme->category_service_id);
        $oderbuyme['distance'] = $dataDistances;
        $oderbuyme['categoryService'] = $dataCategoryService;
        // $notebook = NoteBook::where('user_id',$user->id)->first();

        // $notebuyme=NoteBuyMe::where('notebook_id',$notebook->id)->get();
        // $dataFinal = array(
        //     'oder_buy_me' => $oderbuyme,
        //     'note_buy_me' => $notebuyme
        // );

        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $oderbuyme);
    }
}
