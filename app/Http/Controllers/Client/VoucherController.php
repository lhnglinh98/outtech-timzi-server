<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Voucher;
use App\Models\VoucherUser;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    //
    public function confirmVoucher($id)
    {
        $data = Voucher::where('id', $id)->update([
            'status' => 2,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $data);
    }
    public function listVoucherInOrder($store_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if($request->title){
            $data = VoucherUser::select('v.*')->join('voucher as v', 'v.id', 'voucher_user.voucher_id')
            ->where([['voucher_user.user_id', $user->id], ['voucher_user.status', 1], ['v.time_close', '>=', date('Y-m-d', time())], ['v.status', 2], ['v.store_id', null], ['v.title', 'LIKE', '%' . $request->title . '%']])
            ->orWhere([['voucher_user.user_id', $user->id], ['voucher_user.status', 1], ['v.time_close', '>=', date('Y-m-d', time())], ['v.status', 2], ['v.store_id', $store_id], ['v.title', 'LIKE', '%' . $request->title . '%']])
            ->get();
        }else{
            $data = VoucherUser::select('v.*')->join('voucher as v', 'v.id', 'voucher_user.voucher_id')
            ->where([['voucher_user.user_id', $user->id], ['voucher_user.status', 1], ['v.time_close', '>=', date('Y-m-d', time())], ['v.status', 2], ['v.store_id', null]])
            ->orWhere([['voucher_user.user_id', $user->id], ['voucher_user.status', 1], ['v.time_close', '>=', date('Y-m-d', time())], ['v.status', 2], ['v.store_id', $store_id]])
            ->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listVoucher(Request $request)
    {
        $token = $request->header('Authorization');
        if ($request->store_id) {
            $data1 = Voucher::where([['status', 2], ['store_id', $request->store_id], ['is_transfer', 0]])->pluck('id')->toArray();
        } else {
            $data1 = Voucher::where([['status', 2], ['is_transfer', 0]])->pluck('id')->toArray();
            // $data1 = Voucher::where([['status', 2], ['is_transfer', 0]])->with('store')->orderby('id', 'desc')->paginate(12);
        }
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            if ($request->store_id) {
                $data2 = Voucher::select('voucher.*')->join('users as u', 'u.id', 'voucher.transfer_id')
                ->where([['voucher.status', 2], ['voucher.store_id', $request->store_id], ['voucher.is_transfer', 1], ['u.manage_province_id', $user->province_id], ['u.manage_district_id', null]])
                ->orwhere([['voucher.status', 2], ['voucher.store_id', $request->store_id], ['voucher.is_transfer', 1], ['u.manage_province_id', $user->province_id], ['u.manage_district_id', $user->district_id]])
                ->pluck('voucher.id')->toArray();
            } else {
                $data2 = Voucher::select('voucher.*')->join('users as u', 'u.id', 'voucher.transfer_id')
                ->where([['voucher.status', 2], ['voucher.is_transfer', 1], ['u.manage_province_id', $user->province_id], ['u.manage_district_id', null]])
                ->orwhere([['voucher.status', 2], ['voucher.is_transfer', 1], ['u.manage_province_id', $user->province_id], ['u.manage_district_id', $user->district_id]])
                ->pluck('voucher.id')->toArray();
            }
            $ls_id = array_merge($data1, $data2);
            $data = Voucher::whereIn('id', $ls_id)->with('store')->orderby('id', 'desc')->paginate(12);
            foreach ($data as $d) {
                $check = VoucherUser::where([['voucher_id', $d->id], ['user_id', $user->id]])->first();
                if ($check) {
                    $d['check_receive_voucher'] = 1;
                } else {
                    $d['check_receive_voucher'] = 0;
                }
            }
        }else{
            $data = Voucher::whereIn('id', $data1)->with('store')->orderby('id', 'desc')->paginate(12);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function voucherDetail($id)
    {
        $data = Voucher::where([['status', 2], ['id', $id]])->with('store')->firstorfail();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function receiveVoucherWithCustomer($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check = Voucher::where([['id', $id], ['status', 2]])->firstorfail();
        $check_voucher_user = VoucherUser::where([['voucher_id', $id], ['user_id', $user->id]])->first();
        if ($check_voucher_user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_voucher_user_duplicate, null);
        }
        if($check->quantity == 0){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_out_of_stock_voucher, null);
        }
        $create = VoucherUser::insert([
            'voucher_id' => $id,
            'user_id' => $user->id,
            'status' => 1,
        ]);
        $check->update([
            'quantity' => $check->quantity - 1
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::receive_success, $create);
    }
    public function listVoucherUser(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = VoucherUser::select('v.*')->join('voucher as v', 'v.id', 'voucher_user.voucher_id')
            ->where([['voucher_user.user_id', $user->id], ['voucher_user.status', 1], ['v.time_close', '>=', date('Y-m-d', time())], ['v.status', 2]])
            ->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
}
