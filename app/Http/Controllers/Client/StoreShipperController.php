<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Store;
use App\Models\UserStore;
use App\Models\ShipperStore;
use App\Http\Utils\SystemParam;
use Illuminate\Support\Facades\Validator;
class StoreShipperController extends Controller
{
    //
    protected $notifycontroller;
    public function __construct()
    {
        $this->notifycontroller = new NotifyController();
    }
    public function listShipper(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if($request->name){
            $data = User::where([['status', 1], ['role_id', 4], ['name', 'LIKE', '%' . $request->name . '%']])
                        ->orWhere([['status', 1], ['role_id', 4], ['phone',  'LIKE', '%' . $request->name . '%']])
                        ->orderby('id', 'desc')
                        ->get();
        }else{
            $data = User::where([['status', 1], ['role_id', 4]])->orderby('id', 'desc')->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listShipperInWeb(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $shipper_id = [];
        if($request->store_id){
            $shipper_id = ShipperStore::where([['store_id', $request->store_id]])->pluck('user_id')->toArray();
        }
        if($request->name){
            $data = User::where([['status', 1], ['role_id', 4], ['name', 'LIKE', '%' . $request->name . '%']])
                        ->orWhere([['status', 1], ['role_id', 4], ['phone',  'LIKE', '%' . $request->name . '%']])
                        ->whereNotIn('id', $shipper_id)
                        ->orderby('id', 'desc')
                        ->paginate(12);
        }else{
            $data = User::whereNotIn('id', $shipper_id)->where([['status', 1], ['role_id', 4]])->orderby('id', 'desc')->paginate(12);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function chooseShipper(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'shipper_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'shipper_id.required' => 'Vui lòng chọn shipper',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $store = Store::where([['id', $request->store_id], ['status', 1]])->first();
        if (!$store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        if (!User::where([['id', $request->shipper_id], ['status', 1], ['role_id', 4]])->first()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_shipper_not_found, null);
        }
        $check = UserStore::where([['store_id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if(!$check){
            $check_store = Store::where([['id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if(!$check_store){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $create = ShipperStore::insert([
            'user_id' => $request->shipper_id,
            'store_id' => $request->store_id,
            'status' => 0
        ]);
        // $id = UserStore::where([['is_owner', 1], ['status', 1]])->pluck('user_id')->toArray();
        // if (count($id) == 0) {
        //     $id = [$store->user_id];
        // }
        $this->notifycontroller->pushNotify([$request->shipper_id], 'Cửa hàng ' . $store->name . ' đã đề nghị bạn làm người giao hàng chính của cửa hàng!', $store->id, SystemParam::type_store, $store, $store->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::choose_shipper_success, $create);
    }
    public function listShipperWithStore(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'status' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'status.required' => 'Vui lòng chọn trạng thái',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $shipper_id = ShipperStore::where([['store_id', $request->store_id], ['status', $request->status]])->pluck('user_id')->toArray();
        $where = [['status', 1], ['role_id', 4]];
        if($request->name){
            $data = User::whereIn('id', $shipper_id)->where([['status', 1], ['role_id', 4], ['name', 'LIKE', '%' . $request->name . '%']])
                        ->orWhere([['status', 1], ['role_id', 4], ['phone',  'LIKE', '%' . $request->name . '%']])
                        ->orderby('id', 'desc')
                        ->get();
        }else{
            $data = User::whereIn('id', $shipper_id)->where([['status', 1], ['role_id', 4]])->orderby('id', 'desc')->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function cancelShipperWithStore(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'shipper_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'shipper_id.required' => 'Vui lòng chọn người giao hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $store = Store::findorfail($request->store_id);
        $check = UserStore::where([['store_id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if(!$check){
            $check_store = Store::where([['id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if(!$check_store){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check = ShipperStore::where([['store_id', $request->store_id], ['user_id', $request->shipper_id]])->firstorfail();
        $check->delete();
        $this->notifycontroller->pushNotify([$request->shipper_id], 'Cửa hàng ' . $store->name . ' đã hủy đề nghị bạn làm người giao hàng chính của cửa hàng!', $store->id, SystemParam::type_store, $store, $store->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, 'Hủy hợp tác thành công', $check);
    }
}
