<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\BookFood;
use App\Models\BookFoodTopping;
use App\Models\BookTable;
use App\Models\CardFood;
use App\Models\CardToppingFood;
use App\Models\CategoryFood;
use App\Models\CategoryStoreFood;
use App\Models\CategoryToppingFood;
use App\Models\ComboFood;
use App\Models\ComboFoodDetail;
use App\Models\Food;
use App\Models\FoodSize;
use App\Models\OrderFood;
use App\Models\OrderFoodDetail;
use App\Models\OrderToppingFoodDetail;
use App\Models\ProgramStore;
use App\Models\ProgramStoreDetail;
use App\Models\Store;
use App\Models\UserStore;
use App\Models\ToppingFood;
use App\Models\TypeFood;
use App\Models\FoodSuggestion;
use App\Models\ReviewShipper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Image;
use QrCode;
use File;
use Illuminate\Support\Facades\DB;
class FoodController extends Controller
{
    //
    public function listFoodInstore($store_id){
        $data = Food::where([['store_id', $store_id], ['status', 1]])->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listTypeFood()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = TypeFood::where('status', 1)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'image' => 'required',
            'price' => 'required',
            'status' => 'required',
            'category_food_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'name.required' => 'Vui lòng nhập tên món ăn',
            'description.required' => 'Vui lòng nhập mô tả món ăn',
            'image.required' => 'Vui lòng chọn ảnh món ăn',
            'price.required' => 'Vui lòng nhập giá món ăn',
            'status.required' => 'Vui lòng chọn tình trạng món ăn',
            'category_food_id.required' => 'Vui lòng chọn danh mục món ăn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (Food::where('code', $req['code'])->exists());
        $req['image'] = $this->uploadImage($request->file('image'));
        $req['price'] = preg_replace('/[^0-9\-]/', '', $request->price);
        if ($request->price_discount) {
            if($request->price_discount == "undefined"){
                $req['price_discount'] = 0;
            }else{
                $req['is_sale'] = 1;
            }
        }
        if (!CategoryFood::where([['status', 1], ['id', $request->category_food_id]])->first()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_category_food_not_found, null);
        }
        if ($request->category_store_food_id) {
            if (!CategoryStoreFood::find($request->category_store_food_id)) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_category_food_not_found, null);
            }
        }
        if (!Store::where([['id', $request->store_id], ['status', 1]])->first()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        if($request->is_group_food){
            if(!$request->quantity_food || $request->quantity_food <= 0){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_quantity_food_not_found, null);
            }
        }
        if($request->type_food_id){
            if(!TypeFood::find($request->type_food_id)){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_type_food_not_found, null);
            }
        }
        $create = Food::create($req);
        if(isset($request->food_suggestions_id) && is_array($request->food_suggestions_id) && count($request->food_suggestions_id) > 0){
            $food_suggestions_id = array_unique($request->food_suggestions_id);
            foreach($food_suggestions_id as $f){
                $check = Food::where([['id', $f], ['id', '!=', $create->id], ['store_id', $request->store_id]])->first();
                if($check){
                    FoodSuggestion::insert([
                        'food_id' => $create->id,
                        'food_suggestions_id' => $f
                    ]);
                }
            }
        }
        // if ($request->size && is_array(json_decode($request->size))) {
        //     $size = json_decode($request->size);
        //     foreach ($size as $s) {
        //         FoodSize::insert([
        //             'food_id' => $create->id,
        //             'name' => $s->name,
        //             'quantity' => $s->quantity,
        //             'price' => $s->price,
        //         ]);
        //     }
        // }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function updateFood($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Food::findorfail($id);
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'status' => 'required',
            'category_food_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'name.required' => 'Vui lòng nhập tên món ăn',
            'description.required' => 'Vui lòng nhập mô tả món ăn',
            'price.required' => 'Vui lòng nhập giá món ăn',
            'status.required' => 'Vui lòng chọn tình trạng món ăn',
            'category_food_id.required' => 'Vui lòng chọn danh mục món ăn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($request->category_store_food_id) {
            if (!CategoryStoreFood::find($request->category_store_food_id)) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_category_food_not_found, null);
            }
        }
        $req = $request->all();
        if (!Store::where([['id', $request->store_id], ['status', 1]])->first()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        if (!CategoryFood::where([['status', 1], ['id', $request->category_food_id]])->first()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_category_food_not_found, null);
        }
        $req['price'] = preg_replace('/[^0-9\-]/', '', $request->price);
        if ($request->price_discount) {
            if($request->price_discount == "undefined"){
                $req['price_discount'] = 0;
            }else{
                $req['is_sale'] = 1;
            }
        }
        if($request->is_group_food){
            if(!$request->quantity_food || $request->quantity_food <= 0){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_quantity_food_not_found, null);
            }
        }
        if($request->type_food_id && $request->type_food_id != null){
            if(!TypeFood::find($request->type_food_id)){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_type_food_not_found, null);
            }
        }
        if ($request->file('image')) {
            $check_image = DB::table('food')->where('id', $id)->first();
            if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
                unlink(ltrim($check_image->image, '/'));
            }
            $req['image'] = $this->uploadImage($request->file('image'));
        }
        $check->update($req);
        if(isset($request->food_suggestions_id) && is_array($request->food_suggestions_id) && count($request->food_suggestions_id) > 0){
            $food_suggestions_id = array_unique($request->food_suggestions_id);
            FoodSuggestion::where('food_id', $id)->delete();
            foreach($food_suggestions_id as $f){
                $check2 = Food::where([['id', $f], ['id', '!=', $id], ['store_id', $check->store_id]])->first();
                if($check2){
                    FoodSuggestion::insert([
                        'food_id' => $id,
                        'food_suggestions_id' => $f
                    ]);
                }
            }
        }
        // if ($request->size && is_array(json_decode($request->size))) {
        //     FoodSize::where('food_id', $id)->delete();
        //     $size = json_decode($request->size);
        //     foreach ($size as $s) {
        //         FoodSize::insert([
        //             'food_id' => $id,
        //             'name' => $s->name,
        //             'quantity' => $s->quantity,
        //             'price' => $s->price,
        //         ]);
        //     }
        // }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function updateSizeFood($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Food::findorfail($id);
        $validator = Validator::make($request->all(), [
            'size' => 'required',
        ], [
            'size.required' => 'Vui lòng nhập size',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        // $check->update($req);
        if (is_array(json_decode($request->size))) {
            FoodSize::where('food_id', $id)->delete();
            $size = json_decode($request->size);
            foreach ($size as $s) {
                FoodSize::insert([
                    'food_id' => $id,
                    'name' => $s->name,
                    'price' => isset($s->price) ? $s->price : 0,
                ]);
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check->load('size'));
        }
        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::update_size_food_error, null);
    }
    public function addSizeFood(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'food_id' => 'required',
            'name' => 'required',
            'price' => 'required',
        ], [
            'food_id.required' => 'Vui lòng chọn món ăn',
            'name.required' => 'Vui lòng nhập tên size',
            'price.required' => 'Vui lòng nhập giá cộng thêm',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = Food::find($request->food_id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Không tìm thấy món ăn.", null);
        }
        $data = FoodSize::create($request->all());
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $data);
    }
    public function updateSizeFoodWeb($id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên size',
            'price.required' => 'Vui lòng nhập giá cộng thêm',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = FoodSize::find($id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Không tìm thấy size món ăn.", null);
        }
        $check->update($request->all());
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteSizeFood($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = FoodSize::where('id', $id)->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $data);
    }
    public function addToppingFood(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'category_topping_food_id' => 'required',
            'name' => 'required',
            'price' => 'required',
        ], [
            'category_topping_food_id.required' => 'Vui lòng chọn danh mục topping',
            'name.required' => 'Vui lòng nhập tên topping',
            'price.required' => 'Vui lòng nhập giá cộng thêm',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = CategoryToppingFood::find($request->category_topping_food_id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Không tìm thấy danh mục topping món ăn.", null);
        }
        $data = ToppingFood::create($request->all());
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $data);
    }
    public function updateToppingFoodWeb($id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên topping',
            'price.required' => 'Vui lòng nhập giá cộng thêm',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = ToppingFood::find($id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Không tìm thấy topping món ăn.", null);
        }
        $check->update($request->all());
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteToppingFood($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = ToppingFood::where('id', $id)->update([
            'status' => 0
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $data);
    }
    public function addCategoryToppingFood(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'food_id' => 'required',
            'name' => 'required',
            'limit' => 'required',
        ], [
            'food_id.required' => 'Vui lòng chọn món ăn',
            'name.required' => 'Vui lòng nhập tên size',
            'limit.required' => 'Vui lòng nhập giới hạn đặt món',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = Food::find($request->food_id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Không tìm thấy món ăn.", null);
        }
        $data = CategoryToppingFood::create($request->all());
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $data);
    }
    public function updateCategoryToppingFoodWeb($id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'food_id' => 'required',
            'name' => 'required',
            'limit' => 'required',
        ], [
            'food_id.required' => 'Vui lòng chọn món ăn',
            'name.required' => 'Vui lòng nhập tên size',
            'limit.required' => 'Vui lòng nhập giới hạn đặt món',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = CategoryToppingFood::find($id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Không tìm thấy danh mục topping món ăn.", null);
        }
        $check->update($request->all());
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteCategoryToppingFood($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = CategoryToppingFood::where('id', $id)->update([
            'status' => 0
        ]);
        ToppingFood::where('category_topping_food_id', $id)->update([
            'status' => 0
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $data);
    }
    public function listCategoryToppingFood($food_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = CategoryToppingFood::where([['food_id', $food_id], ['status', 1]])->with('toppingFood')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function updateCategoryToppingFood($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Food::findorfail($id);
        $validator = Validator::make($request->all(), [
            'category_topping' => 'required',
        ], [
            'category_topping.required' => 'Vui lòng nhập danh mục topping món ăn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        // $check->update($req);
        if (is_array(json_decode($request->category_topping))) {
            $category_topping = json_decode($request->category_topping);
            $array_name = [];
            foreach ($category_topping as $c) {
                $check_category = CategoryToppingFood::where([['food_id', $id], ['name', $c->name], ['status', 1]])->first();
                if ($check_category) {
                    $check_category->update([
                        'limit' => $c->limit,
                    ]);
                    $array_name[] = $c->name;
                } else {
                    CategoryToppingFood::insert([
                        'food_id' => $id,
                        'name' => $c->name,
                        'limit' => $c->limit,
                        'status' => 1
                    ]);
                    $array_name[] = $c->name;
                }
            }
            $update_status = CategoryToppingFood::whereNotIn('name', $array_name)->update([
                'status' => 0,
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check->load('categoryToppingFood'));
        }
        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::update_size_food_error, null);
    }
    public function updateToppingFood($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = CategoryToppingFood::findorfail($id);
        $validator = Validator::make($request->all(), [
            'topping_food' => 'required',
        ], [
            'topping_food.required' => 'Vui lòng nhập topping món ăn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        // $check->update($req);
        if (is_array(json_decode($request->topping_food))) {
            // ToppingFood::where('category_topping_food_id', $id)->delete();
            // $topping_food = json_decode($request->topping_food);
            // foreach ($topping_food as $t) {
            //     ToppingFood::insert([
            //         'category_topping_food_id' => $id,
            //         'name' => $t->name,
            //         'price' => $t->price,
            //     ]);
            // }
            $topping_food = json_decode($request->topping_food);
            $array_name = [];
            foreach ($topping_food as $t) {
                $check_topping_food = ToppingFood::where([['category_topping_food_id', $id], ['name', $t->name], ['status', 1]])->first();
                if ($check_topping_food) {
                    $check_topping_food->update([
                        'name' => $t->name,
                        'price' => isset($t->price) ? $t->price : 0,
                    ]);
                    $array_name[] = $t->name;
                } else {
                    ToppingFood::insert([
                        'category_topping_food_id' => $id,
                        'name' => $t->name,
                        'price' => isset($t->price) ? $t->price : 0,
                    ]);
                    $array_name[] = $t->name;
                }
            }
            $update_status = ToppingFood::whereNotIn('name', $array_name)->update([
                'status' => 0,
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check->load('toppingFood'));
        }
        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::update_size_food_error, null);
    }
    public function foodDetail($id)
    {
        $data = Food::findorfail($id)->load('size', 'categoryFood', 'categoryToppingFood.toppingFood', 'orderFoodDetail.user', 'store', 'typeFood', 'categoryStoreFood');
        if (count($data->categoryToppingFood) > 0) {
            foreach ($data->categoryToppingFood as $key => $c) {
                if (count($c->toppingFood) <= 0) {
                    unset($data->categoryToppingFood[$key]);
                }
            }
        }
        $ls_food_id = FoodSuggestion::where('food_id', $id)->pluck('food_suggestions_id')->toArray();
        $list_suggestions = Food::whereIn('id', $ls_food_id)->with('size', 'categoryToppingFood.toppingFood')->get();
        $data['list_suggestions'] = $list_suggestions;
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function deleteFood($id)
    {
        // try {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            $data = Food::findorfail($id);
            $check_staff = UserStore::where([['store_id', $data->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_staff) {
                $check_store_owner = Store::where([['id', $data->store_id], ['status', 1], ['user_id', $user->id]])->first();
                if (!$check_store_owner) {
                    return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
                }
            }
            FoodSize::where('food_id', $id)->delete();
            $card_food = CardFood::where('food_id', $id)->get();
            foreach ($card_food as $c) {
                CardToppingFood::where('card_food_id', $c->id)->delete();
            }
            CardFood::where('food_id', $id)->delete();
            $check_order_food_detail = OrderFoodDetail::where('food_id', $id)->pluck('order_food_id')->toArray();
            $order_detail_id = OrderFoodDetail::whereIn('order_food_id', $check_order_food_detail)->pluck('id')->toArray();
            OrderToppingFoodDetail::where('order_food_detail_id', $order_detail_id)->delete();
            OrderFoodDetail::whereIn('order_food_id', $check_order_food_detail)->delete();
            ReviewShipper::whereIn('order_food_id', $check_order_food_detail)->delete();
            OrderFood::whereIn('id', $check_order_food_detail)->delete();
            $check_combo_food_detail = ComboFoodDetail::where('food_id', $id)->get();
            foreach ($check_combo_food_detail as $cf) {
                $check_book_food1 = BookFood::where('combo_food_id', $cf->combo_food_id)->get();
                foreach ($check_book_food1 as $b1) {
                    if ($b1->status == SystemParam::status_booking_food_finish) {
                        $check_book_food2 = BookFood::where('book_table_id', $b1->book_table_id)->pluck('id')->toArray();
                        BookFoodTopping::whereIn('book_food_id', $check_book_food2)->delete();
                        BookFood::where('book_table_id', $b1->book_table_id)->delete();
                        BookTable::where('id', $b1->book_table_id)->delete();
                    } else {
                        $check_book_food2 = BookFood::where('book_table_id', $b1->book_table_id)->pluck('id')->toArray();
                        BookFoodTopping::whereIn('book_food_id', $check_book_food2)->delete();
                        BookFood::where('book_table_id', $b1->book_table_id)->delete();
                    }
                }
                $check_order_food_detail1 = OrderFoodDetail::where('combo_food_id', $cf->combo_food_id)->pluck('order_food_id')->toArray();
                $order_detail_id1 = OrderFoodDetail::whereIn('order_food_id', $check_order_food_detail1)->pluck('id')->toArray();
                OrderToppingFoodDetail::where('order_food_detail_id', $order_detail_id1)->delete();
                OrderFoodDetail::whereIn('order_food_id', $check_order_food_detail1)->delete();
                OrderFood::whereIn('id', $check_order_food_detail1)->delete();
                $card_food1 = CardFood::where('combo_food_id', $cf->combo_food_id)->pluck('id')->toArray();
                CardToppingFood::whereIn('card_food_id', $card_food1)->delete();
                CardFood::where('combo_food_id', $cf->combo_food_id)->delete();
                $order = ComboFoodDetail::where('combo_food_id', $cf->combo_food_id)->delete();
                ComboFood::where('id', $cf->combo_food_id)->delete();
            }
            $check_book_food = BookFood::where('food_id', $id)->get();
            foreach ($check_book_food as $b) {
                if ($b->status == SystemParam::status_booking_food_finish) {
                    $check_book_food2 = BookFood::where('book_table_id', $b->book_table_id)->pluck('id')->toArray();
                    BookFoodTopping::whereIn('book_food_id', $check_book_food2)->delete();
                    BookFood::where('book_table_id', $b->book_table_id)->delete();
                    BookTable::where('id', $b->book_table_id)->delete();
                } else {
                    $check_book_food2 = BookFood::where('book_table_id', $b->book_table_id)->pluck('id')->toArray();
                    BookFoodTopping::whereIn('book_food_id', $check_book_food2)->delete();
                    BookFood::where('book_table_id', $b->book_table_id)->delete();
                }
            }
            $check_program_store = ProgramStoreDetail::where('food_id', $id)->get();
            foreach ($check_program_store as $p) {
                $check_program = ProgramStoreDetail::where([['program_store_id', $p->program_store_id], ['id', '!=', $p->id]])->get();
                if (count($check_program) > 0) {
                    ProgramStoreDetail::where('id', $p->id)->delete();
                } else {
                    ProgramStoreDetail::where('program_store_id', $p->program_store_id)->delete();
                    ProgramStore::where('id', $p->program_store_id)->delete();
                }
            }
            $check_category_topping_food = CategoryToppingFood::where('food_id', $id)->pluck('id')->toArray();
            ToppingFood::whereIn('category_topping_food_id', $check_category_topping_food)->delete();
            CategoryToppingFood::where('food_id', $id)->delete();
            $check_image = DB::table('food')->where('id', $id)->first();
            if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
                unlink(ltrim($check_image->image, '/'));
            }
            $data->delete();
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $data);
        // } catch (\Exception $e) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::message_error_server, null);
        // }
    }
}
