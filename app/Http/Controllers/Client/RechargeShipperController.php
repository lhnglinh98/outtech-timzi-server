<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\RechargeShipper;
use App\Models\HistoryMoneyShipper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RechargeShipperController extends Controller
{
    //
    public function rechargeShipper(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'money' => 'required',
        ], [
            'money.required' => 'Vui lòng nhập số tiền',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        if($request->money <= 0){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập lại số tiền", null);
        }
        $req['shipper_id'] = $user->id;
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (RechargeShipper::where('code', $req['code'])->exists());
        $data = RechargeShipper::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::recharge_success, $data);
    }
    public function vnpayIPNRechargeShipper(Request $request)
    {
        try {
            $inputData = array();
            foreach ($request->all() as $key => $value) {
                if (substr($key, 0, 4) == "vnp_" && $value != null && trim($value) != "") {
                    $inputData[$key] = $value;
                }
            }
            unset($inputData['vnp_SecureHashType']);    
            unset($inputData['vnp_SecureHash']);
            ksort($inputData);
            $vnp_SecureHash = $request->vnp_SecureHash;
            $i = 0;
            $hashData = "";
            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashData = $hashData . '&' . $key . "=" . $value;
                } else {
                    $hashData = $hashData . $key . "=" . $value;
                    $i = 1;
                }
            }
            $vnp_HashSecret = "BY1ZJ3VEYNDVV5T2FRVTD9ZEGUGYW1EC";
            $secureHash = hash($request->vnp_SecureHashType, $vnp_HashSecret . $hashData);
            if ($request->vnp_Amount) {
                $vnp_Amount = $request->vnp_Amount / 100;
            }
            $check = RechargeShipper::where([['code', $request->vnp_TxnRef]])->first();
            if (!$check) {
                return response()->json([
                    'Message' => "Order Not Found",
                    'RspCode' => "01",
                ]);
            }
            if ($vnp_Amount != $check->money) {
                return response()->json([
                    'Message' => "Invalid amount",
                    'RspCode' => "04",
                ]);
            }
            if (trim($request->vnp_SecureHash) == trim($secureHash)) {
                if ($check->status_payment_vnpay == 2) {
                    return response()->json([
                        'Message' => "Order already confirmed",
                        'RspCode' => "02",
                    ]);
                }
                if ($request->vnp_ResponseCode != null) {
                    if ($request->vnp_ResponseCode == "00") {
                        RechargeShipper::where('code', $request->vnp_TxnRef)->update([
                            'status' => 1
                        ]);
                        $check_user = User::find($check->shipper_id);
                        if($check_user){
                            HistoryMoneyShipper::insert([
                                'shipper_id' => $check->shipper_id,
                                'money' => $check->money,
                                'type' => 1,
                                'type_order' => 1,
                                'content' => "Bạn đã nạp thành công số tiền " . number_format($check->money) . " VNĐ.",
                                'created_at' => date('Y-m-d H:i:s', time()),
                            ]);
                            $check_user->update([
                                'money' => $check_user->money + $check->money
                            ]); 
                        }
                    } else {
                        RechargeShipper::where('code', $request->vnp_TxnRef)->update([
                            'status' => 2,
                        ]);
                    }
                    if ($request->vnp_ResponseCode == "99") {
                        RechargeShipper::where('code', $request->vnp_TxnRef)->update([
                            'status' => 2,
                        ]);
                        return response()->json([
                            'Message' => "Confirm Success",
                            'RspCode' => "00",
                        ]);
                    }
                }
                return response()->json([
                    'Message' => "Confirm Success",
                    'RspCode' => "00",
                ]);
            } else {
                return response()->json([
                    'Message' => "Invalid Checksum",
                    'RspCode' => "97",
                ]);
            }
        } catch (\Exception $exception) {
            return response()->json([
                "RspCode" => "99",
                "Message" => "Unknow error"
            ]);
        }
    }
}
