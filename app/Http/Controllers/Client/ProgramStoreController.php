<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Food;
use App\Models\ProgramStore;
use App\Models\ProgramStoreDetail;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProgramStoreController extends Controller
{
    //
    public function listProgramStore(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        // }
        if ($request->store_id) {
            $data = ProgramStore::where('store_id', $request->store_id)->orderby('id', 'desc')->with('store', 'food')->paginate(12);
        } else {
            $data = ProgramStore::orderby('id', 'desc')->with('store', 'food')->paginate(12);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function programStoreDetail($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = ProgramStore::findorfail($id)->load('food', 'store');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createProgramStore(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'name' => 'required',
            'name_public' => 'required',
            'image' => 'required',
            'type_percent_or_money' => 'required',
            'content' => 'required',
            'time_open' => 'required|date',
            'time_close' => 'required|date|after_or_equal:time_open',
            'food_id' => 'required',
            'quantity' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng diễn ra chương trình',
            'name.required' => 'Vui lòng nhập tên chương trình',
            'name_public.required' => 'Vui lòng nhập tên chương trình hiển thị cho khách hàng',
            'image.required' => 'Vui lòng chọn ảnh chương trình',
            'type_percent_or_money.required' => 'Vui lòng chọn loại giảm giá',
            'time_open.required' => 'Vui lòng nhập thời gian bắt đầu',
            'time_open.date' => 'Vui lòng nhập đúng định dạng',
            'time_open.after_or_equal' => 'Vui lòng nhập thời gian bắt đầu sau ngày hôm nay',
            'time_close.date' => 'Vui lòng nhập đúng định dạng',
            'time_close.required' => 'Vui lòng nhập thời gian kết thúc',
            'time_close.after_or_equal' => 'Thời gian kết thúc không thể trước thời gian bắt đầu',
            'food_id.required' => 'Vui lòng chọn sản phẩm theo chương trình',
            'content.required' => 'Vui lòng nhập nội dung chương trình',
            'quantity.required' => 'Vui lòng nhập số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if (date('Y-m-d', strtotime($request->time_open)) < date('Y-m-d')) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập thời gian bắt đầu sau ngày hôm nay", null);
        }
        if (!Store::find($request->store_id)) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        if ($request->type_percent_or_money == 1) {
            if (!$request->percent) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập phần trăm giảm giá", null);
            }
            if ($request->percent > 100) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Phần trăm giảm giá không vượt quá 100%", null);
            }
        } else {
            if (!$request->money) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập số tiền giảm giá", null);
            }
        }
        if ($request->quantity <= 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập số lượng lớn hơn 0.", null);
        }
        $req = $request->all();
        $req['user_id'] = $user->id;
        $req['time_open'] = date('Y-m-d', strtotime($request->time_open));
        $req['time_close'] = date('Y-m-d', strtotime($request->time_close));
        $req['image'] = $this->uploadImage($request->file('image'));
        $req['status'] = 0;
        if (date('Y-m-d', strtotime($request->time_open)) == date('Y-m-d')) {
            $req['status'] = 1;
        }
        $program_store = ProgramStore::create($req);
        if (is_array($request->food_id)) {
            $ls_food_id = array_unique($request->food_id);
            foreach ($ls_food_id as $f) {
                if (Food::where([['id', $f], ['store_id', $request->store_id]])->first()) {
                    $program_store->food()->attach($f);
                }
            }
        }
        if (date('Y-m-d', strtotime($request->time_open)) == date('Y-m-d')) {
            $check_program = ProgramStore::where('status', 1)->where('id', $program_store->id)->with('food')->first();
            if (count($check_program->food) > 0) {
                foreach ($check_program->food as $f) {
                    if ($check_program->type_percent_or_money == 1) {
                        $price = $f->price - $f->price * $check_program->percent / 100;
                        if ($f->price_discount > 0) {
                            $price = $f->price_discount - $f->price_discount * $check_program->percent / 100;
                        }
                    } else {
                        $price = $f->price - $check_program->money;
                        if ($f->price_discount > 0) {
                            $price = $f->price_discount - $check_program->money;
                        }
                    }
                    $update = Food::where('id', $f->id)->update([
                        'price_discount_with_program' => $price,
                    ]);
                }
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $program_store);
    }
    public function updateProgramStore($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = ProgramStore::findorfail($id);
        $validator = Validator::make($request->all(), [
            // 'store_id' => 'required',
            // 'name' => 'required',
            // 'name_public' => 'required',
            // 'type_percent_or_money' => 'required',
            // 'time_open' => 'required|date',
            // 'time_close' => 'required|date|after_or_equal:time_open',
            // 'food_id' => 'required',
            // 'content' => 'required',
            'quantity' => 'required',
        ], [
            // 'store_id.required' => 'Vui lòng chọn cửa hàng diễn ra chương trình',
            // 'name.required' => 'Vui lòng nhập tên chương trình',
            // 'name_public.required' => 'Vui lòng nhập tên chương trình hiển thị cho khách hàng',
            // 'type_percent_or_money.required' => 'Vui lòng chọn loại giảm giá',
            // 'time_open.required' => 'Vui lòng nhập thời gian bắt đầu',
            // 'time_open.date' => 'Vui lòng nhập đúng định dạng',
            // 'time_open.after_or_equal' => 'Vui lòng nhập thời gian bắt đầu sau ngày hôm nay',
            // 'time_close.date' => 'Vui lòng nhập đúng định dạng',
            // 'time_close.required' => 'Vui lòng nhập thời gian kết thúc',
            // 'time_close.after_or_equal' => 'Thời gian kết thúc không thể trước thời gian bắt đầu',
            // 'food_id.required' => 'Vui lòng chọn sản phẩm theo chương trình',
            // 'content.required' => 'Vui lòng nhập nội dung chương trình',
            'quantity.required' => 'Vui lòng nhập số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        // if (!Store::find($request->store_id)) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        // }
        // if (date('Y-m-d', strtotime($request->time_open)) < date('Y-m-d')) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập thời gian bắt đầu sau ngày hôm nay", null);
        // }
        $req = $request->only('quantity');
        // $req['user_id'] = $user->id;
        // $req['time_open'] = date('Y-m-d', strtotime($request->time_open));
        // $req['time_close'] = date('Y-m-d', strtotime($request->time_close));
        // if ($request->quantity <= 0) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập số lượng lớn hơn 0.", null);
        // }
        // if ($request->type_percent_or_money == 1) {
        //     if (!$request->percent) {
        //         return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập phần trăm giảm giá", null);
        //     }
        //     if ($request->percent > 100) {
        //         return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Phần trăm giảm giá không vượt quá 100%", null);
        //     }
        // } else {
        //     if (!$request->money) {
        //         return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập số tiền giảm giá", null);
        //     }
        // }
        // if ($request->file('image')) {
        //     $check_image = DB::table('program_store')->where('id', $id)->first();
        //     if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
        //         unlink(ltrim($check_image->image, '/'));
        //     }
        //     $req['image'] = $this->uploadImage($request->file('image'));
        // }
        $check->update($req);
        // if (is_array($request->food_id)) {
        //     $ls_food_id = array_unique($request->food_id);
        //     $check->food()->detach();
        //     foreach ($ls_food_id as $f) {
        //         if (Food::where([['id', $f], ['store_id', $request->store_id]])->first()) {
        //             $check->food()->attach($f);
        //         }
        //     }
        // }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function stopProgramStore($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = ProgramStore::findorfail($id);
        $check_food = ProgramStoreDetail::where('program_store_id', $id)->get();
        foreach ($check_food as $c) {
            Food::where('id', $c->food_id)->update([
                'price_discount_with_program' => 0,
            ]);
        }
        $check->update([
            'is_active' => 0,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::stop_success, $check);
    }
    public function activeProgramStore($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = ProgramStore::where([['id', $id]])->first();
        if (!$check || $check->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Không tìm thấy chương trình hoặc chương trình của bạn đã hết hạn.", null);
        }
        $check_food = ProgramStoreDetail::where('program_store_id', $id)->get();
        if($check->is_active == 0){
            foreach ($check_food as $l) {
                $f = Food::where('id', $l->food_id)->first();
                if($f){
                    if ($check->type_percent_or_money == 1) {
                        $price = $f->price - $f->price * $check->percent / 100;
                        if ($f->price_discount > 0) {
                            $price = $f->price_discount - $f->price_discount * $check->percent / 100;
                        }
                    } else {
                        $price = $f->price - $check->money;
                        if ($f->price_discount > 0) {
                            $price = $f->price_discount - $check->money;
                        }
                    }
                    $update = Food::where('id', $l->food_id)->update([
                        'price_discount_with_program' => $price,
                    ]);
                }
            }
            $check->update([
                'is_active' => 1,
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::active_success, $check);

        }else{
            $check->update([
                'is_active' => 0,
            ]);
            foreach ($check_food as $d) {
                Food::where('id', $d->food_id)->update([
                    'price_discount_with_program' => 0,
                ]);
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::stop_success, $check);
        }
    }
    public function deleteProgramStore($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = ProgramStore::findorfail($id);
        $check_food = ProgramStoreDetail::where('program_store_id', $id)->get();
        foreach ($check_food as $c) {
            Food::where('id', $c->food_id)->update([
                'price_discount_with_program' => 0,
            ]);
        }
        $check_image = DB::table('program_store')->where('id', $id)->first();
        if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
            unlink(ltrim($check_image->image, '/'));
        }
        $check->food()->detach();
        $check->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check);
    }
    public function confirmProgramStore($id)
    {
        $check = ProgramStore::findorfail($id)->update([
            'status' => 1,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
}
