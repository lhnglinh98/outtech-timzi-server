<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\HistoryMoneyShipper;
use App\Models\ShipperStore;
use App\Models\Store;
use App\Models\UserStore;
use App\Models\WithdrawMoney;
use App\Models\SetupWithdrawMoney;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class ShipperController extends Controller
{
    //
    protected $notifycontroller;
    public function __construct()
    {
        $this->notifycontroller = new NotifyController();
    }
    public function listHistoryMoney()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = HistoryMoneyShipper::where('shipper_id', $user->id)->orderby('id', 'desc')->paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStoreShipperConfirm(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $list_store_id = ShipperStore::where([['user_id', $user->id], ['status', 0]])->pluck('store_id')->toArray();
        $where = [];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        $data = Store::where($where)->whereIn('id', $list_store_id)->orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStoreShipper(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $list_store_id = ShipperStore::where([['user_id', $user->id], ['status', 1]])->pluck('store_id')->toArray();
        $where = [];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        $data = Store::where($where)->whereIn('id', $list_store_id)->orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function confirmStoreWithShipper($store_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $store = Store::findorfail($store_id);
        $check = ShipperStore::where([['user_id', $user->id], ['store_id', $store_id], ['status', 0]])->first();
        if (!$check) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_shipper_store_confirm_not_found, null);
        }
        $check->update([
            'status' => 1,
        ]);
        $id = UserStore::where([['is_owner', 1], ['status', 1], ['store_id', $store_id]])->pluck('user_id')->toArray();
        $id = [$store->user_id];
        $this->notifycontroller->pushNotify($id, 'Người giao hàng tên ' . $user->name . ' đã xác nhận làm người giao hàng chính cho cửa hàng ' . $store->name . '.', $store->id, SystemParam::type_store, $store, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $check);
    }
    public function cancelStoreWithShipper($store_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $store = Store::findorfail($store_id);
        $check = ShipperStore::where([['user_id', $user->id], ['store_id', $store_id]])->first();
        if (!$check) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_shipper_store_cancel_not_found, null);
        }
        $check->delete();
        $id = UserStore::where([['is_owner', 1], ['status', 1], ['store_id', $store_id]])->pluck('user_id')->toArray();
        $id = [$store->user_id];
        $this->notifycontroller->pushNotify($id, 'Người giao hàng tên ' . $user->name . ' đã không xác nhận làm người giao hàng chính cho cửa hàng ' . $store->name . '.', $store->id, SystemParam::type_store, $store, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check);
    }
    public function withdrawMoney(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'money' => 'required',
        ], [
            'money.required' => 'Vui lòng nhập số tiền muốn rút',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if($request->money > $user->money){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Số dư trong tài khoản không đủ để thực hiện giao dịch.", null);
        }
        $checkWithdrawMoney = SetupWithdrawMoney::first();
        if($checkWithdrawMoney && $request->money < $checkWithdrawMoney->money){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Số tiền tối thiểu bạn có thể rút là " . number_format($checkWithdrawMoney->money) . ' VNĐ.', null);
        }
        $data = WithdrawMoney::create([
            'shipper_id' => $user->id,
            'money' => $request->money,
        ]);
        $user->update([
            'money' => $user->money - $request->money
        ]);
        $admin_location2 = User::where([['role_id', 13], ['manage_province_id', $user->province_id], ['manage_district_id', null]])
            ->pluck('id')->toArray();
        $admin_location3 = User::Where([['role_id', 14], ['manage_province_id', $user->province_id], ['manage_district_id', $user->district_id]])
            ->pluck('id')->toArray();
        $admin = array_merge($admin_location2, $admin_location3);
        $this->notifycontroller->pushNotify($admin, 'Shipper ' . $user->name . ' có yêu cầu rút tiền với số tiền là ' . number_format($request->money) . ' VNĐ', $user->id, SystemParam::withdraw_money, $user, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
}
