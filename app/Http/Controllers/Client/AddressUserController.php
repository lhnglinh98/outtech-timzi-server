<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\AddressUser;
use App\Models\Province;
use App\Models\District;
use App\Models\Ward;
use Illuminate\Support\Facades\Validator;
use App\Http\Utils\SystemParam;
use GuzzleHttp\Client;
class AddressUserController extends Controller
{
    //
    public function listProvince()
    {
        $data = Province::get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listDistrict(Request $request)
    {
        $data = [];
        if ($request->province_id) {
            $data = District::where('province_id', $request->province_id)->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listWard(Request $request)
    {
        $data = [];
        if ($request->province_id && $request->district_id) {
            $data = Ward::where([['province_id', $request->province_id], ['district_id', $request->district_id]])->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listAddressUser(){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = AddressUser::where('user_id', $user->id)->with('user')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function addressUserDetail($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = AddressUser::findorfail($id);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createAddressUser(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'user_name' => 'required|string|max:255',
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'address' => 'required',
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'user_name.required' => 'Vui lòng nhập tên',
            'user_name.string' => 'Tên phải là kiểu chuỗi',
            'user_name.max' => 'Tên không quá :max kí tự',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'address.required' => 'Vui lòng nhập địa chỉ cụ thể',
            'latitude.required' => 'Vui lòng nhập vĩ độ của bạn',
            'longtidue.required' => 'Vui lòng nhập kinh độ của bạn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if((float)$request->latitude == 0 || (float)$request->latitude == 0){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $req = $request->all();
        $req['user_id'] = $user->id;
        $create = AddressUser::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function updateAddressUser($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check = AddressUser::where([['id', $id], ['user_id', $user->id]])->firstorfail();
        $validator = Validator::make($request->all(), [
            'user_name' => 'required|string|max:255',
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'address' => 'required',
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'user_name.required' => 'Vui lòng nhập tên',
            'user_name.string' => 'Tên phải là kiểu chuỗi',
            'user_name.max' => 'Tên không quá :max kí tự',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'address.required' => 'Vui lòng nhập địa chỉ cụ thể',
            'latitude.required' => 'Vui lòng nhập vĩ độ của bạn',
            'longtidue.required' => 'Vui lòng nhập kinh độ của bạn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if((float)$request->latitude == 0 || (float)$request->latitude == 0){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $req = $request->all();
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteAddressUser($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = AddressUser::where([['id', $id], ['user_id', $user->id]])->firstorfail();
        $data->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $data);
    }
    public function updateAddressDefault($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = AddressUser::where([['id', $id], ['user_id', $user->id]])->firstorfail();
        $data->update([
            'type' => 1
        ]);
        $update = AddressUser::where([['id', '!=', $id], ['user_id', $user->id]])->update([
            'type' => 0
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $data);
    }
}
