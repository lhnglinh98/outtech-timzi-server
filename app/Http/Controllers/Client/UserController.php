<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\SpeedSMSAPI;
use App\Http\Utils\SystemParam;
use App\Models\CoinHistory;
use App\Models\Store;
use App\Models\UserStore;
use App\User;
use Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
// use Tymon\JWTAuth\JWTAuth;

class UserController extends Controller
{
    //
    public function tinhTien(Request $request){
        return $this->googleApi($request->latitude1, $request->longtidue1, $request->latitude2, $request->longtidue2);
    }
    public function callGoogleApiGeocode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $client = new Client();
        $data1 = $client->get("https://rsapi.goong.io/Geocode?api_key=vROxUKAJSC9lvVOW8QLidwgmEQtW94V0WhrvqIQp&latlng=$request->latitude,$request->longtidue");
        $response = json_decode($data1->getBody()->getContents(), true);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $response);
    }
    public function callGoogleApiPlace(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'place_id' => 'required',
        ], [
            'place_id.required' => 'Vui lòng nhập place_id',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $client = new Client();
        $data1 = $client->get("https://rsapi.goong.io/Place/Detail?place_id=$request->place_id&api_key=vROxUKAJSC9lvVOW8QLidwgmEQtW94V0WhrvqIQp");
        $response = json_decode($data1->getBody()->getContents(), true);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $response);
    }
    public function callGoogleApiAutoComplete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'input' => 'required',
        ], [
            'input.required' => 'Vui lòng nhập input',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $client = new Client();
        $data1 = $client->get("https://rsapi.goong.io/Place/AutoComplete?input=$request->input&api_key=vROxUKAJSC9lvVOW8QLidwgmEQtW94V0WhrvqIQp");
        $response = json_decode($data1->getBody()->getContents(), true);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $response);
    }
    public function updateDeviceId(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'device_id' => 'required',
        ], [
            'device_id.required' => 'Vui lòng gửi lên mã máy',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $user->update([
            'device_id' => $request->device_id,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $user);
    }
    public function loginUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'password' => 'required',
        ], [
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'password.required' => 'Vui lòng nhập mật khẩu',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $input = $request->only('phone', 'password', 'device_id');
        $user = null;
        if ($request->phone) {
            $user = User::where([['phone', $input['phone']], ['role_id', 5], ['status', '!=', 3]])->first();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_user_not_found, null);
            }
            if (!Hash::check($request->password, $user->password)) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_user_error_password, null);
            }
            if ($user->status == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_error_status, null);
            }
            if ($user->status == 2) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
            }
            // if ($user->status == 3) {
            //     return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_delete, null);
            // }
        }
        $token = null;
        if (!$token = JWTAuth::fromUser($user, ['exp' => Carbon\Carbon::now()->addDays(7)->timestamp])) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_error, null);
        }
        $data1 = array(
            'token' => $token,
            'user' => $user->load('role'),
        );
        if ($request->device_id) {
            $update = User::where([['phone', $input['phone']]])->update([
                'device_id' => $request->device_id,
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::login_success, $data1);
    }
    public function loginShipper(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'password' => 'required',
        ], [
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'password.required' => 'Vui lòng nhập mật khẩu',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $input = $request->only('phone', 'password', 'device_id');
        $user = null;
        if ($request->phone) {
            $user = User::where([['phone', $input['phone']], ['role_id', 4], ['status', '!=', 3]])->first();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_user_not_found, null);
            }
            if (!Hash::check($request->password, $user->password)) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_user_error_password, null);
            }
            if ($user->status == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_error_status, null);
            }
            if ($user->status == 2) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_no_confirm, null);
            }
            // if ($user->status == 3) {
            //     return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_delete, null);
            // }
        }
        $token = null;
        if (!$token = JWTAuth::fromUser($user, ['exp' => Carbon\Carbon::now()->addDays(7)->timestamp])) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_error, null);
        }
        // if($user != null && $user->token != null){
        //     // return 'Bearer  ' . $user->token;
        //     JWTAuth::parseToken()->invalidate('Bearer ' . $user->token);
        // }
        // if($user != null){
        //     // $user->update([
        //     //     'token' => $token
        //     // ]);
        // }
        $data1 = array(
            'token' => $token,
            'user' => $user->load('role'),
            'token_old' => $user->token,
        );
        if ($user != null) {
            $user->update([
                'token' => $token,
            ]);
        }
        if ($request->device_id) {
            $update = User::where([['phone', $input['phone']]])->update([
                'device_id' => $request->device_id,
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::login_success, $data1);
    }
    public function loginStaff(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'password' => 'required',
        ], [
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'password.required' => 'Vui lòng nhập mật khẩu',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $input = $request->only('phone', 'password', 'device_id');
        $user = null;
        if ($request->phone) {
            $user = User::where([['phone', $input['phone']], ['role_id', 2], ['status', '!=', 3]])->orWhere([['phone', $input['phone']], ['role_id', 3], ['status', '!=', 3]])->orWhere([['phone', $input['phone']], ['role_id', 6], ['status', '!=', 3]])->first();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_user_not_found, null);
            }
            if (!Hash::check($request->password, $user->password)) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_user_error_password, null);
            }
            if ($user->status == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_error_status, null);
            }
            if ($user->status == 2) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
            }
            // if ($user->status == 3) {
            //     return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_delete, null);
            // }
        }
        $token = null;
        if (!$token = JWTAuth::fromUser($user, ['exp' => Carbon\Carbon::now()->addDays(7)->timestamp])) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_error, null);
        }
        $data1 = array(
            'token' => $token,
            'user' => $user->load('role', 'staffStore'),
        );
        if ($request->device_id) {
            $update = User::where([['phone', $input['phone']]])->update([
                'device_id' => $request->device_id,
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::login_success, $data1);
    }
    public function registerCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'password' => 'required|min:6|max:100|confirmed',
        ], [
            'name.required' => 'Vui lòng nhập tên người dùng',
            'name.string' => 'Tên người dùng phải là kiểu chuỗi',
            'name.max' => 'Tên người dùng không quá :max kí tự',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.min' => "Mật khẩu không dưới :min kí tự!",
            'password.max' => "Mật khẩu không quá :max kí tự!",
            'password.confirmed' => "Xác nhận mật khẩu không chính xác!",
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($request->phone && User::where([['phone', $request->phone], ['status', '!=', 3]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_phone_already_exist, null);
        }
        $req = $request->only('name', 'phone', 'password', 'province_id', 'district_id');
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (User::where('code', $req['code'])->exists());
        $req['role_id'] = 5;
        $user = User::create($req);
        do {
            $otp = rand(1000, 9999);
        } while (User::where('otp', $otp)->exists());
        $sms_api = new SpeedSMSAPI(config('constants.sms_api'));
        $content = $otp . " la ma xac nhan cua ban tai TIMZI (Ma xac nhan chi ton tai 20 phut)";
        $response = $sms_api->sendSMS([$request->phone], $content, SpeedSMSAPI::SMS_TYPE_BRANDNAME, "TIMZI");
        if ($response['status'] == 'success') {
            $user->update([
                'otp' => $otp,
                'date_check_code' => date('Y-m-d H:i:s', time()),
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $user);
    }
    public function registerShipper(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'password' => 'required|min:6|max:100|confirmed',
            'avatar' => 'required',
            'driver_license_image' => 'required',
            'vehicle_registration_image' => 'required',
            'image_cmnd_front' => 'required',
            'image_cmnd_back' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên người dùng',
            'name.string' => 'Tên người dùng phải là kiểu chuỗi',
            'name.max' => 'Tên người dùng không quá :max kí tự',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.min' => "Mật khẩu không dưới :min kí tự!",
            'password.max' => "Mật khẩu không quá :max kí tự!",
            'password.confirmed' => "Xác nhận mật khẩu không chính xác!",
            'avatar.required' => 'Vui lòng chọn ảnh đại diện',
            'driver_license_image.required' => 'Vui lòng chọn ảnh giấy phép lái xe',
            'vehicle_registration_image.required' => 'Vui lòng chọn ảnh đăng ký xe',
            'image_cmnd_front.required' => 'Vui lòng chọn ảnh chứng minh thư mặt trước',
            'image_cmnd_back.required' => 'Vui lòng chọn ảnh chứng minh thư mặt sau',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($request->phone && User::where([['phone', $request->phone], ['status', '!=', 3]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_phone_already_exist, null);
        }
        $req = $request->only('name', 'phone', 'password', 'province_id', 'district_id');
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (User::where('code', $req['code'])->exists());
        $req['role_id'] = 4;
        $req['avatar'] = $this->uploadImage($request->file('avatar'));
        $req['driver_license_image'] = $this->uploadImage($request->file('driver_license_image'));
        $req['vehicle_registration_image'] = $this->uploadImage($request->file('vehicle_registration_image'));
        $req['image_cmnd_front'] = $this->uploadImage($request->file('image_cmnd_front'));
        $req['image_cmnd_back'] = $this->uploadImage($request->file('image_cmnd_back'));
        $req['status_shipper'] = 0;
        $req['status'] = 2;
        $user = User::create($req);
        do {
            $otp = rand(1000, 9999);
        } while (User::where('otp', $otp)->exists());
        $sms_api = new SpeedSMSAPI(config('constants.sms_api'));
        $content = $otp . " la ma xac nhan cua ban tai TIMZI (Ma xac nhan chi ton tai 20 phut)";
        $response = $sms_api->sendSMS([$request->phone], $content, SpeedSMSAPI::SMS_TYPE_BRANDNAME, "TIMZI");
        if ($response['status'] == 'success') {
            $user->update([
                'otp' => $otp,
                'date_check_code' => date('Y-m-d H:i:s', time()),
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $user);
    }
    public function checkOtpRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
        ], [
            'otp.required' => 'Vui lòng nhập mã xác nhận',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = User::where([['otp', $request->otp]])->orderby('id', 'desc')->first();
        if ($check && $check->role_id == 4) {
            if (strtotime($check->date_check_code) + 20 * 60 >= time() && $check->status == 2) {
                $token = JWTAuth::fromUser($check, ['exp' => Carbon\Carbon::now()->addDays(7)->timestamp]);
                $check->update([
                    'otp' => null,
                    'date_check_code' => null,
                    'status' => 1,
                    'token' => $token,
                ]);
                return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::check_code_verify_success, $token);
            }
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::check_code_verify_error, null);
        } else {
            if ($check && strtotime($check->date_check_code) + 20 * 60 >= time() && $check->status == 0) {
                $token = JWTAuth::fromUser($check, ['exp' => Carbon\Carbon::now()->addDays(7)->timestamp]);
                $check->update([
                    'otp' => null,
                    'date_check_code' => null,
                    'status' => 1,
                    'token' => $token,
                ]);
                return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::check_code_verify_success, $token);
            }
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::check_code_verify_error, null);
        }
    }
    public function uploadImageInfomation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar' => 'required',
            'driver_license_image' => 'required',
            'vehicle_registration_image' => 'required',
            'image_cmnd_front' => 'required',
            'image_cmnd_back' => 'required',
        ], [
            'avatar.required' => 'Vui lòng chọn ảnh đại diện',
            'driver_license_image.required' => 'Vui lòng chọn ảnh giấy phép lái xe',
            'vehicle_registration_image.required' => 'Vui lòng chọn ảnh đăng ký xe',
            'image_cmnd_front.required' => 'Vui lòng chọn ảnh chứng minh thư mặt trước',
            'image_cmnd_back.required' => 'Vui lòng chọn ảnh chứng minh thư mặt sau',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $req['avatar'] = $this->uploadImage($request->file('avatar'));
        $req['driver_license_image'] = $this->uploadImage($request->file('driver_license_image'));
        $req['vehicle_registration_image'] = $this->uploadImage($request->file('vehicle_registration_image'));
        $req['image_cmnd_front'] = $this->uploadImage($request->file('image_cmnd_front'));
        $req['image_cmnd_back'] = $this->uploadImage($request->file('image_cmnd_back'));
        $req['status_shipper'] = 1;
        $user->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::upload_image_infomation_success, $user);
    }
    public function registerStaff(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'password' => 'required|min:6|max:100|confirmed',
        ], [
            'name.required' => 'Vui lòng nhập tên người dùng',
            'name.string' => 'Tên người dùng phải là kiểu chuỗi',
            'name.max' => 'Tên người dùng không quá :max kí tự',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.min' => "Mật khẩu không dưới :min kí tự!",
            'password.max' => "Mật khẩu không quá :max kí tự!",
            'password.confirmed' => "Xác nhận mật khẩu không chính xác!",
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->only('name', 'phone', 'password', 'province_id', 'district_id');
        if ($request->phone && User::where([['phone', $request->phone], ['status', '!=', 3]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_phone_already_exist, null);
        }
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (User::where('code', $req['code'])->exists());
        $req['role_id'] = 3;
        $user = User::create($req);
        do {
            $otp = rand(1000, 9999);
        } while (User::where('otp', $otp)->exists());
        $sms_api = new SpeedSMSAPI(config('constants.sms_api'));
        $content = $otp . " la ma xac nhan cua ban tai TIMZI (Ma xac nhan chi ton tai 20 phut)";
        $response = $sms_api->sendSMS([$request->phone], $content, SpeedSMSAPI::SMS_TYPE_BRANDNAME, "TIMZI");
        if ($response['status'] == 'success') {
            $user->update([
                'otp' => $otp,
                'date_check_code' => date('Y-m-d H:i:s', time()),
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $user);
    }
    public function logout(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $token = $request->header('Authorization');
        if (!$token) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::user_not_found, null);
        }
        // return $token;
        JWTAuth::parseToken()->invalidate($token);
        $user->update([
            'token' => null,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::logout_success, null);
    }
    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
        ], [
            'phone.required' => 'Vui lòng nhập số điện thoại',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_phone = User::where([['phone', $request->phone], ['status', 1]])->first();
        if (!$check_phone) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::user_not_found, null);
        }
        do {
            $otp = rand(1000, 9999);
        } while (User::where('otp', $otp)->exists());
        $sms_api = new SpeedSMSAPI(config('constants.sms_api'));
        $content = $otp . " la ma xac nhan cua ban tai TIMZI (Ma xac nhan chi ton tai 20 phut)";
        $response = $sms_api->sendSMS([$request->phone], $content, SpeedSMSAPI::SMS_TYPE_BRANDNAME, "TIMZI");
        if ($response['status'] == 'success') {
            $check_phone->update([
                'otp' => $otp,
                'date_check_code' => date('Y-m-d H:i:s', time()),
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::send_otp_create_user_investment_holding_success, $check_phone);
        }
        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::send_otp_create_user_investment_holding_error, null);

    }
    public function checkOTPVerify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
        ], [
            'otp.required' => 'Vui lòng gửi lên mã otp',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = User::where([['otp', $request->otp]])->first();
        if ($check && strtotime($check->date_check_code) + 20 * 60 >= time()) {
            $check->update([
                'otp' => null,
                'date_check_code' => null,
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::check_code_verify_success, $check);
        }
        return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::check_code_verify_error, null);
    }
    public function updateNewPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'password' => 'required|min:6|max:100|confirmed',
        ], [
            'phone.required' => 'Vui lòng nhập số điện thoại hoặc email',
            'password.required' => "Vui lòng nhập mật khẩu mới!",
            'password.min' => "Mật khẩu không dưới :min kí tự!",
            'password.max' => "Mật khẩu không quá :max kí tự!",
            'password.confirmed' => "Xác nhận mật khẩu không chính xác!",
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $user = User::where('phone', $request->phone)->orwhere('email', $request->phone)->first();
        if ($user) {
            $user->update([
                'password' => $request->password,
            ]);
            $token = JWTAuth::fromUser($user, ['exp' => Carbon\Carbon::now()->addDays(7)->timestamp]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_new_password_success, $token);
        } else {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::user_not_found, null);
        }
    }
    public function getUserInfo()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id == 3 || $user->role_id == 6) {
            $store = UserStore::where([['user_id', $user->id], ['status', 1]])->first();
            if ($store) {
                $data_store = Store::where([['id', $store->store_id], ['status', 1]])->first();
                $user['store'] = $data_store;
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $user->load('role', 'staffStore'));
    }
    public function updateUser(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'sometimes|string|max:255',
        ], [
            'name.string' => 'Tên người dùng phải là kiểu chuỗi',
            'name.max' => 'Tên người dùng không quá :max kí tự',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->except(['role_id', 'phone']);
        if ($request->email && User::where([['id', '<>', $user->id], ['email', $request->email]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_email_already_exist, null);
        }
        if ($request->birthday) {
            $req['birthday'] = date('Y-m-d H:i:s', strtotime($request->birthday));
        }
        if ($request->file('avatar')) {
            $check_avatar = DB::table('users')->where('id', $user->id)->first();
            if (file_exists(ltrim($check_avatar->avatar, '/')) && $check_avatar->avatar != "") {
                unlink(ltrim($check_avatar->avatar, '/'));
            }
            $req['avatar'] = $this->uploadImage($request->file('avatar'));
        }
        $user->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $user);
    }
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'current_password',
            'password' => 'required|min:6|max:50|confirmed',
        ], [
            'current_password.current_password' => "Mật khẩu cũ không chính xác!",
            'password.required' => "Vui lòng nhập mật khẩu mới!",
            'password.min' => "Mật khẩu mới không dưới :min kí tự!",
            'password.max' => "Mật khẩu mới không quá :max kí tự!",
            'password.confirmed' => "Xác nhận mật khẩu mới không chính xác!",
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $input = $request->only('password');
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $user->update($input);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_password_success, $user);
    }
    public function updateLocation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $input = $request->only('latitude', 'longtidue');
        $user->update($input);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $user);
    }
    public function listCoinHistory()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = CoinHistory::where('user_id', $user->id)->orderby('id', 'desc')->paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, " ", $data);
    }
}
