<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Material;
use App\Models\Supplier;
use App\Models\TypeMaterial;
use App\Models\Store;
use App\Models\UserStore;
use App\Http\Utils\SystemParam;
use Illuminate\Support\Facades\Validator;
class MaterialController extends Controller
{
    public function listMaterial($store_id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $data = Material::where([['store_id', $store_id], ['status', 1]])->with('supplier', 'type')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function MaterialDetail($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Material::where('id', $id)->first();
        if(!$data){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_Material_not_found, null);
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $data->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $data->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data->load('material.type'));
    }
    public function createMaterial(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'type_material_id' => 'required',
            'supplier' => 'required',
            'name' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'type_material_id.required' => 'Vui lòng chọn loại vật tư',
            'supplier.required' => 'Vui lòng chọn nhà cung cấp',
            'name.required' => 'Vui lòng nhập tên vật tư',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $req = $request->all();
        $create = Material::create($req);
        if (is_array($request->supplier)) {
            $ls_supplier_id = array_unique($request->supplier);
            foreach ($ls_supplier_id as $s) {
                if (Supplier::where([['status', 1], ['id', $s]])->first()) {
                    $create->supplier()->attach($s);
                }
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create->load('supplier'));
    }
    public function updateMaterial($id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Material::find($id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_Material_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'type_material_id' => 'required',
            'supplier' => 'required',
            'name' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'type_material_id.required' => 'Vui lòng chọn loại vật tư',
            'supplier.required' => 'Vui lòng chọn nhà cung cấp',
            'name.required' => 'Vui lòng nhập tên vật tư',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $req = $request->all();
        $check->update($req);
        if (is_array($request->supplier)) {
            $ls_supplier_id = array_unique($request->supplier);
            $check->supplier()->detach();
            foreach ($ls_supplier_id as $s) {
                if (Supplier::where([['status', 1], ['id', $s]])->first()) {
                    $check->supplier()->attach($s);
                }
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check->load('supplier'));
    }
    public function deleteMaterial($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Material::find($id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_Material_not_found, null);
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $check->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $check->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $check->supplier()->detach();
        $check->update([
            'status' => 0
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check);
    }
}
