<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Banner;
use App\Models\BookFood;
use App\Models\BookFoodTopping;
use App\Models\BookTable;
use App\Models\CardFood;
use App\Models\CardToppingFood;
use App\Models\Category;
use App\Models\CategoryFood;
use App\Models\CategoryPermanent;
use App\Models\CategoryStoreDetail;
use App\Models\CategoryStoreFood;
use App\Models\CategoryToppingFood;
use App\Models\ComboFood;
use App\Models\Food;
use App\Models\FoodSize;
use App\Models\OrderFood;
use App\Models\OrderFoodDetail;
use App\Models\OrderToppingFoodDetail;
use App\Models\ProgramDetail;
use App\Models\ProgramStore;
use App\Models\ProgramStoreDetail;
use App\Models\RevenueStore;
use App\Models\ReviewShipper;
use App\Models\ShipperStore;
use App\Models\Store;
use App\Models\Suggestion;
use App\Models\TableMerge;
use App\Models\TableStore;
use App\Models\ToppingFood;
use App\Models\UserStore;
use App\Models\Voucher;
use App\User;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    //
    protected $notifycontroller;
    public function __construct()
    {
        $this->notifycontroller = new NotifyController();
    }
    public function dashboardStore(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if($request->store_id){
            $data = Store::where('id', $request->store_id)->get();
        }else{
            $data = Store::where('user_id', $user->id)->get();
        }
        foreach($data as $store){
            $store['count_order_food'] = OrderFood::where('store_id', $store->id)->count();
            $store['count_book_table'] = BookTable::where('store_id', $store->id)->count();
            $store['count_food'] = Food::where('store_id', $store->id)->count();
            $store['total_revenue'] = RevenueStore::where('store_id', $store->id)->get()->SUM('money');
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCategoryFoodWithClient(){
        $data = CategoryFood::where('status', 1)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCategoryInCreateStore(){
        $data = Category::where('status', 1)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function storeDetailInWeb($id){
        $data = Store::find($id);
        if(!$data){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data->load('categoryStoreDetail', 'category'));
    }
    public function openStore($store_id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Store::find($store_id);
        if(!$data){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check = UserStore::where([['store_id', $store_id], ['status', 1], ['is_owner', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        if($data->is_open == 1){
            $data->update([
                'is_open' => 0
            ]);
        }else{
            $data->update([
                'is_open' => 1
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $data);
    }
    public function listStoreWebsite(Request $request)
    {
        $where = [];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        if ($request->limit) {
            $data = Store::where('status', 1)->where($where)->orderby('id', 'desc')->limit(5)->get();
        } else {
            $data = Store::where('status', 1)->where($where)->orderby('id', 'desc')->paginate(20);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function storeDetailWebsite($id)
    {
        $data = Store::where('id', $id)->with('categoryStoreDetail')->first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listSuggestion(Request $request)
    {
        $where = [];
        if ($request->category_id) {
            $where[] = ['category_id', $request->category_id];
        }
        $data = Suggestion::where($where)->orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function searchStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $where = [['stores.status', 1]];
        if ($request->name) {
            $where[] = ['stores.name', 'LIKE', '%' . $request->name . '%'];
        }
        if ($request->category_id) {
            $where[] = ['cs.category_id', $request->category_id];
        }
        $store_id1 = Store::join('category_store as cs', 'cs.store_id', 'stores.id')->where($where)->pluck('stores.id')->toArray();
        $where2 = [];
        if ($request->name) {
            $where2[] = ['food.name', 'LIKE', '%' . $request->name . '%'];
        }
        if ($request->category_id) {
            $where2[] = ['cs.category_id', $request->category_id];
        }
        $store_id2 = Food::join('stores as s', 's.id', 'food.store_id')
            ->join('category_store as cs', 'cs.store_id', 's.id')
            ->where($where2)->where('s.status', 1)->pluck('food.store_id')->toArray();
        $store_id = array_merge($store_id1, $store_id2);
        $store_id = array_unique($store_id);
        $data = Store::whereIn('id', $store_id)->with(['programStore' => function ($query1) {
            $query1->where([['program_store.status', 1], ['program_store.is_active', 1]])->orderby('program_store.percent', 'desc')->limit(2)->get();
        }])->with('categoryStoreDetail')->get();
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        if (count($data) > 0) {
            foreach ($data as $i) {
                $latitude_store = $i->latitude;
                $longtidue_store = $i->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                // return $response;
                if (isset($response)) {
                    $i['distance'] = $response;
                    $i['distance_value'] = $response['value'];
                } else {
                    $i['distance'] = 0;
                    $i['distance_value'] = 0;
                }
            }
            $store_new = $data->sortBy('distance_value');
            unset($data);
            $data = $store_new->values()->all();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);

    }
    public function listTableEmpty($book_table_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::findorfail($book_table_id);
        $id1 = TableMerge::where('book_table_id', $book_table_id)->pluck('table_store_id')->toArray();
        $id2 = TableStore::where([['store_id', $check->store_id], ['status', SystemParam::status_table_store_empty]])->pluck('id')->toArray();
        $ls_id = array_merge($id1, $id2);
        $data = TableStore::whereIn('id', $ls_id)->get();
        foreach ($data as $d) {
            if (in_array($d->id, $id1)) {
                $d['is_merge'] = 1;
            } else {
                $d['is_merge'] = 0;
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listRevenueStore($store_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = UserStore::where([['store_id', $store_id], ['status', 1], ['is_owner', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $where = [];
        if ($request->month) {
            $month = date('Y-m', strtotime($request->month));
            $where[] = ['month', 'LIKE', '%' . $month . '%'];
        }
        $data1 = RevenueStore::where($where)->where('store_id', $store_id)->orderby('id', 'desc')->paginate(12);
        $check_money = RevenueStore::where($where)->where('store_id', $store_id)->get();
        $total_money = $check_money->SUM('money');
        $data = array(
            'list_revenue' => $data1,
            'total_money' => $total_money,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listRevenueFoodStore($store_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = UserStore::where([['store_id', $store_id], ['status', 1], ['is_owner', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $where1 = [['store_id', $store_id]];
        $where2 = [['store_id', $store_id]];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where1[] = ['date_time', '>=', $from_date];
            $where2[] = ['time_booking', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where1[] = ['date_time', '<=', $to_date];
            $where2[] = ['time_booking', '<=', $to_date];
        }
        $check_order_food = OrderFood::where($where1)->where('is_payment_store', 1)->get();
        $check_book_table = BookTable::where($where2)->where('status', SystemParam::status_booking_table_has_served)->get();
        $total_money = $check_order_food->SUM('total_money') - $check_order_food->SUM('fee_ship') + $check_book_table->SUM('total_money');
        $ls_id_order_food = OrderFood::where($where1)->where('is_payment_store', 1)->pluck('id')->toArray();
        $ls_food_id1 = OrderFoodDetail::whereIn('order_food_id', $ls_id_order_food)->where('quantity', '>', 0)->pluck('food_id')->toArray();
        $ls_food_id1 = array_unique($ls_food_id1);
        $ls_combo_id1 = OrderFoodDetail::whereIn('order_food_id', $ls_id_order_food)->where('quantity', '>', 0)->pluck('combo_food_id')->toArray();
        $ls_combo_id1 = array_unique($ls_combo_id1);
        $ls_id_book_table = BookTable::where($where2)->where('status', SystemParam::status_booking_table_has_served)->pluck('id')->toArray();
        $ls_food_id2 = BookFood::whereIn('book_table_id', $ls_id_book_table)->where('quantity', '>', 0)->pluck('food_id')->toArray();
        $ls_food_id2 = array_unique($ls_food_id2);
        $ls_combo_id2 = BookFood::whereIn('book_table_id', $ls_id_book_table)->where('quantity', '>', 0)->pluck('combo_food_id')->toArray();
        $ls_combo_id2 = array_unique($ls_combo_id2);
        $ls_food_id = array_merge($ls_food_id1, $ls_food_id2);
        $ls_combo_id = array_merge($ls_combo_id1, $ls_combo_id2);
        $list_food = Food::whereIn('id', $ls_food_id)->get();
        $list_combo = ComboFood::whereIn('id', $ls_combo_id)->get();
        foreach ($list_food as $f) {
            $quantity_order = OrderFoodDetail::whereIn('order_food_id', $ls_id_order_food)->where('food_id', $f->id)->get()->SUM('quantity');
            $quantity_book_table = BookFood::whereIn('book_table_id', $ls_id_book_table)->where('food_id', $f->id)->get();
            $f['total_quantity'] = $quantity_order + $quantity_book_table->SUM('quantity');
            $f['total_quantity_minus'] = $quantity_book_table->SUM('quantity_minus_group_food');
        }
        foreach ($list_combo as $c) {
            $quantity_order2 = OrderFoodDetail::whereIn('order_food_id', $ls_id_order_food)->where('combo_food_id', $c->id)->get()->SUM('quantity');
            $quantity_book_table2 = BookFood::whereIn('book_table_id', $ls_id_book_table)->where('combo_food_id', $c->id)->get();
            $c['total_quantity'] = $quantity_order2 + $quantity_book_table2->SUM('quantity');
            $c['total_quantity_minus'] = $quantity_book_table2->SUM('quantity_minus_group_food');
        }
        $data = array(
            'list_food' => $list_food,
            'list_combo_food' => $list_combo,
            'total_money' => $total_money,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function discountThisMonthStore($store_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = UserStore::where([['store_id', $store_id], ['status', 1], ['is_owner', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $data = RevenueStore::where([['store_id', $store_id], ['status', '>', 1]])->orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listDiscountStore($store_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = UserStore::where([['store_id', $store_id], ['status', 1], ['is_owner', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $where = [];
        if ($request->month) {
            $month = date('Y-m', strtotime($request->month));
            $where[] = ['month', 'LIKE', '%' . $month . '%'];
        }
        $data1 = RevenueStore::where($where)->where([['store_id', $store_id], ['status', 1]])->orderby('id', 'desc')->paginate(12);
        $check_money = RevenueStore::where($where)->where([['store_id', $store_id], ['status', 1]])->get();
        $total_money = $check_money->SUM('fee_money');
        $data = array(
            'list_revenue' => $data1,
            'total_money' => $total_money,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function vnpayIPNPaymentFeeRevenue(Request $request)
    {
        try {
            $inputData = array();
            foreach ($request->all() as $key => $value) {
                if (substr($key, 0, 4) == "vnp_" && $value != null && trim($value) != "") {
                    $inputData[$key] = $value;
                }
            }
            unset($inputData['vnp_SecureHashType']);
            unset($inputData['vnp_SecureHash']);
            ksort($inputData);
            $vnp_SecureHash = $request->vnp_SecureHash;
            $i = 0;
            $hashData = "";
            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashData = $hashData . '&' . $key . "=" . $value;
                } else {
                    $hashData = $hashData . $key . "=" . $value;
                    $i = 1;
                }
            }
            $vnp_HashSecret = "88CO54399XDYPLDD6WK8BG854K8NMZWF";
            $secureHash = hash($request->vnp_SecureHashType, $vnp_HashSecret . $hashData);
            if ($request->vnp_Amount) {
                $vnp_Amount = $request->vnp_Amount / 100;
            }
            $check = RevenueStore::where([['code', $request->vnp_TxnRef]])->with('store')->first();
            if (!$check) {
                return response()->json([
                    'Message' => "Order Not Found",
                    'RspCode' => "01",
                ]);
            }
            if ($vnp_Amount != $check->fee_money) {
                return response()->json([
                    'Message' => "Invalid amount",
                    'RspCode' => "04",
                ]);
            }
            if (trim($request->vnp_SecureHash) == trim($secureHash)) {
                if ($check->status_payment_vnpay == 1 || $check->status_payment_vnpay == 4) {
                    return response()->json([
                        'Message' => "Order already confirmed",
                        'RspCode' => "02",
                    ]);
                }
                if ($request->vnp_ResponseCode != null) {
                    if ($request->vnp_ResponseCode == "00") {
                        RevenueStore::where('code', $request->vnp_TxnRef)->update([
                            'status' => 1,
                        ]);
                        $month = date('m', strtotime($check->month));
                        $year = date('Y', strtotime($check->month));
                        $admin = User::where('role_id', 1)->pluck('id')->toArray();
                        $this->notifycontroller->pushNotify($admin, 'Cửa hàng ' . $check->store->name . ' đã đóng phí chiết khấu doanh thu tháng ' . $month . ' năm ' . $year . ' với số tiền là ' . ($check->fee_money ) . 'VNĐ', $check->store->id, SystemParam::type_revenue_store, $check->store, $check->store->name);
                    } else {
                        RevenueStore::where('code', $request->vnp_TxnRef)->update([
                            'status' => 4,
                        ]);
                    }
                    if ($request->vnp_ResponseCode == "99") {
                        RevenueStore::where('code', $request->vnp_TxnRef)->update([
                            'status' => 4,
                        ]);
                        return response()->json([
                            'Message' => "Confirm Success",
                            'RspCode' => "00",
                        ]);
                    }
                }
                return response()->json([
                    'Message' => "Confirm Success",
                    'RspCode' => "00",
                ]);
            } else {
                return response()->json([
                    'Message' => "Invalid Checksum",
                    'RspCode' => "97",
                ]);
            }
        } catch (\Exception $exception) {
            return response()->json([
                "RspCode" => "99",
                "Message" => "Unknow error",
            ]);
        }
    }
    // public function confirmPaymentFeeRevenueStore($id, Request $request)
    // {
    //     $user = $this->getAuthenticatedUser();
    //     if (!$user) {
    //         return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
    //     }
    //     if ($user->role_id != 2 && $user->role_id != 6) {
    //         return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
    //     }
    //     $check_revenue = RevenueStore::where([['id', $id]])->firstorfail();
    //     $store = Store::findorfail($check_revenue->store_id);
    //     if ($check_revenue->status < 2) {
    //         return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_revenue_wait_payment, null);
    //     }
    //     $check = UserStore::where([['store_id', $check_revenue->store_id], ['status', 1], ['is_owner', 1], ['user_id', $user->id]])->first();
    //     if (!$check) {
    //         $check_store = Store::where([['id', $check_revenue->store_id], ['status', 1], ['user_id', $user->id]])->first();
    //         if (!$check_store) {
    //             return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
    //         }
    //     }
    //     $check_revenue->update([
    //         'status' => 1,
    //     ]);
    //     $month = date('m', strtotime($check_revenue->month));
    //     $year = date('Y', strtotime($check_revenue->month));
    //     $admin = User::where('role_id', 1)->pluck('id')->toArray();
    //     $this->notifycontroller->pushNotify($admin, 'Cửa hàng ' . $store->name . ' đã đóng phí chiết khấu doanh thu tháng ' . $month . ' năm ' . $year . ' với số tiền là ' . ($check_revenue->money * $store->percent_discount_revenue / 100) . 'VNĐ', $store->id, SystemParam::type_revenue_store, $store);
    //     return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_payment_success, $check_revenue);
    // }
    public function deleteStore($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_store = Store::where([['id', $id], ['user_id', $user->id]])->first();
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check_book_table = BookTable::where('store_id', $id)->pluck('id')->toArray();
        $check_book_food = BookFood::whereIn('book_table_id', $check_book_table)->pluck('id')->toArray();
        Banner::where('store_id', $id)->delete();
        BookFoodTopping::whereIn('book_food_id', $check_book_food)->delete();
        BookFood::whereIn('book_table_id', $check_book_table)->delete();
        BookTable::where('store_id', $id)->delete();
        $table_store = TableStore::where('store_id', $id)->delete();
        $check_program_store = ProgramStore::where('store_id', $id)->pluck('id')->toArray();
        ProgramStoreDetail::whereIn('program_store_id', $check_program_store)->delete();
        ProgramStore::where('store_id', $id)->delete();
        ProgramDetail::where('store_id', $id)->delete();
        $card_food = CardFood::where('store_id', $id)->pluck('id')->toArray();
        CardToppingFood::whereIn('card_food_id', $card_food)->delete();
        CardFood::where('store_id', $id)->delete();
        $check_order_food = OrderFood::where('store_id', $id)->pluck('id')->toArray();
        $order_detail_id1 = OrderFoodDetail::whereIn('order_food_id', $check_order_food)->pluck('id')->toArray();
        OrderToppingFoodDetail::where('order_food_detail_id', $order_detail_id1)->delete();
        OrderFoodDetail::whereIn('order_food_id', $check_order_food)->delete();
        ReviewShipper::whereIn('order_food_id', $check_order_food)->delete();
        OrderFood::where('store_id', $id)->delete();
        UserStore::where('store_id', $id)->delete();
        ShipperStore::where('store_id', $id)->delete();
        Voucher::where('store_id', $id)->delete();
        ComboFood::where('store_id', $id)->delete();
        $food = Food::where('store_id', $id)->pluck('id')->toArray();
        FoodSize::whereIn('food_id', $food)->delete();
        $check_category_topping_food = CategoryToppingFood::whereIn('food_id', $food)->pluck('id')->toArray();
        ToppingFood::whereIn('category_topping_food_id', $check_category_topping_food)->delete();
        CategoryToppingFood::whereIn('food_id', $food)->delete();
        DB::table('category_store')->where('store_id', $id)->delete();
        CategoryStoreDetail::where('store_id', $id)->delete();
        CategoryStoreFood::where('store_id', $id)->delete();
        Store::where('id', $id)->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check_store);
    }
    public function listCategoryWithStore()
    {
        $data = CategoryFood::where('status', 1)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $ls_category = Category::where([['status', 1]])->get();
        $where = [['status', 1]];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        $listStoreRecently = Store::where($where)->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
            $query1->where([['program_store.status', 1], ['program_store.is_active', 1]])->orderby('program_store.percent', 'desc')->limit(2)->get();
        }])->get();
        if (count($listStoreRecently) > 0) {
            foreach ($listStoreRecently as $i) {
                $latitude_store = $i->latitude;
                $longtidue_store = $i->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                // return $response;
                if (isset($response)) {
                    $i['distance'] = $response;
                    $i['distance_value'] = $response['value'];
                } else {
                    $i['distance'] = 0;
                    $i['distance_value'] = 0;
                }
            }
            $store_new = $listStoreRecently->sortBy('distance_value');
            unset($listStoreRecently);
            $listStoreRecently = $store_new->values()->all();
        }

        //quán yêu thích gần đây start
        $whereRecently = [['status', 1],['percent_discount_revenue','>=',7]];
        if ($request->name) {
            $whereRecently[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        $listStoreRecently2 = Store::where($whereRecently)->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
            $query1->where([['program_store.status', 1], ['program_store.is_active', 1]])->orderby('program_store.percent', 'desc')->limit(2)->get();
        }])->get();
        // dd($listStoreRecently2);
        if (count($listStoreRecently2) > 0) {
            foreach ($listStoreRecently2 as $i) {
                $latitude_store = $i->latitude;
                $longtidue_store = $i->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                // return $response;
                if (isset($response)) {
                    $i['distance'] = $response;
                    $i['distance_value'] = $response['value'];
                } else {
                    $i['distance'] = 0;
                    $i['distance_value'] = 0;
                }
            }
            $store_new = $listStoreRecently2->sortBy('distance_value');
            unset($listStoreRecently2);
            $listStoreRecently2 = $store_new->values()->all();
        }
        //quán yêu thích gần đây end

        $where2 = [['stores.status', 1], ['program_store.status', 1]];
        if ($request->name) {
            $where2[] = ['stores.name', 'LIKE', '%' . $request->name . '%'];
        }
        // if ($request->limit) {
        $listStoreNew = Store::select('stores.*')->join('program_store', 'program_store.store_id', 'stores.id')->where($where2)->where([['program_store.status', 1], ['program_store.is_active', 1], ['program_store.quantity', '>', 0]])->with('categoryStoreDetail')->with('programStore')->orderby('stores.id', 'desc')->limit(10)->get();
        // } else {
        //     $listStoreNew = Store::select('stores.*')->join('program_store', 'program_store.store_id', 'stores.id')->where($where2)->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
        //         $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
        //     }])->orderby('stores.id', 'desc')->get();
        // }
        if (count($listStoreNew) > 0) {
            foreach ($listStoreNew as $d) {
                $latitude_store = $d->latitude;
                $longtidue_store = $d->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                // return $response;
                if (isset($response)) {
                    $d['distance'] = $response;
                    $d['distance_value'] = $response['value'];
                } else {
                    $d['distance'] = 0;
                    $d['distance_value'] = 0;
                }
            }
            $store_new = $listStoreNew->sortBy('distance_value');
            unset($listStoreNew);
            $listStoreNew = $store_new->values()->all();
        }
        $store_id = Voucher::where([['type', 2], ['status', 2], ['store_id', null]])->pluck('store_id')->toArray();
        $listStorePartner = Store::where($where)->whereIn('id', $store_id)->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
            $query1->where([['program_store.status', 1], ['program_store.is_active', 1]])->orderby('program_store.percent', 'desc')->limit(2)->get();
        }])->orderby('id', 'desc')->get();
        if (count($listStorePartner) > 0) {
            foreach ($listStorePartner as $a) {
                $latitude_store = $a->latitude;
                $longtidue_store = $a->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                // return $response;
                if (isset($response)) {
                    $a['distance'] = $response;
                    $a['distance_value'] = $response['value'];
                } else {
                    $a['distance'] = 0;
                    $a['distance_value'] = 0;
                }
            }
            $store_new = $listStorePartner->sortBy('distance_value');
            unset($listStorePartner);
            $listStorePartner = $store_new->values()->all();
        }
        // if ($request->limit) {
        $listStoreManyOrders = Store::where($where)->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
            $query1->where([['program_store.status', 1], ['program_store.is_active', 1]])->orderby('program_store.percent', 'desc')->limit(2)->get();
        }])->orderby('count_order', 'desc')->limit(10)->get();
        // } else {
        //     $listStoreManyOrders = Store::where($where)->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
        //         $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
        //     }])->orderby('count_order', 'desc')->get();
        // }

        if (count($listStoreManyOrders) > 0) {
            foreach ($listStoreManyOrders as $m) {
                $latitude_store = $m->latitude;
                $longtidue_store = $m->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                // return $response;
                if (isset($response)) {
                    $m['distance'] = $response;
                    $m['distance_value'] = $response['value'];
                } else {
                    $m['distance'] = 0;
                    $m['distance_value'] = 0;
                }
            }
            $store_new = $listStoreManyOrders->sortBy('distance_value');
            unset($listStoreManyOrders);
            $listStoreManyOrders = $store_new->values()->all();
        }
        if (count($listStoreRecently) > 0) {
            $listStoreRecently = array_slice($listStoreRecently, 0, 15);
        }
        // if ($request->limit && count($listStoreNew) > 0) {
        //     $listStoreNew = array_slice($listStoreNew, 0, $request->limit);
        // }
        if (count($listStorePartner) > 0) {
            $listStorePartner = array_slice($listStorePartner, 0, 4);
        }
        // if ($request->limit && count($listStoreManyOrders) > 0) {
        //     $listStoreManyOrders = array_slice($listStoreManyOrders, 0, $request->limit);
        // }
        $list_program_store = ProgramStore::where([['status', 1], ['is_active', 1], ['quantity', '>', 0]])->orderby('id', 'desc')->with('store')->limit(10)->get();
        $list_banner = Banner::all();
        $data = array(
            'list_category' => $ls_category,
            'list_store_recently' => $listStoreRecently,
            'list_store_recently_2' => $listStoreRecently2,
            'list_store_new' => $listStoreNew,
            'list_store_partner' => $listStorePartner,
            'list_store_many_orders' => $listStoreManyOrders,
            'list_program_store' => $list_program_store,
            'list_banner' => $list_banner,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStoreInHome(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
            'type' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ',
            'longtidue.required' => 'Vui lòng nhập kinh độ',
            'type.required' => 'Vui lòng truyền loại danh sách cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        if ($request->type == 1) {
            $where = [['status', 1]];
            if ($request->name) {
                $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
            }
            $data = Store::where($where)->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
                $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
            }])->get();
            $latitude = $request->latitude;
            $longtidue = $request->longtidue;
            if (count($data) > 0) {
                foreach ($data as $i) {
                    $latitude_store = $i->latitude;
                    $longtidue_store = $i->longtidue;
                    $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                    // return $response;
                    if (isset($response)) {
                        $i['distance'] = $response;
                        $i['distance_value'] = $response['value'];
                    } else {
                        $i['distance'] = 0;
                        $i['distance_value'] = 0;
                    }
                }
                $store_new = $data->sortBy('distance_value');
                unset($data);
                $data = $store_new->values()->all();
            }
            if ($request->limit && count($data) > 0) {
                $data = array_slice($data, 0, $request->limit);
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
        } else if ($request->type == 2) {
            $where = [['stores.status', 1], ['program_store.status', 1]];
            if ($request->name) {
                $where[] = ['stores.name', 'LIKE', '%' . $request->name . '%'];
            }
            // if ($request->limit) {
            //     $data = Store::where($where)->orderby('id', 'desc')->limit($request->limit)->get();
            // } else {
            $data = Store::select('stores.*')->join('program_store', 'program_store.store_id', 'stores.id')->where($where)->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
                $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
            }])->orderby('stores.id', 'desc')->get();
            // }
            $latitude = $request->latitude;
            $longtidue = $request->longtidue;
            if (count($data) > 0) {
                foreach ($data as $i) {
                    $latitude_store = $i->latitude;
                    $longtidue_store = $i->longtidue;
                    $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                    // return $response;
                    if (isset($response)) {
                        $i['distance'] = $response;
                        $i['distance_value'] = $response['value'];
                    } else {
                        $i['distance'] = 0;
                        $i['distance_value'] = 0;
                    }
                }
                $store_new = $data->sortBy('distance_value');
                unset($data);
                $data = $store_new->values()->all();
            }
            if ($request->limit && count($data) > 0) {
                $data = array_slice($data, 0, $request->limit);
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
        } else if ($request->type == 3) {
            $where = [['status', 1]];
            if ($request->name) {
                $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
            }

            $store_id = Voucher::where([['type', 2], ['status', 2], ['store_id', null]])->pluck('store_id')->toArray();
            // if ($request->limit) {
            //     $data = Store::where($where)->whereIn('id', $store_id)->orderby('id', 'desc')->limit($request->limit)->get();
            // }else{
            $data = Store::where($where)->with('categoryStoreDetail')->whereIn('id', $store_id)->with(['programStore' => function ($query1) {
                $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
            }])->orderby('id', 'desc')->get();
            // }
            $latitude = $request->latitude;
            $longtidue = $request->longtidue;
            if (count($data) > 0) {
                foreach ($data as $i) {
                    $latitude_store = $i->latitude;
                    $longtidue_store = $i->longtidue;
                    $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                    // return $response;
                    if (isset($response)) {
                        $i['distance'] = $response;
                        $i['distance_value'] = $response['value'];
                    } else {
                        $i['distance'] = 0;
                        $i['distance_value'] = 0;
                    }
                }
                $store_new = $data->sortBy('distance_value');
                unset($data);
                $data = $store_new->values()->all();
            }
            if ($request->limit && count($data) > 0) {
                $data = array_slice($data, 0, $request->limit);
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
        } else {
            $where = [['status', 1]];
            if ($request->name) {
                $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
            }
            // if ($request->limit) {
            //     $data = Store::where($where)->orderby('count_order', 'desc')->limit($request->limit)->get();
            // } else {
            $data = Store::where($where)->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
                $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
            }])->orderby('count_order', 'desc')->get();
            // }
            $latitude = $request->latitude;
            $longtidue = $request->longtidue;
            if (count($data) > 0) {
                foreach ($data as $i) {
                    $latitude_store = $i->latitude;
                    $longtidue_store = $i->longtidue;
                    $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                    // return $response;
                    if (isset($response)) {
                        $i['distance'] = $response;
                        $i['distance_value'] = $response['value'];
                    } else {
                        $i['distance'] = 0;
                        $i['distance_value'] = 0;
                    }
                }
                $store_new = $data->sortBy('distance_value');
                unset($data);
                $data = $store_new->values()->all();
            }
            if ($request->limit && count($data) > 0) {
                $data = array_slice($data, 0, $request->limit);
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
        }
    }
    public function listStoreRecently(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ',
            'longtidue.required' => 'Vui lòng nhập kinh độ',

        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $where = [['status', 1]];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        $data = Store::where($where)->with(['programStore' => function ($query1) {
            $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
        }])->get();
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        if (count($data) > 0) {
            foreach ($data as $i) {
                $latitude_store = $i->latitude;
                $longtidue_store = $i->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                // return $response;
                if (isset($response)) {
                    $i['distance'] = $response;
                    $i['distance_value'] = $response['value'];
                } else {
                    $i['distance'] = 0;
                    $i['distance_value'] = 0;
                }
            }
            $store_new = $data->sortBy('distance_value');
            unset($data);
            $data = $store_new->values()->all();
        }
        if ($request->limit && count($data) > 0) {
            $data = array_slice($data, 0, $request->limit);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStoreNew(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ',
            'longtidue.required' => 'Vui lòng nhập kinh độ',

        ]);
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $where = [['status', 1]];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        // if ($request->limit) {
        //     $data = Store::where($where)->orderby('id', 'desc')->limit($request->limit)->get();
        // } else {
        $data = Store::where($where)->with(['programStore' => function ($query1) {
            $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
        }])->orderby('id', 'desc')->get();
        // }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        if (count($data) > 0) {
            foreach ($data as $i) {
                $latitude_store = $i->latitude;
                $longtidue_store = $i->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                // return $response;
                if (isset($response)) {
                    $i['distance'] = $response;
                    $i['distance_value'] = $response['value'];
                } else {
                    $i['distance'] = 0;
                    $i['distance_value'] = 0;
                }
            }
            $store_new = $data->sortBy('distance_value');
            unset($data);
            $data = $store_new->values()->all();
        }
        if ($request->limit && count($data) > 0) {
            $data = array_slice($data, 0, $request->limit);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStorePartner(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ',
            'longtidue.required' => 'Vui lòng nhập kinh độ',

        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $where = [['status', 1]];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }

        $store_id = Voucher::where([['type', 2], ['status', 2], ['store_id', null]])->pluck('store_id')->toArray();
        // if ($request->limit) {
        //     $data = Store::where($where)->whereIn('id', $store_id)->orderby('id', 'desc')->limit($request->limit)->get();
        // }else{
        $data = Store::where($where)->whereIn('id', $store_id)->with(['programStore' => function ($query1) {
            $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
        }])->orderby('id', 'desc')->get();
        // }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        if (count($data) > 0) {
            foreach ($data as $i) {
                $latitude_store = $i->latitude;
                $longtidue_store = $i->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                // return $response;
                if (isset($response)) {
                    $i['distance'] = $response;
                    $i['distance_value'] = $response['value'];
                } else {
                    $i['distance'] = 0;
                    $i['distance_value'] = 0;
                }
            }
            $store_new = $data->sortBy('distance_value');
            unset($data);
            $data = $store_new->values()->all();
        }
        if ($request->limit && count($data) > 0) {
            $data = array_slice($data, 0, $request->limit);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStoreManyOrders(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ',
            'longtidue.required' => 'Vui lòng nhập kinh độ',

        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $where = [['status', 1]];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        // if ($request->limit) {
        //     $data = Store::where($where)->orderby('count_order', 'desc')->limit($request->limit)->get();
        // } else {
        $data = Store::where($where)->with(['programStore' => function ($query1) {
            $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
        }])->orderby('count_order', 'desc')->get();
        // }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        if (count($data) > 0) {
            foreach ($data as $i) {
                $latitude_store = $i->latitude;
                $longtidue_store = $i->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                if (isset($response)) {
                    $i['distance'] = $response;
                    $i['distance_value'] = $response['value'];
                } else {
                    $i['distance'] = 0;
                    $i['distance_value'] = 0;
                }
            }
            $store_new = $data->sortBy('distance_value');
            unset($data);
            $data = $store_new->values()->all();
        }
        if ($request->limit && count($data) > 0) {
            $data = array_slice($data, 0, $request->limit);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function confirmStore($store_id)
    {
        $update = Store::findorfail($store_id)->update([
            'status' => 1,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $update);
    }
    public function listStoreWithCustomer(Request $request)
    {
        $where = [];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        $data = Store::where($where)->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
            $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
        }])->orderby('id', 'desc')->paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStoreWithCategory($category_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $where = [['stores.status', 1]];
        if ($request->name) {
            $where[] = ['stores.name', 'LIKE', '%' . $request->name . '%'];
        }
        $data = Category::where([['status', 1], ['id', $category_id]])->firstorfail()->load(['store' => function ($query) use ($where) {
            $query->where($where)->orderby('stores.id', 'desc')->with('categoryStoreDetail')->with(['programStore' => function ($query1) {
                $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->limit(2)->get();
            }])->paginate(12);
        }]);
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        if (count($data['store']) > 0) {
            foreach ($data['store'] as $i) {
                $latitude_store = $i->latitude;
                $longtidue_store = $i->longtidue;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                // return $response;
                if (isset($response)) {
                    $i['distance'] = $response;
                    $i['distance_value'] = $response['value'];
                } else {
                    $i['distance'] = 0;
                    $i['distance_value'] = 0;
                }
            }
            $store_new = $data['store']->sortBy('distance_value');
            unset($data['store']);
            $data['store'] = $store_new->values()->all();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function storeDetailWithCustomer($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $token = $request->header('Authorization');
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            $where = [['user_id', $user->id], ['store_id', $id]];
            $data = Store::findorfail($id)->load('categoryStoreDetail', 'tableStore')->load(['comboFood' => function ($query) use ($where) {
                $query->where('combo_food.status', 1)->with('food.typeFood')->get();
            }])->load(['programStore' => function ($query1) use ($where) {
                $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->with('food.size', 'food.categoryToppingFood.toppingFood', 'food.typeFood')->get();
            }]);
            $data['is_eat_store'] = 0;
            $check_category_store = DB::table('category_store')->select('c.*')->join('category as c', 'c.id', 'category_store.category_id')->where('category_store.store_id', $id)->get();
            foreach ($check_category_store as $cs) {
                if ($cs->is_eat_store == 1) {
                    $data['is_eat_store'] = 1;
                    break;
                }
            }
            $latitude_store = $data->latitude;
            $longtidue_store = $data->longtidue;
            $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
            if (isset($response)) {
                $data['distance'] = $response;
            } else {
                $data['distance'] = 0;
            }
            foreach ($data->programStore as $p) {
                if (count($p->food) > 0) {
                    foreach ($p->food as $p1) {
                        $check_food_program = CardFood::where([['food_id', $p1->id], ['status', 0], ['user_id', $user->id]])->get();
                        $p1['count_card'] = $check_food_program->SUM('quantity');
                        $p1['card'] = $check_food_program->load('toppingFood');
                    }
                }
            }
            foreach ($data->comboFood as $cf) {
                $check_combo_food = CardFood::where([['combo_food_id', $cf->id], ['status', 0], ['user_id', $user->id]])->get();
                $cf['count_card'] = $check_combo_food->SUM('quantity');
                $cf['card'] = $check_combo_food->load('toppingFood');
            }
            $ls_category_food_id = Food::where('store_id', $id)->pluck('category_food_id')->toArray();
            $category_food_id = array_unique($ls_category_food_id);
            $category_food = CategoryFood::where('status', 1)->whereIn('id', $category_food_id)->get()->load(['food' => function ($query3) use ($id) {
                $query3->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->get();
            }]);
            foreach ($category_food as $c) {
                if (count($c->food) > 0) {
                    foreach ($c->food as $c1) {
                        $check_combo_food = CardFood::where([['food_id', $c1->id], ['status', 0], ['user_id', $user->id]])->get();
                        $c1['count_card'] = $check_combo_food->SUM('quantity');
                        $c1['card'] = $check_combo_food->load('toppingFood');
                    }
                }
            }
            $ls_category_store_food_id = Food::where('store_id', $id)->pluck('category_store_food_id')->toArray();
            $category_store_food_id = array_unique($ls_category_store_food_id);
            $category_store_food = CategoryStoreFood::whereIn('id', $category_store_food_id)->get()->load(['food' => function ($query4) use ($id) {
                $query4->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->get();
            }]);
            foreach ($category_store_food as $s) {
                if (count($s->food) > 0) {
                    foreach ($s->food as $s1) {
                        $check_combo_food = CardFood::where([['food_id', $s1->id], ['status', 0], ['user_id', $user->id]])->get();
                        $s1['count_card'] = $check_combo_food->SUM('quantity');
                        $s1['card'] = $check_combo_food->load('toppingFood');
                    }
                }
            }
            $table_id_booked = null;
            $data3 = CardFood::where([['store_id', $id], ['user_id', $user->id], ['status', 0]])->with('food', 'store', 'toppingFood', 'comboFood.food')->get();
            $check_book_table = BookTable::where([['user_id', $user->id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service])->first();
            $data1 = array(
                "store" => $data,
                "category_food" => $category_food,
                "category_store_food" => $category_store_food,
                'list_card' => $data3,
                'total_quantity' => $data3->SUM('quantity'),
                'total_price' => $data3->SUM('price'),
                'book_table' => $check_book_table,
            );
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data1);
        } else {
            $data = Store::findorfail($id)->load('categoryStoreDetail', 'tableStore')->load(['comboFood' => function ($query) {
                $query->where('combo_food.status', 1)->with('food.typeFood')->get();
            }])->load(['programStore' => function ($query1) {
                $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->with('food.size', 'food.categoryToppingFood.toppingFood', 'food.typeFood')->get();
            }]);
            $latitude_store = $data->latitude;
            $longtidue_store = $data->longtidue;
            $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
            if (isset($response)) {
                $data['distance'] = $response;
            } else {
                $data['distance'] = 0;
            }
            // foreach ($data->programStore as $p) {
            //     if (count($p->food) > 0) {
            //         foreach ($p->food as $p1) {
            //             $p1['count_card'] = CardFood::where([['food_id', $p1->id], ['status', 0]])->get()->SUM('quantity');
            //         }
            //     }
            // }
            // foreach ($data->comboFood as $cf) {
            //     $cf['count_card'] = CardFood::where([['combo_food_id', $cf->id], ['status', 0]])->get()->SUM('quantity');
            // }
            $ls_category_food_id = Food::where('store_id', $id)->pluck('category_food_id')->toArray();
            $category_food_id = array_unique($ls_category_food_id);
            $category_food = CategoryFood::where('status', 1)->whereIn('id', $category_food_id)->get()->load(['food' => function ($query3) use ($id) {
                $query3->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->get();
            }]);
            $ls_category_store_food_id = Food::where('store_id', $id)->pluck('category_store_food_id')->toArray();
            $category_store_food_id = array_unique($ls_category_store_food_id);
            $category_store_food = CategoryStoreFood::whereIn('id', $category_store_food_id)->get()->load(['food' => function ($query4) use ($id) {
                $query4->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->get();
            }]);
            // foreach ($category_food as $c) {
            //     if (count($c->food) > 0) {
            //         foreach ($c->food as $c1) {
            //             $c1['count_card'] = CardFood::where([['food_id', $c1->id], ['status', 0]])->get()->SUM('quantity');
            //         }
            //     }
            // }
            // $data3 = CardFood::where([['store_id', $id], ['user_id', $user->id], ['status', 0]])->with('food', 'store', 'toppingFood', 'comboFood.food')->get();
            $data1 = array(
                "store" => $data,
                "category_food" => $category_food,
                "category_store_food" => $category_store_food,
            );
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data1);
        }
    }
    public function storeDetail($store_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $data = Store::findorfail($store_id)->load('categoryStoreDetail');
        $data['is_eat_store'] = 0;
        $check_category_store = DB::table('category_store')->select('c.*')->join('category as c', 'c.id', 'category_store.category_id')->where('category_store.store_id', $store_id)->get();
        foreach ($check_category_store as $cs) {
            if ($cs->is_eat_store == 1) {
                $data['is_eat_store'] = 1;
                break;
            }
        }
        $latitude_store = $data->latitude;
        $longtidue_store = $data->longtidue;
        $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
        if (isset($response)) {
            $data['distance'] = $response;
        } else {
            $data['distance'] = 0;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listTableStore($store_id, Request $request)
    {
        $data = TableStore::where('store_id', $store_id)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listComboFoodStore($store_id, Request $request)
    {
        $token = $request->header('Authorization');
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            $data = ComboFood::where([['store_id', $store_id], ['status', 1], ['is_active', 1]])->with('food.typeFood')->limit(5)->get();
            $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $store_id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
            foreach ($data as $cf) {
                $check_combo_food = CardFood::where([['combo_food_id', $cf->id], ['status', 0], ['user_id', $user->id]])->get();
                $cf['count_card'] = $check_combo_food->SUM('quantity');
                $cf['card'] = $check_combo_food->load('toppingFood');
                if ($check_book_table) {
                    $check_combo_food2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.combo_food_id', $cf->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                        ->get();
                    $cf['count_book_food'] = $check_combo_food2->SUM('quantity');
                    $cf['book_food'] = $check_combo_food2->load('toppingFood');
                } else {
                    $cf['count_book_food'] = 0;
                    $cf['book_food'] = [];
                }
            }
        } else {
            $data = ComboFood::where([['store_id', $store_id], ['status', 1], ['is_active', 1]])->with('food.typeFood')->limit(5)->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listAllComboFoodStore($store_id, Request $request)
    {
        $token = $request->header('Authorization');
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            $data = ComboFood::where([['store_id', $store_id], ['status', 1], ['is_active', 1]])->with('food.typeFood')->get();
            $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $store_id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
            foreach ($data as $cf) {
                $check_combo_food = CardFood::where([['combo_food_id', $cf->id], ['status', 0], ['user_id', $user->id]])->get();
                $cf['count_card'] = $check_combo_food->SUM('quantity');
                $cf['card'] = $check_combo_food->load('toppingFood');
                if ($check_book_table) {
                    $check_combo_food2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.combo_food_id', $cf->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                        ->get();
                    $cf['count_book_food'] = $check_combo_food2->SUM('quantity');
                    $cf['book_food'] = $check_combo_food2->load('toppingFood');
                } else {
                    $cf['count_book_food'] = 0;
                    $cf['book_food'] = [];
                }
            }
        } else {
            $data = ComboFood::where([['store_id', $store_id], ['status', 1], ['is_active', 1]])->with('food.typeFood')->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listProgramStoreWithCustomer($store_id, Request $request)
    {
        $token = $request->header('Authorization');
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            $data = ProgramStore::where([['store_id', $store_id], ['status', 1], ['quantity', '>', 0], ['is_active', 1]])->orderby('percent', 'desc')->get();
            foreach ($data as $p) {
                $list_food_id = ProgramStoreDetail::where('program_store_id', $p->id)->pluck('food_id')->toArray();
                $food = Food::whereIn('id', $list_food_id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit(5)->get();
                if (count($food) > 0) {
                    $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $store_id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
                    foreach ($food as $p1) {
                        $check_food_program = CardFood::where([['food_id', $p1->id], ['status', 0], ['user_id', $user->id]])->get();
                        $p1['count_card'] = $check_food_program->SUM('quantity');
                        $p1['card'] = $check_food_program->load('toppingFood');
                        if ($check_book_table) {
                            $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                                ->where([['book_food.food_id', $p1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                                ->get();
                            $p1['count_book_food'] = $check_food_program2->SUM('quantity');
                            $p1['book_food'] = $check_food_program2->load('toppingFood');
                        } else {
                            $p1['count_book_food'] = 0;
                            $p1['book_food'] = [];
                        }
                    }
                }
                $p['food'] = $food;
            }
        } else {
            $data = ProgramStore::where([['store_id', $store_id], ['status', 1]])->orderby('percent', 'desc')->with('food.size', 'food.categoryToppingFood.toppingFood')->get();
            foreach ($data as $p) {
                $list_food_id = ProgramStoreDetail::where('program_store_id', $p->id)->pluck('food_id')->toArray();
                $food = Food::whereIn('id', $list_food_id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit(5)->get();
                $p['food'] = $food;
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listFoodInProgramStore($id, Request $request)
    {
        $check = ProgramStore::findorfail($id);
        $token = $request->header('Authorization');
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            $list_food_id = ProgramStoreDetail::where('program_store_id', $id)->pluck('food_id')->toArray();
            $food = Food::whereIn('id', $list_food_id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->paginate(12);
            if (count($food) > 0) {
                $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $check->store_id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
                foreach ($food as $p1) {
                    $check_food_program = CardFood::where([['food_id', $p1->id], ['status', 0], ['user_id', $user->id]])->get();
                    $p1['count_card'] = $check_food_program->SUM('quantity');
                    $p1['card'] = $check_food_program->load('toppingFood');
                    if ($check_book_table) {
                        $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                            ->where([['book_food.food_id', $p1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                            ->get();
                        $p1['count_book_food'] = $check_food_program2->SUM('quantity');
                        $p1['book_food'] = $check_food_program2->load('toppingFood');
                    } else {
                        $p1['count_book_food'] = 0;
                        $p1['book_food'] = [];
                    }
                }
            }
            $p['food'] = $food;
        } else {
            $list_food_id = ProgramStoreDetail::where('program_store_id', $id)->pluck('food_id')->toArray();
            $food = Food::whereIn('id', $list_food_id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->paginate(12);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $food);
    }
    public function listFoodInProgramStoreLimit($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'required',
        ], [
            'limit.required' => 'Vui lòng truyền limit',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = ProgramStore::findorfail($id);
        $token = $request->header('Authorization');
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            $list_food_id = ProgramStoreDetail::where('program_store_id', $id)->pluck('food_id')->toArray();
            $food = Food::whereIn('id', $list_food_id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit($request->limit)->get();
            if (count($food) > 0) {
                $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $check->store_id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
                foreach ($food as $p1) {
                    $check_food_program = CardFood::where([['food_id', $p1->id], ['status', 0], ['user_id', $user->id]])->get();
                    $p1['count_card'] = $check_food_program->SUM('quantity');
                    $p1['card'] = $check_food_program->load('toppingFood');
                    if ($check_book_table) {
                        $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                            ->where([['book_food.food_id', $p1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                            ->get();
                        $p1['count_book_food'] = $check_food_program2->SUM('quantity');
                        $p1['book_food'] = $check_food_program2->load('toppingFood');
                    } else {
                        $p1['count_book_food'] = 0;
                        $p1['book_food'] = [];
                    }
                }
            }
            $p['food'] = $food;
        } else {
            $list_food_id = ProgramStoreDetail::where('program_store_id', $id)->pluck('food_id')->toArray();
            $food = Food::whereIn('id', $list_food_id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit($request->limit)->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $food);
    }
    public function listCategoryFoodStore($store_id, Request $request)
    {
        $token = $request->header('Authorization');
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            $ls_category_food_id = Food::where('store_id', $store_id)->pluck('category_food_id')->toArray();
            $category_food_id = array_unique($ls_category_food_id);
            $data = CategoryFood::where('status', 1)->whereIn('id', $category_food_id)->get();
            foreach ($data as $c) {
                $food = Food::where([['category_food_id', $c->id], ['store_id', $store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit(5)->get();
                if (count($food) > 0) {
                    $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $store_id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
                    foreach ($food as $c1) {
                        $check_combo_food = CardFood::where([['food_id', $c1->id], ['status', 0], ['user_id', $user->id]])->get();
                        $c1['count_card'] = $check_combo_food->SUM('quantity');
                        $c1['card'] = $check_combo_food->load('toppingFood');
                        if ($check_book_table) {
                            $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                                ->where([['book_food.food_id', $c1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                                ->get();
                            $c1['count_book_food'] = $check_food_program2->SUM('quantity');
                            $c1['book_food'] = $check_food_program2->load('toppingFood');
                        } else {
                            $c1['count_book_food'] = 0;
                            $c1['book_food'] = [];
                        }
                    }
                }
                $c['food'] = $food;
            }
        } else {
            $ls_category_food_id = Food::where('store_id', $store_id)->pluck('category_food_id')->toArray();
            $category_food_id = array_unique($ls_category_food_id);
            $data = CategoryFood::where('status', 1)->whereIn('id', $category_food_id)->get();
            foreach ($data as $c) {
                $food = Food::where([['category_food_id', $c->id], ['store_id', $store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit(5)->get();
                $c['food'] = $food;
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCategoryStoreFoodStore($id, Request $request)
    {
        $token = $request->header('Authorization');
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            $ls_category_store_food_id = Food::where('store_id', $id)->pluck('category_store_food_id')->toArray();
            $category_store_food_id = array_unique($ls_category_store_food_id);
            $data = CategoryStoreFood::whereIn('id', $category_store_food_id)->get();
            foreach ($data as $s) {
                $food = Food::where([['category_store_food_id', $s->id], ['store_id', $id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit(5)->get();
                if (count($food) > 0) {
                    $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
                    foreach ($food as $s1) {
                        $check_combo_food = CardFood::where([['food_id', $s1->id], ['status', 0], ['user_id', $user->id]])->get();
                        $s1['count_card'] = $check_combo_food->SUM('quantity');
                        $s1['card'] = $check_combo_food->load('toppingFood');
                        if ($check_book_table) {
                            $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                                ->where([['book_food.food_id', $s1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                                ->get();
                            $s1['count_book_food'] = $check_food_program2->SUM('quantity');
                            $s1['book_food'] = $check_food_program2->load('toppingFood');
                        } else {
                            $s1['count_book_food'] = 0;
                            $s1['book_food'] = [];
                        }
                    }
                }
                $s['food'] = $food;
            }
        } else {
            $ls_category_store_food_id = Food::where('store_id', $id)->pluck('category_store_food_id')->toArray();
            $category_store_food_id = array_unique($ls_category_store_food_id);
            $data = CategoryStoreFood::whereIn('id', $category_store_food_id)->get();
            foreach ($data as $s) {
                $food = Food::where([['category_store_food_id', $s->id], ['store_id', $id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit(5)->get();
                $s['food'] = $food;
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listFoodWithCategoryInOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'category_id' => 'required',
            'type' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'category_id.required' => 'Vui lòng chọn danh mục',
            'type.required' => 'Vui lòng chọn loại danh mục',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $token = $request->header('Authorization');
        $food = [];
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            if ($request->type == 1) {
                $food = Food::where([['category_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->paginate(12);
                if (count($food) > 0) {
                    $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $request->store_id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
                    foreach ($food as $s1) {
                        $check_combo_food = CardFood::where([['food_id', $s1->id], ['status', 0], ['user_id', $user->id]])->get();
                        $s1['count_card'] = $check_combo_food->SUM('quantity');
                        $s1['card'] = $check_combo_food->load('toppingFood');
                        if ($check_book_table) {
                            $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                                ->where([['book_food.food_id', $s1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                                ->get();
                            $s1['count_book_food'] = $check_food_program2->SUM('quantity');
                            $s1['book_food'] = $check_food_program2->load('toppingFood');
                        } else {
                            $s1['count_book_food'] = 0;
                            $s1['book_food'] = [];
                        }
                    }
                }
            } else {
                $food = Food::where([['category_store_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->paginate(12);
                if (count($food) > 0) {
                    $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $request->store_id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
                    foreach ($food as $s1) {
                        $check_combo_food = CardFood::where([['food_id', $s1->id], ['status', 0], ['user_id', $user->id]])->paginate(12);
                        $s1['count_card'] = $check_combo_food->SUM('quantity');
                        $s1['card'] = $check_combo_food->load('toppingFood');
                        if ($check_book_table) {
                            $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                                ->where([['book_food.food_id', $s1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                                ->get();
                            $s1['count_book_food'] = $check_food_program2->SUM('quantity');
                            $s1['book_food'] = $check_food_program2->load('toppingFood');
                        } else {
                            $s1['count_book_food'] = 0;
                            $s1['book_food'] = [];
                        }
                    }
                }
            }
        } else {
            if ($request->type == 1) {
                $food = Food::where([['category_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->paginate(12);
            } else {
                $food = Food::where([['category_store_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->paginate(12);
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $food);
    }
    public function listFoodWithCategoryInOrderLimit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'category_id' => 'required',
            'type' => 'required',
            'limit' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'category_id.required' => 'Vui lòng chọn danh mục',
            'type.required' => 'Vui lòng chọn loại danh mục',
            'limit.required' => 'Vui lòng nhập limit',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $token = $request->header('Authorization');
        $food = [];
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            if ($request->type == 1) {
                $food = Food::where([['category_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit($request->limit)->get();
                if (count($food) > 0) {
                    $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $request->store_id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
                    foreach ($food as $s1) {
                        $check_combo_food = CardFood::where([['food_id', $s1->id], ['status', 0], ['user_id', $user->id]])->get();
                        $s1['count_card'] = $check_combo_food->SUM('quantity');
                        $s1['card'] = $check_combo_food->load('toppingFood');
                        if ($check_book_table) {
                            $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                                ->where([['book_food.food_id', $s1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                                ->get();
                            $s1['count_book_food'] = $check_food_program2->SUM('quantity');
                            $s1['book_food'] = $check_food_program2->load('toppingFood');
                        } else {
                            $s1['count_book_food'] = 0;
                            $s1['book_food'] = [];
                        }
                    }
                }
            } else {
                $food = Food::where([['category_store_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit($request->limit)->get();
                if (count($food) > 0) {
                    $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $request->store_id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
                    foreach ($food as $s1) {
                        $check_combo_food = CardFood::where([['food_id', $s1->id], ['status', 0], ['user_id', $user->id]])->paginate(12);
                        $s1['count_card'] = $check_combo_food->SUM('quantity');
                        $s1['card'] = $check_combo_food->load('toppingFood');
                        if ($check_book_table) {
                            $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                                ->where([['book_food.food_id', $s1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                                ->get();
                            $s1['count_book_food'] = $check_food_program2->SUM('quantity');
                            $s1['book_food'] = $check_food_program2->load('toppingFood');
                        } else {
                            $s1['count_book_food'] = 0;
                            $s1['book_food'] = [];
                        }
                    }
                }
            }
        } else {
            if ($request->type == 1) {
                $food = Food::where([['category_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit($request->limit)->get();
            } else {
                $food = Food::where([['category_store_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit($request->limit)->get();
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $food);
    }
    public function checkFoodInCategory($food_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check_food = Food::findorfail($food_id);
        $check_program = 0;
        $check_category_food = 0;
        $check_category_store = 0;
        $list_program_id = ProgramStore::where([['store_id', $check_food->store_id], ['status', 1]])->pluck('id')->toArray();
        if (ProgramStoreDetail::whereIn('program_store_id', $list_program_id)->where('food_id', $food_id)->first()) {
            $check_program = 1;
        }
        if ($check_food->category_food_id != null) {
            $check_category_food = 1;
        }
        if ($check_food->category_store_food_id != null) {
            $check_category_store = 1;
        }
        $data = array(
            'check_program' => $check_program,
            'check_category_food' => $check_category_food,
            'check_category_store' => $check_category_store,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCardWithStore($id, Request $request)
    {
        $token = $request->header('Authorization');
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            $data3 = CardFood::where([['store_id', $id], ['user_id', $user->id], ['status', 0]])->with('food', 'store', 'toppingFood', 'comboFood.food')->get();
            $data = array(
                // 'list_card' => $data3,
                'total_quantity' => $data3->SUM('quantity'),
                'total_price' => $data3->SUM('price'),
            );
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
        }
        else{
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', null);
        }
    }
    public function bookTableWithStore($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->with('tableStore')->first();
        if ($check_book_table) {
            $data3 = BookFood::select('book_food.*')
                ->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                ->where([['bt.store_id', $id], ['book_food.book_table_id', $check_book_table->id], ['bt.user_id', $user->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                ->with('food')->get();
            $data = array(
                'book_table' => $check_book_table,
                'total_quantity' => $data3->SUM('quantity'),
                'total_price' => $data3->SUM('price'),
            );
        } else {
            $data = array(
                'book_table' => $check_book_table,
                'total_quantity' => 0,
                'total_price' => 0,
            );
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function storeDetailWithBookTable($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $token = $request->header('Authorization');
        if ($token && $token != "Bearer null") {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            $where = [['user_id', $user->id], ['store_id', $id]];
            $data = Store::findorfail($id)->load('categoryStoreDetail', 'tableStore')->load(['comboFood' => function ($query) use ($where) {
                $query->where('combo_food.status', 1)->with('food.typeFood')->get();
            }])->load(['programStore' => function ($query1) use ($where) {
                $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->with('food.size', 'food.categoryToppingFood.toppingFood', 'food.typeFood')->get();
            }]);
            $latitude_store = $data->latitude;
            $longtidue_store = $data->longtidue;
            $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
            if (isset($response)) {
                $data['distance'] = $response;
            } else {
                $data['distance'] = 0;
            }
            $check_book_table = BookTable::where([['user_id', $user->id], ['store_id', $id]])->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->first();
            foreach ($data->programStore as $p) {
                if (count($p->food) > 0) {
                    foreach ($p->food as $p1) {
                        if ($check_book_table) {
                            $check_food_program = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                                ->where([['book_food.food_id', $p1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                                ->get();
                            $p1['count_book_food'] = $check_food_program->SUM('quantity');
                            $p1['book_food'] = $check_food_program->load('toppingFood');
                        } else {
                            $p1['count_book_food'] = 0;
                            $p1['book_food'] = [];
                        }
                    }
                }
            }
            foreach ($data->comboFood as $cf) {
                if ($check_book_table) {
                    $check_combo_food = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.combo_food_id', $cf->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                        ->get();
                    $cf['count_book_food'] = $check_combo_food->SUM('quantity');
                    $cf['book_food'] = $check_combo_food->load('toppingFood');
                } else {
                    $cf['count_book_food'] = 0;
                    $cf['book_food'] = [];
                }
            }
            $ls_category_food_id = Food::where('store_id', $id)->pluck('category_food_id')->toArray();
            $category_food_id = array_unique($ls_category_food_id);
            $category_food = CategoryFood::where('status', 1)->whereIn('id', $category_food_id)->get()->load(['food' => function ($query4) use ($id) {
                $query4->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->get();
            }]);
            foreach ($category_food as $c) {
                if (count($c->food) > 0) {
                    foreach ($c->food as $c1) {
                        $check_combo_food1 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                            ->where([['book_food.food_id', $c1->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                            ->get();
                        $c1['count_book_food'] = $check_combo_food1->SUM('quantity');
                        $c1['book_food'] = $check_combo_food1->load('toppingFood');
                    }
                }
            }
            $ls_category_store_food_id = Food::where('store_id', $id)->pluck('category_store_food_id')->toArray();
            $category_store_food_id = array_unique($ls_category_store_food_id);
            $category_store_food = CategoryStoreFood::whereIn('id', $category_store_food_id)->get()->load(['food' => function ($query4) use ($id) {
                $query4->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->get();
            }]);
            foreach ($category_store_food as $s) {
                if (count($s->food) > 0) {
                    foreach ($s->food as $s1) {
                        $check_combo_food1 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                            ->where([['book_food.food_id', $s1->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['bt.user_id', $user->id]])
                            ->get();
                        $s1['count_book_food'] = $check_combo_food1->SUM('quantity');
                        $s1['book_food'] = $check_combo_food1->load('toppingFood');
                    }
                }
            }
            $table_id_booked = null;
            $total_price = 0;
            $total_quantity = 0;
            if ($check_book_table) {
                $data3 = BookFood::select('book_food.*')
                    ->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                    ->where([['bt.store_id', $id], ['book_food.book_table_id', $check_book_table->id], ['bt.user_id', $user->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                    ->with('food')->get();
                if ($data3) {
                    $total_price = $data3->SUM('price');
                    $total_quantity = $data3->SUM('quantity');
                }
            }

            $data1 = array(
                "store" => $data,
                "category_food" => $category_food,
                "category_store_food" => $category_store_food,
                'total_quantity' => $total_quantity,
                'total_price' => $total_price,
                'book_table' => $check_book_table,
            );
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data1);
        } else {
            $data = Store::findorfail($id)->load('categoryStoreDetail', 'tableStore')->load(['comboFood' => function ($query) {
                $query->where('combo_food.status', 1)->with('food.typeFood')->get();
            }])->load(['programStore' => function ($query1) {
                $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->with('food.size', 'food.categoryToppingFood.toppingFood', 'food.typeFood')->get();
            }]);
            $latitude_store = $data->latitude;
            $longtidue_store = $data->longtidue;
            $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
            if (isset($response)) {
                $data['distance'] = $response;
            } else {
                $data['distance'] = 0;
            }
            // foreach ($data->programStore as $p) {
            //     if (count($p->food) > 0) {
            //         foreach ($p->food as $p1) {
            //             $p1['count_card'] = CardFood::where([['food_id', $p1->id], ['status', 0]])->get()->SUM('quantity');
            //         }
            //     }
            // }
            // foreach ($data->comboFood as $cf) {
            //     $cf['count_card'] = CardFood::where([['combo_food_id', $cf->id], ['status', 0]])->get()->SUM('quantity');
            // }
            $ls_category_food_id = Food::where('store_id', $id)->pluck('category_food_id')->toArray();
            $category_food_id = array_unique($ls_category_food_id);
            $category_food = CategoryFood::where('status', 1)->whereIn('id', $category_food_id)->get()->load(['food' => function ($query5) use ($id) {
                $query5->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->get();
            }]);
            $ls_category_store_food_id = Food::where('store_id', $id)->pluck('category_store_food_id')->toArray();
            $category_store_food_id = array_unique($ls_category_store_food_id);
            $category_store_food = CategoryStoreFood::whereIn('id', $category_store_food_id)->get()->load(['food' => function ($query4) use ($id) {
                $query4->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->get();
            }]);
            // foreach ($category_food as $c) {
            //     if (count($c->food) > 0) {
            //         foreach ($c->food as $c1) {
            //             $c1['count_card'] = CardFood::where([['food_id', $c1->id], ['status', 0]])->get()->SUM('quantity');
            //         }
            //     }
            // }
            // $data3 = CardFood::where([['store_id', $id], ['user_id', $user->id], ['status', 0]])->with('food', 'store', 'toppingFood', 'comboFood.food')->get();
            $data1 = array(
                "store" => $data,
                "category_food" => $category_food,
                "category_store_food" => $category_store_food,
            );
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data1);
        }

    }
    public function listStoreWithOwner(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [['user_id', $user->id]];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        $data = Store::where($where)->with('category', 'staff', 'categoryStoreDetail')->orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listProgramStore(Request $request)
    {
        $data = ProgramStore::where([['status', 1], ['quantity', '>', 0]])->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function storeDetailWithOwner($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Store::findorfail($id)->load('category', 'staff', 'categoryStoreDetail', 'tableStore')->load(['comboFood' => function ($query) {
            $query->where('combo_food.status', 1)->with('food')->get();
        }])->load(['programStore' => function ($query1) {
            $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->with('food.size', 'food.categoryToppingFood.toppingFood')->get();
        }]);
        $ls_category_food_id = Food::where('store_id', $id)->pluck('category_food_id')->toArray();
        $category_food_id = array_unique($ls_category_food_id);
        $category_food = CategoryFood::where('status', 1)->whereIn('id', $category_food_id)->get()->load(['food' => function ($query5) use ($id) {
            $query5->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood')->get();
        }]);
        $ls_category_store_food_id = Food::where('store_id', $id)->pluck('category_store_food_id')->toArray();
        $category_store_food_id = array_unique($ls_category_store_food_id);
        $category_store_food = CategoryStoreFood::whereIn('id', $category_store_food_id)->get()->load(['food' => function ($query4) use ($id) {
            $query4->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood')->get();
        }]);
        $owner_store = UserStore::where([['store_id', $id], ['is_owner', 1]])->with('user')->first();
        $data1 = array(
            "store" => $data,
            "category_food" => $category_food,
            "category_store_food" => $category_store_food,
            'owner_store' => $owner_store,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data1);
    }
    public function listFoodWithCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_food_id' => 'required',
            'store_id' => 'required',
        ], [
            'category_food_id.required' => 'Vui lòng chọn danh mục món ăn',
            'store_id.required' => 'Vui lòng chọn cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($request->type) {
            $data = Food::where([['category_store_food_id', $request->category_food_id], ['store_id', $request->store_id]])->with('store', 'categoryFood', 'size', 'categoryToppingFood.toppingFood')->paginate(12);
        } else {
            $data = Food::where([['category_food_id', $request->category_food_id], ['store_id', $request->store_id]])->with('store', 'categoryFood', 'size', 'categoryToppingFood.toppingFood')->paginate(12);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createStore(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longtidue' => 'required',
            'average_price' => 'required',
            'open_hours' => 'required',
            'close_hours' => 'required',
            'hotline' => 'required',
            'category_id' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên cửa hàng',
            'image.required' => 'Vui lòng chọn ảnh cửa hàng',
            'province_id.required' => 'Vui lòng chọn tỉnh thành',
            'district_id.required' => 'Vui lòng chọn quận huyện',
            'address.required' => 'Vui lòng nhập địa chỉ cửa hàng',
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
            'average_price.required' => 'Vui lòng nhập giá trung bình',
            'open_hours.required' => 'Vui lòng nhập giờ mở cửa',
            'close_hours.required' => 'Vui lòng nhập giờ đóng cửa',
            'hotline.required' => 'Vui lòng nhập hotline',
            'category_id.required' => 'Vui lòng chọn danh mục cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $req = $request->except('percent_discount_revenue');
        $req['user_id'] = $user->id;
        $req['percent_discount_revenue'] = 0;
        $req['average_price'] = preg_replace('/[^0-9\-]/', '', $request->average_price);
        $req['image'] = $this->uploadImage($request->file('image'));
        if ($request->file('banner')) {
            $req['banner'] = $this->uploadImage($request->file('banner'));
        }
        $store = Store::create($req);
        if (is_array($request->category_id)) {
            $ls_category_id = array_unique($request->category_id);
            foreach ($ls_category_id as $c) {
                if (Category::where([['status', 1], ['id', $c]])->first()) {
                    $store->category()->attach($c);
                    $check_category_permanent = CategoryPermanent::where('category_id', $c)->get();
                    $array = [];
                    foreach ($check_category_permanent as $cp) {
                        $array[] = array(
                            'store_id' => $store->id,
                            'name' => $cp->name,
                        );
                        // if(CategoryStoreFood::where([['']]))
                    }
                    // return $array;
                    CategoryStoreFood::insert($array);
                }
            }
        }
        if ($request->category_business && is_array($request->category_business)) {
            foreach ($request->category_business as $i) {
                if ($i != null && $i != "") {
                    CategoryStoreDetail::insert([
                        'store_id' => $store->id,
                        'category_name' => $i,
                    ]);
                }
            }
        }
        $admin1 = User::whereIn('role_id', [1, 9])->pluck('id')->toArray();
        $admin_location = User::where([['role_id', 11], ['manage_province_id', $request->province_id], ['manage_district_id', null]])
            ->orWhere([['role_id', 11], ['manage_province_id', $request->province_id], ['manage_district_id', $request->district_id]])
            ->pluck('id')->toArray();
        $admin_location2 = User::where([['role_id', 13], ['manage_province_id', $request->province_id], ['manage_district_id', null]])
            ->pluck('id')->toArray();
        $admin_location3 = User::Where([['role_id', 14], ['manage_province_id', $request->province_id], ['manage_district_id', $request->district_id]])
            ->pluck('id')->toArray();
        $admin2 = array_merge($admin1, $admin_location);
        $admin3 = array_merge($admin2, $admin_location2);
        $admin = array_merge($admin3, $admin_location3);
        $this->notifycontroller->pushNotify($admin, 'Có 1 cửa hàng mới vừa được tạo đang chờ bạn kiểm định!', $store->id, SystemParam::type_store, $store, $store->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $store);
    }
    public function updateStore($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Store::findorfail($id);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longtidue' => 'required',
            'average_price' => 'required',
            'open_hours' => 'required',
            'close_hours' => 'required',
            'hotline' => 'required',
            'category_id' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên cửa hàng',
            'image.required' => 'Vui lòng chọn ảnh cửa hàng',
            'province_id.required' => 'Vui lòng chọn tỉnh thành',
            'district_id.required' => 'Vui lòng chọn quận huyện',
            'address.required' => 'Vui lòng nhập địa chỉ cửa hàng',
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
            'average_price.required' => 'Vui lòng nhập giá trung bình',
            'open_hours.required' => 'Vui lòng nhập giờ mở cửa',
            'close_hours.required' => 'Vui lòng nhập giờ đóng cửa',
            'hotline.required' => 'Vui lòng nhập hotline',
            'category_id.required' => 'Vui lòng chọn danh mục cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $req = $request->except('percent_discount_revenue');
        $req['average_price'] = preg_replace('/[^0-9\-]/', '', $request->average_price);
        // $req['user_id'] = $user->id;
        if ($request->file('image')) {
            $check_image = DB::table('stores')->where('id', $id)->first();
            if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
                unlink(ltrim($check_image->image, '/'));
            }
            $req['image'] = $this->uploadImage($request->file('image'));
        }
        if ($request->file('banner')) {
            $check_image = DB::table('stores')->where('id', $id)->first();
            if (file_exists(ltrim($check_image->banner, '/')) && $check_image->banner != "") {
                unlink(ltrim($check_image->banner, '/'));
            }
            $req['banner'] = $this->uploadImage($request->file('banner'));
        }
        $check->update($req);
        if (is_array($request->category_id)) {
            $ls_category_id = array_unique($request->category_id);
            $check->category()->detach();
            foreach ($ls_category_id as $c) {
                if (Category::where([['status', 1], ['id', $c]])->first()) {
                    $check->category()->attach($c);
                    $check_category_permanent = CategoryPermanent::where('category_id', $c)->get();
                    $array = [];
                    foreach ($check_category_permanent as $cp) {
                        if (!$check_cate = CategoryStoreFood::where([['store_id', $id], ['name', $cp->name]])->first()) {
                            $array[] = array(
                                'store_id' => $id,
                                'name' => $cp->name,
                            );
                        }
                    }
                    CategoryStoreFood::insert($array);
                }
            }
        }
        if ($request->category_business && is_array($request->category_business)) {
            CategoryStoreDetail::where('store_id', $id)->delete();
            foreach ($request->category_business as $i) {
                if ($i != null && $i != "") {
                    CategoryStoreDetail::insert([
                        'store_id' => $id,
                        'category_name' => $i,
                    ]);
                }
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function searchStoreWithStaff(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [['status', 1]];
        if ($request->name) {
            $where[] = ['name', 'LIKE', '%' . $request->name . '%'];
        }
        if ($request->province_id) {
            $where[] = ['province_id', $request->province_id];
        }
        $data = Store::where($where)->orderby('id', 'desc')->paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function chooseStore($store_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_user_store = UserStore::where([['user_id', $user->id]])->first();
        if ($check_user_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_user_store_duplicate, null);
        }
        $check = Store::where([['id', $store_id], ['status', 1]])->firstorfail();
        $create = UserStore::insert([
            'user_id' => $user->id,
            'store_id' => $store_id,
            'status' => 0,
        ]);
        $id = $check->user_id;
        $check_owner = UserStore::where([['is_owner', 1], ['status', 1]])->first();
        if ($check_owner) {
            $id = $check_owner->user_id;
        }
        $this->notifycontroller->pushNotify([$id], 'Tài khoản có tên là ' . $user->name . ' đã yêu cầu được tham gia vào cửa hàng của bạn!', $check->id, SystemParam::type_store, $check, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::choose_store_success, $check);
    }
    public function deleteChooseStore($store_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_store = Store::where([['id', $store_id], ['status', 1]])->firstorfail();
        $check_user_store = UserStore::where([['user_id', $user->id], ['store_id', $store_id]])->first();
        if (!$check_user_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_user_store_not_found, null);
        }
        $id = $check_store->user_id;
        $check_user_store->delete();
        $check_owner = UserStore::where([['is_owner', 1], ['status', 1]])->first();
        if ($check_owner) {
            $id = $check_owner->user_id;
        }
        $this->notifycontroller->pushNotify([$id], 'Tài khoản có tên là ' . $user->name . ' đã hủy yêu cầu được tham gia vào cửa hàng của bạn!', $check_store->id, SystemParam::type_store, $check_store, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check_user_store);
    }
    public function storeStaff($status)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 3) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_user_store = UserStore::where([['user_id', $user->id], ['status', $status]])->first();
        $data = null;
        if ($check_user_store) {
            $data = Store::where('id', $check_user_store->store_id)->first();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function storeDetailWithStaff()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 3) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_user_store = UserStore::where([['user_id', $user->id]])->first();
        if (!$check_user_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_user_store_not_found, null);
        }
        if ($check_user_store == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_user_store_status_0, null);
        }
        $data = Store::where('id', $check_user_store->store_id)->first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStoreConfirmWithOwner()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Store::where([['user_id', $user->id], ['status', 1]])->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function confirmStoreWithOwner()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $user->update([
            'status_system_owner' => 1,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_store_success, $user);
    }
    public function listStaffInStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'status' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'status.required' => 'Vui lòng chọn trạng thái nhân viên',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }

        $check_store = Store::where([['id', $request->store_id]])->first();
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_store_with_owner_not_found, null);
        }
        $check_staff = UserStore::where([['store_id', $request->store_id], ['status', $request->status], ['user_id', '!=', $user->id]])->pluck('user_id')->toArray();
        $data = User::whereIn('id', $check_staff)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function deleteStaff(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $check_store = Store::findorfail($request->store_id);
        if ($request->staff_id) {
            $check_staff = UserStore::where([['user_id', $request->staff_id], ['store_id', $request->store_id], ['status', '!=', 0]])->first();
            if (!$check_staff) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_staff_not_found, null);
            }
            $update_staff = User::where('id', $request->staff_id)->update([
                'status_staff' => 0,
            ]);
            $check_staff->delete();
            $this->notifycontroller->pushNotify([$request->staff_id], 'Bạn đã bị loại bỏ ra khỏi cửa hàng', $check_store->id, SystemParam::type_store, $check_store, $check_store->name);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check_staff);
        } else {
            $ls_staff_id = UserStore::where([['status', '!=', 0], ['store_id', $request->store_id], ['user_id', '!=', $user->id]])->pluck('user_id')->toArray();
            $staff_id = array_unique($ls_staff_id);
            $update_staff = User::whereIn('id', $staff_id)->update([
                'status_staff' => 0,
            ]);
            $delete_staff = UserStore::where([['status', '!=', 0], ['store_id', $request->store_id]])->delete();
            $this->notifycontroller->pushNotify($staff_id, 'Bạn đã bị loại bỏ ra khỏi cửa hàng', $check_store->id, SystemParam::type_store, $check_store, $check_store->name);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $delete_staff);
        }
    }
    public function confirmStaff(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $check_store = Store::findorfail($request->store_id);
        if ($request->staff_id) {
            $check_staff = UserStore::where([['user_id', $request->staff_id], ['store_id', $request->store_id], ['status', 0]])->first();
            if (!$check_staff) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_staff_not_found, null);
            }
            $update_staff = User::where('id', $request->staff_id)->update([
                'status_staff' => 1,
            ]);
            $check_staff->update([
                'status' => 1,
            ]);
            $this->notifycontroller->pushNotify([$request->staff_id], 'Bạn đã được xác nhận làm nhân viên của cửa hàng', $check_store->id, SystemParam::type_store, $check_store, $check_store->name);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $check_staff);
        } else {
            $ls_staff_id = UserStore::where([['status', 0], ['store_id', $request->store_id]])->pluck('user_id')->toArray();
            $staff_id = array_unique($ls_staff_id);
            $update_staff = User::whereIn('id', $staff_id)->update([
                'status_staff' => 1,
            ]);
            $check_staff = UserStore::where([['status', 0], ['store_id', $request->store_id]])->update([
                'status' => 1,
            ]);
            $this->notifycontroller->pushNotify($staff_id, 'Bạn đã được xác nhận làm nhân viên của cửa hàng', $check_store->id, SystemParam::type_store, $check_store, $check_store->name);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $check_staff);
        }
    }
    public function stopStaff(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $check_store = Store::findorfail($request->store_id);
        if ($request->staff_id) {
            $check_staff = UserStore::where([['user_id', $request->staff_id], ['store_id', $request->store_id], ['status', 1]])->first();
            if (!$check_staff) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_staff_not_found, null);
            }
            $update_staff = User::where('id', $request->staff_id)->update([
                'status_staff' => 0,
            ]);
            $check_staff->update([
                'status' => 2,
            ]);
            $this->notifycontroller->pushNotify([$request->staff_id], 'Bạn đã bị tạm dừng làm nhân viên của cửa hàng', $check_store->id, SystemParam::type_store, $check_store, $check_store->name);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::stop_success, $check_staff);
        } else {
            $ls_staff_id = UserStore::where([['status', 1], ['store_id', $request->store_id]])->pluck('user_id')->toArray();
            $staff_id = array_unique($ls_staff_id);
            $update_staff = User::whereIn('id', $staff_id)->update([
                'status_staff' => 0,
            ]);
            $check_staff = UserStore::where([['status', 1], ['store_id', $request->store_id]])->update([
                'status' => 2,
            ]);
            $this->notifycontroller->pushNotify($staff_id, 'Bạn đã bị tạm dừng làm nhân viên của cửa hàng', $check_store->id, SystemParam::type_store, $check_store, $check_store->name);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::stop_success, $check_staff);
        }
    }
    public function activeStaff(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $check_store = Store::findorfail($request->store_id);
        if ($request->staff_id) {
            $check_staff = UserStore::where([['user_id', $request->staff_id], ['store_id', $request->store_id], ['status', 2]])->first();
            if (!$check_staff) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_staff_not_found, null);
            }
            $update_staff = User::where('id', $request->staff_id)->update([
                'status_staff' => 1,
            ]);
            $check_staff->update([
                'status' => 1,
            ]);
            $this->notifycontroller->pushNotify([$request->staff_id], 'Bạn đã được kích hoạt làm nhân viên của cửa hàng', $check_store->id, SystemParam::type_store, $check_store, $check_store->name);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::active_success, $check_staff);
        } else {
            $ls_staff_id = UserStore::where([['status', 2], ['store_id', $request->store_id]])->pluck('user_id')->toArray();
            $staff_id = array_unique($ls_staff_id);
            $update_staff = User::whereIn('id', $staff_id)->update([
                'status_staff' => 1,
            ]);
            $check_staff = UserStore::where([['status', 2]])->update([
                'status' => 1,
            ]);
            $this->notifycontroller->pushNotify($staff_id, 'Bạn đã được kích hoạt làm nhân viên của cửa hàng', $check_store->id, SystemParam::type_store, $check_store, $check_store->name);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::active_success, $check_staff);
        }
    }
    public function listStoreOwner($store_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $ls_id = UserStore::where([['status', 0], ['store_id', $store_id]])->pluck('user_id')->toArray();
        $data = User::whereIn('id', $ls_id)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function confirmStoreOwner(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'owner_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'owner_id.required' => 'Vui lòng chọn người cần xác nhận',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_store = Store::findorfail($request->store_id);
        $check_user = User::findorfail($request->owner_id);
        $check_owner = UserStore::where([['user_id', $request->owner_id], ['store_id', $request->store_id], ['status', 0]])->first();
        if (!$check_owner) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_staff_not_found, null);
        }
        $update_staff = User::where('id', $request->owner_id)->update([
            'status_owner' => 1,
            'store_id' => $request->store_id,
            'role_id' => 6,
        ]);
        $check_owner->update([
            'status' => 1,
            'is_owner' => 1,
        ]);
        $check_store->update([
            'is_owner' => 1,
        ]);
        $this->notifycontroller->pushNotify([$request->owner_id], 'Bạn đã được chủ chuỗi cửa hàng ủy quyền làm chủ của cửa hàng ' . $check_store->name, $check_store->id, SystemParam::type_store, $check_store, $check_store->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $check_owner);
    }
    public function deleteStoreOwner(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'owner_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'owner_id.required' => 'Vui lòng chọn người cần xác nhận',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_store = Store::findorfail($request->store_id);
        $check_user = User::findorfail($request->owner_id);
        $check_owner = UserStore::where([['user_id', $request->owner_id], ['store_id', $request->store_id], ['status', 1], ['is_owner', 1]])->first();
        if (!$check_owner) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_staff_not_found, null);
        }
        $check_user->update([
            'status_owner' => 0,
            'store_id' => null,
            'role_id' => 3,
        ]);
        $check_owner->delete();
        $check_store->update([
            'is_owner' => 0,
        ]);
        $check_staff = UserStore::where([['store_id', $request->store_id], ['status', '!=', 0], ['is_owner', 0]])->get();
        foreach ($check_staff as $c) {
            $update_status_staff = User::where('id', $c->user_id)->update([
                'status_staff' => 0,
            ]);
        }
        $update = UserStore::where([['store_id', $request->store_id], ['status', '!=', 0], ['is_owner', 0]])->update([
            'status' => 0,
        ]);
        $this->notifycontroller->pushNotify([$request->owner_id], 'Bạn đã bị chủ chuỗi cửa hàng loại bỏ ra khỏi cửa hàng ' . $check_store->name, $check_store->id, SystemParam::type_store, $check_store, $check_store->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check_owner);
    }
    public function searchGoogleMap(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'key' => 'required',
        ], [
            'key.required' => 'Vui lòng nhập địa điểm cần tìm',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $data = $this->searchMap($request->key);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, "", $data);
    }
    public function confirmDoStoreOwner(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_store = Store::findorfail($request->store_id);
        $check_owner = UserStore::where([['store_id', $request->store_id], ['is_owner', 1], ['status', 1]])->first();
        if ($check_owner) {
            if ($check_owner->user_id == $user->id) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_staff_user_id, null);
            }
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_staff_already_exist, null);
        }
        UserStore::insert([
            'user_id' => $user->id,
            'store_id' => $request->store_id,
            'is_owner' => 1,
            'status' => 1,
        ]);
        $user->update([
            'is_store_owner_and_chain_owner' => 1,
            'store_id' => $request->store_id,
        ]);
        $check_store->update([
            'is_owner' => 1,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $user);
    }
    public function cancelDoStoreOwner(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_store = Store::findorfail($request->store_id);
        $check_owner = UserStore::where([['user_id', $user->id], ['store_id', $request->store_id], ['is_owner', 1], ['status', 1]])->first();
        if (!$check_owner) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_staff_user_not_found, null);
        }
        $check_owner->delete();
        $user->update([
            'is_store_owner_and_chain_owner' => 0,
            'store_id' => null,
        ]);
        $check_store->update([
            'is_owner' => 0,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $user);
    }
}
