<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\CategoryStoreFood;
use App\Models\Store;
use App\Models\BookFood;
use App\Models\BookFoodTopping;
use App\Models\BookTable;
use App\Models\CardFood;
use App\Models\CardToppingFood;
use App\Models\CategoryFood;
use App\Models\CategoryToppingFood;
use App\Models\ComboFood;
use App\Models\ComboFoodDetail;
use App\Models\Food;
use App\Models\FoodSize;
use App\Models\OrderFood;
use App\Models\OrderFoodDetail;
use App\Models\OrderToppingFoodDetail;
use App\Models\ProgramStore;
use App\Models\ProgramStoreDetail;
use App\Models\ToppingFood;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryStoreFoodController extends Controller
{
    //
    public function listCategoryStoreFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if($request->store_id){
            $data = CategoryStoreFood::where('store_id', $request->store_id)->get();
        }else{
            $data = CategoryStoreFood::get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function categoryStoreFoodDetail($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = CategoryStoreFood::findorfail($id);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createCategoryStoreFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'store_id' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên danh mục',
            'store_id.required' => 'Vui lòng chọn cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if (!Store::where([['id', $request->store_id], ['status', 1]])->first()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $req = $request->all();
        $create = CategoryStoreFood::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function updateCategoryStoreFood($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'store_id' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên danh mục',
            'store_id.required' => 'Vui lòng chọn cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = CategoryStoreFood::findorfail($id);
        if (!Store::where([['id', $request->store_id], ['status', 1]])->first()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $req = $request->all();
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteCategoryStoreFood($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = CategoryStoreFood::findorfail($id);
        $data = Food::where('category_store_food_id', $id)->get();
        foreach($data as $d){
            FoodSize::where('food_id', $d->id)->delete();
            $card_food = CardFood::where('food_id', $d->id)->get();
            foreach ($card_food as $c) {
                CardToppingFood::where('card_food_id', $c->id)->delete();
            }
            CardFood::where('food_id', $d->id)->delete();
            $check_order_food_detail = OrderFoodDetail::where('food_id', $d->id)->pluck('order_food_id')->toArray();
            $order_detail_id = OrderFoodDetail::whereIn('order_food_id', $check_order_food_detail)->pluck('id')->toArray();
            OrderToppingFoodDetail::where('order_food_detail_id', $order_detail_id)->delete();
            OrderFoodDetail::whereIn('order_food_id', $check_order_food_detail)->delete();
            OrderFood::whereIn('id', $check_order_food_detail)->delete();
            $check_combo_food_detail = ComboFoodDetail::where('food_id', $d->id)->get();
            foreach ($check_combo_food_detail as $cf) {
                $check_book_food1 = BookFood::where('combo_food_id', $cf->combo_food_id)->get();
                foreach ($check_book_food1 as $b1) {
                    if ($b1->status == SystemParam::status_booking_food_finish) {
                        $check_book_food2 = BookFood::where('book_table_id', $b1->book_table_id)->pluck('id')->toArray();
                        BookFoodTopping::whereIn('book_food_id', $check_book_food2)->delete();
                        BookFood::where('book_table_id', $b1->book_table_id)->delete();
                        BookTable::where('id', $b1->book_table_id)->delete();
                    } else {
                        $check_book_food2 = BookFood::where('book_table_id', $b1->book_table_id)->pluck('id')->toArray();
                        BookFoodTopping::whereIn('book_food_id', $check_book_food2)->delete();
                        BookFood::where('book_table_id', $b1->book_table_id)->delete();
                    }
                }
                $check_order_food_detail1 = OrderFoodDetail::where('combo_food_id', $cf->combo_food_id)->pluck('order_food_id')->toArray();
                $order_detail_id1 = OrderFoodDetail::whereIn('order_food_id', $check_order_food_detail1)->pluck('id')->toArray();
                OrderToppingFoodDetail::where('order_food_detail_id', $order_detail_id1)->delete();
                OrderFoodDetail::whereIn('order_food_id', $check_order_food_detail1)->delete();
                OrderFood::whereIn('id', $check_order_food_detail1)->delete();
                $card_food1 = CardFood::where('combo_food_id', $cf->combo_food_id)->pluck('id')->toArray();
                CardToppingFood::whereIn('card_food_id', $card_food1)->delete();
                CardFood::where('combo_food_id', $cf->combo_food_id)->delete();
                $order = ComboFoodDetail::where('combo_food_id', $cf->combo_food_id)->delete();
                ComboFood::where('id', $cf->combo_food_id)->delete();
            }
            $check_book_food = BookFood::where('food_id', $d->id)->get();
            foreach ($check_book_food as $b) {
                if ($b->status == SystemParam::status_booking_food_finish) {
                    $check_book_food2 = BookFood::where('book_table_id', $b->book_table_id)->pluck('id')->toArray();
                    BookFoodTopping::whereIn('book_food_id', $check_book_food2)->delete();
                    BookFood::where('book_table_id', $b->book_table_id)->delete();
                    BookTable::where('id', $b->book_table_id)->delete();
                } else {
                    $check_book_food2 = BookFood::where('book_table_id', $b->book_table_id)->pluck('id')->toArray();
                    BookFoodTopping::whereIn('book_food_id', $check_book_food2)->delete();
                    BookFood::where('book_table_id', $b->book_table_id)->delete();
                }
            }
            $check_program_store = ProgramStoreDetail::where('food_id', $d->id)->get();
            foreach ($check_program_store as $p) {
                $check_program = ProgramStoreDetail::where([['program_store_id', $p->program_store_id], ['id', '!=', $p->id]])->get();
                if (count($check_program) > 0) {
                    ProgramStoreDetail::where('id', $p->id)->delete();
                } else {
                    ProgramStoreDetail::where('program_store_id', $p->program_store_id)->delete();
                    ProgramStore::where('id', $p->program_store_id)->delete();
                }
            }
            $check_category_topping_food = CategoryToppingFood::where('food_id', $d->id)->pluck('id')->toArray();
            ToppingFood::whereIn('category_topping_food_id', $check_category_topping_food)->delete();
            CategoryToppingFood::where('food_id', $d->id)->delete();
        }
        Food::where('category_store_food_id', $id)->delete();
        $check->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check);
    }
}
