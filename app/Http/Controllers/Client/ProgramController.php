<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Program;
use App\Models\ProgramDetail;
use App\Models\ProgramStore;
use App\Models\Store;
use App\Models\UserStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProgramController extends Controller
{
    //
    public function confirmProgramSystem($id){
        $check = Program::findorfail($id)->update([
            'status' => 1
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function listProgramSystem(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Program::where([['status', 1]])->paginate(12);
        $program_id = [];
        if ($request->store_id) {
            $program_id = ProgramDetail::where('store_id', $request->store_id)->pluck('program_id')->toArray();
        }
        foreach ($data as $d) {
            if (in_array($d->id, $program_id)) {
                $d['status_store'] = 1;
            } else {
                $d['status_store'] = 0;
            }
        }
        $array = array(
            'list_program' => $data,
            'count_program' => Program::where([['status', 1]])->count(),
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
    public function programSystemDetail($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Program::findorfail($id);
        $program_id = [];
        if ($request->store_id) {
            $program_id = ProgramDetail::where('srore_id', $request->store_id)->pluck('program_id')->toArray();
        }
        if (in_array($data->id, $program_id)) {
            $data['status_store'] = 1;
        } else {
            $data['status_store'] = 0;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function receiveProgramSystem(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'program_id' => 'required',
            'store_id' => 'required',
        ], [
            'program_id.required' => 'Vui lòng chọn chương trình bạn muốn tham gia',
            'store_id.required' => 'Vui lòng chọn cửa hàng tham gia',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if (!Store::find($request->store_id)) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check = UserStore::where([['store_id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_program = Program::where([['id', $request->program_id], ['status', 1]])->first();
        if (!$check_program) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_program_system_not_found, null);
        }
        $delete = ProgramDetail::where('store_id', $request->store_id)->delete();
        $create = ProgramDetail::insert([
            'program_id' => $request->program_id,
            'store_id' => $request->store_id,
            'status' => 1,
        ]);
        $update = ProgramStore::where([['store_id', $request->store_id], ['status', 1]])->update([
            'status' => 3,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function listProgramSystemWithStore($store_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = UserStore::where([['store_id', $store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $program_id = ProgramDetail::where('store_id', $store_id)->pluck('program_id')->toArray();
        $data = Program::whereIn('id', $program_id)->where('status', 1)->get();
        $array = array(
            'list_program' => $data,
            'count_program' => count($data),
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
    public function stopProgramSystemWithStore(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'program_id' => 'required',
            'store_id' => 'required',
        ], [
            'program_id.required' => 'Vui lòng chọn chương trình bạn muốn tham gia',
            'store_id.required' => 'Vui lòng chọn cửa hàng tham gia',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if (!Store::find($request->store_id)) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check = UserStore::where([['store_id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_program = ProgramDetail::where([['program_id', $request->program_id], ['store_id', $request->store_id]])->first();
        if (!$check_program) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_program_system_not_found, null);
        }
        $check_program->delete();
        $date_check = date('Y-m-d', time());
        $update = ProgramStore::where([['store_id', $request->store_id], ['status', 3], ['time_open', '<=', $date_check], ['time_close', '>', $date_check]])->update([
            'status' => 1,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::stop_program_success, $check_program);
    }
}
