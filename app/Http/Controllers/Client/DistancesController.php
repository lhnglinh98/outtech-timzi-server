<?php


namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Distances;
use App\Http\Utils\SystemParam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DistancesController extends Controller
{
    public function createDistances(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'price_discount' => 'required',
        ], [
            'title.required' => 'Vui lòng chọn ngưỡng KM',
            'price_discount.required' => 'Vui lòng nhập giá ',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }

        $req = $request->all();
        $distances = Distances::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $distances);
    }
    public function updateDistances($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check = Distances::findorfail($id);

        $req = $request->all();

        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function listdataDistances()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $dataDistances = Distances::select('distances.*')->orderby('id', 'desc')->paginate(20);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $dataDistances);
    }
    public function listdataDistancesStatus($status)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $dataDistances = Distances::select('distances.*')->where('status', $status)->orderby('id', 'desc')->paginate(20);

        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $dataDistances);
    }
    public function deleteDistances($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check_distances = Distances::where([['id', $id]])->firstorfail();
        $check_distances->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check_distances);
    }
}
