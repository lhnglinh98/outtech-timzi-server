<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TypeMaterial;
use App\Models\Material;
use App\Models\Store;
use App\Models\UserStore;
use App\Http\Utils\SystemParam;
use Illuminate\Support\Facades\Validator;
class TypeMaterialController extends Controller
{
    public function listTypeMaterial($store_id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $data = TypeMaterial::where([['store_id', $store_id], ['status', 1]])->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function typeMaterialDetail($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = TypeMaterial::where('id', $id)->first();
        if(!$data){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_TypeMaterial_not_found, null);
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $data->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $data->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createTypeMaterial(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'name' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng.',
            'name.required' => 'Vui lòng nhập tên vật tư.',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $req = $request->all();
        $create = TypeMaterial::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function updateTypeMaterial($id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = TypeMaterial::find($id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_TypeMaterial_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'name' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng.',
            'name.required' => 'Vui lòng nhập tên vật tư.',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $req = $request->all();
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteTypeMaterial($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = TypeMaterial::find($id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_TypeMaterial_not_found, null);
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $check->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $check->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        Material::where('type_material_id', $id)->update([
            'status' => 0
        ]);
        $check->update([
            'status' => 0
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check);
    }
}
