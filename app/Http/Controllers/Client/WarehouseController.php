<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ImportWarehouse;
use App\Models\ImportWarehouseDetail;
use App\Models\ExportWarehouse;
use App\Models\ExportWarehouseDetail;
use App\Models\Store;
use App\Models\UserStore;
use App\Http\Utils\SystemParam;
use App\Models\Material;
use App\Models\Supplier;
use Illuminate\Support\Facades\Validator;
class WarehouseController extends Controller
{
    public function importWarehouse(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'supplier_id' => 'required',
            'user_import_name' => 'required',
            'date_import' => 'required',
            'note' => 'required',
            'material' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng.',
            'supplier_id.required' => 'Vui lòng chọn nhà cung cấp.',
            'user_import_name.required' => 'Vui lòng điền tên người nhập.',
            'date_import.required' => 'Vui lòng điền ngày nhập.',
            'note.required' => 'Vui lòng điền ghi chú.',
            'material.required' => 'Vui lòng chọn vật tư.',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $check_supplier = Supplier::find($request->supplier_id);
        if(!$check_supplier){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_supplier_not_found, null);
        }
        $req = $request->only('store_id', 'supplier_id', 'user_import_name', 'date_import', 'note');
        do {
            $req['code'] = 'TZW' . $this->makeCodeRandom(8);
        } while (ImportWarehouse::where('code', $req['code'])->exists());
        $create = ImportWarehouse::create($req);
        $material = json_decode($request->material);
        $total_quantity = 0;
        $total_money = 0;
        if(is_array($material) && count($material) > 0){
            foreach($material as $item){
                $check_material = Material::where([['id', $item->material_id], ['status', 1]])->first();
                if($check_material){
                    ImportWarehouseDetail::insert([
                        'import_warehouse_id' => $create->id,
                        'material_id' => $item->material_id,
                        'quantity' => $item->quantity,
                        'money' => $item->money
                    ]);
                    $total_quantity += $item->quantity;
                    $total_money += ($item->money * $item->quantity);
                }
            }
        }
        $create->update([
            'quantity' => $total_quantity,
            'total_money' => $total_money
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function exportWarehouse(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'user_export_name' => 'required',
            'date_export' => 'required',
            'note' => 'required',
            'material' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng.',
            'user_export_name.required' => 'Vui lòng điền tên người nhập.',
            'date_export.required' => 'Vui lòng điền ngày nhập.',
            'note.required' => 'Vui lòng điền ghi chú.',
            'material.required' => 'Vui lòng chọn vật tư.',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $request->store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $request->store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $req = $request->only('store_id', 'user_export_name', 'date_export', 'note');
        do {
            $req['code'] = 'TZW' . $this->makeCodeRandom(8);
        } while (ExportWarehouse::where('code', $req['code'])->exists());
        $material = json_decode($request->material);
        if(is_array($material) && count($material) > 0){
            foreach($material as $item){
                $check_material = Material::where([['id', $item->material_id], ['status', 1]])->first();
                if($check_material){
                    $quantity_import = ImportWarehouseDetail::where('material_id', $item->id)->SUM('quantity');
                    $quantity_export = ExportWarehouseDetail::where('material_id', $item->id)->SUM('quantity');
                    if($item->quantity > ($quantity_import - $quantity_export)){
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Số lượng vật tư trong kho không đủ để xuất kho.", null);
                    };
                }
            }
        }
        $create = ExportWarehouse::create($req);
        $total_quantity = 0;
        $total_money = 0;
        if(is_array($material) && count($material) > 0){
            foreach($material as $item){
                $check_material = Material::where([['id', $item->material_id], ['status', 1]])->first();
                if($check_material){
                    ExportWarehouseDetail::insert([
                        'export_warehouse_id' => $create->id,
                        'material_id' => $item->material_id,
                        'quantity' => $item->quantity,
                        'money' => $item->money
                    ]);
                    $total_quantity += $item->quantity;
                    $total_money += ($item->money * $item->quantity);
                }
            }
        }
        $create->update([
            'quantity' => $total_quantity,
            'total_money' => $total_money
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function listImportWarehouse($store_id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['date_import', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['date_import', '<=', $to_date];
        }
        $data = ImportWarehouse::where('store_id', $store_id)->where($where)->with('supplier', 'importWarehouseDetail.material')->orderby('id', 'desc')->paginate(12);
        $data2 = ImportWarehouse::where('store_id', $store_id)->where($where)->get();
        $array = array(
            'list' => $data,
            'total_quantity' => $data2->SUM('quantity'),
            'total_money' => $data2->SUM('total_money')
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
    public function listExportWarehouse($store_id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['date_export', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['date_export', '<=', $to_date];
        }
        $data = ExportWarehouse::where('store_id', $store_id)->where($where)->with('exportWarehouseDetail.material')->orderby('id', 'desc')->paginate(12);
        $data2 = ExportWarehouse::where('store_id', $store_id)->where($where)->get();
        $array = array(
            'list' => $data,
            'total_quantity' => $data2->SUM('quantity'),
            'total_money' => $data2->SUM('total_money')
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
    public function listMaterialWarehouse($store_id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->role_id == 2) {
            if ($user->is_store_owner_and_chain_owner == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            if(!Store::where([['id', $store_id], ['user_id', $user->id]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }else{
            if(!UserStore::where([['store_id', $store_id], ['user_id', $user->id], ['is_owner', 1]])->first()){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
        }
        $where1 = [];
        $where2 = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where1[] = ['import_warehouse.date_import', '>=', $from_date];
            $where2[] = ['export_warehouse.date_export', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where1[] = ['import_warehouse.date_import', '<=', $to_date];
            $where2[] = ['export_warehouse.date_export', '<=', $to_date];
        }
        $list_material = Material::where([['store_id', $store_id], ['status', 1]])->paginate(12);
        foreach($list_material as $item){
            $quantity_import = ImportWarehouseDetail::Join('import_warehouse', 'import_warehouse.id', 'import_warehouse_detail.import_warehouse_id')->where('import_warehouse_detail.material_id', $item->id)->where($where1)->get()->SUM('import_warehouse_detail.quantity');
            $quantity_export = ExportWarehouseDetail::Join('export_warehouse', 'export_warehouse.id', 'export_warehouse_detail.export_warehouse_id')->where('export_warehouse_detail.material_id', $item->id)->where($where2)->get()->SUM('export_warehouse_detail.quantity');
            $item['total_quantity'] = (int)$quantity_import - (int)$quantity_export;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $list_material);
    }
}
