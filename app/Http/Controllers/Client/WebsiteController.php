<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Utils\SystemParam;
use App\Models\CategoryQuestionWebsite;
use App\Models\QuestionWebsite;
use App\Models\HomeWebsite;
use App\Models\FeatureWebsite;
use App\Models\ContactWebsite;
use App\Models\LeadershipTeam;
use App\Models\TutorialDocument;
use App\Models\UsedSolution;
use App\Models\LinkYoutube;
use App\Models\ContentToward;
use App\User;
use Illuminate\Support\Facades\Validator;
class WebsiteController extends Controller
{
    //
    public function linkYoutubeDetail(){
        $data = LinkYoutube::first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function contentTowardDetail(){
        $data = ContentToward::first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function homeWebSiteDetail(){
        $data = HomeWebsite::first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listFeatureWebsite(){
        $data = FeatureWebsite::all();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listLeadershipTeam(){
        $data = LeadershipTeam::all();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listTutorialDocument(){
        $data = TutorialDocument::all();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listUsedSolution(){
        $data = UsedSolution::orderby('id', 'desc')->limit(7)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCategoryQuestionWebsite()
    {
        $data = CategoryQuestionWebsite::limit(4)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createContact(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên',
            'name.string' => 'Tên phải là kiểu chuỗi',
            'name.max' => 'Tên không quá :max kí tự',
            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'Định dạng email không đúng',
            'message.required' => 'Vui lòng nhập nội dung muốn gửi',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $create = ContactWebsite::create($request->all());
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
}
