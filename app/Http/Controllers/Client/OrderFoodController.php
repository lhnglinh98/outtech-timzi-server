<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\AddressUser;
use App\Models\CardFood;
use App\Models\CardToppingFood;
use App\Models\ComboFood;
use App\Models\FeeShip;
use App\Models\Food;
use App\Models\FoodSize;
use App\Models\HistoryMoneyShipper;
use App\Models\OrderFood;
use App\Models\OrderFoodDetail;
use App\Models\OrderToppingFoodDetail;
use App\Models\OrderUserProduct;
use App\Models\PaymentMethod;
use App\Models\ProgramStore;
use App\Models\ProgramStoreDetail;
use App\Models\Radius;
use App\Models\RevenueStore;
use App\Models\ReviewShipper;
use App\Models\SetupPercentCoin;
use App\Models\SetupPercentFeeShipper;
use App\Models\ShipperStore;
use App\Models\Store;
use App\Models\ToppingFood;
use App\Models\UserStore;
use App\Models\Voucher;
use App\Models\VoucherUser;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Polyline;

class OrderFoodController extends Controller
{
    //
    protected $notifycontroller;
    public function __construct()
    {
        $this->notifycontroller = new NotifyController();
    }
    public function updateTotalMoneyFood()
    {
        $data = OrderFood::get();
        foreach ($data as $d) {
            OrderFood::where('id', $d->id)->update([
                'total_money_food' => $d->total_money,
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, null);
    }
    public function addCardFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'food_id' => 'required',
            'quantity' => 'required',
        ], [
            'food_id.required' => 'Vui lòng chọn món ăn',
            'quantity.required' => 'Vui lòng chọn số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $food = Food::find($request->food_id);
        if (!$food) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_food_not_found, null);
        }
        if ($food->status != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_food_status, null);
        }
        // $check_order_detail = OrderFoodDetail::where('order_food_id', $order_id)->get();
        // foreach($check_order_detail as $c){
        //     if($c->food_id){
        // $ls_program = ProgramStoreDetail::where('food_id', $c->food_id)->pluck('program_store_id')->toArray();
        // $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
        // if(count($check_program) > 0){
        //     foreach($check_program as $p){
        //         if($p->quantity >= $c->quantity){
        //             ProgramStore::where('id', $p->id)->update([
        //                 'quantity' => $p->quantity - $c->quantity
        //             ]);
        //         }else{
        //             return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình không đủ để nhận hàng.", null);
        //         }
        //     }
        // }
        //     }
        // }
        $check_store_food = Store::find($food->store_id);
        if ($check_store_food && $check_store_food->is_open == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_store_not_open, null);
        }
        $req = $request->all();
        $req['user_id'] = $user->id;
        $req['store_id'] = $food->store_id;
        $price = $food->price * $request->quantity;
        if ($food->price_discount == 0 && $food->price_discount_with_program == 0) {
            $price = $food->price * $request->quantity;
        } else if ($food->price_discount != 0 && $food->price_discount_with_program == 0) {
            $price = $food->price_discount * $request->quantity;
        } else if ($food->price_discount_with_program != 0) {
            $price = $food->price_discount_with_program * $request->quantity;
        }
        $ls_program = ProgramStoreDetail::where('food_id', $request->food_id)->pluck('program_store_id')->toArray();
        $check_program = ProgramStore::whereIn('id', $ls_program)->where([['status', 1], ['is_active', 1]])->get();
        if (count($check_program) > 0) {
            $card_2 = CardFood::where([['food_id', $request->food_id], ['user_id', $user->id], ['status', 0]])->get();
            if ($card_2->SUM('quantity') + $request->quantity > $check_program->SUM('quantity')) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình được phép đặt chỉ còn " . $check_program->SUM('quantity') . " số món.", null);
            }
        }
        if ($request->size_id) {
            $size = FoodSize::find($request->size_id);
            if (!$size) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_size_not_found, null);
            }
            $req['size'] = $size->name;
            $price = $price + $size->price * $request->quantity;
        }
        if ($request->topping_food_id && is_array($request->topping_food_id)) {
            $ls_topping_food_id = array_unique($request->topping_food_id);
            $topping_food = ToppingFood::whereIn('id', $ls_topping_food_id)->with('categoryToppingFood')->get();
            if (count($topping_food) == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_topping_food_id_not_found, null);
            }
            if (isset($req['size'])) {
                $check = CardFood::where([['food_id', $request->food_id], ['user_id', $user->id], ['size', $req['size']], ['status', 0]])->get();
            } else {
                $check = CardFood::where([['food_id', $request->food_id], ['user_id', $user->id], ['status', 0]])->get();
            }
            $card_food_id = null;
            if (count($check) > 0) {
                foreach ($check as $c) {
                    $card_topping_id = CardToppingFood::where('card_food_id', $c->id)->pluck('topping_food_id')->toArray();
                    $check_array = array_intersect($card_topping_id, $ls_topping_food_id);
                    if (count($check_array) == count($card_topping_id) && count($check_array) == count($ls_topping_food_id)) {
                        $card_food_id = $c->id;
                    }
                }
            }
            $price = $price + $topping_food->SUM('price') * $request->quantity;
            if ($card_food_id != null) {
                $check_card = CardFood::find($card_food_id);
                $check_card->update([
                    'quantity' => $check_card->quantity + $request->quantity,
                    'price' => $check_card->price + $price,
                ]);
            } else {
                $req['price'] = $price;

                $check_card = CardFood::create($req);
                foreach ($topping_food as $t) {
                    $create = CardToppingFood::insert([
                        'card_food_id' => $check_card->id,
                        'topping_food_id' => $t->id,
                        'category_topping_food_name' => $t->categoryToppingFood->name,
                        'topping_food_name' => $t->name,
                        'price' => $t->price,
                    ]);
                }
            }
        } else {
            $req['price'] = $price;
            if (isset($req['size'])) {
                $check_card = CardFood::where([['food_id', $request->food_id], ['user_id', $user->id], ['size', $req['size']], ['status', 0]])->first();
            } else {
                $check_card = CardFood::where([['food_id', $request->food_id], ['user_id', $user->id], ['status', 0]])->first();
            }
            if ($check_card) {
                $check_card->update([
                    'quantity' => $check_card->quantity + $request->quantity,
                    'price' => $check_card->price + $req['price'],
                ]);
            } else {
                $check_card = CardFood::create($req);
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::add_card_food_success, $check_card->load('toppingFood'));
    }
    public function addCardComboFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'combo_food_id' => 'required',
            'quantity' => 'required',
        ], [
            'combo_food_id.required' => 'Vui lòng chọn combo',
            'quantity.required' => 'Vui lòng chọn số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $combo_food = ComboFood::where([['id', $request->combo_food_id], ['status', 1], ['is_active', 1]])->first();
        if (!$combo_food) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_combo_food_not_found, null);
        }
        // if ($combo_food->quantity < $request->quantity) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_id_combo_food_quantity, null);
        // }
        $card_2 = CardFood::where([['combo_food_id', $request->combo_food_id], ['user_id', $user->id], ['status', 0]])->get();
        if ($card_2->SUM('quantity') + $request->quantity > $combo_food->quantity) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Combo được phép đặt chỉ còn " . $combo_food->quantity . " số món.", null);
        }
        $check_store_food = Store::find($combo_food->store_id);
        if ($check_store_food && $check_store_food->is_open == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_store_not_open, null);
        }
        $req = $request->all();
        $req['user_id'] = $user->id;
        $req['store_id'] = $combo_food->store_id;
        $price = $combo_food->price * $request->quantity;
        $req['price'] = $price;
        $check_card = CardFood::where([['combo_food_id', $request->combo_food_id], ['user_id', $user->id], ['status', 0]])->first();
        if ($check_card) {
            $check_card->update([
                'quantity' => $check_card->quantity + $request->quantity,
                'price' => $check_card->price + $req['price'],
            ]);
        } else {
            $check_card = CardFood::create($req);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::add_card_food_success, $check_card);
    }
    public function listCardwithStore($store_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_store = Store::findorfail($store_id);
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = CardFood::where([['store_id', $store_id], ['user_id', $user->id], ['status', 0]])->with('food', 'store', 'toppingFood', 'comboFood.food')->get();
        $total_price = $data->SUM('price');
        $latitude_store = $check_store->latitude;
        $longtidue_store = $check_store->longtidue;
        $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
        if (isset($response)) {
            $distance = $response['value'] / 1000;
        } else {
            $distance = 0;
        }
        $check = FeeShip::where([['from_range', '<=', $distance], ['to_range', '>', $distance]])
            ->orWhere([['from_range', '<=', $distance], ['to_range', null]])->first();
        if ($check) {
            $fee_ship = $check->money;
        } else {
            $fee_ship = 0;
        }
        $money_voucher = 0;
        if ($request->voucher_id) {
            $check_voucher = Voucher::where([['status', 2], ['id', $request->voucher_id]])->first();
            $check_voucher_user = VoucherUser::where([['voucher_id', $request->voucher_id], ['user_id', $user->id], ['status', 1]])->first();
            if (!$check_voucher || !$check_voucher_user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_voucher_not_found, null);
            }
            if ($check_voucher->proviso > $total_price) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_voucher_proviso_invalid, null);
            }
            if ($check_voucher->type == 1) {
                if ($check_voucher->type_percent_or_money == 1) {
                    $money_voucher = $total_price * $check_voucher->percent / 100;
                } else {
                    $money_voucher = $check_voucher->money;

                }
                $total_price = $total_price - $money_voucher;
                if ($total_price < 0) {
                    $total_price = 0;
                    $money_voucher = $total_price;
                }
            } else {
                if ($check_voucher->type_percent_or_money == 1) {
                    $money_voucher = $fee_ship * $check_voucher->percent / 100;
                    $fee_ship = $fee_ship - $fee_ship * $check_voucher->percent / 100;
                    if ($fee_ship < 0) {
                        $fee_ship = 0;
                        $money_voucher = $fee_ship;
                    }
                } else {
                    $money_voucher = $check_voucher->money;
                    $fee_ship = $fee_ship - $check_voucher->money;
                    if ($fee_ship < 0) {
                        $fee_ship = 0;
                        $money_voucher = $fee_ship;
                    }
                }
            }
        }
        $coin_use = 0;
        $coin_residual = 0;
        if ($request->is_coin == 1) {
            if ($user->coin > 0) {
                if ($total_price >= $user->coin) {
                    $coin_use = $user->coin;
                    $coin_residual = 0;
                } else {
                    $coin_use = $total_price;
                    $coin_residual = $user->coin - $total_price;
                }
            }
        }
        if ($fee_ship < 0) {
            $fee_ship = 0;
        }
        $data1 = array(
            'list_card' => $data,
            'total_quantity' => $data->SUM('quantity'),
            'total_price' => $data->SUM('price'),
            'total_money_order' => $total_price - $coin_use + $fee_ship,
            'money_voucher' => $money_voucher,
            'fee_ship' => $fee_ship,
            'coin_use' => $coin_use,
            'coin_residual' => $coin_residual,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data1);
    }
    public function listCardWithFood($food_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = CardFood::where([['food_id', $food_id], ['user_id', $user->id], ['status', 0]])->with('food', 'store', 'toppingFood')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function cardDetail($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = CardFood::where([['id', $id], ['user_id', $user->id]])->firstorfail()->load('food', 'store', 'toppingFood', 'comboFood.food');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function deleteCard($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = CardFood::where([['id', $id], ['user_id', $user->id]])->firstorfail();
        $delete_topping_card = CardToppingFood::where('card_food_id', $id)->delete();
        $data->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $data);
    }
    public function subtractQuantityFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'card_id' => 'required',
            'quantity' => 'required',
        ], [
            'card_id.required' => 'Vui lòng chọn sản phẩm',
            'quantity.required' => 'Vui lòng chọn số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $data = CardFood::where([['id', $request->card_id], ['user_id', $user->id]])->firstorfail();
        if ($request->quantity <= 0) {
            $delete_topping_card = CardToppingFood::where('card_food_id', $data->id)->delete();
            $data->delete();
        } else {
            if ($data->food_id) {
                $ls_program = ProgramStoreDetail::where('food_id', $data->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where([['status', 1], ['is_active', 1]])->get();
                if (count($check_program) > 0) {
                    $card_2 = CardFood::where([['food_id', $data->food_id], ['user_id', $user->id], ['status', 0]])->get();
                    if ($card_2->SUM('quantity') + $request->quantity > $check_program->SUM('quantity')) {
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình được phép đặt chỉ còn " . $check_program->SUM('quantity') . " số món.", null);
                    }
                }
            } else if ($data->combo_food_id) {
                $check_combo = ComboFood::find($data->combo_food_id);
                if ($check_combo) {
                    $card_2 = CardFood::where([['combo_food_id', $request->combo_food_id], ['user_id', $user->id], ['status', 0]])->get();
                    if ($card_2->SUM('quantity') + $request->quantity > $check_combo->quantity) {
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Combo được phép đặt chỉ còn " . $check_combo->quantity . " số món.", null);
                    }
                }
            }
            $price_1_product = $data->price / $data->quantity;
            $data->update([
                'quantity' => $request->quantity,
                'price' => $price_1_product * $request->quantity,
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $data);
    }
    public function checkFeeShip(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'distance_value' => 'required',
        ], [
            'distance_value.required' => 'Vui lòng nhập khoảng cách',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = FeeShip::where([['from_range', '<', $request->distance_value], ['to_range', '>=', $request->distance_value]])
            ->orWhere([['from_range', '<', $request->distance_value], ['to_range', null]])->first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $check);
    }
    public function testRadius(Request $request)
    {
        $list_shipper = User::where([['status', 1], ['role_id', 4]])->get();
        // return $list_shipper;
        $data = [];
        $check_radius = Radius::where('id', 1)->first();
        foreach ($list_shipper as $ls) {
            $client = new Client();
            $data1 = $this->googleApi($ls->latitude, $ls->longtidue, 21.0042496, 105.8033839);
            // $data[] =  $data1;
            // $data1 = $client->get("https://maps.googleapis.com/maps/api/directions/json?origin=$ls->latitude, $ls->longtidue&destination=$latitude_store, $longtidue_store&key=AIzaSyCR2s4FVEi38-3U4az5ykzCl6mWGiBT61w");
            // $response = json_decode($data1->getBody()->getContents(), true);
            if (isset($data1)) {
                $distance_value = $data1['value'] / 1000;
                if ($distance_value <= $check_radius->radius) {
                    $data[] = $ls->id;
                }
            }
        }
        return $data;
    }
    public function orderFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'date_time' => 'required|after_or_equal:today',
            'user_name' => 'required|string|max:255',
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'address' => 'required',
            'latitude' => 'required',
            'longtidue' => 'required',
            'fee_ship' => 'required',
            'discount' => 'required',
            'total_money' => 'required',
            'coin' => 'required',
            'payment_method_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'date_time.required' => 'Vui lòng chọn giày giờ giao hàng',
            'date_time.date_format' => 'Vui lòng nhập đúng định dạng',
            'date_time.after_or_equal' => 'Không được nhập thời gian trước ngày hôm nay',
            'user_name.required' => 'Vui lòng nhập tên',
            'user_name.string' => 'Tên phải là kiểu chuỗi',
            'user_name.max' => 'Tên không quá :max kí tự',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'address.required' => 'Vui lòng nhập địa chỉ cụ thể',
            'latitude.required' => 'Vui lòng nhập vĩ độ của bạn',
            'longtidue.required' => 'Vui lòng nhập kinh độ của bạn',
            'fee_ship.required' => 'Vui lòng gửi phí ship',
            'discount.required' => 'Vui lòng gửi tiền giảm giá',
            'total_money.required' => 'Vui lòng gửi tổng tiền',
            'coin.required' => 'Vui lòng gửi số xu',
            'payment_method_id.required' => 'Vui lòng chọn phương thức thanh toán',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $check_store_food = Store::find($request->store_id);
        if ($check_store_food && $check_store_food->is_open == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_store_not_open, null);
        }
        if ($request->payment_method_id == 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Chức năng thanh toán Vnpay hiện đang bảo trì, vui lòng chọn thanh toán bằng tiền mặt.", null);
        }
        $check_payment_method = PaymentMethod::findorfail($request->payment_method_id);
        $check_store = Store::findorfail($request->store_id);
        $check_card_food = CardFood::where([['user_id', $user->id], ['store_id', $request->store_id], ['status', 0]])->with('toppingFood')->get();
        if (count($check_card_food) == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_card_food_not_found, null);
        }
        if ($check_store->open_hours && $check_store->close_hours) {
            $time_open_hours = date('H', strtotime($check_store->open_hours)) * 3600 + date('i', strtotime($check_store->open_hours)) * 60;
            $time_close_hours = date('H', strtotime($check_store->close_hours)) * 3600 + date('i', strtotime($check_store->close_hours)) * 60;
            $time_order = date('H', strtotime($request->date_time)) * 3600 + date('i', strtotime($request->date_time)) * 60;
            if ($time_open_hours > $time_order || $time_close_hours < $time_order) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_time_order, null);
            }
        }
        $req = $request->all();
        if ($request->coin > 0 && $user->coin < $request->coin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_coin_user, null);
        }
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (OrderFood::where('code', $req['code'])->exists());
        $req['user_id'] = $user->id;
        $req['date_time'] = date('Y-m-d H:i:s', strtotime($request->date_time));
        $user_name = trim($request->user_name);
        $phone = trim($request->phone);
        $address = trim($request->address);
        if ($request->type && $request->type == 1) {
            $req['status'] = SystemParam::status_order_confirmed;
        } else {
            $req['status'] = SystemParam::status_order_wait_update;
        }
        if ($request->is_relatives) {
            $req['is_relatives'] = 1;
        }
        if ($request->payment_method_id == 2) {
            $req['status_payment_success'] = 1;
        }
        foreach ($check_card_food as $c1) {
            if ($c1->food_id) {
                $check_food = Food::find($c1->food_id);
                if($check_food && $check_food->price_discount_with_program > 0){
                    $ls_program = ProgramStoreDetail::where('food_id', $c1->food_id)->pluck('program_store_id')->toArray();
                    $check_program = ProgramStore::whereIn('id', $ls_program)->where([['status', 1], ['is_active', 1]])->get();
                    if (count($check_program) > 0) {
                        $card = CardFood::where([['user_id', $user->id], ['store_id', $request->store_id], ['status', 0], ['food_id', $c1->food_id]])->get();
                        foreach ($check_program as $p) {
                            if ($p->quantity < $card->SUM('quantity')) {
                                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình không đủ để đặt hàng.", null);
                            }
                        }
                        foreach ($check_program as $p) {
                            $card = CardFood::where([['user_id', $user->id], ['store_id', $request->store_id], ['status', 0], ['food_id', $c1->food_id]])->get();
                            if ($p->quantity >= $card->SUM('quantity')) {
                                ProgramStore::where('id', $p->id)->update([
                                    'quantity' => $p->quantity - $c1->quantity,
                                ]);
                            }
                        }
                    }else{
                        $check_program_near = ProgramStore::whereIn('id', $ls_program)->where([['status', 2], ['time_open', '<=', date('Y-m-d', strtotime($c1->created_at))], ['time_close', '>', date('Y-m-d', strtotime($c1->created_at))]])->orwhere([['is_active', 0], ['time_open', '<=', date('Y-m-d', strtotime($c1->created_at))], ['time_close', '>', date('Y-m-d', strtotime($c1->created_at))]])->orderby('id', 'desc')->first();
                        if($check_program_near){
                            if ($check_program_near->type_percent_or_money == 1) {
                                $price = $check_food->price - $check_food->price * $check_program_near->percent / 100;
                                if ($check_food->price_discount > 0) {
                                    $price = $check_food->price_discount - $check_food->price_discount * $check_program_near->percent / 100;
                                }
                            } else {
                                $price = $check_food->price - $check_program_near->money;
                                if ($check_food->price_discount > 0) {
                                    $price = $check_food->price_discount - $check_program_near->money;
                                }
                            }
                            if($price == $check_food->price_discount_with_program){
                                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Chương trình danh cho món ăn " . $check_food->name . " đã hết, quý khách vui lòng đặt lại món khác.", null);
                            }
                        }
                    }
                }

            } else if ($c1->combo_food_id) {
                $check_combo = ComboFood::find($c1->combo_food_id);
                if ($check_combo) {
                    $card = CardFood::where([['user_id', $user->id], ['store_id', $request->store_id], ['status', 0], ['combo_food_id', $c1->combo_food_id]])->get();
                    if ($card->SUM('quantity') > $check_combo->quantity) {
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Combo không đủ để đặt hàng.", null);
                    } else {
                        $check_combo->update([
                            'quantity' => $check_combo->quantity - $card->SUM('quantity'),
                        ]);
                    }
                }
            }
        }
        if ($request->voucher_id) {
            $check_voucher = Voucher::where([['status', 2], ['id', $request->voucher_id]])->first();
            $check_voucher_user = VoucherUser::where([['voucher_id', $request->voucher_id], ['user_id', $user->id], ['status', 1]])->first();
            if (!$check_voucher || !$check_voucher_user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_voucher_not_found, null);
            }
            // if ($check_voucher->proviso > $request->total_money) {
            //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_voucher_proviso_invalid, null);
            // }
            if ($request->discount > 0) {
                $req['type_voucher'] = $check_voucher->type;
            }
            $check_voucher_user = VoucherUser::where([['voucher_id', $request->voucher_id], ['user_id', $user->id], ['status', 1]])->update([
                'status' => 2,
            ]);
        }
        $create = OrderFood::create($req);
        $check_store->update([
            'count_order' => $check_store->count_order + 1,
        ]);
        // if ($request->coin > 0) {
        //     // use App\Models\SetupPercentCoin;
        //     $user->update([
        //         'coin' => $user->coin - $request->coin,
        //     ]);
        //     CoinHistory::insert([
        //         'user_id' => $user->id,
        //         'content' => "Bạn đã sử dụng " . $request->coin . " xu để đặt đơn đặt hàng có mã là " . $req['code'],
        //         'coin' => $request->coin,
        //         'type' => 2,
        //         'created_at' => date('Y-m-d H:i:s', time()),
        //     ]);
        // }

        foreach ($check_card_food as $c) {
            $create_detail = OrderFoodDetail::insertGetId([
                'order_food_id' => $create->id,
                'combo_food_id' => $c->combo_food_id,
                'food_id' => $c->food_id,
                'user_id' => $user->id,
                'quantity' => $c->quantity,
                'price' => $c->price,
                'store_id' => $request->store_id,
            ]);
            if ($c->combo_food_id) {
                $update_combo = ComboFood::where('id', $c->combo_food_id)->first();
                if ($update_combo) {
                    $update_combo->update([
                        'quantity' => $update_combo->quantity - $c->quantity,
                    ]);
                }
            }
            if (count($c->toppingFood) > 0) {
                foreach ($c->toppingFood as $d) {
                    $create_topping_order = OrderToppingFoodDetail::insert([
                        'order_food_detail_id' => $create_detail,
                        'topping_food_id' => $d->topping_food_id,
                        'category_topping_food_name' => $d->category_topping_food_name,
                        'topping_food_name' => $d->topping_food_name,
                        'price' => $d->price,
                    ]);
                }
            }
        }
        $check_address = AddressUser::where([['user_name', $user_name], ['phone', $phone], ['address', $address]])->first();
        if (!$check_address) {
            $create_address = AddressUser::insert([
                'user_id' => $user->id,
                'user_name' => $user_name,
                'phone' => $phone,
                'address' => $address,
                'latitude' => $request->latitude,
                'longtidue' => $request->longtidue,
            ]);
        }
        $update = CardFood::where([['user_id', $user->id], ['store_id', $request->store_id], ['status', 0]])->update([
            'status' => 1,
        ]);
        $client = new Client();
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $latitude_store = $check_store->latitude;
        $longtidue_store = $check_store->longtidue;

        $data1 = $client->get("https://rsapi.goong.io/Direction?origin=$latitude_store,$longtidue_store&destination=$latitude,$longtidue&alternatives=false&vehicle=car&api_key=30upqHvPKEJYB36pUzKdcO5gj6g6wtVqevvBjrZf");
        // $data1 = $client->get("https://maps.googleapis.com/maps/api/directions/json?origin=$latitude_store, $longtidue_store&destination=$latitude, $longtidue&key=AIzaSyCR2s4FVEi38-3U4az5ykzCl6mWGiBT61w");
        $response = json_decode($data1->getBody()->getContents(), true);
        if (isset($response['routes'][0]['overview_polyline']['points'])) {
            $encoded = $response['routes'][0]['overview_polyline']['points'];
            $points = Polyline::decode($encoded);
            $points = Polyline::pair($points);
            $array = [[
                'latitude' => (float) $latitude_store,
                'longitude' => (float) $longtidue_store,
            ]];
            foreach ($points as $p) {
                $array[] = array(
                    'latitude' => $p[0],
                    'longitude' => $p[1],
                );
            }
            $array[] = array(
                'latitude' => (float) $latitude,
                'longitude' => (float) $longtidue,
            );
            OrderFood::where('id', $create->id)->update([
                'polyline' => json_encode($array),
            ]);
        }
        if ($req['status'] == SystemParam::status_order_confirmed) {
            $id = UserStore::where([['status', 1], ['store_id', $request->store_id]])->pluck('user_id')->toArray();
            $id = [$check_store->user_id];
            $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' đã đặt 1 đơn hàng mới', $create->id, SystemParam::type_order_with_store, $create, $user->name);
            $ls_shipper_id = ShipperStore::where([['store_id', $check_store->id], ['status', 1]])->pluck('user_id')->toArray();
            if (count($ls_shipper_id) > 0) {
                $this->notifycontroller->pushNotify($ls_shipper_id, 'Có 1 đơn hàng mới của cửa hàng tên ' . $check_store->name . ' cần bạn giao đến khách hàng ' . $user->name, $create->id, SystemParam::type_order_with_shipper, $create, $user->name);

            } else {
                $list_shipper = User::where([['status', 1], ['role_id', 4]])->get();
                $data = [];
                $check_radius = Radius::where('id', 1)->first();
                $latitude_store = $check_store->latitude;
                $longtidue_store = $check_store->longtidue;
                foreach ($list_shipper as $ls) {
                    $client = new Client();
                    $data1 = $this->googleApi($ls->latitude, $ls->longtidue, $latitude_store, $longtidue_store);
                    // $data1 = $client->get("https://maps.googleapis.com/maps/api/directions/json?origin=$ls->latitude, $ls->longtidue&destination=$latitude_store, $longtidue_store&key=AIzaSyCR2s4FVEi38-3U4az5ykzCl6mWGiBT61w");
                    // $response = json_decode($data1->getBody()->getContents(), true);
                    if (isset($data1)) {
                        $distance_value = $data1['value'] / 1000;
                        if ($distance_value <= $check_radius->radius) {
                            $data[] = $ls->id;
                        }
                    }
                }
                $update_wait_shipper = OrderFood::where('id', $create->id)->update([
                    'time_check' => date('Y-m-d H"i"s', time()),
                    'quantity_push' => 1,
                ]);
                $this->notifycontroller->pushNotify($data, 'Có 1 đơn hàng mới của cửa hàng tên ' . $check_store->name . ' cần bạn giao đến khách hàng ' . $user->name, $create->id, SystemParam::type_order_with_shipper, $create, $user->name);
            }
            // $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' đã đặt 1 đơn hàng mới', $create->id, SystemParam::type_order_with_store, $create);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::order_success, $create);
    }
    public function vnpayIPNRechargeOrder(Request $request)
    {
        try {
            $inputData = array();
            foreach ($request->all() as $key => $value) {
                if (substr($key, 0, 4) == "vnp_" && $value != null && trim($value) != "") {
                    $inputData[$key] = $value;
                }
            }
            unset($inputData['vnp_SecureHashType']);
            unset($inputData['vnp_SecureHash']);
            ksort($inputData);
            $vnp_SecureHash = $request->vnp_SecureHash;
            $i = 0;
            $hashData = "";
            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashData = $hashData . '&' . $key . "=" . $value;
                } else {
                    $hashData = $hashData . $key . "=" . $value;
                    $i = 1;
                }
            }
            $vnp_HashSecret = "88CO54399XDYPLDD6WK8BG854K8NMZWF";
            $secureHash = hash($request->vnp_SecureHashType, $vnp_HashSecret . $hashData);
            if ($request->vnp_Amount) {
                $vnp_Amount = $request->vnp_Amount / 100;
            }
            $check = OrderFood::where([['code', $request->vnp_TxnRef]])->first();
            if (!$check) {
                return response()->json([
                    'Message' => "Order Not Found",
                    'RspCode' => "01",
                ]);
            }
            if ($vnp_Amount != $check->total_money) {
                return response()->json([
                    'Message' => "Invalid amount",
                    'RspCode' => "04",
                ]);
            }
            if (trim($request->vnp_SecureHash) == trim($secureHash)) {
                if ($check->status_payment_vnpay != 0) {
                    return response()->json([
                        'Message' => "Order already confirmed",
                        'RspCode' => "02",
                    ]);
                }
                if ($request->vnp_ResponseCode != null) {
                    if ($request->vnp_ResponseCode == "00") {
                        OrderFood::where('code', $request->vnp_TxnRef)->update([
                            'status_payment_vnpay' => 1,
                            'status_payment_success' => 1,
                        ]);
                    } else {
                        OrderFood::where('code', $request->vnp_TxnRef)->update([
                            'status_payment_vnpay' => 2,
                        ]);
                    }
                    if ($request->vnp_ResponseCode == "99") {
                        OrderFood::where('code', $request->vnp_TxnRef)->update([
                            'status_payment_vnpay' => 2,
                        ]);
                        return response()->json([
                            'Message' => "Confirm Success",
                            'RspCode' => "00",
                        ]);
                    }
                }
                return response()->json([
                    'Message' => "Confirm Success",
                    'RspCode' => "00",
                ]);
            } else {
                return response()->json([
                    'Message' => "Invalid Checksum",
                    'RspCode' => "97",
                ]);
            }
        } catch (\Exception $exception) {
            return response()->json([
                "RspCode" => "99",
                "Message" => "Unknow error",
            ]);
        }
    }
    public function vnpayIPNRechargeOrderLive(Request $request)
    {
        try {
            $inputData = array();
            foreach ($request->all() as $key => $value) {
                if (substr($key, 0, 4) == "vnp_" && $value != null && trim($value) != "") {
                    $inputData[$key] = $value;
                }
            }
            unset($inputData['vnp_SecureHashType']);
            unset($inputData['vnp_SecureHash']);
            ksort($inputData);
            $vnp_SecureHash = $request->vnp_SecureHash;
            $i = 0;
            $hashData = "";
            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashData = $hashData . '&' . $key . "=" . $value;
                } else {
                    $hashData = $hashData . $key . "=" . $value;
                    $i = 1;
                }
            }
            $vnp_HashSecret = "PHVGAXUFOCCFVYGDVQUOBNOMAOMRPKPU";
            $secureHash = hash($request->vnp_SecureHashType, $vnp_HashSecret . $hashData);
            if ($request->vnp_Amount) {
                $vnp_Amount = $request->vnp_Amount / 100;
            }
            $check = OrderFood::where([['code', $request->vnp_TxnRef]])->first();
            if (!$check) {
                return response()->json([
                    'Message' => "Order Not Found",
                    'RspCode' => "01",
                ]);
            }
            if ($vnp_Amount != $check->total_money) {
                return response()->json([
                    'Message' => "Invalid amount",
                    'RspCode' => "04",
                ]);
            }
            if (trim($request->vnp_SecureHash) == trim($secureHash)) {
                if ($check->status_payment_vnpay != 0) {
                    return response()->json([
                        'Message' => "Order already confirmed",
                        'RspCode' => "02",
                    ]);
                }
                if ($request->vnp_ResponseCode != null) {
                    if ($request->vnp_ResponseCode == "00") {
                        OrderFood::where('code', $request->vnp_TxnRef)->update([
                            'status_payment_vnpay' => 1,
                            'status_payment_success' => 1,
                        ]);
                    } else {
                        OrderFood::where('code', $request->vnp_TxnRef)->update([
                            'status_payment_vnpay' => 2,
                        ]);
                    }
                    if ($request->vnp_ResponseCode == "99") {
                        OrderFood::where('code', $request->vnp_TxnRef)->update([
                            'status_payment_vnpay' => 2,
                        ]);
                        return response()->json([
                            'Message' => "Confirm Success",
                            'RspCode' => "00",
                        ]);
                    }
                }
                return response()->json([
                    'Message' => "Confirm Success",
                    'RspCode' => "00",
                ]);
            } else {
                return response()->json([
                    'Message' => "Invalid Checksum",
                    'RspCode' => "97",
                ]);
            }
        } catch (\Exception $exception) {
            return response()->json([
                "RspCode" => "99",
                "Message" => "Unknow error",
            ]);
        }
    }
    public function listOrderWithUser(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'type' => 'required',
        ], [
            'type.required' => 'Vui lòng chọn loại đơn hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $order = OrderFood::select(
            'id',
            'code',
            'user_id',
            'user_name',
            'phone',
            'address',
            'latitude',
            'longtidue',
            'date_time',
            'store_id',
            'fee_ship',
            'discount',
            'total_money',
            'coin',
            'note',
            'payment_method_id',
            'shipper_id',
            'star',
            'status',
            'created_at',
            'is_review_shipper',
            'time_check',
            'quantity_push',
            'money_voucher',
            'type_voucher',
            'voucher_id',
            'content',
            'total_money_food'
        );
        if ($request->type == 1) {
            $status = [0, 1, 2, 3, 8, 9, 11];
            $data = $order->whereIn('status', $status)->where([['user_id', $user->id]])->with('store')->orderby('id', 'desc')->paginate(12);
            foreach ($data as $d) {
                $latitude_store = $d->store->latitude;
                $longtidue_store = $d->store->longtidue;
                $latitude = $d->latitude;
                $longtidue = $d->longtidue;
                $quantity = OrderFoodDetail::where([['order_food_id', $d->id]])->get()->SUM('quantity');
                $d['total_quantity'] = $quantity;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                if (isset($response)) {
                    $d->store['distance'] = $response;
                    $d->store['distance_value'] = $response['value'];
                } else {
                    $d->store['distance'] = 0;
                    $d->store['distance_value'] = 0;
                }
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
        } else {
            $status = [4, 5, 6, 7, 10, 12];
            $data = $order->whereIn('status', $status)->where([['user_id', $user->id]])->with('store')->orderby('id', 'desc')->paginate(12);
            foreach ($data as $d) {
                $latitude_store = $d->store->latitude;
                $longtidue_store = $d->store->longtidue;
                $latitude = $d->latitude;
                $longtidue = $d->longtidue;
                $quantity = OrderFoodDetail::where([['order_food_id', $d->id]])->get()->SUM('quantity');
                $d['total_quantity'] = $quantity;
                $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
                if (isset($response)) {
                    $d->store['distance'] = $response;
                    $d->store['distance_value'] = $response['value'];
                } else {
                    $d->store['distance'] = 0;
                    $d->store['distance_value'] = 0;
                }
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
        }
    }
    public function listOrderDraftWithUser(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $data = CardFood::select('store_id')->where([['user_id', $user->id], ['status', 0]])->distinct()->with('store')->paginate();
        foreach ($data as $d) {
            $latitude_store = $d->store->latitude;
            $longtidue_store = $d->store->longtidue;
            $check_card = CardFood::where([['user_id', $user->id], ['status', 0], ['store_id', $d->store_id]])->get();
            $d['total_money'] = $check_card->SUM('price');
            $d['total_quantity'] = $check_card->SUM('quantity');
            $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
            if (isset($response)) {
                $d->store['distance'] = $response;
                $d->store['distance_value'] = $response['value'];
            } else {
                $d->store['distance'] = 0;
                $d->store['distance_value'] = 0;
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function orderDetailWithUser($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = OrderFood::where([['id', $id], ['user_id', $user->id]])->firstorfail()->load('store', 'user', 'shipper', 'orderFoodDetail.orderToppingFoodDetail', 'orderFoodDetail.food', 'orderFoodDetail.comboFood', 'paymentMethod');
        $quantity = OrderFoodDetail::where([['order_food_id', $data->id]])->get()->SUM('quantity');
        $data['total_quantity'] = $quantity;
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function deleteOrderDraftWithUser(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($request->store_id) {
            $delete = CardFood::where([['store_id', $request->store_id], ['status', 0], ['user_id', $user->id]])->get();
            foreach ($delete as $d) {
                CardToppingFood::where('card_food_id', $d->id)->delete();
            }
            $delete = CardFood::where([['store_id', $request->store_id], ['status', 0], ['user_id', $user->id]])->delete();
        } else {
            $delete = CardFood::where([['user_id', $user->id], ['status', 0]])->get();
            foreach ($delete as $d) {
                CardToppingFood::where('card_food_id', $d->id)->delete();
            }
            $delete = CardFood::where([['user_id', $user->id], ['status', 0]])->delete();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $delete);
    }
    public function listOrderWithOwner(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'status' => 'required',
            'store_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'status.required' => 'Vui lòng chọn trạng thái đơn hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $store = Store::findorfail($request->store_id);
        $check = UserStore::where([['store_id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $request->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['created_at', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['created_at', '<=', $to_date];
        }
        if ($request->code) {
            $where[] = ['code', 'LIKE', '%' . $request->code . '%'];
        }
        $order = OrderFood::select(
            'id',
            'code',
            'user_id',
            'user_name',
            'phone',
            'address',
            'latitude',
            'longtidue',
            'date_time',
            'store_id',
            'fee_ship',
            'discount',
            'total_money',
            'coin',
            'note',
            'payment_method_id',
            'shipper_id',
            'star',
            'status',
            'created_at',
            'is_review_shipper',
            'time_check',
            'quantity_push',
            'money_voucher',
            'type_voucher',
            'voucher_id',
            'content',
            'total_money_food'
        );
        if ($request->status == 1) {
            $data = $order->where($where)
                ->where([['status', SystemParam::status_order_wait_confirm], ['store_id', $request->store_id]])
                ->OrWhere([['status', SystemParam::status_order_wait_update], ['store_id', $request->store_id], ['is_push_store', 1]])->with('store', 'orderFoodDetail', 'shipper', 'user')->orderby('id', 'desc')->paginate(12);
        } else if ($request->status == 2) {
            $data = $order->where($where)->whereIn('status', [SystemParam::status_order_confirmed, SystemParam::status_order_confirmed_shipper, SystemParam::status_order_has_arrived_store_shipper])->where('store_id', $request->store_id)->with('store', 'orderFoodDetail', 'shipper', 'user')->orderby('id', 'desc')->paginate(12);
        } else if ($request->status == 3) {
            $data = $order->where($where)->whereIn('status', [SystemParam::status_order_received_shipper, SystemParam::status_order_has_arrived_customer_shipper])->where('store_id', $request->store_id)->with('store', 'orderFoodDetail', 'shipper', 'user')->orderby('id', 'desc')->paginate(12);
        } else if ($request->status == 4) {
            $data = $order->where($where)->where([['status', SystemParam::status_order_finish], ['store_id', $request->store_id]])->with('store', 'orderFoodDetail', 'shipper', 'user')->orderby('id', 'desc')->paginate(12);
        } else {
            $data = $order->where($where)->whereIn('status', [SystemParam::status_order_store_cancel, SystemParam::status_order_customer_cancel, SystemParam::status_order_time_out_with_store, SystemParam::status_order_shipper_cancel, SystemParam::status_order_time_out_with_shipper, SystemParam::status_order_time_out_with_24h, SystemParam::status_order_cancel_with_admin])->where('store_id', $request->store_id)->with('store', 'orderFoodDetail', 'shipper', 'user')->orderby('id', 'desc')->paginate(12);
        }
        foreach ($data as $d) {
            $latitude_store = $store->latitude;
            $longtidue_store = $store->longtidue;
            $latitude = $d->latitude;
            $longtidue = $d->longtidue;
            $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
            if (isset($response)) {
                $d['distance'] = $response;
            } else {
                $d['distance'] = 0;
            }
            $d['quantity'] = $d->orderFoodDetail->SUM('quantity');
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function orderDetailWithOwner($order_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $order = OrderFood::select(
            'id',
            'code',
            'user_id',
            'user_name',
            'phone',
            'address',
            'latitude',
            'longtidue',
            'date_time',
            'store_id',
            'fee_ship',
            'discount',
            'total_money',
            'coin',
            'note',
            'payment_method_id',
            'shipper_id',
            'star',
            'status',
            'created_at',
            'is_review_shipper',
            'time_check',
            'quantity_push',
            'money_voucher',
            'type_voucher',
            'voucher_id',
            'content',
            'total_money_food'
        );
        $check_order = $order->findorfail($order_id)->load('user', 'shipper', 'store', 'paymentMethod', 'orderFoodDetail.orderToppingFoodDetail', 'orderFoodDetail.food', 'orderFoodDetail.comboFood');
        $check = UserStore::where([['store_id', $check_order->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $check_order->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $store = Store::findorfail($check_order->store_id);
        $latitude_store = $store->latitude;
        $longtidue_store = $store->longtidue;
        $latitude = $check_order->latitude;
        $longtidue = $check_order->longtidue;
        $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
        if (isset($response)) {
            $check_order['distance'] = $response;
        } else {
            $check_order['distance'] = 0;
        }
        $check_order['quantity'] = $check_order->orderFoodDetail->SUM('quantity');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $check_order);
    }
    public function confirmOrderWithOwner($order_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_order = OrderFood::findorfail($order_id)->load('store', 'user');
        $check = UserStore::where([['store_id', $check_order->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $check_order->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_order->update([
            'status' => SystemParam::status_order_confirmed,
            'time_check' => date('Y-m-d H:i:s', time()),
        ]);
        $ls_shipper_id = ShipperStore::where([['store_id', $check_order->store_id], ['status', 1]])->pluck('user_id')->toArray();
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Đơn hàng có mã ' . $check_order->code . ' của bạn đã được xác nhận', $order_id, SystemParam::type_order, $check_order, 'Timzi');
        $this->notifycontroller->pushNotify($ls_shipper_id, 'Có 1 đơn hàng mới của cửa hàng tên ' . $check_order->store->name . ' cần bạn giao đến khách hàng ' . $check_order->user->name, $order_id, SystemParam::type_order_with_shipper, $check_order, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $check_order);
    }
    public function cancelOrderWithOwner($order_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_order = OrderFood::findorfail($order_id);
        $check = UserStore::where([['store_id', $check_order->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $check_order->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_order_detail = OrderFoodDetail::where('order_food_id', $order_id)->get();
        foreach ($check_order_detail as $c1) {
            if ($c1->food_id) {
                $ls_program = ProgramStoreDetail::where('food_id', $c1->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    foreach ($check_program as $p) {
                        ProgramStore::where('id', $p->id)->update([
                            'quantity' => $p->quantity + $c1->quantity,
                        ]);
                    }
                }
            } else if ($c1->combo_food_id) {
                $check_combo = ComboFood::find($c1->combo_food_id);
                if ($check_combo) {
                    $check_combo->update([
                        'quantity' => $check_combo->quantity + $c1->quantity,
                    ]);
                }
            }
        }
        $check_order->update([
            'status' => SystemParam::status_order_store_cancel,
        ]);
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Đơn hàng có mã ' . $check_order->code . ' của bạn đã bị cửa hàng hủy', $order_id, SystemParam::type_order, $check_order, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check_order);
    }
    public function shipperReceivedOrderWithOwner($order_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_order = OrderFood::where([['id', $order_id], ['shipper_id', '!=', null], ['status', SystemParam::status_order_has_arrived_store_shipper]])->first();
        $check = UserStore::where([['store_id', $check_order->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store2 = Store::where([['id', $check_order->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store2) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_store = Store::find($check_order->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        // $check_order_detail = OrderFoodDetail::where('order_food_id', $order_id)->get();
        // foreach ($check_order_detail as $c) {
        //     if ($c->food_id) {
        //         $ls_program = ProgramStoreDetail::where('food_id', $c->food_id)->pluck('program_store_id')->toArray();
        //         $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
        //         if (count($check_program) > 0) {
        //             foreach ($check_program as $p) {
        //                 if ($p->quantity >= $c->quantity) {
        //                     ProgramStore::where('id', $p->id)->update([
        //                         'quantity' => $p->quantity - $c->quantity,
        //                     ]);
        //                 } else {
        //                     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Món ăn trong chương trình không đủ để nhận hàng.", null);
        //                 }
        //             }
        //         }
        //     }
        // }
        $check_order->update([
            'status' => SystemParam::status_order_received_shipper,
            'is_payment_store' => 1,
        ]);
        $month = date('Y-m', time());
        $check_revenue = RevenueStore::where([['store_id', $check_order->store_id], ['month', 'LIKE', '%' . $month . '%'], ['status', 0]])->first();
        if ($check_revenue) {
            $check_revenue->update([
                'money' => $check_revenue->money + ($check_order->total_money - $check_order->fee_ship),
                'fee_money' => $check_revenue->fee_money + ($check_order->total_money - $check_order->fee_ship) * ($check_store->percent_discount_revenue / 100),
            ]);
        } else {
            do {
                $code_revenue = 'TZ' . $this->makeCodeRandom(8);
            } while (RevenueStore::where('code', $code_revenue)->exists());
            RevenueStore::insert([
                'code' => $code_revenue,
                'store_id' => $check_order->store_id,
                'money' => $check_order->total_money - $check_order->fee_ship,
                'fee_money' => ($check_order->total_money - $check_order->fee_ship) * ($check_store->percent_discount_revenue / 100),
                'month' => date('Y-m-d', time()),
            ]);
        }
        $check_order_detail = OrderFoodDetail::where('order_food_id', $order_id)->get();
        foreach ($check_order_detail as $c1) {
            if ($c1->food_id) {
                $ls_program = ProgramStoreDetail::where('food_id', $c1->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    foreach ($check_program as $p) {
                        ProgramStore::where('id', $p->id)->update([
                            'quantity_bought' => $p->quantity_bought + $c1->quantity,
                        ]);
                    }
                }
            } else if ($c1->combo_food_id) {
                $check_combo = ComboFood::find($c1->combo_food_id);
                if ($check_combo) {
                    $check_combo->update([
                        'quantity_bought' => $check_combo->quantity_bought + $c1->quantity,
                    ]);
                }
            }
        }
        // $shipper = User::where('id', $check_order->shipper_id)->first();
        // if($shipper && $shipper->money > $check_order->fee_ship){
        //     $shipper->update([
        //         'money' => $shipper->money - $check_order->fee_ship
        //     ]);
        //     HistoryMoneyShipper::insert([
        //         'shipper_id' => $check_order->shipper_id,
        //         'money' => $check_order->fee_ship,
        //         'type' => 2,
        //         'content' => "Lấy hàng thành công. Timzi đã trừ " . $check_order->fee_ship . " trong ví liên kết của bạn!"
        //     ]);
        //     $this->notifycontroller->pushNotify([$check_order->shipper_id], "Lấy hàng thành công. Timzi đã trừ " . $check_order->fee_ship . " trong ví liên kết của bạn!", $order_id, SystemParam::type_order, $check_order);
        // }
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Đơn hàng có mã ' . $check_order->code . ' của bạn đã được người giao hàng đến lấy', $order_id, SystemParam::type_order, $check_order, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_shipper_received_success, $check_order);
    }
    public function updateLocation(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ người giao hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ người giao hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $user->update([
            'latitude' => $latitude,
            'longtidue' => $longtidue,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $user);
    }
    public function listOrderPendingWithShipper(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ người giao hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ người giao hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $user->update([
            'latitude' => $latitude,
            'longtidue' => $longtidue,
        ]);
        $order = OrderFood::select(
            'id',
            'code',
            'user_id',
            'user_name',
            'phone',
            'address',
            'latitude',
            'longtidue',
            'date_time',
            'store_id',
            'fee_ship',
            'discount',
            'total_money',
            'coin',
            'note',
            'payment_method_id',
            'shipper_id',
            'star',
            'status',
            'created_at',
            'is_review_shipper',
            'time_check',
            'quantity_push',
            'money_voucher',
            'type_voucher',
            'voucher_id',
            'content',
            'total_money_food'
        )->where('status_payment_success', 1);
        $array_status = [SystemParam::status_order_finish, SystemParam::status_order_store_cancel, SystemParam::status_order_customer_cancel, SystemParam::status_order_time_out_with_store, SystemParam::status_order_shipper_cancel, SystemParam::status_order_time_out_with_shipper, SystemParam::status_order_time_out_with_24h, SystemParam::status_order_cancel_with_admin];
        $data1 = OrderFood::where([['shipper_id', $user->id]])->whereNotIn('status', $array_status)->first();
        if ($data1) {
            $data = $order->where('id', $data1->id)->with('store', 'user', 'orderFoodDetail.orderToppingFoodDetail', 'orderFoodDetail.food', 'orderFoodDetail.comboFood')->paginate(12);
        } else {
            $data = $order->where([['status', SystemParam::status_order_confirmed], ['fee_ship', '<=', $user->money]])
                ->orWhere([['status', SystemParam::status_order_received_shipper], ['shipper_id', $user->id], ['fee_ship', '<=', $user->money]])
                ->orWhere([['status', SystemParam::status_order_confirmed_shipper], ['shipper_id', $user->id], ['fee_ship', '<=', $user->money]])
                ->orWhere([['status', SystemParam::status_order_has_arrived_store_shipper], ['shipper_id', $user->id], ['fee_ship', '<=', $user->money]])
                ->orWhere([['status', SystemParam::status_order_has_arrived_customer_shipper], ['shipper_id', $user->id], ['fee_ship', '<=', $user->money]])
                ->with('store', 'user', 'orderFoodDetail.orderToppingFoodDetail', 'orderFoodDetail.food', 'orderFoodDetail.comboFood')
                ->orderby('id', 'asc')->paginate(12);
        }
        foreach ($data as $d) {
            $latitude_store = $d->store->latitude;
            $longtidue_store = $d->store->longtidue;
            $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
            if (isset($response)) {
                $d->store['distance'] = $response;
            } else {
                $d->store['distance'] = 0;
            }
            $response2 = $this->googleApi($latitude, $longtidue, $d->latitude, $d->longtidue);
            if (isset($response2)) {
                $d->user['distance'] = $response2;
            } else {
                $d->user['distance'] = 0;
            }
        }
        // $array_status = [SystemParam::status_order_finish, SystemParam::status_order_received_shipper, SystemParam::status_order_has_arrived_customer_shipper, SystemParam::status_order_has_arrived_store_shipper];

        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function orderDetailWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ người giao hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ người giao hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $data = OrderFood::where([['id', $order_id], ['status', SystemParam::status_order_confirmed], ['fee_ship', '<=', $user->money]])
            ->orWhere([['status', SystemParam::status_order_received_shipper], ['shipper_id', $user->id], ['id', $order_id], ['fee_ship', '<=', $user->money]])
            ->orWhere([['status', SystemParam::status_order_confirmed_shipper], ['shipper_id', $user->id], ['id', $order_id], ['fee_ship', '<=', $user->money]])
            ->orWhere([['status', SystemParam::status_order_has_arrived_store_shipper], ['shipper_id', $user->id], ['id', $order_id], ['fee_ship', '<=', $user->money]])
            ->orWhere([['status', SystemParam::status_order_has_arrived_customer_shipper], ['shipper_id', $user->id], ['id', $order_id], ['fee_ship', '<=', $user->money]])->with('user', 'store', 'orderFoodDetail.orderToppingFoodDetail', 'orderFoodDetail.food', 'orderFoodDetail.comboFood')->first();
        if(!$data){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Đơn hàng hiện tại không tồn tại.", null);
        }
        $latitude_store = $data->store->latitude;
        $longtidue_store = $data->store->longtidue;
        $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
        if (isset($response)) {
            $data->store['distance'] = $response;
        } else {
            $data->store['distance'] = 0;
        }
        $response2 = $this->googleApi($latitude, $longtidue, $data->latitude, $data->longtidue);
        if (isset($response2)) {
            $data->user['distance'] = $response2;
        } else {
            $data->user['distance'] = 0;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function currentOrderWithShipper(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ người giao hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ người giao hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $array_status = [SystemParam::status_order_finish, SystemParam::status_order_store_cancel, SystemParam::status_order_customer_cancel, SystemParam::status_order_time_out_with_store, SystemParam::status_order_shipper_cancel, SystemParam::status_order_time_out_with_shipper, SystemParam::status_order_time_out_with_24h, SystemParam::status_order_cancel_with_admin];
        $data = OrderFood::where([['shipper_id', $user->id]])->whereNotIn('status', $array_status)
            ->with('user', 'store', 'orderFoodDetail.orderToppingFoodDetail', 'orderFoodDetail.food', 'orderFoodDetail.comboFood')->first();
        if (!$data) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, '', null);
        }
        $latitude_store = $data->store->latitude;
        $longtidue_store = $data->store->longtidue;
        $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
        if (isset($response)) {
            $data->store['distance'] = $response;
        } else {
            $data->store['distance'] = 0;
        }
        $response2 = $this->googleApi($latitude, $longtidue, $data->latitude, $data->longtidue);
        if (isset($response2)) {
            $data->user['distance'] = $response2;
        } else {
            $data->user['distance'] = 0;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function receiveOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $array_status = [SystemParam::status_order_finish, SystemParam::status_order_store_cancel, SystemParam::status_order_customer_cancel, SystemParam::status_order_time_out_with_store, SystemParam::status_order_shipper_cancel, SystemParam::status_order_time_out_with_shipper, SystemParam::status_order_time_out_with_24h, SystemParam::status_order_cancel_with_admin];
        $check_order_old = OrderFood::where([['shipper_id', $user->id]])->whereNotIn('status', $array_status)->first();
        $array_status2 = [SystemParam::status_order_user_product_finish, SystemParam::status_order_user_product_sender_cancel, SystemParam::status_order_user_product_shipper_cancel, SystemParam::status_order_user_product_receiver_cancel, SystemParam::status_order_user_product_time_out_shipper, SystemParam::status_order_user_product_timzi_cancel];
        $check_order_old2 = OrderUserProduct::where([['shipper_id', $user->id]])->whereNotIn('status', $array_status2)->first();
        if ($check_order_old || $check_order_old2) {
            // $check_order_old->update([
            //     'shipper_id' => null,
            // ]);
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_order_food_with_shipper_not_finish, null);
        }
        // if ($check_order_old) {
        //     // $check_order_old->update([
        //     //     'shipper_id' => null,
        //     // ]);
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_order_food_with_shipper_not_finish, null);
        // }
        $check_order = OrderFood::where([['status', SystemParam::status_order_confirmed], ['id', $order_id], ['fee_ship', '<=', $user->money]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Không tìm thấy đơn hàng.", null);
        }
        $check_order->update([
            'status' => SystemParam::status_order_confirmed_shipper,
            'shipper_id' => $user->id,
        ]);
        $id = UserStore::where([['status', 1], ['store_id', $check_order->store_id]])->pluck('user_id')->toArray();
        $check_store = Store::where('id', $check_order->store_id)->first();
        if ($check_store) {
            $id = [$check_store->user_id];
        }
        $this->notifycontroller->pushNotify($id, 'Người giao hàng ' . $user->name . ' đã nhận đơn hàng có mã ' . $check_order->code, $order_id, SystemParam::type_order, $check_order, $user->name);
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Người giao hàng ' . $user->name . ' đã nhận đơn hàng có mã ' . $check_order->code, $order_id, SystemParam::type_order, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::receive_order_success, $check_order);
    }
    public function pickupStoreOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $check_order = OrderFood::where([['status', SystemParam::status_order_has_arrived_store_shipper], ['id', $order_id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa tới cửa hàng.", null);
        }
        $check_store = Store::find($check_order->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }

        $check_order->update([
            'status' => SystemParam::status_order_received_shipper,
            'is_payment_store' => 1,
        ]);
        $month = date('Y-m', time());
        $check_revenue = RevenueStore::where([['store_id', $check_order->store_id], ['month', 'LIKE', '%' . $month . '%'], ['status', 0]])->first();
        if ($check_revenue) {
            $check_revenue->update([
                'money' => $check_revenue->money + ($check_order->total_money - $check_order->fee_ship),
                'fee_money' => $check_revenue->fee_money + ($check_order->total_money - $check_order->fee_ship) * ($check_store->percent_discount_revenue / 100),
            ]);
        } else {
            do {
                $code_revenue = 'TZ' . $this->makeCodeRandom(8);
            } while (RevenueStore::where('code', $code_revenue)->exists());
            RevenueStore::insert([
                'code' => $code_revenue,
                'store_id' => $check_order->store_id,
                'money' => $check_order->total_money - $check_order->fee_ship,
                'fee_money' => ($check_order->total_money - $check_order->fee_ship) * ($check_store->percent_discount_revenue / 100),
                'month' => date('Y-m-d', time()),
            ]);
        }
        $check_order_detail = OrderFoodDetail::where('order_food_id', $order_id)->get();
        foreach ($check_order_detail as $c1) {
            if ($c1->food_id) {
                $ls_program = ProgramStoreDetail::where('food_id', $c1->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    foreach ($check_program as $p) {
                        ProgramStore::where('id', $p->id)->update([
                            'quantity_bought' => $p->quantity_bought + $c1->quantity,
                        ]);
                    }
                }
            }else if ($c1->combo_food_id) {
                $check_combo = ComboFood::find($c1->combo_food_id);
                if ($check_combo) {
                    $check_combo->update([
                        'quantity_bought' => $check_combo->quantity_bought + $c1->quantity,
                    ]);
                }
            }
        }
        // if($user->money > $check_order->fee_ship){
        //     $user->update([
        //         'money' => $user->money - $check_order->fee_ship
        //     ]);
        //     HistoryMoneyShipper::insert([
        //         'shipper_id' => $check_order->shipper_id,
        //         'money' => $check_order->fee_ship,
        //         'type' => 2,
        //         'content' => "Lấy hàng thành công. Timzi đã trừ " . $check_order->fee_ship . " trong ví liên kết của bạn!"
        //     ]);
        //     $this->notifycontroller->pushNotify([$check_order->shipper_id], "Lấy hàng thành công. Timzi đã trừ " . $check_order->fee_ship . " trong ví liên kết của bạn!", $order_id, SystemParam::type_order, $check_order);
        // }
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Đơn hàng có mã ' . $check_order->code . ' của bạn đã được người giao hàng đến lấy', $order_id, SystemParam::type_order, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::receive_order_success, $check_order);
    }
    public function arrivedStoreOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $check_order = OrderFood::where([['status', SystemParam::status_order_confirmed_shipper], ['id', $order_id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa nhận đơn.", null);
        }
        $check_order->update([
            'status' => SystemParam::status_order_has_arrived_store_shipper,
        ]);
        $id = UserStore::where([['status', 1], ['store_id', $check_order->store_id]])->pluck('user_id')->toArray();
        $check_store = Store::where('id', $check_order->store_id)->first();
        if ($check_store) {
            $id = [$check_store->user_id];
        }
        $this->notifycontroller->pushNotify($id, 'Người giao hàng ' . $user->name . ' đã đến cửa hàng để nhận đơn hàng có mã ' . $check_order->code, $order_id, SystemParam::type_order, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check_order);
    }
    public function arrivedCustomerOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $check_order = OrderFood::where([['status', SystemParam::status_order_received_shipper], ['id', $order_id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa lấy hàng.", null);
        }
        $check_order->update([
            'status' => SystemParam::status_order_has_arrived_customer_shipper,
        ]);
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Người giao hàng ' . $user->name . ' đã đến vị trí cần giao đơn hàng có mã ' . $check_order->code, $order_id, SystemParam::type_order, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check_order);
    }
    public function storeCancelOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_order = OrderFood::where([['status', SystemParam::status_order_has_arrived_store_shipper], ['id', $order_id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa tới cửa hàng.", null);
        }
        $check_order_detail = OrderFoodDetail::where('order_food_id', $order_id)->get();
        foreach ($check_order_detail as $c1) {
            if ($c1->food_id) {
                $ls_program = ProgramStoreDetail::where('food_id', $c1->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    foreach ($check_program as $p) {
                        ProgramStore::where('id', $p->id)->update([
                            'quantity' => $p->quantity + $c1->quantity,
                        ]);
                    }
                }
            }else if ($c1->combo_food_id) {
                $check_combo = ComboFood::find($c1->combo_food_id);
                if ($check_combo) {
                    $check_combo->update([
                        'quantity' => $check_combo->quantity + $c1->quantity,
                    ]);
                }
            }
        }
        $check_order->update([
            'status' => SystemParam::status_order_store_cancel,
        ]);
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Người giao hàng ' . $user->name . ' đã xác nhận đơn hàng có mã ' . $check_order->code . ' của bạn đã bị cửa hàng hủy', $order_id, SystemParam::type_order, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check_order);
    }
    public function customerCancelOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $check_order = OrderFood::where([['status', SystemParam::status_order_has_arrived_customer_shipper], ['id', $order_id], ['shipper_id', $user->id]])->firstorfail();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa tới địa điểm giao.", null);
        }
        // $check_order_detail = OrderFoodDetail::where('order_food_id', $order_id)->get();
        // foreach ($check_order_detail as $c1) {
        //     if ($c1->food_id) {
        //         $ls_program = ProgramStoreDetail::where('food_id', $c1->food_id)->pluck('program_store_id')->toArray();
        //         $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
        //         if (count($check_program) > 0) {
        //             foreach ($check_program as $p) {
        //                 ProgramStore::where('id', $p->id)->update([
        //                     'quantity' => $p->quantity + $c1->quantity,
        //                 ]);
        //             }
        //         }
        //     }
        // }
        $check_order->update([
            'status' => SystemParam::status_order_customer_cancel,
        ]);
        $id = UserStore::where([['status', 1], ['store_id', $check_order->store_id]])->pluck('user_id')->toArray();
        $check_store = Store::where('id', $check_order->store_id)->first();
        if ($check_store) {
            $id = [$check_store->user_id];
        }
        $this->notifycontroller->pushNotify($id, 'Người giao hàng ' . $user->name . ' đã xác nhận đơn hàng có mã ' . $check_order->code . ' đã bị khách hủy', $order_id, SystemParam::type_order, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check_order);
    }
    public function cancelOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $check_order = OrderFood::where([['id', $order_id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Không tìm thấy đơn hàng.", null);
        }
        $check_order_detail = OrderFoodDetail::where('order_food_id', $order_id)->get();
        foreach ($check_order_detail as $c1) {
            if ($c1->food_id) {
                $ls_program = ProgramStoreDetail::where('food_id', $c1->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    foreach ($check_program as $p) {
                        ProgramStore::where('id', $p->id)->update([
                            'quantity' => $p->quantity + $c1->quantity,
                        ]);
                    }
                }
            }else if ($c1->combo_food_id) {
                $check_combo = ComboFood::find($c1->combo_food_id);
                if ($check_combo) {
                    $check_combo->update([
                        'quantity' => $check_combo->quantity + $c1->quantity,
                    ]);
                }
            }
        }
        if ($check_order->status == SystemParam::status_order_received_shipper) {
            $check_order->update([
                'status' => SystemParam::status_order_confirmed,
                'shipper_id' => null,
            ]);
        } else {
            $check_order->update([
                'status' => SystemParam::status_order_shipper_cancel,
            ]);
        }
        $id = UserStore::where([['status', 1], ['store_id', $check_order->store_id]])->pluck('user_id')->toArray();
        $check_store = Store::where('id', $check_order->store_id)->first();
        if ($check_store) {
            $id = [$check_store->user_id];
        }
        $this->notifycontroller->pushNotify($id, 'Người giao hàng ' . $user->name . ' đã hủy đơn hàng có mã ' . $check_order->code, $order_id, SystemParam::type_order, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check_order);
    }
    public function receivedOrderWithCustomer($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($user->role_id != 5) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        // }
        $check_order = OrderFood::where([['status', SystemParam::status_order_has_arrived_customer_shipper], ['id', $order_id], ['user_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Người giao hàng chưa tới địa điểm giao.", null);
        }
        $check_order->update([
            'status' => SystemParam::status_order_finish,
        ]);
        // if ($check_percent_coin && $check_percent_coin->percent > 0) {
        //     $coin = ($check_order->total_money - $check_order->fee_ship) * $check_percent_coin->percent / 100;
        //     $user->update([
        //         'coin' => $user->coin + $coin,
        //     ]);
        //     CoinHistory::insert([
        //         'user_id' => $user->id,
        //         'content' => "Bạn đã được tặng " . $coin . " xu khi hoàn thành đơn đặt hàng có mã " . $check_order->code,
        //         'coin' => $coin,
        //         'type' => 1,
        //         'created_at' => date('Y-m-d H:i:s', time()),
        //     ]);
        //     $this->notifycontroller->pushNotify([$user->id], "Bạn đã được tặng " . $coin . " xu khi hoàn thành đơn đặt hàng có mã " . $check_order->code . ".", $check_order->id, SystemParam::type_order, $check_order);
        // }
        $check_shipper = User::find($check_order->shipper_id);
        $check_percent_shipper = SetupPercentFeeShipper::first();
        if ($check_percent_shipper && $check_shipper && $check_shipper->money >= ($check_order->fee_ship * $check_percent_shipper->percent / 100)) {
            $check_shipper->update([
                'money' => $check_shipper->money - ($check_order->fee_ship * $check_percent_shipper->percent / 100),
            ]);
            HistoryMoneyShipper::insert([
                'shipper_id' => $check_order->shipper_id,
                'money' => ($check_order->fee_ship * $check_percent_shipper->percent / 100),
                'type' => 2,
                'type_order' => 1,
                'content' => "Giao hàng thành công. Timzi đã trừ " . number_format($check_order->fee_ship * $check_percent_shipper->percent / 100) . " VNĐ trong ví liên kết của bạn!",
                'created_at' => date('Y-m-d H:i:s', time()),
            ]);
            $this->notifycontroller->pushNotify([$check_order->shipper_id], "Giao hàng thành công. Timzi đã trừ " . ($check_order->fee_ship * $check_percent_shipper->percent / 100) . " trong ví liên kết của bạn!", $order_id, SystemParam::type_money_shipper, $check_order, 'Timzi');
        }
        if($check_percent_shipper && $check_order->type_voucher != 0){
            $check_shipper->update([
                'money' => $check_shipper->money + $check_order->discount - ($check_order->discount * $check_percent_shipper->percent / 100)
            ]);
            HistoryMoneyShipper::insert([
                'shipper_id' => $check_order->shipper_id,
                'money' => $check_order->discount - ($check_order->discount * $check_percent_shipper->percent / 100),
                'type' => 1,
                'type_order' => 1,
                'content' => "Timzi đã cộng " . number_format($check_order->discount - ($check_order->discount * $check_percent_shipper->percent / 100)) . " VNĐ vào trong ví liên kết của bạn khi giao hàng online thành công!",
                'created_at' => date('Y-m-d H:i:s', time()),
            ]);
            $this->notifycontroller->pushNotify([$check_order->shipper_id], "Timzi đã cộng " . number_format($check_order->discount - ($check_order->discount * $check_percent_shipper->percent / 100)) . " VNĐ vào trong ví liên kết của bạn khi giao hàng online thành công!", $order_id, SystemParam::type_money_shipper, $check_order, 'Timzi');
        }
        $id = UserStore::where([['status', 1], ['store_id', $check_order->store_id]])->pluck('user_id')->toArray();
        $check_store = Store::where('id', $check_order->store_id)->first();
        if ($check_store) {
            $id = [$check_store->user_id];
        }
        $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' đã nhận được đơn hàng có mã ' . $check_order->code, $order_id, SystemParam::type_order, $check_order, $user->name);
        if ($check_order->shipper_id) {
            $this->notifycontroller->pushNotify([$check_order->shipper_id], 'Khách hàng ' . $user->name . ' đã nhận được đơn hàng có mã ' . $check_order->code, $order_id, SystemParam::type_order, $check_order, $user->name);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check_order);
    }
    public function finishOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_order = OrderFood::where([['status', SystemParam::status_order_has_arrived_customer_shipper], ['id', $order_id], ['shipper_id', $user->id]])->with('user')->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Người giao hàng chưa tới địa điểm giao.", null);
        }
        $check_order->update([
            'status' => SystemParam::status_order_finish,
        ]);
        $check_percent_coin = SetupPercentCoin::first();
        $customer = User::find($check_order->user_id);
        // if ($customer) {
        //     if ($check_percent_coin && $check_percent_coin->percent > 0) {
        //         $coin = ($check_order->total_money - $check_order->fee_ship) * $check_percent_coin->percent / 100;
        //         $customer->update([
        //             'coin' => $customer->coin + $coin,
        //         ]);
        //         CoinHistory::insert([
        //             'user_id' => $customer->id,
        //             'content' => "Bạn đã được tặng " . $coin . " xu khi hoàn thành đơn đặt hàng có mã " . $check_order->code,
        //             'coin' => $coin,
        //             'type' => 1,
        //             'created_at' => date('Y-m-d H:i:s', time()),
        //         ]);
        //         $this->notifycontroller->pushNotify([$customer->id], "Bạn đã được tặng " . $coin . " xu khi hoàn thành đơn đặt hàng có mã " . $check_order->code . ".", $check_order->id, SystemParam::type_order, $check_order, 'Timzi');
        //     }
        // }
        $check_percent_shipper = SetupPercentFeeShipper::first();
        if ($check_percent_shipper && $user->money >= ($check_order->fee_ship * $check_percent_shipper->percent / 100)) {
            $user->update([
                'money' => $user->money - ($check_order->fee_ship * $check_percent_shipper->percent / 100),
            ]);
            HistoryMoneyShipper::insert([
                'shipper_id' => $user->id,
                'money' => ($check_order->fee_ship * $check_percent_shipper->percent / 100),
                'type' => 2,
                'type_order' => 1,
                'content' => "Giao hàng thành công. Timzi đã trừ " . number_format($check_order->fee_ship * $check_percent_shipper->percent / 100) . " VNĐ trong ví liên kết của bạn!",
                'created_at' => date('Y-m-d H:i:s', time()),
            ]);
            $this->notifycontroller->pushNotify([$user->id], "Giao hàng thành công. Timzi đã trừ " . ($check_order->fee_ship * $check_percent_shipper->percent / 100) . " trong ví liên kết của bạn!", $order_id, SystemParam::type_money_shipper, $check_order, 'Timzi');
        }
        if($check_percent_shipper && $check_order->type_voucher != 0){
            $user->update([
                'money' => $user->money + $check_order->discount - ($check_order->discount * $check_percent_shipper->percent / 100)
            ]);
            HistoryMoneyShipper::insert([
                'shipper_id' => $check_order->shipper_id,
                'money' => $check_order->discount - ($check_order->discount * $check_percent_shipper->percent / 100),
                'type' => 1,
                'type_order' => 1,
                'content' => "Timzi đã cộng " . number_format($check_order->discount - ($check_order->discount * $check_percent_shipper->percent / 100)) . " VNĐ vào trong ví liên kết của bạn khi giao hàng online thành công!",
                'created_at' => date('Y-m-d H:i:s', time()),
            ]);
            $this->notifycontroller->pushNotify([$check_order->shipper_id], "Timzi đã cộng " . number_format($check_order->discount - ($check_order->discount * $check_percent_shipper->percent / 100)) . " VNĐ vào trong ví liên kết của bạn khi giao hàng online thành công!", $order_id, SystemParam::type_money_shipper, $check_order, 'Timzi');
        }
        $id = UserStore::where([['status', 1], ['store_id', $check_order->store_id]])->pluck('user_id')->toArray();
        $check_store = Store::where('id', $check_order->store_id)->first();
        if ($check_store) {
            $id = [$check_store->user_id];
        }
        $this->notifycontroller->pushNotify($id, 'Người giao hàng ' . $user->name . ' đã xác nhận giao thành công đơn hàng có mã ' . $check_order->code . ' đến khách hàng ' . $check_order->user->name, $order_id, SystemParam::type_order, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check_order);
    }
    public function cancelOrderWithCustomer($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($user->role_id != 5) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        // }
        $array_status = [SystemParam::status_order_finish, SystemParam::status_order_received_shipper, SystemParam::status_order_has_arrived_customer_shipper, SystemParam::status_order_has_arrived_store_shipper];
        $check_order = OrderFood::where([['id', $order_id], ['user_id', $user->id], ['status', '!=', SystemParam::status_order_finish]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Không tìm thấy đơn hàng.", null);
        }
        $check_order_detail = OrderFoodDetail::where('order_food_id', $order_id)->get();
        foreach ($check_order_detail as $c1) {
            if ($c1->food_id) {
                $ls_program = ProgramStoreDetail::where('food_id', $c1->food_id)->pluck('program_store_id')->toArray();
                $check_program = ProgramStore::whereIn('id', $ls_program)->where('status', 1)->get();
                if (count($check_program) > 0) {
                    foreach ($check_program as $p) {
                        ProgramStore::where('id', $p->id)->update([
                            'quantity' => $p->quantity + $c1->quantity,
                        ]);
                    }
                }
            }else if ($c1->combo_food_id) {
                $check_combo = ComboFood::find($c1->combo_food_id);
                if ($check_combo) {
                    $check_combo->update([
                        'quantity' => $check_combo->quantity + $c1->quantity,
                    ]);
                }
            }
        }
        $check_order->update([
            'status' => SystemParam::status_order_customer_cancel,
        ]);
        $id = UserStore::where([['status', 1], ['store_id', $check_order->store_id]])->pluck('user_id')->toArray();
        $check_store = Store::where('id', $check_order->store_id)->first();
        if ($check_store) {
            $id = [$check_store->user_id];
        }
        $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' đã hủy đơn hàng có mã ' . $check_order->code, $order_id, SystemParam::type_order, $check_order, $user->name);
        if ($check_order->shipper_id) {
            $this->notifycontroller->pushNotify([$check_order->shipper_id], 'Khách hàng ' . $user->name . ' đã hủy đơn hàng có mã ' . $check_order->code, $order_id, SystemParam::type_order, $check_order, $user->name);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check_order);
    }
    public function listEvaluateWithStore($store_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = UserStore::where([['store_id', $store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check) {
            $check_store = Store::where([['id', $store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $data = OrderFood::select('star', 'content', 'created_at', 'user_id')->where([['store_id', $store_id], ['star', '!=', null]])->with('user')->orderby('id', 'desc')->paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);

    }
    public function listEvaluateWithShipper(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = ReviewShipper::where([['shipper_id', $user->id]])->with('user')->orderby('id', 'desc')->paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);

    }
    public function evaluateOrder(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'order_id' => 'required',
            'star' => 'required',
            'content' => 'required',
        ], [
            'order_id.required' => 'Vui lòng chọn đơn hàng cần đánh giá',
            'star.required' => 'Vui lòng chọn số sao',
            'content.required' => 'Vui lòng nhập nội dung đánh giá',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_order = OrderFood::where([['id', $request->order_id], ['user_id', $user->id], ['status', SystemParam::status_order_finish]])->firstorfail();
        $check_order->update([
            'star' => $request->star,
            'content' => $request->content,
        ]);
        $check_star = OrderFood::where([['store_id', $check_order->store_id], ['user_id', $user->id], ['star', '!=', null]])->get();
        $star = $check_star->SUM('star') / count($check_star);
        $update_star_store = Store::where('id', $check_order->store_id)->update([
            'star' => $star,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::evaluate_success, $check_order);
    }
    public function evaluateFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'order_detail_id' => 'required',
            'star' => 'required',
            'content' => 'required',
        ], [
            'order_detail_id.required' => 'Vui lòng chọn món ăn cần đánh giá',
            'star.required' => 'Vui lòng chọn số sao',
            'content.required' => 'Vui lòng nhập nội dung đánh giá',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_order_detail = OrderFoodDetail::findorfail($request->order_detail_id);
        $check_order_detail->update([
            'star' => $request->star,
            'content' => $request->content,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::evaluate_success, $check_order_detail);
    }
    public function evaluateShipper(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'order_id' => 'required',
            'star' => 'required',
            'content' => 'required',
        ], [
            'order_id.required' => 'Vui lòng chọn đơn hàng bạn cần đánh giá người giao hàng',
            'star.required' => 'Vui lòng chọn số sao',
            'content.required' => 'Vui lòng nhập nội dung đánh giá',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_order = OrderFood::where([['id', $request->order_id], ['user_id', $user->id], ['status', SystemParam::status_order_finish]])->firstorfail();
        $create = ReviewShipper::insert([
            'user_id' => $user->id,
            'shipper_id' => $check_order->shipper_id,
            'star' => $request->star,
            'content' => $request->content,
            'order_food_id' => $check_order->id,
        ]);
        $check_order->update([
            'is_review_shipper' => 1,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::evaluate_success, $check_order);
    }
    public function listOrderSuccessWithShipper(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [['shipper_id', $user->id], ['status', SystemParam::status_order_finish]];
        if ($request->date) {
            $date = date('Y-m-d', strtotime($request->date));
            $where[] = ['date_time', 'like', '%' . $date . '%'];
        } else if ($request->from_date && $request->to_date) {
            $from_date = date('Y-m-d', strtotime($request->from_date));
            $to_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($request->to_date))) + 24 * 60 * 60 - 1);
            $where[] = ['date_time', '>=', $from_date];
            $where[] = ['date_time', '<=', $to_date];
        }
        // $validator = Validator::make($request->all(), [
        //     'latitude' => 'required',
        //     'longtidue' => 'required',
        // ], [
        //     'latitude.required' => 'Vui lòng nhập vĩ độ người giao hàng',
        //     'longtidue.required' => 'Vui lòng nhập kinh độ người giao hàng',
        // ]);
        // if ($validator->fails()) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        // }
        // if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        // }
        $order = OrderFood::select(
            'id',
            'code',
            'user_id',
            'user_name',
            'phone',
            'address',
            'latitude',
            'longtidue',
            'date_time',
            'store_id',
            'fee_ship',
            'discount',
            'total_money',
            'coin',
            'note',
            'payment_method_id',
            'shipper_id',
            'star',
            'status',
            'created_at',
            'is_review_shipper',
            'time_check',
            'quantity_push',
            'money_voucher',
            'type_voucher',
            'voucher_id',
            'content',
            'total_money_food',
            'created_at',
            'updated_at'
        );
        $data1 = $order->where($where)->with('user', 'store')->orderby('id', 'desc')->get();
        $order2 = OrderFood::select(
            'id',
            'code',
            'user_id',
            'user_name',
            'phone',
            'address',
            'latitude',
            'longtidue',
            'date_time',
            'store_id',
            'fee_ship',
            'discount',
            'total_money',
            'coin',
            'note',
            'payment_method_id',
            'shipper_id',
            'star',
            'status',
            'created_at',
            'is_review_shipper',
            'time_check',
            'quantity_push',
            'money_voucher',
            'type_voucher',
            'voucher_id',
            'content',
            'total_money_food',
            'created_at',
            'updated_at'
        );
        $data2 = $order2->where($where)->with('user', 'store')->orderby('id', 'desc')->paginate(12);
        foreach ($data2 as $d) {
            $latitude = $d->latitude;
            $longtidue = $d->longtidue;
            $client = new Client();
            $latitude_store = $d->store->latitude;
            $longtidue_store = $d->store->longtidue;
            $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
            if (isset($response)) {
                $d->store['distance'] = $response;
            } else {
                $d->store['distance'] = 0;
            }
        }
        $where2 = [['shipper_id', $user->id]];
        if ($request->date) {
            $date = date('Y-m-d', strtotime($request->date));
            $where2[] = ['created_at', 'like', '%' . $date . '%'];
        } else if ($request->from_date && $request->to_date) {
            $from_date = date('Y-m-d', strtotime($request->from_date));
            $to_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($request->to_date))) + 24 * 60 * 60 - 1);
            $where2[] = ['created_at', '>=', $from_date];
            $where2[] = ['created_at', '<=', $to_date];
        }
        $where3 = [['shipper_id', $user->id], ['status', SystemParam::status_order_user_product_finish]];
        if ($request->date) {
            $date = date('Y-m-d', strtotime($request->date));
            $where3[] = ['created_at', 'like', '%' . $date . '%'];
        } else if ($request->from_date && $request->to_date) {
            $from_date = date('Y-m-d', strtotime($request->from_date));
            $to_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($request->to_date))) + 24 * 60 * 60 - 1);
            $where3[] = ['created_at', '>=', $from_date];
            $where3[] = ['created_at', '<=', $to_date];
        }
        $history = HistoryMoneyShipper::where($where2)->where([['type', 2], ['type_order', 1]])->get();
        $data = array(
            'list_order' => $data2,
            'count_order' => count($data1),
            'sum_money' => $data1->Sum('fee_ship'),
            'sum_actual_money_received' => $data1->Sum('fee_ship') - $history->SUM('money')
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    // public function orderDetailWithShipper($order_id, Request $request)
    // {
    //     $user = $this->getAuthenticatedUser();
    //     if (!$user) {
    //         return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
    //     }
    //     if ($user->role_id != 4) {
    //         return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
    //     }
    //     $validator = Validator::make($request->all(), [
    //         'latitude' => 'required',
    //         'longtidue' => 'required',
    //     ], [
    //         'latitude.required' => 'Vui lòng nhập vĩ độ người giao hàng',
    //         'longtidue.required' => 'Vui lòng nhập kinh độ người giao hàng',
    //     ]);
    //     if ($validator->fails()) {
    //         return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
    //     }
    //     if((float)$request->latitude == 0 || (float)$request->latitude == 0){
    //         return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
    //     }
    //     $latitude = $request->latitude;
    //     $longtidue = $request->longtidue;
    //     $data = OrderFood::where([['id', $order_id], ['status', SystemParam::status_order_confirmed]])
    //         ->orWhere([['status', SystemParam::status_order_received_shipper], ['shipper_id', $user->id], ['id', $order_id], ['total_money', '<=', $user->money]])
    //         ->orWhere([['status', SystemParam::status_order_store_cancel], ['shipper_id', $user->id], ['id', $order_id], ['total_money', '<=', $user->money]])
    //         ->orWhere([['status', SystemParam::status_order_customer_cancel], ['shipper_id', $user->id], ['id', $order_id], ['total_money', '<=', $user->money]])
    //         ->orWhere([['status', SystemParam::status_order_confirmed_shipper], ['shipper_id', $user->id], ['id', $order_id], ['total_money', '<=', $user->money]])
    //         ->orWhere([['status', SystemParam::status_order_has_arrived_store_shipper], ['shipper_id', $user->id], ['id', $order_id], ['total_money', '<=', $user->money]])
    //         ->orWhere([['status', SystemParam::status_order_shipper_cancel], ['shipper_id', $user->id], ['id', $order_id], ['total_money', '<=', $user->money]])
    //         ->orWhere([['status', SystemParam::status_order_has_arrived_customer_shipper], ['shipper_id', $user->id], ['id', $order_id], ['total_money', '<=', $user->money]])->with('user', 'store', 'orderFoodDetail.orderToppingFoodDetail', 'orderFoodDetail.food', 'orderFoodDetail.comboFood')->firstorfail();
    //     $latitude_store = $data->store->latitude;
    //     $longtidue_store = $data->store->longtidue;
    //     $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
    //     if (isset($response)) {
    //         $data->store['distance'] = $response;
    //     } else {
    //         $data->store['distance'] = 0;
    //     }
    //     $response2 = $this->googleApi($latitude, $longtidue, $data->latitude, $data->longtidue);
    //     if (isset($response2)) {
    //         $data->user['distance'] = $response2;
    //     } else {
    //         $data->user['distance'] = 0;
    //     }
    //     return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    // }
    public function orderDetailSuccessWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = OrderFood::where([['id', $order_id], ['shipper_id', $user->id], ['status', SystemParam::status_order_finish]])
            ->with('user', 'store', 'orderFoodDetail.orderToppingFoodDetail', 'orderFoodDetail.food', 'orderFoodDetail.comboFood')->firstorfail();
        $latitude = $data->latitude;
        $longtidue = $data->longtidue;
        $latitude_store = $data->store->latitude;
        $longtidue_store = $data->store->longtidue;
        $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
        if (isset($response)) {
            $data->store['distance'] = $response;
        } else {
            $data->store['distance'] = 0;
        }
        $response2 = $this->googleApi($latitude, $longtidue, $data->latitude, $data->longtidue);
        if (isset($response2)) {
            $data->user['distance'] = $response2;
        } else {
            $data->user['distance'] = 0;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
}
