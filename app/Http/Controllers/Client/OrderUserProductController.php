<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\AddressUser;
use App\Models\FeeShip;
use App\Models\HistoryMoneyShipper;
use App\Models\OrderFood;
use App\Models\OrderUserProduct;
use App\Models\Radius;
use App\User;
use App\Models\ProductWeight;
use App\Models\SetupPercentFeeShipper;
use App\Models\VoucherDelivery;
use App\Models\VoucherDeliveryUser;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Polyline;

class OrderUserProductController extends Controller
{
    protected $notifycontroller;
    public function __construct()
    {
        $this->notifycontroller = new NotifyController();
    }
    public function listVoucherDelivery(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $where = [];
        if($request->title){
            $where[] = ['title', 'LIKE', '%' . $request->title . '%'];
        }
        $data = VoucherDelivery::where($where)->where('status', 2)->orderby('id', 'desc')->get();
        $array = [];
        foreach($data as $d){
            if($d->type == 2){
                $check = VoucherDeliveryUser::where([['user_id', $user->id], ['voucher_id', $d->id]])->first();
                if(!$check || $check->quantity < $d->quantity_use_with_user_new){
                    if(!$check){
                        $d['available_quantity_user'] = $d->quantity_use_with_user_new;
                        $array[] = $d;
                    }else{
                        $d['available_quantity_user'] = $d->quantity_use_with_user_new - $check->quantity;
                        $array[] = $d;
                    }
                }
            }else{
                $array[] = $d;
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
    public function listProductWeight(){
        $data = ProductWeight::all();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function feeShip(Request $request){
        $validator = Validator::make($request->all(), [
            'latitude_sender' => 'required',
            'longtidue_sender' => 'required',
            'latitude_receiver' => 'required',
            'longtidue_receiver' => 'required',
        ], [
            'latitude_sender.required' => 'Vui lòng nhập vĩ độ của người gửi',
            'longtidue_sender.required' => 'Vui lòng nhập kinh độ của người gửi',
            'latitude_receiver.required' => 'Vui lòng nhập vĩ độ của người nhận',
            'longtidue_receiver.required' => 'Vui lòng nhập kinh độ của người nhận',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        // $latitude_store = $check_store->latitude;
        // $longtidue_store = $check_store->longtidue;
        $response = $this->googleApi($request->latitude_sender, $request->longtidue_sender, $request->latitude_receiver, $request->longtidue_receiver);
        if (isset($response)) {
            $distance = $response['value'] / 1000;
        } else {
            $distance = 0;
        }
        $check = FeeShip::where([['from_range', '<=', $distance], ['to_range', '>', $distance]])
            ->orWhere([['from_range', '<=', $distance], ['to_range', null]])->first();
        if ($check) {
            $fee_ship = $check->money;
        } else {
            $fee_ship = 15000;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $fee_ship);
    }
    public function create(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $validator = Validator::make($request->all(), [
            'sender_name' => 'required|string|max:255',
            'sender_phone' => 'required|regex:/^0([0-9]{9})+$/',
            'sender_address' => 'required',
            'latitude_sender' => 'required',
            'longtidue_sender' => 'required',
            'latitude_receiver' => 'required',
            'longtidue_receiver' => 'required',
            'receiver_name' => 'required|string|max:255',
            'receiver_phone' => 'required|regex:/^0([0-9]{9})+$/',
            'receiver_address' => 'required',
            'product_name' => 'required',
            'image_product' => 'required',
            'fee_ship' => 'required',
            'money' => 'required',
            'product_weight_id' => 'required',
        ], [
            'sender_name.required' => 'Vui lòng nhập tên người gửi',
            'sender_phone.string' => 'Tên người gửi phải là kiểu chuỗi',
            'sender_address.max' => 'Tên người gửi không quá :max kí tự',
            'sender_phone.required' => 'Vui lòng nhập số điện thoại người gửi',
            'sender_phone.regex' => 'Định dạng số điện thoại người gửi không đúng',
            'sender_address.required' => 'Vui lòng nhập địa chỉ cụ thể người gửi',
            'latitude_sender.required' => 'Vui lòng nhập vĩ độ của người gửi',
            'longtidue_sender.required' => 'Vui lòng nhập kinh độ của người gửi',
            'receiver_name.required' => 'Vui lòng nhập tên người nhận',
            'receiver_phone.string' => 'Tên người nhận phải là kiểu chuỗi',
            'receiver_address.max' => 'Tên người nhận không quá :max kí tự',
            'receiver_phone.required' => 'Vui lòng nhập số điện thoại người nhận',
            'receiver_phone.regex' => 'Định dạng số điện thoại người nhận không đúng',
            'receiver_address.required' => 'Vui lòng nhập địa chỉ cụ thể người nhận',
            'latitude_receiver.required' => 'Vui lòng nhập vĩ độ của người nhận',
            'longtidue_receiver.required' => 'Vui lòng nhập kinh độ của người nhận',
            'fee_ship.required' => 'Vui lòng gửi phí ship',
            'money.required' => 'Vui lòng nhập tiền sản phẩm',
            'image_product.required' => 'Vui lòng gửi lên ảnh sản phẩm',
            'product_name.required' => 'Vui lòng nhập tên sản phẩm',
            'product_weight_id.required' => 'Vui lòng chọn khối lượng sản phẩm',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude_sender == 0 || (float) $request->longtidue_sender == 0 || (float) $request->latitude_receiver == 0 || (float) $request->longtidue_receiver == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $array_status = [SystemParam::status_order_user_product_finish, SystemParam::status_order_user_product_sender_cancel, SystemParam::status_order_user_product_shipper_cancel, SystemParam::status_order_user_product_receiver_cancel, SystemParam::status_order_user_product_time_out_shipper, SystemParam::status_order_user_product_timzi_cancel];
        $check = OrderUserProduct::where([['shipper_id', $user->id]])->whereNotIn('status', $array_status)->first();
        if ($check) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_order_user_product_exists, null);
        }
        $req = $request->all();
        $req['money_with_product_weight'] = 0;
        if($request->product_weight_id){
            $check_weight = ProductWeight::find($request->product_weight_id);
            if(!$check_weight){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Không tìm thấy mã khối lượng hàng hóa.", null);
            }
            $req['from_weight'] = $check_weight->from_weight;
            $req['to_weight'] = $check_weight->to_weight;
            $req['money_with_product_weight'] = $check_weight->money;
        }
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (OrderUserProduct::where('code', $req['code'])->exists());
        $check_phone = User::where([['phone', $request->receiver_phone], ['status', 1]])->first();
        if($check_phone){
            $req['receiver_id'] = $check_phone->id;
        }
        $req['discount_voucher'] = 0;
        if ($request->voucher_id) {
            $check_voucher = VoucherDelivery::where([['status', 2], ['id', $request->voucher_id]])->first();
            if(!$check_voucher){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_voucher_not_found, null);
            }
            if ($check_voucher->proviso > $request->fee_ship) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_voucher_proviso_invalid, null);
            }
            if($check_voucher->type == 1){
                if(($check_voucher->quantity - $check_voucher->quantity_used) <= 0){
                    return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, 'Voucher đã hết số lượng sử dụng.', null);
                }
                $req['type_voucher'] = $check_voucher->type;
                if ($check_voucher->type_percent_or_money == 1) {
                    $req['discount_voucher'] = $req['fee_ship'] * $check_voucher->percent / 100;
                } else {
                    $req['discount_voucher'] = $check_voucher->money;
                }
                $check_voucher->update([
                    'quantity_used' => $check_voucher->quantity_used + 1
                ]);
            }else{
                $check_voucher_user = VoucherDeliveryUser::where([['voucher_id', $request->voucher_id], ['user_id', $user->id]])->first();
                if (!$check_voucher_user) {
                    VoucherDeliveryUser::insert([
                        'voucher_id' => $request->voucher_id,
                        'user_id' => $user->id,
                        'quantity' => 1
                    ]);
                    $req['type_voucher'] = $check_voucher->type;
                    if ($check_voucher->type_percent_or_money == 1) {
                        $req['discount_voucher'] = $req['fee_ship'] * $check_voucher->percent / 100;
                    } else {
                        $req['discount_voucher'] = $check_voucher->money;
                    }
                    $check_voucher->update([
                        'quantity_used' => $check_voucher->quantity_used + 1
                    ]);
                }else{
                    if($check_voucher_user->quantity < $check_voucher->quantity_use_with_user_new){
                        $check_voucher_user->update([
                            'quantity' => $check_voucher_user->quantity + 1
                        ]);
                        $req['type_voucher'] = $check_voucher->type;
                        if ($check_voucher->type_percent_or_money == 1) {
                            $req['discount_voucher'] = $req['fee_ship'] * $check_voucher->percent / 100;
                        } else {
                            $req['discount_voucher'] = $check_voucher->money;
                        }
                        $check_voucher->update([
                            'quantity_used' => $check_voucher->quantity_used + 1
                        ]);
                    }else{
                        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, 'Bạn đã hết lượt lượng sử dụng voucher dành cho người mới.', null);
                    }
                }
            }
        }
        $req['fee_ship'] = $req['fee_ship'] - $req['discount_voucher'];
        if($req['fee_ship'] < 0){
            $req['fee_ship'] = 0;
        }
        $req['sender_id'] = $user->id;
        $request->money = preg_replace('/[^0-9\-]/', '', $request->money);
        $req['money'] = preg_replace('/[^0-9\-]/', '', $request->money);
        $req['total_money'] = $request->money + $request->fee_ship + $req['money_with_product_weight'];
        $req['image_product'] = null;
        if($request->image_product){
            $req['image_product'] = $this->uploadArrayImage($request->file('image_product'));
        }
        if($req['image_product'] == null){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng gửi lên ảnh sản phẩm", null);
        }
        $user_name = trim($request->user_name);
        $phone = trim($request->phone);
        $address = trim($request->address);
        $create = OrderUserProduct::create($req);
        $check_address = AddressUser::where([['user_name', $user_name], ['phone', $phone], ['address', $address]])->first();
        if (!$check_address) {
            $create_address = AddressUser::insert([
                'user_id' => $user->id,
                'user_name' => $user_name,
                'phone' => $phone,
                'address' => $address,
                'latitude' => $request->latitude_sender,
                'longtidue' => $request->longtidue_sender,
            ]);
        }
        $client = new Client();
        $latitude = $request->latitude_sender;
        $longtidue = $request->longtidue_sender;
        $latitude_store = $request->latitude_receiver;
        $longtidue_store = $request->longtidue_receiver;

        $data1 = $client->get("https://rsapi.goong.io/Direction?origin=$latitude_store,$longtidue_store&destination=$latitude,$longtidue&alternatives=false&vehicle=car&api_key=30upqHvPKEJYB36pUzKdcO5gj6g6wtVqevvBjrZf");
        // $data1 = $client->get("https://maps.googleapis.com/maps/api/directions/json?origin=$latitude_store, $longtidue_store&destination=$latitude, $longtidue&key=AIzaSyCR2s4FVEi38-3U4az5ykzCl6mWGiBT61w");
        $response = json_decode($data1->getBody()->getContents(), true);
        if (isset($response['routes'][0]['overview_polyline']['points'])) {
            $encoded = $response['routes'][0]['overview_polyline']['points'];
            $points = Polyline::decode($encoded);
            $points = Polyline::pair($points);
            $array = [[
                'latitude' => (float) $latitude_store,
                'longitude' => (float) $longtidue_store,
            ]];
            foreach ($points as $p) {
                $array[] = array(
                    'latitude' => $p[0],
                    'longitude' => $p[1],
                );
            }
            $array[] = array(
                'latitude' => (float) $latitude,
                'longitude' => (float) $longtidue,
            );
            OrderUserProduct::where('id', $create->id)->update([
                'polyline' => json_encode($array),
            ]);
        }
        $list_shipper = User::where([['status', 1], ['role_id', 4]])->get();
        $data = [];
        $check_radius = Radius::where('id', 1)->first();
        $latitude_store = $request->latitude_sender;
        $longtidue_store = $request->longtidue_sender;
        foreach ($list_shipper as $ls) {
            $client = new Client();
            $data1 = $this->googleApi($ls->latitude, $ls->longtidue, $latitude_store, $longtidue_store);
            // $data1 = $client->get("https://maps.googleapis.com/maps/api/directions/json?origin=$ls->latitude, $ls->longtidue&destination=$latitude_store, $longtidue_store&key=AIzaSyCR2s4FVEi38-3U4az5ykzCl6mWGiBT61w");
            // $response = json_decode($data1->getBody()->getContents(), true);
            if (isset($data1)) {
                $distance_value = $data1['value'] / 1000;
                if ($distance_value <= $check_radius->radius) {
                    $data[] = $ls->id;
                }
            }
        }
        $update_wait_shipper = OrderUserProduct::where('id', $create->id)->update([
            'time_check' => date('Y-m-d H"i"s', time()),
            'quantity_push' => 1,
        ]);
        $this->notifycontroller->pushNotify($data, 'Khách hàng tên ' . $request->sender_name . ' cần bạn giao hàng đến khách hàng ' . $request->receiver_name, $create->id, SystemParam::type_order_user_product_with_shipper, $create, $request->sender_name);
        if($check_phone){
            $this->notifycontroller->pushNotify([$check_phone->id], 'Người gửi tên ' . $request->sender_name . ' đã đặt đơn giao món hàng có tên là ' . $request->product_name . ' cho bạn.' , $create->id, SystemParam::type_order_user_product_with_shipper, $create, $request->sender_name);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::order_success, $create);
    }
    public function listOrderWithUser(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['created_at', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['created_at', '<=', $to_date];
        }
        $data = OrderUserProduct::select(
            'id',
            'code',
            'sender_id',
            'sender_name',
            'sender_phone',
            'sender_address',
            'latitude_sender',
            'longtidue_sender',
            'latitude_receiver',
            'longtidue_receiver',
            'receiver_id',
            'receiver_name',
            'receiver_phone',
            'receiver_address',
            'product_name',
            'image_product',
            'shipper_id',
            'image_receive_product',
            'fee_ship',
            'money',
            'total_money',
            'note',
            'status',
            'money_with_product_weight',
            'from_weight',
            'to_weight',
            'image_send_product',
            'voucher_id',
            'discount_voucher',
            'type_voucher',
            'created_at',
            'updated_at'
        );
        if($request->type && $request->type == 2){
            $data = $data->where([['receiver_id', $user->id]])->where($where)->orderby('id', 'desc')->paginate(12);
        }else{
            $data = $data->where([['sender_id', $user->id]])->where($where)->orderby('id', 'desc')->paginate(12);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function orderDetailWithUser($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = OrderUserProduct::where([['id', $id]])->with('shipper')->first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listOrderWithReceiver(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['created_at', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['created_at', '<=', $to_date];
        }
        $data = OrderUserProduct::select(
            'id',
            'code',
            'sender_id',
            'sender_name',
            'sender_phone',
            'sender_address',
            'latitude_sender',
            'longtidue_sender',
            'latitude_receiver',
            'longtidue_receiver',
            'receiver_id',
            'receiver_name',
            'receiver_phone',
            'receiver_address',
            'product_name',
            'image_product',
            'shipper_id',
            'image_receive_product',
            'fee_ship',
            'money',
            'total_money',
            'note',
            'status',
            'money_with_product_weight',
            'from_weight',
            'to_weight',
            'image_send_product',
            'voucher_id',
            'discount_voucher',
            'type_voucher',
            'created_at',
            'updated_at'
        )->where([['receiver_id', $user->id]])->where($where)->orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function historyOrder(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $where = [];
        if($request->receiver_phone){
            $where[] = ['receiver_phone', 'LIKE', '%' . $request->receiver_phone . '%'];
        }
        $data = OrderUserProduct::select(
            'id',
            'code',
            'sender_id',
            'latitude_receiver',
            'longtidue_receiver',
            'receiver_id',
            'receiver_name',
            'receiver_phone',
            'receiver_address'
        )->where([['sender_id', $user->id]])->where($where)->orderby('id', 'desc')->get();
        $array = [];
        $receiver_name = [];
        $receiver_phone = [];
        $receiver_address = [];
        foreach($data as $d){
            if(!in_array($d->receiver_phone, $receiver_phone) || !in_array($d->receiver_name, $receiver_name) || !in_array($d->receiver_address, $receiver_address)){
                $array[] = $d;
                $receiver_name[] = $d->receiver_name;
                $receiver_phone[] = $d->receiver_phone;
                $receiver_address[] = $d->receiver_address;
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
    public function orderDetailWithReceiver($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $data = OrderUserProduct::where([['receiver_id', $user->id], ['id', $id]])->with('shipper', 'sender')->first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listOrderPending(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ người giao hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ người giao hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $order = OrderUserProduct::select(
            'id',
            'code',
            'sender_id',
            'sender_name',
            'sender_phone',
            'sender_address',
            'latitude_sender',
            'longtidue_sender',
            'latitude_receiver',
            'longtidue_receiver',
            'receiver_id',
            'receiver_name',
            'receiver_phone',
            'receiver_address',
            'product_name',
            'image_product',
            'shipper_id',
            'image_receive_product',
            'fee_ship',
            'money',
            'total_money',
            'note',
            'status',
            'image_send_product',
            'money_with_product_weight',
            'from_weight',
            'to_weight',
            'image_send_product',
            'voucher_id',
            'discount_voucher',
            'type_voucher',
            'created_at',
            'updated_at'
        );
        $array_status = [SystemParam::status_order_user_product_finish, SystemParam::status_order_user_product_sender_cancel, SystemParam::status_order_user_product_shipper_cancel, SystemParam::status_order_user_product_receiver_cancel, SystemParam::status_order_user_product_time_out_shipper, SystemParam::status_order_user_product_timzi_cancel];
        $data1 = OrderUserProduct::where([['shipper_id', $user->id]])->whereNotIn('status', $array_status)->first();
        if ($data1) {
            $data = $order->where('id', $data1->id)->with('sender')->paginate(12);
        } else {
            $data = $order->where([['status', SystemParam::status_order_user_product_pending], ['fee_ship', '<=', $user->money]])
                ->orWhere([['status', SystemParam::status_order_user_product_confirmed_shipper], ['shipper_id', $user->id], ['fee_ship', '<=', $user->money]])
                ->orWhere([['status', SystemParam::status_order_user_product_arrived_sender_shipper], ['shipper_id', $user->id], ['fee_ship', '<=', $user->money]])
                ->orWhere([['status', SystemParam::status_order_user_product_received_shipper], ['shipper_id', $user->id], ['fee_ship', '<=', $user->money]])
                ->orWhere([['status', SystemParam::status_order_user_product_arrived_receiver_shipper], ['shipper_id', $user->id], ['fee_ship', '<=', $user->money]])
                ->with('sender')
                ->orderby('id', 'asc')->paginate(12);
        }
        foreach ($data as $d) {
            $latitude_store = $d->latitude_sender;
            $longtidue_store = $d->longtidue_sender;
            $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
            if (isset($response)) {
                $d['distance_sender'] = $response;
            } else {
                $d['distance_sender'] = 0;
            }
            $response2 = $this->googleApi($latitude, $longtidue, $d->latitude_receiver, $d->longtidue_receiver);
            if (isset($response2)) {
                $d['distance_receiver'] = $response2;
            } else {
                $d['distance_receiver'] = 0;
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function orderDetailWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longtidue' => 'required',
        ], [
            'latitude.required' => 'Vui lòng nhập vĩ độ người giao hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ người giao hàng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $latitude = $request->latitude;
        $longtidue = $request->longtidue;
        $data = OrderUserProduct::where([['id', $order_id], ['status', SystemParam::status_order_user_product_pending], ['fee_ship', '<=', $user->money]])
            ->orWhere([['status', SystemParam::status_order_user_product_confirmed_shipper], ['shipper_id', $user->id], ['id', $order_id], ['fee_ship', '<=', $user->money]])
            ->orWhere([['status', SystemParam::status_order_user_product_arrived_sender_shipper], ['shipper_id', $user->id], ['id', $order_id], ['fee_ship', '<=', $user->money]])
            ->orWhere([['status', SystemParam::status_order_user_product_received_shipper], ['shipper_id', $user->id], ['id', $order_id], ['fee_ship', '<=', $user->money]])
            ->orWhere([['status', SystemParam::status_order_user_product_arrived_receiver_shipper], ['shipper_id', $user->id], ['id', $order_id], ['fee_ship', '<=', $user->money]])->with('sender')->first();
        if(!$data){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Đơn hàng giao hộ hiện tại không tồn tại.", null);
        }
        $latitude_store = $data->latitude_sender;
        $longtidue_store = $data->longtidue_sender;
        $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
        if (isset($response)) {
            $d['distance_sender'] = $response;
        } else {
            $d['distance_sender'] = 0;
        }
        $response2 = $this->googleApi($latitude, $longtidue, $data->latitude_receiver, $data->longtidue_receiver);
        if (isset($response2)) {
            $d['distance_receiver'] = $response2;
        } else {
            $d['distance_receiver'] = 0;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function receiveOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($user->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
        }
        $array_status2 = [SystemParam::status_order_finish, SystemParam::status_order_store_cancel, SystemParam::status_order_customer_cancel, SystemParam::status_order_time_out_with_store, SystemParam::status_order_shipper_cancel, SystemParam::status_order_time_out_with_shipper, SystemParam::status_order_time_out_with_24h, SystemParam::status_order_cancel_with_admin];
        $check_order_old2 = OrderFood::where([['shipper_id', $user->id]])->whereNotIn('status', $array_status2)->first();
        $array_status = [SystemParam::status_order_user_product_finish, SystemParam::status_order_user_product_sender_cancel, SystemParam::status_order_user_product_shipper_cancel, SystemParam::status_order_user_product_receiver_cancel, SystemParam::status_order_user_product_time_out_shipper, SystemParam::status_order_user_product_timzi_cancel];
        $check_order_old = OrderUserProduct::where([['shipper_id', $user->id]])->whereNotIn('status', $array_status)->first();
        if ($check_order_old || $check_order_old2) {
            // $check_order_old->update([
            //     'shipper_id' => null,
            // ]);
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_order_food_with_shipper_not_finish, null);
        }
        $check_order = OrderUserProduct::where([['status', SystemParam::status_order_user_product_pending], ['id', $order_id], ['fee_ship', '<=', $user->money]])->firstorfail();
        $check_order->update([
            'status' => SystemParam::status_order_user_product_confirmed_shipper,
            'shipper_id' => $user->id,
        ]);
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Người giao hàng ' . $user->name . ' đã nhận đơn hàng giao hộ của bạn có mã ' . $check_order->code, $order_id, SystemParam::type_order_user_product_with_shipper, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::receive_order_success, $check_order);
    }
    public function arrivedSenderOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_order = OrderUserProduct::where([['status', SystemParam::status_order_user_product_confirmed_shipper], ['id', $order_id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa nhận đơn hàng giao hộ này.", null);
        }
        $check_order->update([
            'status' => SystemParam::status_order_user_product_arrived_sender_shipper,
        ]);
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Người giao hàng ' . $user->name . ' đã đến địa điểm lấy hàng của đơn hàng giao hộ có mã ' . $check_order->code, $order_id, SystemParam::type_order_user_product_with_shipper, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check_order);
    }
    public function pickupSenderOrderWithShipper(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ], [
            'id.required' => 'Vui lòng chọn đơn giao hộ',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_order = OrderUserProduct::where([['status', SystemParam::status_order_user_product_arrived_sender_shipper], ['id', $request->id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa đến địa điểm người gửi.", null);
        }
        $req = [];
        $req['status'] = SystemParam::status_order_user_product_received_shipper;
        if ($request->file('image_send_product')) {
            $req['image_send_product'] = $this->uploadArrayImage($request->file('image_send_product'));
        }
        $check_order->update($req);
        // $check_order->update([
        //     'status' => SystemParam::status_order_user_product_received_shipper
        // ]);
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Đơn hàng giao hộ có mã ' . $check_order->code . ' của bạn đã được người giao hàng đến lấy.', $request->id, SystemParam::type_order_user_product_with_shipper, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::receive_order_success, $check_order);
    }
    public function arrivedReceiverOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_order = OrderUserProduct::where([['status', SystemParam::status_order_user_product_received_shipper], ['id', $order_id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa lấy hàng từ người gửi.", null);
        }
        $check_order->update([
            'status' => SystemParam::status_order_user_product_arrived_receiver_shipper,
        ]);
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Người giao hàng ' . $user->name . ' đã đến địa điểm người nhận của đơn hàng giao hộ có mã ' . $check_order->code, $order_id, SystemParam::type_order_user_product_with_shipper, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check_order);
    }
    public function finishOrderWithShipper(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ], [
            'id.required' => 'Vui lòng chọn đơn giao hộ',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_order = OrderUserProduct::where([['status', SystemParam::status_order_user_product_arrived_receiver_shipper], ['id', $request->id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa đến địa điểm người nhận.", null);
        }
        $req = [];
        $req['status'] = SystemParam::status_order_user_product_finish;
        if ($request->file('image_receive_product')) {
            $req['image_receive_product'] = $this->uploadArrayImage($request->file('image_receive_product'));
        }
        $check_order->update($req);
        $check_shipper = User::find($check_order->shipper_id);
        $check_percent_shipper = SetupPercentFeeShipper::first();
        if ($check_percent_shipper && $check_shipper && $check_shipper->money >= ($check_order->fee_ship * $check_percent_shipper->percent / 100)) {
            $check_shipper->update([
                'money' => $check_shipper->money - ($check_order->fee_ship * $check_percent_shipper->percent / 100),
            ]);
            HistoryMoneyShipper::insert([
                'shipper_id' => $check_order->shipper_id,
                'money' => ($check_order->fee_ship * $check_percent_shipper->percent / 100),
                'type' => 2,
                'type_order' => 2,
                'content' => "Đơn hàng giao hộ đã giao hàng thành công. Timzi đã trừ " . number_format($check_order->fee_ship * $check_percent_shipper->percent / 100) . " VNĐ trong ví liên kết của bạn!",
                'created_at' => date('Y-m-d H:i:s', time()),
            ]);
            $this->notifycontroller->pushNotify([$check_order->shipper_id], "Đơn hàng giao hộ đã giao hàng thành công. Timzi đã trừ " . number_format($check_order->fee_ship * $check_percent_shipper->percent / 100) . " VNĐ trong ví liên kết của bạn!", $request->id, SystemParam::type_money_shipper, $check_order, 'Timzi');
        }
        if($check_percent_shipper && $check_order->discount_voucher > 0){
            $check_shipper->update([
                'money' => $check_shipper->money + $check_order->discount_voucher - ($check_order->discount_voucher * $check_percent_shipper->percent / 100)
            ]);
            HistoryMoneyShipper::insert([
                'shipper_id' => $check_order->shipper_id,
                'money' => $check_order->discount_voucher - ($check_order->discount_voucher * $check_percent_shipper->percent / 100),
                'type' => 1,
                'type_order' => 1,
                'content' => "Timzi đã cộng " . number_format($check_order->discount - ($check_order->discount_voucher * $check_percent_shipper->percent / 100)) . " VNĐ vào trong ví liên kết của bạn khi đơn giao hộ thành công!",
                'created_at' => date('Y-m-d H:i:s', time()),
            ]);
            $this->notifycontroller->pushNotify([$check_order->shipper_id], "Timzi đã cộng " . number_format($check_order->discount - ($check_order->discount_voucher * $check_percent_shipper->percent / 100)) . " VNĐ vào trong ví liên kết của bạn khi đơn giao hộ thành công!", $request->id, SystemParam::type_money_shipper, $check_order, 'Timzi');
        }
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Người giao hàng ' . $user->name . ' đã giao hàng thành công đơn hàng giao hộ có mã ' . $check_order->code, $request->id, SystemParam::type_order_user_product_with_shipper, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check_order);
    }
    public function cancelOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_order = OrderUserProduct::where([['id', $order_id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Không tìm thấy đơn hàng giao hộ.", null);
        }
        if ($check_order->status == SystemParam::status_order_user_product_confirmed_shipper) {
            $check_order->update([
                'status' => SystemParam::status_order_user_product_pending,
                'shipper_id' => null,
            ]);
            $this->notifycontroller->pushNotify([$check_order->user_id], 'Đơn hàng giao hộ có mã ' . $check_order->code . ' của bạn đã bị người giao hàng hủy. Chúng tôi đang tìm lại người giao hàng cho bạn.', $order_id, SystemParam::type_order_user_product_with_shipper, $check_order, $user->name);
        } else {
            $check_order->update([
                'status' => SystemParam::status_order_user_product_shipper_cancel,
            ]);
            if($check_order->voucher_id){
                $check_voucher = VoucherDelivery::where([['id', $check_order->voucher_id]])->first();
                if($check_voucher){
                    $check_voucher->update([
                        'quantity_used' => $check_voucher->quantity_used - 1
                    ]);
                }
            }
            $this->notifycontroller->pushNotify([$check_order->user_id], 'Người giao hàng ' . $user->name . ' đã hủy đơn hàng giao hộ của bạn có mã ' . $check_order->code, $order_id, SystemParam::type_order_user_product_with_shipper, $check_order, $user->name);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check_order);
    }
    public function senderCancelOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_order = OrderUserProduct::where([['status', SystemParam::status_order_user_product_confirmed_shipper], ['id', $order_id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa đến địa điểm người gửi.", null);
        }
        $check_order->update([
            'status' => SystemParam::status_order_user_product_sender_cancel
        ]);
        if($check_order->voucher_id){
            $check_voucher = VoucherDelivery::where([['id', $check_order->voucher_id]])->first();
            if($check_voucher){
                $check_voucher->update([
                    'quantity_used' => $check_voucher->quantity_used - 1
                ]);
            }
        }
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Người giao hàng ' . $user->name . ' xác nhận bạn đã hủy đơn hàng giao hộ có mã ' . $check_order->code, $order_id, SystemParam::type_order_user_product_with_shipper, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check_order);
    }
    public function cancelOrderWithSender($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check_order = OrderUserProduct::where([['status', SystemParam::status_order_user_product_pending], ['id', $order_id], ['sender_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn không được phép hủy đơn.", null);
        }
        $check_order->update([
            'status' => SystemParam::status_order_user_product_sender_cancel
        ]);
        if($check_order->voucher_id){
            $check_voucher = VoucherDelivery::where([['id', $check_order->voucher_id]])->first();
            if($check_voucher){
                $check_voucher->update([
                    'quantity_used' => $check_voucher->quantity_used - 1
                ]);
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check_order);
    }
    public function receiverCancelOrderWithShipper($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_order = OrderUserProduct::where([['status', SystemParam::status_order_user_product_arrived_receiver_shipper], ['id', $order_id], ['shipper_id', $user->id]])->first();
        if(!$check_order){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Bạn chưa đến địa điểm người nhận.", null);
        }
        $check_order->update([
            'status' => SystemParam::status_order_user_product_receiver_cancel
        ]);
        if($check_order->voucher_id){
            $check_voucher = VoucherDelivery::where([['id', $check_order->voucher_id]])->first();
            if($check_voucher){
                $check_voucher->update([
                    'quantity_used' => $check_voucher->quantity_used - 1
                ]);
            }
        }
        $this->notifycontroller->pushNotify([$check_order->user_id], 'Người giao hàng ' . $user->name . ' xác nhận người nhận đã hủy đơn hàng giao hộ của bạn có mã ' . $check_order->code, $order_id, SystemParam::type_order_user_product_with_shipper, $check_order, $user->name);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check_order);
    }
    public function listOrderSuccess(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 4) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where2 = [['shipper_id', $user->id]];
        if ($request->date) {
            $date = date('Y-m-d', strtotime($request->date));
            $where2[] = ['created_at', 'like', '%' . $date . '%'];
        } else if ($request->from_date && $request->to_date) {
            $from_date = date('Y-m-d', strtotime($request->from_date));
            $to_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($request->to_date))) + 24 * 60 * 60 - 1);
            $where2[] = ['created_at', '>=', $from_date];
            $where2[] = ['created_at', '<=', $to_date];
        }
        $where3 = [['shipper_id', $user->id], ['status', SystemParam::status_order_user_product_finish]];
        if ($request->date) {
            $date = date('Y-m-d', strtotime($request->date));
            $where3[] = ['created_at', 'like', '%' . $date . '%'];
        } else if ($request->from_date && $request->to_date) {
            $from_date = date('Y-m-d', strtotime($request->from_date));
            $to_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($request->to_date))) + 24 * 60 * 60 - 1);
            $where3[] = ['created_at', '>=', $from_date];
            $where3[] = ['created_at', '<=', $to_date];
        }
        $order_user_product1 = OrderUserProduct::select(
            'id',
            'code',
            'sender_id',
            'sender_name',
            'sender_phone',
            'sender_address',
            'latitude_sender',
            'longtidue_sender',
            'latitude_receiver',
            'longtidue_receiver',
            'receiver_id',
            'receiver_name',
            'receiver_phone',
            'receiver_address',
            'product_name',
            'image_product',
            'shipper_id',
            'image_receive_product',
            'fee_ship',
            'money',
            'total_money',
            'note',
            'status',
            'image_send_product',
            'created_at',
            'updated_at'
        );
        $order_user_product2 = OrderUserProduct::select(
            'id',
            'code',
            'sender_id',
            'sender_name',
            'sender_phone',
            'sender_address',
            'latitude_sender',
            'longtidue_sender',
            'latitude_receiver',
            'longtidue_receiver',
            'receiver_id',
            'receiver_name',
            'receiver_phone',
            'receiver_address',
            'product_name',
            'image_product',
            'shipper_id',
            'image_receive_product',
            'fee_ship',
            'money',
            'total_money',
            'note',
            'status',
            'image_send_product',
            'created_at',
            'updated_at'
        );
        $order_user_product1 = $order_user_product1->where($where3)->orderby('id', 'desc')->get();
        $order_user_product2 = $order_user_product2->where($where3)->with('sender', 'receiver')->orderby('id', 'desc')->paginate(12);
        $history = HistoryMoneyShipper::where($where2)->where([['type', 2], ['type_order', 2]])->get();
        foreach ($order_user_product2 as $d) {
            $latitude = $d->latitude_sender;
            $longtidue = $d->longtidue_sender;
            $latitude_store = $d->latitude_receiver;
            $longtidue_store = $d->longtidue_receiver;
            $response = $this->googleApi($latitude, $longtidue, $latitude_store, $longtidue_store);
            if (isset($response)) {
                $d['distance'] = $response;
            } else {
                $d['distance'] = 0;
            }
        }
        $data = array(
            'list_order_user_product' => $order_user_product2,
            'count_order_user_product' => count($order_user_product1),
            'sum_money' => $order_user_product1->Sum('fee_ship'),
            'sum_actual_money_received' => $order_user_product1->Sum('fee_ship') - $history->SUM('money'),
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
}
