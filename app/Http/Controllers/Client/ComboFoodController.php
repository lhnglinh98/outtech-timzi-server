<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\BookFood;
use App\Models\BookFoodTopping;
use App\Models\BookTable;
use App\Models\CardFood;
use App\Models\CardToppingFood;
use App\Models\ComboFood;
use App\Models\Food;
use App\Models\OrderFood;
use App\Models\OrderFoodDetail;
use App\Models\OrderToppingFoodDetail;
use App\Models\Store;
use App\Models\UserStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class ComboFoodController extends Controller
{
    //
    public function listComboFood($store_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = ComboFood::where('store_id', $store_id)->with('food')->orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function comboFoodDetail($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        // }
        $data = ComboFood::findorfail($id)->load('food', 'store');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createComboFood(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'name' => 'required',
            'image' => 'required',
            'price' => 'required',
            'quantity' => 'required',
            'time_open' => 'required|date',
            'time_close' => 'required|date|after_or_equal:time_open',
            'food_id' => 'required',
            'content' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng tạo combo',
            'name.required' => 'Vui lòng nhập tên combo',
            'image.required' => 'Vui lòng chọn ảnh combo',
            'price.required' => 'Vui lòng nhập giá combo',
            'quantity.required' => 'Vui lòng nhập số lượng bán',
            'time_open.required' => 'Vui lòng nhập thời gian bắt đầu',
            'time_open.date' => 'Vui lòng nhập đúng định dạng',
            'time_open.after_or_equal' => 'Vui lòng nhập thời gian bắt đầu sau ngày hôm nay',
            'time_close.date' => 'Vui lòng nhập đúng định dạng',
            'time_close.required' => 'Vui lòng nhập thời gian kết thúc',
            'time_close.after_or_equal' => 'Thời gian kết thúc không thể trước thời gian bắt đầu',
            'food_id.required' => 'Vui lòng chọn sản phẩm theo combo',
            'content.required' => 'Vui lòng nhập nội dung chương trình',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if (date('Y-m-d', strtotime($request->time_open)) < date('Y-m-d')) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập thời gian bắt đầu sau ngày hôm nay", null);
        }
        if (!Store::find($request->store_id)) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $req = $request->all();
        $req['user_id'] = $user->id;
        $req['price'] = preg_replace('/[^0-9\-]/', '', $request->price);
        $req['time_open'] = date('Y-m-d', strtotime($request->time_open));
        $req['time_close'] = date('Y-m-d', strtotime($request->time_close));
        $req['image'] = $this->uploadImage($request->file('image'));
        if (date('Y-m-d', strtotime($request->time_open)) == date('Y-m-d')) {
            $req['status'] = 1;
        }
        $program_store = ComboFood::create($req);
        if (is_array($request->food_id)) {
            $ls_food_id = array_unique($request->food_id);
            foreach ($ls_food_id as $f) {
                if (Food::find($f)) {
                    $program_store->food()->attach($f);
                }
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $program_store);
    }
    public function updateComboFood($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = ComboFood::findorfail($id);
        $validator = Validator::make($request->all(), [
            // 'store_id' => 'required',
            // 'name' => 'required',
            // 'image' => 'required',
            // 'price' => 'required',
            'quantity' => 'required',
            // 'time_open' => 'required|date',
            // 'time_close' => 'required|date|after_or_equal:time_open',
            // 'food_id' => 'required',
            // 'content' => 'required',
        ], [
            // 'store_id.required' => 'Vui lòng chọn cửa hàng tạo combo',
            // 'name.required' => 'Vui lòng nhập tên combo',
            // 'image.required' => 'Vui lòng chọn ảnh combo',
            // 'price.required' => 'Vui lòng nhập giá combo',
            'quantity.required' => 'Vui lòng nhập số lượng bán',
            // 'time_open.required' => 'Vui lòng nhập thời gian bắt đầu',
            // 'time_open.date' => 'Vui lòng nhập đúng định dạng',
            // 'time_open.after_or_equal' => 'Vui lòng nhập thời gian bắt đầu sau ngày hôm nay',
            // 'time_close.date' => 'Vui lòng nhập đúng định dạng',
            // 'time_close.required' => 'Vui lòng nhập thời gian kết thúc',
            // 'time_close.after_or_equal' => 'Thời gian kết thúc không thể trước thời gian bắt đầu',
            // 'food_id.required' => 'Vui lòng chọn sản phẩm theo combo',
            // 'content.required' => 'Vui lòng nhập nội dung chương trình',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        // if (date('Y-m-d', strtotime($request->time_open)) < date('Y-m-d')) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập thời gian bắt đầu sau ngày hôm nay", null);
        // }
        $req = $request->only('quantity');
        // $req['user_id'] = $user->id;
        // $req['time_open'] = date('Y-m-d', strtotime($request->time_open));
        // $req['time_close'] = date('Y-m-d', strtotime($request->time_close));
        // if ($request->file('image')) {
        //     $check_image = DB::table('combo_food')->where('id', $id)->first();
        //     if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
        //         unlink(ltrim($check_image->image, '/'));
        //     }
        //     $req['image'] = $this->uploadImage($request->file('image'));
        // }
        $check->update($req);
        // if (is_array($request->food_id)) {
        //     $ls_food_id = array_unique($request->food_id);
        //     $check->food()->detach();
        //     foreach ($ls_food_id as $f) {
        //         if (Food::find($f)) {
        //             $check->food()->attach($f);
        //         }
        //     }
        // }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function activeComboFood($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = ComboFood::where([['id', $id]])->first();
        if (!$check || $check->status == 2) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, "Không tìm thấy combo hoặc combo của bạn đã hết hạn.", null);
        }
        if($check->is_active == 0){
            $check->update([
                'is_active' => 1,
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::active_success, $check);
        }else{
            $check->update([
                'is_active' => 0,
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::stop_success, $check);
        }
    }
    public function deleteComboFood($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = ComboFood::findorfail($id);
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_book_food1 = BookFood::where('combo_food_id', $id)->get();
        foreach ($check_book_food1 as $b1) {
            if ($b1->status == SystemParam::status_booking_food_finish) {
                $check_book_food2 = BookFood::where('book_table_id', $b1->book_table_id)->pluck('id')->toArray();
                BookFoodTopping::whereIn('book_food_id', $check_book_food2)->delete();
                BookFood::where('book_table_id', $b1->book_table_id)->delete();
                BookTable::where('id', $b1->book_table_id)->delete();
            } else {
                $check_book_food2 = BookFood::where('book_table_id', $b1->book_table_id)->pluck('id')->toArray();
                BookFoodTopping::whereIn('book_food_id', $check_book_food2)->delete();
                BookFood::where('book_table_id', $b1->book_table_id)->delete();
            }

        }
        $check_order_food_detail1 = OrderFoodDetail::where('combo_food_id', $id)->pluck('order_food_id')->toArray();
        $order_detail_id1 = OrderFoodDetail::whereIn('order_food_id', $check_order_food_detail1)->pluck('id')->toArray();
        OrderToppingFoodDetail::where('order_food_detail_id', $order_detail_id1)->delete();
        OrderFoodDetail::whereIn('order_food_id', $check_order_food_detail1)->delete();
        OrderFood::whereIn('id', $check_order_food_detail1)->delete();
        $card_food1 = CardFood::where('combo_food_id', $id)->pluck('id')->toArray();
        CardToppingFood::whereIn('card_food_id', $card_food1)->delete();
        CardFood::where('combo_food_id', $id)->delete();
        $check->food()->detach();
        $check->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check);
    }
    public function confirmComboFood($id)
    {
        $check = ComboFood::findorfail($id)->update([
            'status' => 1,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
}
