<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\BookFood;
use App\Models\BookFoodTopping;
use App\Models\BookTable;
use App\Models\Store;
use App\Models\UserStore;
use App\Models\TableStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Image;
use CodeItNow\BarcodeBundle\Utils\QrCode;
use File;
class TableStoreController extends Controller
{
    //
    public function genImageQRCode(Request $request){
        // $image = $qrCode->generate();
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        } 
        $validator = Validator::make($request->all(), [
            'table_store_id' => 'required',
        ], [
            'table_store_id.required' => 'Vui lòng chọn bàn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        $check = TableStore::findorfail($request->table_store_id);
        $qrCode = new QrCode();
        $qrCode
            ->setText($check->code)
            ->setSize(200)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
            ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
            ->setLabel('Scan Qr Code')
            ->setLabelFontSize(16)
            ->setImageType(QrCode::IMAGE_TYPE_PNG)
        ;
        $base64_code = $qrCode->generate();
        $path = 'upload/' . md5(time()) . rand(0,100000) . '.jpg';
        $fp = fopen(public_path($path), "w+");
        fwrite($fp, base64_decode($base64_code));
        fclose($fp);
        // return asset($path);
        // $mime = finfo_file(finfo_open(FILEINFO_MIME_TYPE), public_path($path));
        // return strtolower($mime);
        $img = Image::make(public_path('image_default/image_white.png'));  
        $img->text('Ban so ' . $check->number_table, 150, 10, function($font) {  
            $font->file(public_path('font/Oswald-Bold.ttf'));
            $font->size(20);  
            $font->color('#E9333A');  
            $font->align('center');  
            $font->valign('top');  
            // $font->angle(1000);  
        });  
        $img->text('Pass wifi: '. $check->pass_wifi, 150, 30, function($font) {  
            $font->file(public_path('font/Oswald-Bold.ttf'));
            $font->size(20);  
            $font->color('#E9333A');  
            $font->align('center');  
            $font->valign('top');  
            // $font->angle(1000);  
        });
        // $img->insert("image_default/qrcode_default.jpg", 'bottom-center', 0, 0);
        $img->insert(public_path($path), 'bottom-center', 0, 0);
        // $img->insert(public_path('image_default/qrcode-call.png'), 'bottom-center', 0, 0);
        $path_qr_code =  'upload/' . md5(time()) . rand(0,100000) . '.png'; 
        $img->save(public_path($path_qr_code));  
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, asset($path_qr_code)); 
    }
    public function createTableStore(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'number_table' => 'required',
            'number_people_min' => 'required',
            'number_people_max' => 'required',
            'number_floor' => 'required',
            'pass_wifi' => 'required'
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'number_table.required' => 'Vui lòng nhập số bàn',
            'number_people_min.required' => 'Vui lòng nhập số người tối thiểu',
            'number_people_max.required' => 'Vui lòng nhập số người tối đa',
            'number_floor.required' => 'Vui lòng nhập số tầng',
            'pass_wifi.required' => 'Vui lòng nhập mật khẩu wifi',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        if (!Store::find($request->store_id)) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        if (TableStore::where([['store_id', $request->store_id], ['number_table', $request->number_table]])->first()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_number_table_duplicate, null);
        }
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (TableStore::where('code', $req['code'])->exists());
        $create = TableStore::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function updateTableStore($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = TableStore::findorfail($id);
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'number_table' => 'required',
            'number_people_min' => 'required',
            'number_people_max' => 'required',
            'number_floor' => 'required',
            'pass_wifi' => 'required'
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'number_table.required' => 'Vui lòng nhập số bàn',
            'number_people_min.required' => 'Vui lòng nhập số người tối thiểu',
            'number_people_max.required' => 'Vui lòng nhập số người tối đa',
            'number_floor.required' => 'Vui lòng nhập số tầng',
            'pass_wifi.required' => 'Vui lòng nhập mật khẩu wifi',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        if (!Store::find($request->store_id)) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        if (TableStore::where([['id', '!=', $id], ['store_id', $request->store_id], ['number_table', $request->number_table]])->first()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_number_table_duplicate, null);
        }
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function tableStoreDetail($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = TableStore::findorfail($id)->load('store');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function deleteTableStore($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = TableStore::findorfail($id);
        $check_staff = UserStore::where([['store_id', $data->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $data->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_book_table = BookTable::where('table_store_id', $id)->pluck('id')->toArray();
        $check_book_food = BookFood::whereIn('book_table_id', $check_book_table)->pluck('id')->toArray();
        BookFoodTopping::whereIn('book_food_id', $check_book_food)->delete();
        BookFood::whereIn('book_table_id', $check_book_table)->delete();
        BookTable::where('table_store_id', $id)->delete();
        $data->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $data);
    }
}
