<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\BookFood;
use App\Models\BookFoodTopping;
use App\Models\BookTable;
use App\Models\CategoryFood;
use App\Models\CategoryStoreFood;
use App\Models\ComboFood;
use App\Models\Food;
use App\Models\FoodSize;
use App\Models\PaymentMethod;
use App\Models\ProgramStore;
use App\Models\RevenueStore;
use App\Models\Store;
use App\Models\ProgramStoreDetail;
use App\Models\TableMerge;
use App\Models\TableStore;
use App\Models\ToppingFood;
use App\Models\UserStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookTableInStoreController extends Controller
{
    //
    public function booktableWithStaff(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'table_store_id' => 'required',
            'name' => 'required',
            // 'phone' => 'required',
            // 'number_people' => 'required',
        ], [
            'table_store_id.required' => 'Vui lòng chọn bàn cần đặt',
            'name.required' => 'Vui lòng nhập tên',
            // 'phone.required' => 'Vui lòng nhập số điện thoại',
            // 'number_people.required' => 'Vui lòng nhập số người',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = TableStore::where('id', $request->table_store_id)->firstorfail();
        if ($check->status != SystemParam::status_table_store_empty) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_table_store_not_empty, null);
        }
        if ($request->number_people) {
            if ($check->number_people_min > $request->number_people || $check->number_people_max < $request->number_people) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_table_store_number_people, null);
            }
        }
        $check_store = Store::find($check->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $req = $request->all();
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (BookTable::where('code', $req['code'])->exists());
        $req['time_booking'] = date('Y-m-d H:i:s', time());
        $req['store_id'] = $check->store_id;
        $req['is_shop_book'] = 1;
        $req['right_to_order'] = 1;
        $req['staff_name'] = $user->name;
        $req['status'] = SystemParam::status_booking_table_confirmed;
        $create = BookTable::create($req);
        $check->update([
            'status' => SystemParam::status_table_store_booked,
        ]);
        // $id = UserStore::where([['status', 1], ['store_id', $check->store_id]])->pluck('user_id')->toArray();
        // if (count($id) == 0) {
        //     $id = [$check_store->user_id];
        // }
        // $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' đã đến quán đặt bàn số ' . $check->number_table . ' với ' . $request->number_people . ' người.', $create->id, SystemParam::type_book_table, $create);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::booking_success, $create);
    }
    public function changeTableWithStaff(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'table_store_id' => 'required',
            'book_table_id' => 'required',
        ], [
            'table_store_id.required' => 'Vui lòng chọn bàn muốn đổi',
            'book_table_id.required' => 'Vui lòng chọn bàn đã đặt trước đó',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_table = TableStore::where('id', $request->table_store_id)->firstorfail();
        if ($check_table->status != SystemParam::status_table_store_empty) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_table_store_not_empty, null);
        }
        $check_book = BookTable::where('id', $request->book_table_id)->firstorfail()->load('tableStore');
        if ($check_book->status == SystemParam::status_booking_table_has_served) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_check_book_status, null);
        }
        if ($check_book->number_people != null && $check_book->number_people > 0) {
            if ($check_table->number_people_min > $check_book->number_people || $check_table->number_people_max < $check_book->number_people) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_table_store_number_people_change, null);
            }
        }
        $check_store = Store::find($check_table->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check_staff = UserStore::where([['store_id', $check_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        TableStore::where('id', $check_book->table_store_id)->update([
            'status' => SystemParam::status_table_store_empty,
        ]);
        $check_book->update([
            'table_store_id' => $request->table_store_id,
        ]);
        TableStore::where('id', $request->table_store_id)->update([
            'status' => SystemParam::status_table_store_booked,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::change_success, $check_book);
    }
    public function booktableQRCodeWithStaff(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            // 'name' => 'required',
            // 'phone' => 'required',
            // 'number_people' => 'required',
        ], [
            'code.required' => 'Vui lòng chọn bàn cần đặt',
            // 'name.required' => 'Vui lòng nhập tên',
            // 'phone.required' => 'Vui lòng nhập số điện thoại',
            // 'number_people.required' => 'Vui lòng nhập số người',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = TableStore::where('code', $request->code)->firstorfail();
        if ($check->status != SystemParam::status_table_store_empty) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_table_store_not_empty, null);
        }
        if ($request->number_people) {
            if ($check->number_people_min > $request->number_people || $check->number_people_max < $request->number_people) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_table_store_number_people, null);
            }
        }
        $check_store = Store::find($check->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $req = $request->all();
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (BookTable::where('code', $req['code'])->exists());
        $req['time_booking'] = date('Y-m-d H:i:s', time());
        $req['store_id'] = $check->store_id;
        $req['is_shop_book'] = 1;
        $req['name'] = "Khách vãng lai";
        $req['right_to_order'] = 1;
        $req['staff_name'] = $user->name;
        $req['status'] = SystemParam::status_booking_table_confirmed;
        $create = BookTable::create($req);
        $check->update([
            'status' => SystemParam::status_table_store_booked,
        ]);
        // $id = UserStore::where([['status', 1], ['store_id', $check->store_id]])->pluck('user_id')->toArray();
        // if (count($id) == 0) {
        //     $id = [$check_store->user_id];
        // }
        // $this->notifycontroller->pushNotify($id, 'Khách hàng ' . $user->name . ' đã đến quán đặt bàn số ' . $check->number_table . ' với ' . $request->number_people . ' người.', $create->id, SystemParam::type_book_table, $create);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::booking_success, $create);
    }
    public function orderFoodWithBooktableInStore(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'book_table_id' => 'required',
            'food_id' => 'required',
            'quantity' => 'required',
        ], [
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
            'food_id.required' => 'Vui lòng chọn món ăn',
            'quantity.required' => 'Vui lòng chọn số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        // $status = [SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service];
        // $book_table = BookTable::where([['user_id', $user->id]])->whereIn('status', $status)->first();
        // if (!$book_table) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_book_table_not_found, null);
        // }
        $check_book_table = BookTable::findorfail($request->book_table_id);
        $check_staff = UserStore::where([['store_id', $check_book_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check_book_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $food = Food::find($request->food_id);
        if (!$food) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_food_not_found, null);
        }
        if ($food->status != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_food_status, null);
        }
        $req = $request->all();
        $price = $food->price * $request->quantity;
        if ($food->price_discount == 0 && $food->price_discount_with_program == 0) {
            $price = $food->price * $request->quantity;
        } else if ($food->price_discount != 0 && $food->price_discount_with_program == 0) {
            $price = $food->price_discount * $request->quantity;
        } else if ($food->price_discount_with_program != 0) {
            $price = $food->price_discount_with_program * $request->quantity;
        }
        if ($request->size_id) {
            $size = FoodSize::find($request->size_id);
            if (!$size) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_size_not_found, null);
            }
            $req['size'] = $size->name;
            $price = $price + $size->price * $request->quantity;
        }
        if ($request->topping_food_id && is_array($request->topping_food_id)) {
            $ls_topping_food_id = array_unique($request->topping_food_id);
            $topping_food = ToppingFood::whereIn('id', $ls_topping_food_id)->with('categoryToppingFood')->get();
            if (count($topping_food) == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_topping_food_id_not_found, null);
            }
            if (isset($req['size'])) {
                $check = BookFood::where([['book_table_id', $check_book_table->id], ['food_id', $request->food_id], ['size', $req['size']], ['status', SystemParam::status_booking_food_wait_submit]])->get();
            } else {
                $check = BookFood::where([['book_table_id', $check_book_table->id], ['food_id', $request->food_id], ['status', SystemParam::status_booking_food_wait_submit]])->get();
            }
            $book_food_id = null;
            if (count($check) > 0) {
                foreach ($check as $c) {
                    $book_food_topping_id = BookFoodTopping::where('book_food_id', $c->id)->pluck('topping_food_id')->toArray();
                    $check_array = array_intersect($book_food_topping_id, $ls_topping_food_id);
                    if (count($check_array) == count($book_food_topping_id) && count($check_array) == count($ls_topping_food_id)) {
                        $book_food_id = $c->id;
                    }
                }
            }
            $price = $price + $topping_food->SUM('price') * $request->quantity;
            if ($book_food_id != null) {
                $check_book = BookFood::find($book_food_id);
                $check_book->update([
                    'quantity' => $request->quantity,
                    'price' => $price,
                ]);
            } else {
                $req['price'] = $price;
                $check_book = BookFood::create($req);
                foreach ($topping_food as $t) {
                    $create = BookFoodTopping::insert([
                        'book_food_id' => $check_book->id,
                        'topping_food_id' => $t->id,
                        'category_topping_food_name' => $t->categoryToppingFood->name,
                        'topping_food_name' => $t->name,
                        'price' => $t->price,
                    ]);
                }
            }
        } else {
            $req['price'] = $price;
            if (isset($req['size'])) {
                $check_book = BookFood::where([['book_table_id', $check_book_table->id], ['food_id', $request->food_id], ['size', $req['size']], ['status', SystemParam::status_booking_food_wait_submit]])->first();
            } else {
                $check_book = BookFood::where([['book_table_id', $check_book_table->id], ['food_id', $request->food_id], ['status', SystemParam::status_booking_food_wait_submit]])->first();
            }
            if ($check_book) {
                $check_book->update([
                    'quantity' => $request->quantity,
                    'price' => $req['price'],
                ]);
            } else {
                $check_book = BookFood::create($req);
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::add_book_food_success, $check_book->load('toppingFood'));
    }
    public function orderComboWithBooktableInStore(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'book_table_id' => 'required',
            'combo_food_id' => 'required',
            'quantity' => 'required',
        ], [
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
            'combo_food_id.required' => 'Vui lòng chọn combo',
            'quantity.required' => 'Vui lòng chọn số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_book_table = BookTable::findorfail($request->book_table_id);
        $check_staff = UserStore::where([['store_id', $check_book_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check_book_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $combo_food = ComboFood::where([['id', $request->combo_food_id], ['status', 1]])->firstorfail();
        if (!$combo_food) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_combo_food_not_found, null);
        }
        // if ($combo_food->quantity < $request->quantity) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_id_combo_food_quantity, null);
        // }
        $card_2 = BookFood::where([['combo_food_id', $request->combo_food_id], ['book_table_id', $check_book_table->id], ['status', '<=', 2]])->get();
        if ($card_2->SUM('quantity') + $request->quantity > $combo_food->quantity) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, "Combo được phép đặt chỉ còn " . $combo_food->quantity . " số món.", null);
        }
        $req = $request->all();
        $req['user_id'] = $user->id;
        $price = $combo_food->price * $request->quantity;
        $req['price'] = $price;
        $check_book = BookFood::where([['book_table_id', $check_book_table->id], ['combo_food_id', $request->combo_food_id], ['status', SystemParam::status_booking_food_wait_submit]])->first();
        if ($check_book) {
            $check_book->update([
                'quantity' => $request->quantity,
                'price' => $req['price'],
            ]);
        } else {
            $check_book = BookFood::create($req);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::add_book_food_success, $check_book->load('toppingFood'));
    }
    public function subtractQuantityBookFoodInStore(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'book_food_id' => 'required',
            'quantity' => 'required',
        ], [
            'book_food_id.required' => 'Vui lòng chọn sản phẩm',
            'quantity.required' => 'Vui lòng chọn số lượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $data = BookFood::where([['id', $request->book_food_id]])->firstorfail();
        $check_book_table = BookTable::where([['id', $data->book_table_id]])->firstorfail();
        $check_staff = UserStore::where([['store_id', $check_book_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check_book_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        if ($request->quantity <= 0) {
            $delete_topping = BookFoodTopping::where('book_food_id', $data->id)->delete();
            $data->delete();
        } else {
            $price_1_product = $data->price / $data->quantity;
            $data->update([
                'quantity' => $request->quantity,
                'price' => $price_1_product * $request->quantity,
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $data);
    }
    public function cancelBookFoodWithStaff($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $status = [SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service];
        $check_book_food = BookFood::where([['id', $id]])->whereIn('status', $status)->first();
        if (!$check_book_food) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_check_book_food_not_found, null);
        }
        $check_book_table = BookTable::where([['id', $check_book_food->book_table_id]])->firstorfail();
        $check_staff = UserStore::where([['store_id', $check_book_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check_book_table->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $delete_topping = BookFoodTopping::where('book_food_id', $id)->delete();
        $check_book_food->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check_book_food);
    }
    public function submitOrderFoodWithStaff($book_table_id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        $check = BookTable::where([['id', $book_table_id]])->with('tableStore')->firstorfail();
        if ($check->status == SystemParam::status_booking_table_wait_confirm) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_not_confirm_book_table, null);
        }
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check->update([
            'status' => SystemParam::status_booking_table_in_service,
        ]);
        $update = BookFood::where([['book_table_id', $book_table_id], ['status', SystemParam::status_booking_food_wait_submit]])->update([
            'status' => SystemParam::status_booking_food_not_on_the_table_yet,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::order_food_success, $check);
    }
    public function paymentBookFoodWithStaff(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'book_table_id' => 'required',
            'total_money' => 'required',
            'payment_method_id' => 'required',
        ], [
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
            'total_money.required' => 'Vui lòng chọn lần đặt bàn',
            'payment_method_id.required' => 'Vui lòng chọn phương thức thanh toán',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = BookTable::where([['id', $request->book_table_id], ['status', SystemParam::status_booking_table_in_service]])->firstorfail();
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $check_payment_method = PaymentMethod::find($request->payment_method_id);
        if (!$check_payment_method) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_check_payment_method_not_found, null);
        }
        $check_store = Store::find($check->tableStore->store_id);
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check->update([
            'status' => SystemParam::status_booking_table_has_served,
            'total_money' => $request->total_money,
            'payment_method_id' => $request->payment_method_id,
            'staff_id' => $user->id
        ]);
        $update_book_food = BookFood::where([['book_table_id', $check->id], ['status', SystemParam::status_booking_food_was_on_the_table]])->update([
            'status' => SystemParam::status_booking_food_finish,
        ]);
        $update_table = TableStore::where('id', $check->table_store_id)->update([
            'status' => SystemParam::status_table_store_empty,
        ]);
        $check_merge = TableMerge::where('book_table_id', $request->book_table_id)->get();
        if (count($check_merge) > 0) {
            foreach ($check_merge as $c) {
                TableStore::where('id', $c->table_store_id)->update([
                    'status' => SystemParam::status_table_store_empty,
                ]);
            }
        }
        $month = date('Y-m', time());
        $check_revenue = RevenueStore::where([['store_id', $check->store_id], ['month', 'LIKE', '%' . $month . '%'], ['status', 0]])->first();
        if ($check_revenue) {
            $check_revenue->update([
                'money' => $check_revenue->money + $check->total_money,
                'fee_money' => $check_revenue->fee_money + ($check->total_money) * ($check_store->percent_discount_revenue / 100),
            ]);
        } else {
            do {
                $code_revenue = 'TZ' . $this->makeCodeRandom(8);
            } while (RevenueStore::where('code', $code_revenue)->exists());
            RevenueStore::insert([
                'code' => $code_revenue,
                'store_id' => $check->store_id,
                'money' => $check->total_money,
                'fee_money' => ($check->total_money) * ($check_store->percent_discount_revenue / 100),
                'month' => date('Y-m-d', time()),
            ]);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_payment_success, $check);
    }
    public function storeDetailWithBookTableInStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'book_table_id' => 'required',
        ], [
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::where([['is_shop_book', 1], ['id', $request->book_table_id]])->firstorfail();
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $id = $check->store_id;
        $data = Store::findorfail($check->store_id)->load('categoryStoreDetail', 'tableStore')->load(['comboFood' => function ($query) {
            $query->where('combo_food.status', 1)->with('food')->get();
        }])->load(['programStore' => function ($query1) {
            $query1->where('program_store.status', 1)->orderby('program_store.percent', 'desc')->with('food.size', 'food.categoryToppingFood.toppingFood')->get();
        }]);
        foreach ($data->programStore as $p) {
            if (count($p->food) > 0) {
                foreach ($p->food as $p1) {
                    $check_food_program = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.food_id', $p1->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                        ->get();
                    $p1['count_book_food'] = $check_food_program->SUM('quantity');
                    $p1['book_food'] = $check_food_program->load('toppingFood');
                }
            }
        }
        foreach ($data->comboFood as $cf) {
            $check_combo_food = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                ->where([['book_food.combo_food_id', $cf->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                ->get();
            $cf['count_book_food'] = $check_combo_food->SUM('quantity');
            $cf['book_food'] = $check_combo_food->load('toppingFood');
        }
        $ls_category_food_id = Food::where('store_id', $id)->pluck('category_food_id')->toArray();
        $category_food_id = array_unique($ls_category_food_id);
        $category_food = CategoryFood::where('status', 1)->whereIn('id', $category_food_id)->with('food.size', 'food.categoryToppingFood.toppingFood')->get();
        foreach ($category_food as $c) {
            if (count($c->food) > 0) {
                foreach ($c->food as $c1) {
                    $check_combo_food1 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.food_id', $c1->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['book_food.book_table_id', $check->id]])
                        ->get();
                    $c1['count_book_food'] = $check_combo_food1->SUM('quantity');
                    $c1['book_food'] = $check_combo_food1->load('toppingFood');
                }
            }
        }
        $ls_category_store_food_id = Food::where('store_id', $id)->pluck('category_store_food_id')->toArray();
        $category_store_food_id = array_unique($ls_category_store_food_id);
        $category_store_food = CategoryStoreFood::whereIn('id', $category_store_food_id)->get()->load(['food' => function ($query4) use ($id) {
            $query4->where('food.store_id', $id)->with('size', 'categoryToppingFood.toppingFood')->get();
        }]);
        foreach ($category_store_food as $s) {
            if (count($s->food) > 0) {
                foreach ($s->food as $s1) {
                    $check_combo_food1 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.food_id', $s1->id], ['book_food.status', SystemParam::status_booking_food_wait_submit], ['book_food.book_table_id', $check->id]])
                        ->get();
                    $s1['count_book_food'] = $check_combo_food1->SUM('quantity');
                    $s1['book_food'] = $check_combo_food1->load('toppingFood');
                }
            }
        }
        $total_price = 0;
        $total_quantity = 0;
        $data3 = BookFood::select('book_food.*')
            ->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
            ->where([['bt.store_id', $id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
            ->with('food')->get();
        if ($data3) {
            $total_price = $data3->SUM('price');
            $total_quantity = $data3->SUM('quantity');
        }

        $data1 = array(
            "store" => $data,
            "category_food" => $category_food,
            "category_store_food" => $category_store_food,
            'total_quantity' => $total_quantity,
            'total_price' => $total_price,
            'book_table' => $check,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data1);
    }
    public function listComboFoodStoreWithOwner($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::where([['id', $id]])->firstorfail();
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $store_id = $check->store_id;
        $data = ComboFood::where([['store_id', $store_id], ['status', 1], ['is_active', 1]])->with('food.typeFood')->limit(5)->get();
        foreach ($data as $cf) {
            $check_combo_food2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                ->where([['book_food.combo_food_id', $cf->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                ->get();
            $cf['count_book_food'] = $check_combo_food2->SUM('quantity');
            $cf['book_food'] = $check_combo_food2->load('toppingFood');
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listAllComboFoodStoreWithOwner($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::where([['id', $id]])->firstorfail();
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $store_id = $check->store_id;
        $data = ComboFood::where([['store_id', $store_id], ['status', 1], ['is_active', 1]])->with('food.typeFood')->get();
        foreach ($data as $cf) {
            $check_combo_food2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                ->where([['book_food.combo_food_id', $cf->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                ->get();
            $cf['count_book_food'] = $check_combo_food2->SUM('quantity');
            $cf['book_food'] = $check_combo_food2->load('toppingFood');
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listProgramStoreWithOwner($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::where([['id', $id]])->firstorfail();
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $store_id = $check->store_id;
        $data = ProgramStore::where([['store_id', $store_id], ['status', 1], ['quantity', '>', 0], ['is_active', 1]])->orderby('percent', 'desc')->get();
        foreach ($data as $p) {
            $list_food_id = ProgramStoreDetail::where('program_store_id', $p->id)->pluck('food_id')->toArray();
            $food = Food::whereIn('id', $list_food_id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit(5)->get();
            if (count($food) > 0) {
                foreach ($food as $p1) {
                    $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.food_id', $p1->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                        ->get();
                    $p1['count_book_food'] = $check_food_program2->SUM('quantity');
                    $p1['book_food'] = $check_food_program2->load('toppingFood');
                }
            }
            $p['food'] = $food;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listFoodInProgramStoreWithOwner($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'book_table_id' => 'required',
        ], [
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = ProgramStore::findorfail($id);
        $check_book_table = BookTable::where('id', $request->book_table_id)->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->firstorfail();
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $list_food_id = ProgramStoreDetail::where('program_store_id', $id)->pluck('food_id')->toArray();
        $food = Food::whereIn('id', $list_food_id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->paginate(12);
        if (count($food) > 0) {
            foreach ($food as $p1) {
                $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                    ->where([['book_food.food_id', $p1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                    ->get();
                $p1['count_book_food'] = $check_food_program2->SUM('quantity');
                $p1['book_food'] = $check_food_program2->load('toppingFood');
            }
        }
        $p['food'] = $food;
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $food);
    }
    public function listFoodInProgramStoreLimitWithOwner($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'required',
            'book_table_id' => 'required',
        ], [
            'limit.required' => 'Vui lòng truyền limit',
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = ProgramStore::findorfail($id);
        $check_book_table = BookTable::where('id', $request->book_table_id)->whereIn('status', [SystemParam::status_booking_table_wait_confirm, SystemParam::status_booking_table_confirmed, SystemParam::status_booking_table_in_service, SystemParam::status_booking_table_wait_payment])->firstorfail();
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $list_food_id = ProgramStoreDetail::where('program_store_id', $id)->pluck('food_id')->toArray();
        $food = Food::whereIn('id', $list_food_id)->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit($request->limit)->get();
        if (count($food) > 0) {
            foreach ($food as $p1) {
                $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                    ->where([['book_food.food_id', $p1->id], ['book_food.book_table_id', $check_book_table->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                    ->get();
                $p1['count_book_food'] = $check_food_program2->SUM('quantity');
                $p1['book_food'] = $check_food_program2->load('toppingFood');
            }
        }
        $p['food'] = $food;
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $food);
    }
    public function listCategoryFoodStoreWithOwner($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::where([['id', $id]])->firstorfail();
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $store_id = $check->store_id;
        $ls_category_food_id = Food::where('store_id', $store_id)->pluck('category_food_id')->toArray();
        $category_food_id = array_unique($ls_category_food_id);
        $data = CategoryFood::where('status', 1)->whereIn('id', $category_food_id)->get();
        foreach ($data as $c) {
            $food = Food::where([['category_food_id', $c->id], ['store_id', $store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit(5)->get();
            if (count($food) > 0) {
                foreach ($food as $c1) {
                    $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.food_id', $c1->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                        ->get();
                    $c1['count_book_food'] = $check_food_program2->SUM('quantity');
                    $c1['book_food'] = $check_food_program2->load('toppingFood');
                }
            }
            $c['food'] = $food;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCategoryStoreFoodStoreWithOwner($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::where([['id', $id]])->firstorfail();
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $store_id = $check->store_id;
        $ls_category_store_food_id = Food::where('store_id', $store_id)->pluck('category_store_food_id')->toArray();
        $category_store_food_id = array_unique($ls_category_store_food_id);
        $data = CategoryStoreFood::whereIn('id', $category_store_food_id)->get();
        foreach ($data as $s) {
            $food = Food::where([['category_store_food_id', $s->id], ['store_id', $store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit(5)->get();
            if (count($food) > 0) {
                foreach ($food as $s1) {
                    $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.food_id', $s1->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                        ->get();
                    $s1['count_book_food'] = $check_food_program2->SUM('quantity');
                    $s1['book_food'] = $check_food_program2->load('toppingFood');
                }
            }
            $s['food'] = $food;
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listFoodWithCategoryInStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'category_id' => 'required',
            'type' => 'required',
            'book_table_id' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'category_id.required' => 'Vui lòng chọn danh mục',
            'type.required' => 'Vui lòng chọn loại danh mục',
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $food = [];
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::where([['id', $request->book_table_id]])->firstorfail();
        if ($request->type == 1) {
            $food = Food::where([['category_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->paginate(12);
            if (count($food) > 0) {
                foreach ($food as $s1) {
                    $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.food_id', $s1->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                        ->get();
                    $s1['count_book_food'] = $check_food_program2->SUM('quantity');
                    $s1['book_food'] = $check_food_program2->load('toppingFood');
                }
            }
        } else {
            $food = Food::where([['category_store_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->paginate(12);
            if (count($food) > 0) {
                foreach ($food as $s1) {
                    $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.food_id', $s1->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                        ->get();
                    $s1['count_book_food'] = $check_food_program2->SUM('quantity');
                    $s1['book_food'] = $check_food_program2->load('toppingFood');
                }
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $food);
    }
    public function listFoodWithCategoryInStoreLimit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'store_id' => 'required',
            'category_id' => 'required',
            'type' => 'required',
            'book_table_id' => 'required',
            'limit' => 'required',
        ], [
            'store_id.required' => 'Vui lòng chọn cửa hàng',
            'category_id.required' => 'Vui lòng chọn danh mục',
            'type.required' => 'Vui lòng chọn loại danh mục',
            'book_table_id.required' => 'Vui lòng chọn lần đặt bàn',
            'limit.required' => 'Vui lòng truyền limit',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $food = [];
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::where([['id', $request->book_table_id]])->firstorfail();
        if ($request->type == 1) {
            $food = Food::where([['category_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit($request->limit)->get();
            if (count($food) > 0) {
                foreach ($food as $s1) {
                    $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.food_id', $s1->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                        ->get();
                    $s1['count_book_food'] = $check_food_program2->SUM('quantity');
                    $s1['book_food'] = $check_food_program2->load('toppingFood');
                }
            }
        } else {
            $food = Food::where([['category_store_food_id', $request->category_id], ['store_id', $request->store_id]])->with('size', 'categoryToppingFood.toppingFood', 'typeFood')->orderby('id', 'desc')->limit($request->limit)->get();
            if (count($food) > 0) {
                foreach ($food as $s1) {
                    $check_food_program2 = BookFood::select('book_food.*')->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
                        ->where([['book_food.food_id', $s1->id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
                        ->get();
                    $s1['count_book_food'] = $check_food_program2->SUM('quantity');
                    $s1['book_food'] = $check_food_program2->load('toppingFood');
                }
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $food);
    }
    public function bookTableWithStoreWithOwner($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 2 && $user->role_id != 3 && $user->role_id != 6) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = BookTable::where([['id', $id]])->with('tableStore')->firstorfail();
        $check_staff = UserStore::where([['store_id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
        if (!$check_staff) {
            $check_store_owner = Store::where([['id', $check->store_id], ['status', 1], ['user_id', $user->id]])->first();
            if (!$check_store_owner) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $store_id = $check->store_id;
        $data3 = BookFood::select('book_food.*')
            ->join('book_table as bt', 'bt.id', 'book_food.book_table_id')
            ->where([['bt.store_id', $store_id], ['book_food.book_table_id', $check->id], ['book_food.status', SystemParam::status_booking_food_wait_submit]])
            ->with('food')->get();
        $data = array(
            'book_table' => $check,
            'total_quantity' => $data3->SUM('quantity'),
            'total_price' => $data3->SUM('price'),
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
}
