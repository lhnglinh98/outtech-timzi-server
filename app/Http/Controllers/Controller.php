<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Traits\ApiResponse;
use App\Traits\UploadImage;
use App\Traits\MakeCodeRandom;
use App\Traits\GetAuthenticatedUser;
use App\Traits\GoogleApi;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ApiResponse, GetAuthenticatedUser, UploadImage, MakeCodeRandom, GoogleApi;
}
