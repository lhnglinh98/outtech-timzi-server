<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Client\NotifyController;
use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\BookFood;
use App\Models\BookFoodTopping;
use App\Models\BookTable;
use App\Models\CardFood;
use App\Models\CardToppingFood;
use App\Models\Category;
use App\Models\CategoryFood;
use App\Models\CategoryStoreDetail;
use App\Models\CategoryStoreFood;
use App\Models\CategoryToppingFood;
use App\Models\ComboFood;
use App\Models\Food;
use App\Models\FoodSize;
use App\Models\OrderFood;
use App\Models\OrderFoodDetail;
use App\Models\OrderToppingFoodDetail;
use App\Models\ProgramDetail;
use App\Models\ProgramStore;
use App\Models\ProgramStoreDetail;
use App\Models\ShipperStore;
use App\Models\Store;
use App\Models\TableStore;
use App\Models\ToppingFood;
use App\Models\UserStore;
use App\Models\Voucher;
use App\Models\CategoryPermanent;
use App\Models\ReviewShipper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class StoreController extends Controller
{
    //
    protected $notifycontroller;
    public function __construct()
    {
        $this->notifycontroller = new NotifyController();
    }
    public function listStoreWithPushNotify(Request $request){
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $where = [];
        if ($request->name) {
            $where[] = ['stores.name', 'LIKE', '%' . $request->name . '%'];
        }
        if($admin->role_id == 13 || $admin->role_id == 14){
            if(!$admin->manage_district_id){
                $data = Store::where($where)->where('province_id', $admin->manage_province_id)->orderby('id', 'desc')->get();
            }else{
                $data = Store::where($where)->where('status', 1)->where([['province_id', $admin->manage_province_id], ['district_id', $admin->manage_district_id]])->orderby('id', 'desc')->get();
            }
        }else{
            $data = Store::select('stores.*')->where($where)->where('stores.status', 1)->orderby('id', 'desc')->with('category', 'staff', 'categoryStoreDetail')->get();
        }
        // $data = Store::where($where)->where('status', 1)->orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStore(Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 9 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $where = [];
        if ($request->name) {
            $where[] = ['stores.name', 'LIKE', '%' . $request->name . '%'];
        }
        if ($request->province_id) {
            $where[] = ['stores.province_id', $request->province_id];
        }
        if ($request->user_name) {
            $where[] = ['u.name', 'LIKE', '%' . $request->user_name . '%'];
        }
        if ($request->status) {
            $where[] = ['stores.status', $request->status];
        }
        if($admin->role_id == 11 || $admin->role_id == 13 || $admin->role_id == 14){
            if(!$admin->manage_district_id){
                $data = Store::select('stores.*')->join('users as u', 'u.id', 'stores.user_id')->where($where)->where('stores.province_id', $admin->manage_province_id)->orderby('id', 'desc')->with('category', 'staff', 'categoryStoreDetail')->paginate(12);
            }else{
                $data = Store::select('stores.*')->join('users as u', 'u.id', 'stores.user_id')->where($where)->where([['stores.province_id', $admin->manage_province_id], ['stores.district_id', $admin->manage_district_id]])->orderby('id', 'desc')->with('category', 'staff', 'categoryStoreDetail')->paginate(12);
            }
        }else{
            $data = Store::select('stores.*')->where($where)->join('users as u', 'u.id', 'stores.user_id')->orderby('id', 'desc')->with('category', 'staff', 'categoryStoreDetail')->paginate(12);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function confirmStore($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1  && $admin->role_id != 9 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $check = Store::where([['id', $id], ['status', 0]])->firstorfail();
        $check->update([
            'status' => 1,
        ]);
        $this->notifycontroller->pushNotify([$check->user_id], 'Cửa hàng của bạn đã được hệ thống xác nhận!', $check->id, SystemParam::type_store, $check, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $check);
    }
    public function lockStore($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($admin->role_id != 1) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        // }
        $update = Store::findorfail($id)->update(['status' => 2]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::lock_store_success, $update);
    }
    public function unlockStore($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($admin->role_id != 1) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        // }
        $update = Store::findorfail($id)->update(['status' => 1]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::unlock_store_success, $update);
    }
    public function cancelStore($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1  && $admin->role_id != 9 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $check = Store::where([['id', $id], ['status', 0]])->firstorfail();
        $check->update([
            'status' => 2,
        ]);
        $this->notifycontroller->pushNotify([$check->user_id], 'Cửa hàng của bạn đã bị hệ thống từ chối!', $check->id, SystemParam::type_store, $check, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $check);
    }
    public function storeDetail($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1  && $admin->role_id != 9 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $data = Store::findorfail($id)->load('category', 'staff', 'categoryStoreDetail', 'programStore', 'tableStore')->load('comboFood.food');
        $ls_category_food_id = Food::where('store_id', $id)->pluck('category_food_id')->toArray();
        $category_food_id = array_unique($ls_category_food_id);
        $category_food = CategoryFood::where('status', 1)->whereIn('id', $category_food_id)->get()->load(['food' => function ($query) {
            $query->limit(12)->with('size', 'categoryToppingFood.toppingFood')->get();
        }]);
        $owner_store = UserStore::where([['store_id', $id], ['is_owner', 1]])->first();
        $data1 = array(
            "store" => $data,
            "category_food" => $category_food,
            'owner_store' => $owner_store,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data1);
    }
    public function createStore(Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1  && $admin->role_id != 9 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longtidue' => 'required',
            'average_price' => 'required',
            'open_hours' => 'required',
            'close_hours' => 'required',
            'hotline' => 'required',
            'category_id' => 'required',
            'user_id' => 'required',
            'percent_discount_revenue' => 'required',
        ], [
            'user_id.required' => 'Vui lòng chọn chủ chuỗi cửa hàng',
            'name.required' => 'Vui lòng nhập tên cửa hàng',
            'image.required' => 'Vui lòng chọn ảnh cửa hàng',
            'province_id.required' => 'Vui lòng chọn tỉnh thành',
            'district_id.required' => 'Vui lòng chọn quận huyện',
            'address.required' => 'Vui lòng nhập địa chỉ cửa hàng',
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
            'average_price.required' => 'Vui lòng nhập giá trung bình',
            'open_hours.required' => 'Vui lòng nhập giờ mở cửa',
            'close_hours.required' => 'Vui lòng nhập giờ đóng cửa',
            'hotline.required' => 'Vui lòng nhập hotline',
            'category_id.required' => 'Vui lòng chọn danh mục cửa hàng',
            'percent_discount_revenue.required' => 'Vui lòng nhập phần trăm chiết khấu',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $check_user_id = User::where([['id', $request->user_id], ['role_id', 2], ['status', 1]])->firstorfail();
        $req = $request->all();
        $req['status'] = 1;
        $req['percent_discount_revenue'] = 0;
        $req['image'] = $this->uploadImage($request->file('image'));
        $store = Store::create($req);
        if (is_array($request->category_id)) {
            $ls_category_id = array_unique($request->category_id);
            foreach ($ls_category_id as $c) {
                if (Category::where([['status', 1], ['id', $c]])->first()) {
                    $store->category()->attach($c);
                    $check_category_permanent = CategoryPermanent::where('category_id', $c)->get();
                    $array = [];
                    foreach($check_category_permanent as $cp){
                        $array[] = array(
                            'store_id' => $store->id,
                            'name' => $cp->name
                        );
                        // if(CategoryStoreFood::where([['']]))
                    }
                    CategoryStoreFood::insert($array);
                }
            }
        }
        if ($request->category_business && is_array($request->category_business)) {
            foreach ($request->category_business as $i) {
                if ($i != null && $i != "") {
                    CategoryStoreDetail::insert([
                        'store_id' => $store->id,
                        'category_name' => $i,
                    ]);
                }
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $store);
    }
    public function updateStore($id, Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1  && $admin->role_id != 9 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longtidue' => 'required',
            'average_price' => 'required',
            'open_hours' => 'required',
            'close_hours' => 'required',
            'hotline' => 'required',
            'category_id' => 'required',
            'user_id' => 'required',
            'percent_discount_revenue' => 'required',
        ], [
            'user_id.required' => 'Vui lòng chọn chủ chuỗi cửa hàng',
            'name.required' => 'Vui lòng nhập tên cửa hàng',
            'province_id.required' => 'Vui lòng chọn tỉnh thành',
            'district_id.required' => 'Vui lòng chọn quận huyện',
            'address.required' => 'Vui lòng nhập địa chỉ cửa hàng',
            'latitude.required' => 'Vui lòng nhập vĩ độ cửa hàng',
            'longtidue.required' => 'Vui lòng nhập kinh độ cửa hàng',
            'average_price.required' => 'Vui lòng nhập giá trung bình',
            'open_hours.required' => 'Vui lòng nhập giờ mở cửa',
            'close_hours.required' => 'Vui lòng nhập giờ đóng cửa',
            'hotline.required' => 'Vui lòng nhập hotline',
            'category_id.required' => 'Vui lòng chọn danh mục cửa hàng',
            'percent_discount_revenue.required' => 'Vui lòng nhập phần trăm chiết khấu',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ((float) $request->latitude == 0 || (float) $request->latitude == 0) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_latitude_and_longtidue, null);
        }
        $check_user_id = User::where([['id', $request->user_id], ['role_id', 2], ['status', 1]])->firstorfail();
        $check = Store::findorfail($id);
        $req = $request->all();
        if ($request->file('image')) {
            $check_image = DB::table('stores')->where('id', $id)->first();
            if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
                unlink(ltrim($check_image->image, '/'));
            }
            $req['image'] = $this->uploadImage($request->file('image'));
        }
        $check->update($req);
        if (is_array($request->category_id)) {
            $ls_category_id = array_unique($request->category_id);
            $check->category()->detach();
            foreach ($ls_category_id as $c) {
                if (Category::where([['status', 1], ['id', $c]])->first()) {
                    $check->category()->attach($c);
                    $check_category_permanent = CategoryPermanent::where('category_id', $c)->get();
                    $array = [];
                    foreach($check_category_permanent as $cp){
                        if(!$check_cate = CategoryStoreFood::where([['store_id', $id], ['name', $cp->name]])->first()){
                            $array[] = array(
                                'store_id' => $id,
                                'name' => $cp->name
                            );
                        }
                    }
                    CategoryStoreFood::insert($array);
                }
            }
        }
        if ($request->category_business && is_array($request->category_business)) {
            CategoryStoreDetail::where('store_id', $id)->delete();
            foreach ($request->category_business as $i) {
                if ($i != null && $i != "") {
                    CategoryStoreDetail::insert([
                        'store_id' => $id,
                        'category_name' => $i,
                    ]);
                }
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteStore($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1  && $user->role_id != 9 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_store = Store::where([['id', $id]])->first();
        if (!$check_store) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
        }
        $check_book_table = BookTable::where('store_id', $id)->pluck('id')->toArray();
        $check_book_food = BookFood::whereIn('book_table_id', $check_book_table)->pluck('id')->toArray();
        BookFoodTopping::whereIn('book_food_id', $check_book_food)->delete();
        BookFood::whereIn('book_table_id', $check_book_table)->delete();
        BookTable::where('store_id', $id)->delete();
        $table_store = TableStore::where('store_id', $id)->delete();
        $check_program_store = ProgramStore::where('store_id', $id)->pluck('id')->toArray();
        ProgramStoreDetail::whereIn('program_store_id', $check_program_store)->delete();
        ProgramStore::where('store_id', $id)->delete();
        ProgramDetail::where('store_id', $id)->delete();
        $card_food = CardFood::where('store_id', $id)->pluck('id')->toArray();
        CardToppingFood::whereIn('card_food_id', $card_food)->delete();
        CardFood::where('store_id', $id)->delete();
        $check_order_food = OrderFood::where('store_id', $id)->pluck('id')->toArray();
        $order_detail_id1 = OrderFoodDetail::whereIn('order_food_id', $check_order_food)->pluck('id')->toArray();
        OrderToppingFoodDetail::where('order_food_detail_id', $order_detail_id1)->delete();
        OrderFoodDetail::whereIn('order_food_id', $check_order_food)->delete();
        ReviewShipper::whereIn('order_food_id', $check_order_food)->delete();
        OrderFood::where('store_id', $id)->delete();
        UserStore::where('store_id', $id)->delete();
        ShipperStore::where('store_id', $id)->delete();
        Voucher::where('store_id', $id)->delete();
        ComboFood::where('store_id', $id)->delete();
        $food = Food::where('store_id', $id)->pluck('id')->toArray();
        FoodSize::whereIn('food_id', $food)->delete();
        $check_category_topping_food = CategoryToppingFood::whereIn('food_id', $food)->pluck('id')->toArray();
        ToppingFood::whereIn('category_topping_food_id', $check_category_topping_food)->delete();
        CategoryToppingFood::whereIn('food_id', $food)->delete();
        DB::table('category_store')->where('store_id', $id)->delete();
        CategoryStoreDetail::where('store_id', $id)->delete();
        CategoryStoreFood::where('store_id', $id)->delete();
        Store::where('id', $id)->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check_store);
    }
}
