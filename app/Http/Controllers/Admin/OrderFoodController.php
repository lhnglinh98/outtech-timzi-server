<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OrderFood;
use App\Models\OrderFoodDetail;
use App\Models\OrderUserProduct;
use Illuminate\Support\Facades\Validator;
use App\Http\Utils\SystemParam;
use App\Models\Store;
use App\User;

class OrderFoodController extends Controller
{
    //
    public function listOrderFood(Request $request){
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['order_food.date_time', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['order_food.date_time', '<=', $to_date];
        }
        if ($request->shop_name) {
            $where[] = ['s.name', 'LIKE', '%' . $request->shop_name . '%'];
        }
        if ($request->shipper_name) {
            $where[] = ['us.name', 'LIKE', '%' . $request->shipper_name . '%'];
        }
        if ($request->customer_name) {
            $where[] = ['uc.name', 'LIKE', '%' . $request->customer_name . '%'];
        }
        $order1 = OrderFood::select(
            'order_food.id',
            'order_food.code',
            'order_food.user_id',
            'order_food.user_name',
            'order_food.phone',
            'order_food.address',
            'order_food.latitude',
            'order_food.longtidue',
            'order_food.date_time',
            'order_food.store_id',
            'order_food.fee_ship',
            'order_food.discount',
            'order_food.total_money',
            'order_food.coin',
            'order_food.note',
            'order_food.payment_method_id',
            'order_food.shipper_id',
            'order_food.star',
            'order_food.status',
            'order_food.created_at',
            'order_food.is_review_shipper',
            'order_food.time_check',
            'order_food.quantity_push',
            'order_food.money_voucher',
            'order_food.type_voucher',
            'order_food.voucher_id',
            'order_food.content',
            'order_food.total_money_food',
            'order_food.staff_timzi_id',
            'order_food.updated_at'
        )->leftJoin('stores as s', 's.id', 'order_food.store_id')
        ->leftJoin('users as us', 'us.id', 'order_food.shipper_id')
        ->leftJoin('users as uc', 'uc.id', 'order_food.user_id');
        $order2 = OrderFood::select(
            'order_food.id',
            'order_food.code',
            'order_food.user_id',
            'order_food.user_name',
            'order_food.phone',
            'order_food.address',
            'order_food.latitude',
            'order_food.longtidue',
            'order_food.date_time',
            'order_food.store_id',
            'order_food.fee_ship',
            'order_food.discount',
            'order_food.total_money',
            'order_food.coin',
            'order_food.note',
            'order_food.payment_method_id',
            'order_food.shipper_id',
            'order_food.star',
            'order_food.status',
            'order_food.created_at',
            'order_food.is_review_shipper',
            'order_food.time_check',
            'order_food.quantity_push',
            'order_food.money_voucher',
            'order_food.type_voucher',
            'order_food.voucher_id',
            'order_food.content',
            'order_food.total_money_food',
            'order_food.staff_timzi_id',
            'order_food.updated_at'
        )->leftJoin('stores as s', 's.id', 'order_food.store_id')
        ->leftJoin('users as us', 'us.id', 'order_food.shipper_id')
        ->leftJoin('users as uc', 'uc.id', 'order_food.user_id');
        $order3 = OrderFood::select(
            'order_food.id',
            'order_food.code',
            'order_food.user_id',
            'order_food.user_name',
            'order_food.phone',
            'order_food.address',
            'order_food.latitude',
            'order_food.longtidue',
            'order_food.date_time',
            'order_food.store_id',
            'order_food.fee_ship',
            'order_food.discount',
            'order_food.total_money',
            'order_food.coin',
            'order_food.note',
            'order_food.payment_method_id',
            'order_food.shipper_id',
            'order_food.star',
            'order_food.status',
            'order_food.created_at',
            'order_food.is_review_shipper',
            'order_food.time_check',
            'order_food.quantity_push',
            'order_food.money_voucher',
            'order_food.type_voucher',
            'order_food.voucher_id',
            'order_food.content',
            'order_food.total_money_food',
            'order_food.staff_timzi_id',
            'order_food.updated_at'
        )->leftJoin('stores as s', 's.id', 'order_food.store_id')
        ->leftJoin('users as us', 'us.id', 'order_food.shipper_id')
        ->leftJoin('users as uc', 'uc.id', 'order_food.user_id');
        if($admin->role_id == 11 || $admin->role_id == 13 || $admin->role_id == 14){
            if(!$admin->manage_district_id){
                $ls_store_id = Store::where('province_id', $admin->manage_province_id)->pluck('id')->toArray();
            }else{
                $ls_store_id = Store::where([['province_id', $admin->manage_province_id], ['district_id', $admin->manage_district_id]])->pluck('id')->toArray();
            }
            $count1 = $order1->where($where)->whereIn('order_food.store_id', $ls_store_id)->get();
            $count2 = $order2->where($where)->whereIn('order_food.store_id', $ls_store_id)->where('order_food.status', SystemParam::status_order_finish)->get();
            $data = $order3->where($where)->whereIn('order_food.store_id', $ls_store_id)->with('store', 'shipper', 'user', 'staffTimzi')->orderby('order_food.id', 'desc')->paginate(24);
        }else{
            $count1 = $order1->where($where)->get();
            $count2 = $order2->where($where)->where('order_food.status', SystemParam::status_order_finish)->get();
            $data = $order3->where($where)->with('store', 'shipper', 'user', 'staffTimzi')->orderby('order_food.id', 'desc')->paginate(24);
        }
        $array = array(
            'count' => count($count1),
            'total_money_shop' => $count2->SUM('total_money') - $count2->SUM('fee_ship'),
            'total_money_shipper' => $count2->SUM('fee_ship'),
            'data' => $data
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
    public function orderDetail($id){
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $order = OrderFood::select(
            'order_food.id',
            'order_food.code',
            'order_food.user_id',
            'order_food.user_name',
            'order_food.phone',
            'order_food.address',
            'order_food.latitude',
            'order_food.longtidue',
            'order_food.date_time',
            'order_food.store_id',
            'order_food.fee_ship',
            'order_food.discount',
            'order_food.total_money',
            'order_food.coin',
            'order_food.note',
            'order_food.payment_method_id',
            'order_food.shipper_id',
            'order_food.star',
            'order_food.status',
            'order_food.created_at',
            'order_food.is_review_shipper',
            'order_food.time_check',
            'order_food.quantity_push',
            'order_food.money_voucher',
            'order_food.type_voucher',
            'order_food.voucher_id',
            'order_food.content',
            'order_food.total_money_food',
            'order_food.staff_timzi_id',
            'order_food.updated_at'
        )->leftJoin('stores as s', 's.id', 'order_food.store_id')
        ->leftJoin('users as us', 'us.id', 'order_food.shipper_id')
        ->leftJoin('users as uc', 'uc.id', 'order_food.user_id');
        if($admin->role_id == 11 || $admin->role_id == 13 || $admin->role_id == 14){
            if(!$admin->manage_district_id){
                $ls_store_id = Store::where('province_id', $admin->manage_province_id)->pluck('id')->toArray();
            }else{
                $ls_store_id = Store::where([['province_id', $admin->manage_province_id], ['district_id', $admin->manage_district_id]])->pluck('id')->toArray();
            }
            $data = $order->where('order_food.id', $id)->whereIn('order_food.store_id', $ls_store_id)->with('user', 'shipper', 'store', 'orderFoodDetail.orderToppingFoodDetail', 'orderFoodDetail.food', 'orderFoodDetail.comboFood', 'staffTimzi')->first();
        }else{
            $data = $order->where('order_food.id', $id)->with('user', 'shipper', 'store', 'orderFoodDetail.orderToppingFoodDetail', 'orderFoodDetail.food', 'orderFoodDetail.comboFood', 'staffTimzi')->first();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function cancelOrder($id){
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $order = OrderFood::leftJoin('stores as s', 's.id', 'order_food.store_id')
        ->leftJoin('users as us', 'us.id', 'order_food.shipper_id')
        ->leftJoin('users as uc', 'uc.id', 'order_food.user_id');
        if($admin->role_id == 11 || $admin->role_id == 13 || $admin->role_id == 14){
            if(!$admin->manage_district_id){
                $ls_store_id = Store::where('province_id', $admin->manage_province_id)->pluck('id')->toArray();
            }else{
                $ls_store_id = Store::where([['province_id', $admin->manage_province_id], ['district_id', $admin->manage_district_id]])->pluck('id')->toArray();
            }
            $data = $order->where([['order_food.id', $id], ['order_food.status', '!=', SystemParam::status_order_finish]])->whereIn('order_food.store_id', $ls_store_id)->first();
            if(!$data){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::order_not_found, null);
            }
            OrderFood::where('id', $id)->update([
                'status' => SystemParam::status_order_cancel_with_admin,
                'staff_timzi_id' => $admin->id
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $data);
        }else{
            $data = $order->where([['order_food.id', $id], ['order_food.status', '!=', SystemParam::status_order_finish]])->first();
            if(!$data){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::order_not_found, null);
            }
            OrderFood::where('id', $id)->update([
                'status' => SystemParam::status_order_cancel_with_admin,
                'staff_timzi_id' => $admin->id
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $data);
        }
    }
    public function listOrderUserProduct(Request $request){
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['order_user_product.created_at', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['order_user_product.created_at', '<=', $to_date];
        }
        if ($request->shipper_name) {
            $where[] = ['us.name', 'LIKE', '%' . $request->shipper_name . '%'];
        }
        if ($request->customer_name) {
            $where[] = ['uc.name', 'LIKE', '%' . $request->customer_name . '%'];
        }
        $order1 = OrderUserProduct::select(
            'order_user_product.code',
            'order_user_product.sender_id',
            'order_user_product.sender_name',
            'order_user_product.sender_phone',
            'order_user_product.sender_address',
            'order_user_product.latitude_sender',
            'order_user_product.longtidue_sender',
            'order_user_product.latitude_receiver',
            'order_user_product.longtidue_receiver',
            'order_user_product.receiver_id',
            'order_user_product.receiver_name',
            'order_user_product.receiver_phone',
            'order_user_product.receiver_address',
            'order_user_product.product_name',
            'order_user_product.image_product',
            'order_user_product.shipper_id',
            'order_user_product.image_receive_product',
            'order_user_product.fee_ship',
            'order_user_product.money',
            'order_user_product.total_money',
            'order_user_product.note',
            'order_user_product.status',
            'order_user_product.updated_at'
        )->leftJoin('users as us', 'us.id', 'order_user_product.shipper_id')
        ->leftJoin('users as uc', 'uc.id', 'order_user_product.sender_id');
        $order2 = OrderUserProduct::select(
            'order_user_product.code',
            'order_user_product.sender_id',
            'order_user_product.sender_name',
            'order_user_product.sender_phone',
            'order_user_product.sender_address',
            'order_user_product.latitude_sender',
            'order_user_product.longtidue_sender',
            'order_user_product.latitude_receiver',
            'order_user_product.longtidue_receiver',
            'order_user_product.receiver_id',
            'order_user_product.receiver_name',
            'order_user_product.receiver_phone',
            'order_user_product.receiver_address',
            'order_user_product.product_name',
            'order_user_product.image_product',
            'order_user_product.shipper_id',
            'order_user_product.image_receive_product',
            'order_user_product.fee_ship',
            'order_user_product.money',
            'order_user_product.total_money',
            'order_user_product.note',
            'order_user_product.status',
            'order_user_product.updated_at'
        )->leftJoin('users as us', 'us.id', 'order_user_product.shipper_id')
        ->leftJoin('users as uc', 'uc.id', 'order_user_product.sender_id');
        $order3 = OrderUserProduct::select(
            'order_user_product.id',
            'order_user_product.code',
            'order_user_product.sender_id',
            'order_user_product.sender_name',
            'order_user_product.sender_phone',
            'order_user_product.sender_address',
            'order_user_product.latitude_sender',
            'order_user_product.longtidue_sender',
            'order_user_product.latitude_receiver',
            'order_user_product.longtidue_receiver',
            'order_user_product.receiver_id',
            'order_user_product.receiver_name',
            'order_user_product.receiver_phone',
            'order_user_product.receiver_address',
            'order_user_product.product_name',
            'order_user_product.image_product',
            'order_user_product.shipper_id',
            'order_user_product.image_receive_product',
            'order_user_product.fee_ship',
            'order_user_product.money',
            'order_user_product.total_money',
            'order_user_product.note',
            'order_user_product.status',
            'order_user_product.staff_timzi_id',
            'order_user_product.created_at',
            'order_user_product.money_with_product_weight',
            'order_user_product.from_weight',
            'order_user_product.to_weight',
            'order_user_product.image_send_product',
            'order_user_product.updated_at'
        )->leftJoin('users as us', 'us.id', 'order_user_product.shipper_id')
        ->leftJoin('users as uc', 'uc.id', 'order_user_product.sender_id');
        if($admin->role_id == 11 || $admin->role_id == 13 || $admin->role_id == 14){
            if(!$admin->manage_district_id){
                $ls_id = User::where('province_id', $admin->manage_province_id)->pluck('id')->toArray();
            }else{
                $ls_id = User::where([['province_id', $admin->manage_province_id], ['district_id', $admin->manage_district_id]])->pluck('id')->toArray();
            }
            $count1 = $order1->where($where)->whereIn('order_user_product.sender_id', $ls_id)->get();
            $count2 = $order2->where($where)->whereIn('order_user_product.sender_id', $ls_id)->where('order_user_product.status', SystemParam::status_order_user_product_finish)->get();
            $data = $order3->where($where)->whereIn('order_user_product.sender_id', $ls_id)->with('sender', 'shipper', 'staffTimzi')->orderby('order_user_product.id', 'desc')->paginate(24);
        }else{
            $count1 = $order1->where($where)->get();
            $count2 = $order2->where($where)->where('order_user_product.status', SystemParam::status_order_user_product_finish)->get();
            $data = $order3->where($where)->with('sender', 'shipper', 'staffTimzi')->orderby('order_user_product.id', 'desc')->paginate(24);
        }
        $array = array(
            'count' => count($count1),
            'total_money_sender' => $count2->SUM('money'),
            'total_money_shipper' => $count2->SUM('fee_ship'),
            'data' => $data
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
    public function orderUserProductDetail($id){
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $order = OrderUserProduct::select(
            'order_user_product.id',
            'order_user_product.code',
            'order_user_product.sender_id',
            'order_user_product.sender_name',
            'order_user_product.sender_phone',
            'order_user_product.sender_address',
            'order_user_product.latitude_sender',
            'order_user_product.longtidue_sender',
            'order_user_product.latitude_receiver',
            'order_user_product.longtidue_receiver',
            'order_user_product.receiver_id',
            'order_user_product.receiver_name',
            'order_user_product.receiver_phone',
            'order_user_product.receiver_address',
            'order_user_product.product_name',
            'order_user_product.image_product',
            'order_user_product.shipper_id',
            'order_user_product.image_receive_product',
            'order_user_product.fee_ship',
            'order_user_product.money',
            'order_user_product.total_money',
            'order_user_product.note',
            'order_user_product.status',
            'order_user_product.staff_timzi_id',
            'order_user_product.money_with_product_weight',
            'order_user_product.from_weight',
            'order_user_product.to_weight',
            'order_user_product.image_send_product',
            'order_user_product.created_at',
            'order_user_product.updated_at'
        )->leftJoin('users as us', 'us.id', 'order_user_product.shipper_id')
        ->leftJoin('users as uc', 'uc.id', 'order_user_product.sender_id');
        if($admin->role_id == 11 || $admin->role_id == 13 || $admin->role_id == 14){
            if(!$admin->manage_district_id){
                $ls_id = User::where('province_id', $admin->manage_province_id)->pluck('id')->toArray();
            }else{
                $ls_id = User::where([['province_id', $admin->manage_province_id], ['district_id', $admin->manage_district_id]])->pluck('id')->toArray();
            }
            $data = $order->where('order_user_product.id', $id)->whereIn('order_user_product.sender_id', $ls_id)->with('sender', 'shipper', 'staffTimzi')->first();
        }else{
            $data = $order->where('order_user_product.id', $id)->with('sender', 'shipper', 'staffTimzi')->first();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function cancelOrderUserProduct($id){
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        // $order = OrderUserProduct::leftJoin('stores as s', 's.id', 'order_food.store_id')
        // ->leftJoin('users as us', 'us.id', 'order_food.shipper_id')
        // ->leftJoin('users as uc', 'uc.id', 'order_food.user_id');
        if($admin->role_id == 11 || $admin->role_id == 13 || $admin->role_id == 14){
            if(!$admin->manage_district_id){
                $ls_id = User::where('province_id', $admin->manage_province_id)->pluck('id')->toArray();
            }else{
                $ls_id = User::where([['province_id', $admin->manage_province_id], ['district_id', $admin->manage_district_id]])->pluck('id')->toArray();
            }
            $data = OrderUserProduct::where([['id', $id], ['status', '!=', SystemParam::status_order_finish]])->whereIn('sender_id', $ls_id)->first();
            if(!$data){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::order_not_found, null);
            }
            OrderUserProduct::where('id', $id)->update([
                'status' => SystemParam::status_order_user_product_timzi_cancel,
                'staff_timzi_id' => $admin->id
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $data);
        }else{
            $data = OrderUserProduct::where([['id', $id], ['status', '!=', SystemParam::status_order_finish]])->first();
            if(!$data){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::order_not_found, null);
            }
            OrderUserProduct::where('id', $id)->update([
                'status' => SystemParam::status_order_user_product_timzi_cancel,
                'staff_timzi_id' => $admin->id
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $data);
        }
    }
}
