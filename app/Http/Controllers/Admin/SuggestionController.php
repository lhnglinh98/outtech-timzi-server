<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Suggestion;
use App\Http\Utils\SystemParam;
use Illuminate\Support\Facades\Validator;

class SuggestionController extends Controller
{
    //
    public function listSuggestion(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 12) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [];
        if($request->category_id){
            $where[] = ['category_id', $request->category_id];
        }
        $data = Suggestion::where($where)->orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function SuggestionDetail($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 12) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Suggestion::findorfail($id);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createSuggestion(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 12) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_id' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập từ gợi ý',
            'category_id.required' => 'Vui lòng chọn danh mục',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        $create = Suggestion::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function updateSuggestion($id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 12) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_id' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập từ gợi ý',
            'category_id.required' => 'Vui lòng chọn danh mục',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = Suggestion::findorfail($id);
        $req = $request->all();
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteSuggestion($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 12) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Suggestion::findorfail($id);
        $check->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check);
    }
}
