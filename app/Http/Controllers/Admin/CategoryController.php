<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Utils\SystemParam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class CategoryController extends Controller
{
    //
    public function listCategory()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 12 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Category::where('status', 1)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function categoryDetail($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 12) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Category::findorfail($id);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createCategory(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 12) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required',
            'banner' => 'required',
            'background' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên danh mục',
            'image.required' => 'Vui lòng chọn ảnh danh mục',
            'banner.required' => 'Vui lòng chọn ảnh quảng cáo',
            'background.required' => 'Vui lòng chọn ảnh nền',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        $req['image'] = $this->uploadImage($request->file('image'));
        $req['banner'] = $this->uploadImage($request->file('banner'));
        $req['background'] = $this->uploadImage($request->file('background'));
        $create = Category::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function updateCategory($id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 12) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        // $validator = Validator::make($request->all(), [
        //     'name' => 'required',
        // ], [
        //     'name.required' => 'Vui lòng nhập tên danh mục',
        // ]);
        // if ($validator->fails()) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        // }
        $check = Category::findorfail($id);
        $req = $request->except('name');
        $check_image = DB::table('category')->where('id', $id)->first();
        if ($request->file('image')) {
            if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
                unlink(ltrim($check_image->image, '/'));
            }
            $req['image'] = $this->uploadImage($request->file('image'));
        }
        if ($request->file('banner')) {
            if (file_exists(ltrim($check_image->banner, '/')) && $check_image->banner != "") {
                unlink(ltrim($check_image->banner, '/'));
            }
            $req['banner'] = $this->uploadImage($request->file('banner'));
        }
        if ($request->file('background')) {
            if (file_exists(ltrim($check_image->background, '/')) && $check_image->background != "") {
                unlink(ltrim($check_image->background, '/'));
            }
            $req['background'] = $this->uploadImage($request->file('background'));
        }
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteCategory($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 12) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check_image = DB::table('category')->where('id', $id)->first();
        if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
            unlink(ltrim($check_image->image, '/'));
        }
        if (file_exists(ltrim($check_image->banner, '/')) && $check_image->banner != "") {
            unlink(ltrim($check_image->banner, '/'));
        }
        if (file_exists(ltrim($check_image->background, '/')) && $check_image->background != "") {
            unlink(ltrim($check_image->background, '/'));
        }
        $check = Category::findorfail($id);
        $check->update([
            'status' => 0
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check);
    }
}
