<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransferAccountController extends Controller
{
    //
    public function listTransferAccount(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [['status', '!=', 3]];
        if ($request->name) {
            $where[] = ['name', 'like', '%' . $request->name . '%'];
        }
        if ($request->phone) {
            $where[] = ['phone', 'like', '%' . $request->phone . '%'];
        }
        if ($request->status != null) {
            $where[] = ['status', $request->status];
        }
        if ($request->manage_province_id != null) {
            $where[] = ['manage_province_id', $request->manage_province_id];
        }
        $data = User::where($where)->where('role_id', 13)->orderby('id', 'desc')->with('manageProvince')->paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function deleteTransferAccount($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $update = User::where('id', $id)->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $update);
    }
    public function transferAccountDetail($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $user = User::findorfail($id)->load('role');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $user);
    }
    public function lockTransferAccount($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $update = User::findorfail($id)->update(['status' => 2]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::lock_account_success, $update);
    }
    public function unlockTransferAccount($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $update = User::findorfail($id)->update(['status' => 1]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::unlock_account_success, $update);
    }
    public function createTransferAccount(Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'manage_province_id' => 'required',
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'email' => 'sometimes|email',
            'password' => 'required|min:6|max:100|confirmed',
        ], [
            'name.required' => 'Vui lòng nhập tên chủ chuỗi',
            'name.string' => 'Tên chủ chuỗi phải là kiểu chuỗi',
            'name.max' => 'Tên chủ chuỗi không quá :max kí tự',
            'manage_province_id.required' => 'Vui lòng chọn khu vực chuyển nhượng',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.min' => "Mật khẩu không dưới :min kí tự!",
            'password.max' => "Mật khẩu không quá :max kí tự!",
            'password.confirmed' => "Xác nhận mật khẩu không chính xác!",
            'email.email' => 'Vui lòng nhập email đúng định dạng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($request->phone && User::where([['phone', $request->phone], ['status', '!=', 3]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_phone_already_exist, null);
        }
        $check_user = User::where([['role_id', 13], ['manage_province_id', $request->manage_province_id]])->first();
        if($check_user){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, "Khu vực này đã được chuyển nhượng.", null);
        }
        $req = $request->all();
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (User::where('code', $req['code'])->exists());
        $req['status'] = 1;
        $req['role_id'] = 13;
        $user = User::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $user);
    }
    public function updateTransferAccount($id, Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'sometimes|string|max:255',
            'email' => 'sometimes|email',
            'manage_province_id' => 'required',
        ], [
            'name.string' => 'Tên người dùng phải là kiểu chuỗi',
            'name.max' => 'Tên người dùng không quá :max kí tự',
            'email.email' => 'Vui lòng nhập email đúng định dạng',
            'manage_province_id.required' => 'Vui lòng chọn khu vực chuyển nhượng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check_user = User::where([['role_id', 13], ['id', '!=', $id], ['manage_province_id', $request->manage_province_id]])->first();
        if($check_user){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, "Khu vực này đã được chuyển nhượng.", null);
        }
        $user = User::findorfail($id);
        $req = $request->all();
        if ($request->email && User::where([['id', '<>', $user->id], ['email', $request->email]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_email_already_exist, null);
        }
        if ($request->phone && User::where([['id', '<>', $user->id], ['phone', $request->phone], ['status', '!=', 3]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_phone_already_exist, null);
        }
        $user->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $user);
    }
}
