<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\User;
use App\Http\SpeedSMSAPI;
use Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    //
    public function loginAdmin(Request $request)
    {
        $input = $request->only('phone', 'password', 'device_id');
        $data = [];
        if ($request->phone) {
            $role_array = [1, 7, 8, 9, 10, 11, 12, 13, 14];
            $user = User::where([['phone', $input['phone']], ['status', '!=', 3]])->whereIn('role_id', $role_array)->first();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_user_not_found, null);
            }
            if ($user->status == 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_error_status, null);
            }
            if ($user->status == 2) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_locked, null);
            }
            if ($user->status == 3) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_account_delete, null);
            }
            $data = array(
                'email' => $user->email,
                'password' => $input['password'],
            );
        }
        $token = null;
        if (!$token = JWTAuth::attempt($data, ['exp' => Carbon\Carbon::now()->addDays(7)->timestamp])) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::message_login_error, null);
        }

        if ($request->device_id) {
            $update = User::where([['phone', $input['phone']], ['role_id', 1]])->update([
                'device_id' => $request->device_id,
            ]);
        }
        // $data1 = array(
        //     'token' => $token,
        //     'user' => $user,
        // );
        do {
            $otp = rand(1000, 9999);
        } while (User::where('otp', $otp)->exists());
        if($request->phone == "0329714645"){
            User::where('phone', $input['phone'])->update([
                'otp' => $otp,
                'date_check_code' => date('Y-m-d H:i:s', time()),
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::send_otp_create_user_investment_holding_success, $otp);
        }
        $sms_api = new SpeedSMSAPI(config('constants.sms_api'));
        $content = $otp . " la ma xac nhan cua ban tai TIMZI (Ma xac nhan chi ton tai 20 phut)";
        $response = $sms_api->sendSMS([$request->phone], $content, SpeedSMSAPI::SMS_TYPE_BRANDNAME, "TIMZI");
        $f =([
            'otp'=>$otp,
            'token'=>$token
        ]);
        if ($response['status'] == 'success') {
            User::where('phone', $input['phone'])->update([
                'otp' => $otp,
                'date_check_code' => date('Y-m-d H:i:s', time()),
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::send_otp_create_user_investment_holding_success, $f);
        }

        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::send_otp_create_user_investment_holding_error, $f);
        // return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::login_success, $data1);
    }
    // public function element_to_obj($element) {
    //     $obj = array( "result" => $element->result );
    //     foreach ($element->attributes as $attribute) {
    //         $obj[$attribute->name] = $attribute->value;
    //     }
    //     foreach ($element->childNodes as $subElement) {
    //         if ($subElement->nodeType == XML_TEXT_NODE) {
    //             $obj["html"] = $subElement->wholeText;
    //         }
    //         else {
    //             $obj["children"][] = element_to_obj($subElement);
    //         }
    //     }
    //     return $obj;
    // }
    public function checkOTPVerify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
        ], [
            'otp.required' => 'Vui lòng nhập mã xác nhận',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = User::where([['otp', $request->otp]])->orderby('id', 'desc')->first();
        if ($check && strtotime($check->date_check_code) + 20 * 60 >= time()) {
            $check->update([
                'otp' => null,
                'date_check_code' => null,
                'status' => 1,
            ]);
            $token = JWTAuth::fromUser($check, ['exp' => Carbon\Carbon::now()->addDays(7)->timestamp]);
            $data1 = array(
                'token' => $token,
                'user' => $check->load('role'),
            );
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::login_success, $data1);
        }
        return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::check_code_verify_error, null);
    }
    public function getUserInfo()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $user->load('role'));
    }
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'current_password',
            'password' => 'required|min:6|max:50|confirmed',
        ], [
            'current_password.current_password' => "Mật khẩu cũ không chính xác!",
            'password.required' => "Vui lòng nhập mật khẩu mới!",
            'password.min' => "Mật khẩu mới không dưới :min kí tự!",
            'password.max' => "Mật khẩu mới không quá :max kí tự!",
            'password.confirmed' => "Xác nhận mật khẩu mới không chính xác!",
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $input = $request->only('password');
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($admin->role_id != 1) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        // }
        $admin->update($input);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_password_success, $admin);
    }
    public function updateUser(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($user->role_id != 1) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        // }
        $validator = Validator::make($request->all(), [
            'name' => 'sometimes|string|max:255',
            'email' => 'sometimes|email',
        ], [
            'name.string' => 'Tên người dùng phải là kiểu chuỗi',
            'name.max' => 'Tên người dùng không quá :max kí tự',
            'email.email' => 'Vui lòng nhập email đúng định dạng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->except(['role_id', 'phone']);
        if ($request->email && User::where([['id', '<>', $user->id], ['email', $request->email]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_email_already_exist, null);
        }
        if ($request->birthday) {
            $req['birthday'] = date('Y-m-d H:i:s', strtotime($request->birthday));
        }
        if ($request->file('avatar')) {
            $check_image = DB::table('users')->where('id', $user->id)->first();
            if (file_exists(ltrim($check_image->avatar, '/')) && $check_image->avatar != "") {
                unlink(ltrim($check_image->avatar, '/'));
            }
            $req['avatar'] = $this->uploadImage($request->file('avatar'));
        }
        $user->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $user);
    }
}
