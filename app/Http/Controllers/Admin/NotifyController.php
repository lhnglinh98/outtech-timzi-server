<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Client\NotifyController as NotificationController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Notify;
use App\Models\TransferNotify;
use App\Http\Utils\SystemParam;
use App\Models\Store;
use Illuminate\Support\Facades\Validator;
class NotifyController extends Controller
{
    //
    protected $notifycontroller;
    public function __construct()
    {
        $this->notifycontroller = new NotificationController();
    }
    public function deleteNotify(Request $request){
        $delete = Notify::where([['object_id', $request->object_id], ['type', $request->type]])->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $delete);
    }
    public function pushAllUser(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'content' => 'required|string|max:255',
            'type' => 'required'
        ], [
            'content.required' => 'Vui lòng nhập nội dung',
            'content.string' => 'Nội dung phải là kiểu chuỗi',
            'content.max' => 'Nội dung không quá :max kí tự',
            'type.required' => 'Vui lòng chọn loại tài khoản',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if($request->type == 1){
            $data = User::where([['role_id', 5], ['status', 1]])->pluck('id')->toArray();
        }else if($request->type == 2){
            $data = User::where([['role_id', 4], ['status', 1]])->pluck('id')->toArray();
        }else{
            $data = User::where('status', 1)->whereIn('role_id', [2, 3, 6])->pluck('id')->toArray();
        }
        $title = "Timzi";
        if($request->store_id){
            $store = Store::find($request->store_id);
            if($store){
                $title = $store->name;
                $this->notifycontroller->pushNotifyAllUser($data, $request->content, $request->store_id, SystemParam::type_all_user_with_new_store, null, $request->type, $title);
            }
        }else{
            $this->notifycontroller->pushNotifyAllUser($data, $request->content, null, SystemParam::type_all_user, null, $request->type, $title);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::push_notify_success, $data);
    }
    public function pushAllUserWithTransfer(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'content' => 'required|string|max:255',
            'type' => 'required'
        ], [
            'content.required' => 'Vui lòng nhập nội dung',
            'content.string' => 'Nội dung phải là kiểu chuỗi',
            'content.max' => 'Nội dung không quá :max kí tự',
            'type.required' => 'Vui lòng chọn loại tài khoản',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        $req['transfer_id'] = $user->id;
        $req['status'] = 0;
        $create = TransferNotify::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::push_notify_transfer_success, $create);
    }
    public function listNotifyWithTransfer(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = TransferNotify::where([['transfer_id', $user->id]])->with('transfer.manageProvince', 'transfer.manageDistrict', 'store')->orderby('id', 'desc')->paginate(24);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listNotifyTransferWithAdmin(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = TransferNotify::with('transfer.manageProvince', 'transfer.manageDistrict', 'store')->orderby('id', 'desc')->paginate(24);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function confirmNotifyTransfer($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = TransferNotify::find($id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_notify_not_found, null);
        }
        $check->update([
            'status' => 1
        ]);
        if($check->type == 1){
            $data = User::where([['role_id', 5], ['status', 1]])->pluck('id')->toArray();
        }else if($check->type == 2){
            $data = User::where([['role_id', 4], ['status', 1]])->pluck('id')->toArray();
        }else{
            $data = User::where('status', 1)->whereIn('role_id', [2, 3, 6])->pluck('id')->toArray();
        }
        $title = "Timzi";
        if($check->store_id){
            $store = Store::find($check->store_id);
            if($store){
                $title = $store->name;
                $this->notifycontroller->pushNotifyAllUser($data, $check->content, $check->store_id, SystemParam::type_all_user_with_new_store, null, $check->type, $title);
            }
        }else{
            $this->notifycontroller->pushNotifyAllUser($data, $check->content, null, SystemParam::type_all_user, null, $check->type, $title);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::confirm_success, $check);
    }
    public function cancelNotifyTransfer($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = TransferNotify::find($id);
        if(!$check){
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_notify_not_found, null);
        }
        $check->update([
            'status' => 2
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::cancel_success, $check);
    }
}
