<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\VoucherDelivery;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VoucherDeliveryController extends Controller
{
    public function listVoucher(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($request->type) {
            $data = VoucherDelivery::where([['type', $request->type]])->orderby('id', 'desc')->get();
        } else {
            $data = VoucherDelivery::orderby('id', 'desc')->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function voucherDetail($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = VoucherDelivery::findorfail($id);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createVoucher(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'code' => 'required',
            'description' => 'required',
            'type' => 'required',
        ], [
            'title.required' => 'Vui lòng nhập tiêu đề voucher',
            'code.required' => 'Vui lòng nhập mã voucher',
            'description.required' => 'Vui lòng nhập mô tả voucher',
            'type.required' => 'Vui lòng chọn loại voucher',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        if ($request->percent) {
            $req['money'] = 0;
            $req['type_percent_or_money'] = 1;
        } else if ($request->money) {
            $req['percent'] = 0;
            $req['type_percent_or_money'] = 2;
        }
        if ($request->type == 1) {
            $validator = Validator::make($request->all(), [
                'proviso' => 'required',
                'quantity' => 'required',
                'time_open' => 'required|date',
                'time_close' => 'required|date|after_or_equal:time_open',
            ], [
                'proviso.required' => 'Vui lòng nhập điều kiện sử dụng voucher',
                'quantity.required' => 'Vui lòng nhập số lượng',
                'time_open.required' => 'Vui lòng nhập thời gian bắt đầu',
                'time_open.date' => 'Vui lòng nhập đúng định dạng',
                'time_open.after_or_equal' => 'Vui lòng nhập thời gian bắt đầu sau ngày hôm nay',
                'time_close.date' => 'Vui lòng nhập đúng định dạng',
                'time_close.required' => 'Vui lòng nhập thời gian kết thúc',
                'time_close.after_or_equal' => 'Thời gian kết thúc không thể trước thời gian bắt đầu',
            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }
            if (date('Y-m-d', strtotime($request->time_open)) < date('Y-m-d')) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập thời gian bắt đầu sau ngày hôm nay.", null);
            }
            $req['time_open'] = date('Y-m-d', strtotime($request->time_open));
            $req['time_close'] = date('Y-m-d', strtotime($request->time_close));
            if (date('Y-m-d', strtotime($request->time_open)) == date('Y-m-d')) {
                $req['status'] = 2;
            }
        } else {
            $validator = Validator::make($request->all(), [
                'quantity_use_with_user_new' => 'required',
            ], [
                'quantity_use_with_user_new.required' => 'Vui lòng nhập số lần người mới được sử dụng voucher.',
            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }
            if ($request->quantity_use_with_user_new <= 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập số lượng lớn hơn 0.", null);
            }
            $req['status'] = 2;
        }
        $Voucher = VoucherDelivery::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $Voucher);
    }
    public function updateVoucher($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = VoucherDelivery::findorfail($id);
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'code' => 'required',
            'description' => 'required',
            'type' => 'required',
        ], [
            'title.required' => 'Vui lòng nhập tiêu đề voucher',
            'code.required' => 'Vui lòng nhập mã voucher',
            'description.required' => 'Vui lòng nhập mô tả voucher',
            'type.required' => 'Vui lòng chọn loại voucher',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        if ($request->percent) {
            $req['money'] = 0;
            $req['type_percent_or_money'] = 1;
        } else if ($request->money) {
            $req['percent'] = 0;
            $req['type_percent_or_money'] = 2;
        }
        if ($request->type == 1) {
            $validator = Validator::make($request->all(), [
                'proviso' => 'required',
                'quantity' => 'required',
                'time_open' => 'required|date',
                'time_close' => 'required|date|after_or_equal:time_open',
            ], [
                'proviso.required' => 'Vui lòng nhập điều kiện sử dụng voucher',
                'quantity.required' => 'Vui lòng nhập số lượng',
                'time_open.required' => 'Vui lòng nhập thời gian bắt đầu',
                'time_open.date' => 'Vui lòng nhập đúng định dạng',
                'time_open.after_or_equal' => 'Vui lòng nhập thời gian bắt đầu sau ngày hôm nay',
                'time_close.date' => 'Vui lòng nhập đúng định dạng',
                'time_close.required' => 'Vui lòng nhập thời gian kết thúc',
                'time_close.after_or_equal' => 'Thời gian kết thúc không thể trước thời gian bắt đầu',
            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }
            if (date('Y-m-d', strtotime($request->time_open)) < date('Y-m-d')) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập thời gian bắt đầu sau ngày hôm nay.", null);
            }
            $req['time_open'] = date('Y-m-d', strtotime($request->time_open));
            $req['time_close'] = date('Y-m-d', strtotime($request->time_close));
            if (date('Y-m-d', strtotime($request->time_open)) == date('Y-m-d')) {
                $req['status'] = 2;
            }
        } else {
            $validator = Validator::make($request->all(), [
                'quantity_use_with_user_new' => 'required',
            ], [
                'quantity_use_with_user_new.required' => 'Vui lòng nhập số lần người mới được sử dụng voucher.',
            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }
            if ($request->quantity_use_with_user_new <= 0) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập số lượng lớn hơn 0.", null);
            }
            $req['status'] = 2;
        }
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteVoucher($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = VoucherDelivery::findorfail($id);
        $check->update([
            'status' => 3,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check);
    }
}
