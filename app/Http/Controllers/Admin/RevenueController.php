<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\HistoryMoneyShipper;
use App\Models\RevenueStore;
use App\Models\Store;
use App\User;
use Illuminate\Http\Request;

class RevenueController extends Controller
{
    //
    public function listRevenueStore(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 10 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [];
        if ($request->month) {
            $month = date('Y-m', strtotime($request->month));
            $where[] = ['revenue_store.month', 'LIKE', '%' . $month . '%'];
        }
        if ($request->store_name) {
            $where[] = ['s.name', 'LIKE', '%' . $request->store_name . '%'];
        }
        if ($user->role_id == 11 || $user->role_id == 13 || $user->role_id == 14) {
            if(!$user->manage_district_id){
                $ls_store_id = Store::where('province_id', $user->manage_province_id)->pluck('id')->toArray();
            }else{
                $ls_store_id = Store::where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->pluck('id')->toArray();
            }
            $data1 = RevenueStore::select('revenue_store.*')->join('stores as s', 's.id', 'revenue_store.store_id')->where($where)->whereIn('revenue_store.store_id', $ls_store_id)->orderby('revenue_store.id', 'desc')->with('store')->paginate(12);
            $check_money = RevenueStore::select('revenue_store.*')->join('stores as s', 's.id', 'revenue_store.store_id')->where($where)->whereIn('revenue_store.store_id', $ls_store_id)->where('revenue_store.status', 1)->get();
        } else {
            $data1 = RevenueStore::select('revenue_store.*')->join('stores as s', 's.id', 'revenue_store.store_id')->where($where)->orderby('revenue_store.id', 'desc')->with('store')->paginate(12);
            $check_money = RevenueStore::select('revenue_store.*')->join('stores as s', 's.id', 'revenue_store.store_id')->where($where)->where('revenue_store.status', 1)->get();
        }
        $total_money = $check_money->SUM('fee_money');
        $data = array(
            'list_revenue' => $data1,
            'total_money' => $total_money,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listRevenueShipper(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 10 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [['history_money_shipper.type', 2]];
        if ($request->shipper_name) {
            $where[] = ['s.name', 'LIKE', '%' . $request->shipper_name . '%'];
        }
        if ($request->from_date) {
            $from_date = date('Y-m-d', strtotime($request->from_date));
            $where[] = ['history_money_shipper.created_at', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['history_money_shipper.created_at', '<=', $to_date];
        }
        if ($user->role_id == 11 || $user->role_id == 13 || $user->role_id == 14) {
            if(!$user->manage_district_id){
                $ls_shipper_id = User::where('role_id', 4)->where('province_id', $user->manage_province_id)->pluck('id')->toArray();
            }else{
                $ls_shipper_id = User::where('role_id', 4)->where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->pluck('id')->toArray();
            }
            $data1 = HistoryMoneyShipper::select('history_money_shipper.*')->join('users as s', 's.id', 'history_money_shipper.shipper_id')->where($where)->whereIn('history_money_shipper.shipper_id', $ls_shipper_id)->orderby('history_money_shipper.id', 'desc')->with('shipper')->paginate(12);
            $check_money = HistoryMoneyShipper::select('history_money_shipper.*')->join('users as s', 's.id', 'history_money_shipper.shipper_id')->where($where)->whereIn('history_money_shipper.shipper_id', $ls_shipper_id)->get();
        } else {
            $data1 = HistoryMoneyShipper::select('history_money_shipper.*')->join('users as s', 's.id', 'history_money_shipper.shipper_id')->where($where)->orderby('history_money_shipper.id', 'desc')->with('shipper')->paginate(12);
            $check_money = HistoryMoneyShipper::select('history_money_shipper.*')->join('users as s', 's.id', 'history_money_shipper.shipper_id')->where($where)->get();
        }
        $total_money = $check_money->SUM('money');
        $data = array(
            'list_revenue' => $data1,
            'total_money' => $total_money,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function revenueStoreDetail($store_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 10 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [['store_id', $store_id]];
        if ($request->month) {
            $month = date('Y-m', strtotime($request->month));
            $where[] = ['month', 'LIKE', '%' . $month . '%'];
        }
        // if ($user->role_id == 11) {
        //     if(!$user->manage_district_id){
        //         $ls_store_id = Store::where('province_id', $user->manage_province_id)->pluck('id')->toArray();
        //     }else{
        //         $ls_store_id = Store::where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->pluck('id')->toArray();
        //     }
        //     $data1 = RevenueStore::where($where)->whereIn('store_id', $ls_store_id)->orderby('id', 'desc')->with('store')->paginate(12);
        //     $check_money = RevenueStore::where($where)->where('status', 1)->whereIn('store_id', $ls_store_id)->get();
        // } else {
            $data1 = RevenueStore::where($where)->orderby('id', 'desc')->with('store')->paginate(12);
            $check_money = RevenueStore::where($where)->where('status', 1)->get();
        // }
        $total_money = $check_money->SUM('fee_money');
        $data = array(
            'list_revenue' => $data1,
            'total_money' => $total_money,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function revenueShipperDetail($shipper_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 10 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        // if ($user->role_id == 11) {
        //     if(!$user->manage_district_id){
        //         $ls_shipper_id = User::where('role_id', 4)->where('province_id', $user->manage_province_id)->pluck('id')->toArray();
        //     }else{
        //         $ls_shipper_id = User::where('role_id', 4)->where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->pluck('id')->toArray();
        //     }
        //     $data1 = HistoryMoneyShipper::where([['shipper_id', $shipper_id], ['type', 2]])->whereIn('shipper_id', $ls_shipper_id)->orderby('id', 'desc')->with('shipper')->paginate(12);
        //     $check_money = HistoryMoneyShipper::where('shipper_id', $shipper_id)->whereIn('shipper_id', $ls_shipper_id)->get();
        // } else {
            $data1 = HistoryMoneyShipper::where([['shipper_id', $shipper_id], ['type', 2]])->orderby('id', 'desc')->with('shipper')->paginate(12);
            $check_money = HistoryMoneyShipper::where('shipper_id', $shipper_id)->get();
        // }
        // $data1 = HistoryMoneyShipper::where([['shipper_id', $shipper_id], ['type', 2]])->orderby('id', 'desc')->with('shipper')->paginate(12);
        // $check_money = HistoryMoneyShipper::where('shipper_id', $shipper_id)->get();
        $total_money = $check_money->SUM('money');
        $data = array(
            'list_revenue' => $data1,
            'total_money' => $total_money,
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
}
