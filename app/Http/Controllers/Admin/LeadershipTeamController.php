<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Utils\SystemParam;
use App\Models\LeadershipTeam;
use Illuminate\Support\Facades\Validator;

class LeadershipTeamController extends Controller
{
    //
    public function listLeadershipTeam()
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $data = LeadershipTeam::orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function leadershipTeamDetail($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $data = LeadershipTeam::findorfail($id);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createLeadershipTeam(Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'image' => 'required',
            'position' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên',
            'name.string' => 'Tên phải là kiểu chuỗi',
            'name.max' => 'Tên không quá :max kí tự',
            'image.required' => 'Vui lòng chọn ảnh',
            'position.required' => 'Vui lòng nhập chức vụ',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        $req['image'] = $this->uploadImage($request->file('image'));
        $create = LeadershipTeam::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function updateLeadershipTeam($id, Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'position' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên',
            'name.string' => 'Tên phải là kiểu chuỗi',
            'name.max' => 'Tên không quá :max kí tự',
            'position.required' => 'Vui lòng nhập chức vụ',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = LeadershipTeam::findorfail($id);
        $req = $request->all();
        if ($request->file('image')) {
            $req['image'] = $this->uploadImage($request->file('image'));
        }
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteLeadershipTeam($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $check = LeadershipTeam::findorfail($id);
        $delete = $check->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $delete);
    }
}
