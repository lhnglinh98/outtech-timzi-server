<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\TutorialDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class TutorialDocumentController extends Controller
{
    //
    public function listTutorialDocument()
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $data = TutorialDocument::orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function tutorialDocumentDetail($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $data = TutorialDocument::findorfail($id);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createTutorialDocument(Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'image' => 'required',
            'file' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên',
            'name.string' => 'Tên phải là kiểu chuỗi',
            'name.max' => 'Tên không quá :max kí tự',
            'image.required' => 'Vui lòng chọn ảnh',
            'file.required' => 'Vui lòng chọn file hướng dẫn',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        $req['image'] = $this->uploadImage($request->file('image'));
        $req['file'] = $this->uploadFile($request->file('file'));
        $create = TutorialDocument::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function updateTutorialDocument($id, Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ], [
            'name.required' => 'Vui lòng nhập tên',
            'name.string' => 'Tên phải là kiểu chuỗi',
            'name.max' => 'Tên không quá :max kí tự',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = TutorialDocument::findorfail($id);
        $req = $request->all();
        if ($request->file('image')) {
            $check_image = DB::table('tutorial_document')->where('id', $id)->first();
            if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
                unlink(ltrim($check_image->image, '/'));
            }
            $req['image'] = $this->uploadImage($request->file('image'));
        }
        if ($request->file('file')) {
            $check_image = DB::table('tutorial_document')->where('id', $id)->first();
            if (file_exists(ltrim($check_image->file, '/')) && $check_image->file != "") {
                unlink(ltrim($check_image->file, '/'));
            }
            $req['file'] = $this->uploadFile($request->file('file'));
        }
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteTutorialDocument($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $check_image = DB::table('tutorial_document')->where('id', $id)->first();
        if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
            unlink(ltrim($check_image->image, '/'));
        }
        if (file_exists(ltrim($check_image->file, '/')) && $check_image->file != "") {
            unlink(ltrim($check_image->file, '/'));
        }
        $check = TutorialDocument::findorfail($id);
        $delete = $check->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $delete);
    }
}
