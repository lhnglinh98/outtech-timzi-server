<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{
    //
    public function listRoleStaff(){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $role_array = [7, 8, 9, 10, 11, 12];
        $data = Role::whereIn('id', $role_array)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStaff(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [['status', '!=', 3]];
        if ($request->name) {
            $where[] = ['name', 'like', '%' . $request->name . '%'];
        }
        if ($request->phone) {
            $where[] = ['phone', 'like', '%' . $request->phone . '%'];
        }
        if ($request->status != null) {
            $where[] = ['status', $request->status];
        }
        $user = [];
        $role_array = [7, 8, 9, 10, 11, 12];
        if($request->role_id && in_array($request->role_id, $role_array)){
            $user = User::where($where)->where('role_id', $request->role_id)->orderby('id', 'desc')->with('store', 'staffStore')->paginate(12);
        }else{
            $user = User::where($where)->whereIn('role_id', $role_array)->orderby('id', 'desc')->with('store', 'staffStore')->paginate(12);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $user);
    }
    public function deleteStaff($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $update = User::where('id', $id)->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $update);
    }
    public function staffDetail($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $user = User::findorfail($id)->load('role');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $user);
    }
    public function lockStaff($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $update = User::findorfail($id)->update(['status' => 2]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::lock_account_success, $update);
    }
    public function unlockStaff($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $update = User::findorfail($id)->update(['status' => 1]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::unlock_account_success, $update);
    }
    public function createStaff(Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'role_id' => 'required',
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            'email' => 'sometimes|email',
            'password' => 'required|min:6|max:100|confirmed',
        ], [
            'name.required' => 'Vui lòng nhập tên chủ chuỗi',
            'name.string' => 'Tên chủ chuỗi phải là kiểu chuỗi',
            'name.max' => 'Tên chủ chuỗi không quá :max kí tự',
            'role_id.required' => 'Vui lòng chọn quyền tài khoản',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.min' => "Mật khẩu không dưới :min kí tự!",
            'password.max' => "Mật khẩu không quá :max kí tự!",
            'password.confirmed' => "Xác nhận mật khẩu không chính xác!",
            'email.email' => 'Vui lòng nhập email đúng định dạng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($request->phone && User::where([['phone', $request->phone], ['status', '!=', 3]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_phone_already_exist, null);
        }
        if($request->role_id == 11){
            if(!$request->manage_province_id && !$request->manage_district_id){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_manage_province, null);
            }
        }
        $req = $request->all();
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (User::where('code', $req['code'])->exists());
        $req['status'] = 1;
        $user = User::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $user);
    }
    public function updateStaff($id, Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'sometimes|string|max:255',
            'email' => 'sometimes|email',
            'role_id' => 'required',
        ], [
            'name.string' => 'Tên người dùng phải là kiểu chuỗi',
            'name.max' => 'Tên người dùng không quá :max kí tự',
            'email.email' => 'Vui lòng nhập email đúng định dạng',
            'role_id.required' => 'Vui lòng chọn quyền tài khoản',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if($request->role_id == 11){
            if(!$request->manage_province_id && !$request->manage_district_id){
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, SystemParam::error_manage_province, null);
            }
        }
        $user = User::findorfail($id);
        $req = $request->all();
        if ($request->email && User::where([['id', '<>', $user->id], ['email', $request->email]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_email_already_exist, null);
        }
        if ($request->phone && User::where([['id', '<>', $user->id], ['phone', $request->phone], ['status', '!=', 3]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_phone_already_exist, null);
        }
        $user->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $user);
    }
}
