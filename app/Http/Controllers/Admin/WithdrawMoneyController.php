<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Utils\SystemParam;
use App\User;
use App\Models\WithdrawMoney;
use App\Models\HistoryMoneyShipper;
use App\Models\HistoryRechargeAccount;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Client\NotifyController as NotificationController;

class WithdrawMoneyController extends Controller
{
    protected $notifycontroller;
    public function __construct()
    {
        $this->notifycontroller = new NotificationController();
    }
    public function listWithdrawMoney(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 8 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['withdraw_money.created_at', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['withdraw_money.created_at', '<=', $to_date];
        }
        if ($request->shipper_name) {
            $where[] = ['u.name', 'LIKE', '%' . $request->shipper_name . '%'];
        }
        if ($request->status) {
            $where[] = ['withdraw_money.status', $request->status];
        }
        if ($user->role_id == 11 || $user->role_id == 13 || $user->role_id == 14) {
            if (!$user->manage_district_id) {
                $data = WithdrawMoney::select('withdraw_money.*')->join('users as u', 'u.id', 'withdraw_money.shipper_id')
                    ->where($where)->where('u.province_id', $user->manage_province_id)->with('shipper')
                    ->orderby('withdraw_money.id', 'desc')->paginate(24);
            } else {
                $data = WithdrawMoney::select('withdraw_money.*')->join('users as u', 'u.id', 'withdraw_money.shipper_id')
                    ->where($where)->where([['u.province_id', $user->manage_province_id], ['u.district_id', $user->manage_district_id]])->with('shipper')
                    ->orderby('withdraw_money.id', 'desc')->paginate(24);
            }
        } else {
            $data = WithdrawMoney::select('withdraw_money.*')->join('users as u', 'u.id', 'withdraw_money.shipper_id')
                ->where($where)->with('shipper')->orderby('withdraw_money.id', 'desc')->paginate(24);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function confirmWithdrawMoney($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 8 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = WithdrawMoney::findorfail($id);
        $check->update(['status' => 1]);
        HistoryMoneyShipper::insert([
            'shipper_id' => $check->shipper_id,
            'money' => $check->money,
            'type' => 2,
            'type_order' => 3,
            'content' => "Rút tiền thành công. Timzi đã trừ " . number_format($check->money) . " VNĐ trong ví liên kết của bạn!",
            'created_at' => date('Y-m-d H:i:s', time()),
        ]);
        $this->notifycontroller->pushNotify([$check->shipper_id], "Rút tiền thành công. Timzi đã trừ " . number_format($check->money) . " VNĐ trong ví liên kết của bạn!", $id, SystemParam::withdraw_money, $check, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, 'Xác nhận rút tiền thành công', $check);
    }
    public function cancelWithdrawMoney($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 8 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = WithdrawMoney::findorfail($id);
        $check->update(['status' => 2]);
        $user = User::findorfail($check->shipper_id);
        $user->update([
            'money' => $user->money + $check->money
        ]);
        $this->notifycontroller->pushNotify([$check->shipper_id], "Yêu cầu rút tiền của bạn đã bị hủy.", $id, SystemParam::withdraw_money, $check, 'Timzi');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, 'Hủy rút tiền thành công', $check);
    }
}
