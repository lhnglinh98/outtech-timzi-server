<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\UsedSolution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class UsedSolutionController extends Controller
{
    //
    public function listUsedSolution()
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $data = UsedSolution::orderby('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function usedSolutionDetail($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $data = UsedSolution::findorfail($id);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createUsedSolution(Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'image' => 'required',
        ], [
            'image.required' => 'Vui lòng chọn ảnh',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        $req['image'] = $this->uploadImage($request->file('image'));
        $create = UsedSolution::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }
    public function updateUsedSolution($id, Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $check = UsedSolution::findorfail($id);
        $req = $request->all();
        if ($request->file('image')) {
            $check_image = DB::table('used_solution')->where('id', $id)->first();
            if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
                unlink(ltrim($check_image->image, '/'));
            }
            $req['image'] = $this->uploadImage($request->file('image'));
        }
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteUsedSolution($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $check_image = DB::table('used_solution')->where('id', $id)->first();
        if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
            unlink(ltrim($check_image->image, '/'));
        }
        $check = UsedSolution::findorfail($id);
        $delete = $check->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $delete);
    }
}
