<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SetupWithdrawMoney;
use App\Http\Utils\SystemParam;
use Illuminate\Support\Facades\Validator;
class SetupWithdrawMoneyController extends Controller
{
    public function setupWithdrawMoney(){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = SetupWithdrawMoney::first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function updateSetupWithdrawMoney($id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'money' => 'required',
        ], [
            'money.required' => 'Vui lòng nhập số tiền rút tối thiểu.',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $check = SetupWithdrawMoney::findorfail($id);
        $req = $request->all();
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
}
