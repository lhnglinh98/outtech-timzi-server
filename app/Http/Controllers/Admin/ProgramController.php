<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\CategoryFood;
use App\Models\Program;
use App\Models\ProgramDetail;
use App\Models\ProgramStore;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\ProgramStoreDetail;
use Illuminate\Support\Facades\Validator;

class ProgramController extends Controller
{
    //
    public function listProgram(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if ($request->store_id) {
            $data = Program::where('store_id', $request->store_id)->orderby('id', 'desc')->with('store', 'categoryFood')->get();
        } else {
            $data = Program::orderby('id', 'desc')->with('store', 'categoryFood')->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function programDetail($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Program::findorfail($id)->load('store', 'categoryFood');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createProgram(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required',
            'category_food_id' => 'required',
            'type_percent_or_money' => 'required',
            'content' => 'required',
            'time_open' => 'required|date|after_or_equal:now',
            'time_close' => 'required|date|after_or_equal:time_open',
        ], [
            'name.required' => 'Vui lòng nhập tên chương trình',
            'image.required' => 'Vui lòng chọn ảnh chương trình',
            'type_percent_or_money.required' => 'Vui lòng chọn loại giảm giá',
            'time_open.required' => 'Vui lòng nhập thời gian bắt đầu',
            'time_open.date' => 'Vui lòng nhập đúng định dạng',
            'time_open.after_or_equal' => 'Vui lòng nhập thời gian bắt đầu sau ngày hôm nay',
            'time_close.date' => 'Vui lòng nhập đúng định dạng',
            'time_close.required' => 'Vui lòng nhập thời gian kết thúc',
            'time_close.after_or_equal' => 'Thời gian kết thúc không thể trước thời gian bắt đầu',
            'category_food_id.required' => 'Vui lòng chọn danh mục theo chương trình',
            'content.required' => 'Vui lòng nhập nội dung chương trình',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if ($request->type_percent_or_money == 1) {
            if (!$request->percent) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập phần trăm giảm giá", null);
            }
            if ($request->percent > 100) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Phần trăm giảm giá không vượt quá 100%", null);
            }
        } else {
            if (!$request->money) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập số tiền giảm giá", null);
            }
        }
        $check = CategoryFood::where([['status', 1], ['id', $request->category_food_id]])->firstorfail();
        $req = $request->all();
        $req['user_id'] = $user->id;
        $req['time_open'] = date('Y-m-d', strtotime($request->time_open));
        $req['time_close'] = date('Y-m-d', strtotime($request->time_close));
        $req['image'] = $this->uploadImage($request->file('image'));
        $program = Program::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $program);
    }
    public function updateProgram($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Program::findorfail($id);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_food_id' => 'required',
            'type_percent_or_money' => 'required',
            'content' => 'required',
            'time_open' => 'required|date|after_or_equal:now',
            'time_close' => 'required|date|after_or_equal:time_open',
        ], [
            'name.required' => 'Vui lòng nhập tên chương trình',
            'type_percent_or_money.required' => 'Vui lòng chọn loại giảm giá',
            'time_open.required' => 'Vui lòng nhập thời gian bắt đầu',
            'time_open.date' => 'Vui lòng nhập đúng định dạng',
            'time_open.after_or_equal' => 'Vui lòng nhập thời gian bắt đầu sau ngày hôm nay',
            'time_close.date' => 'Vui lòng nhập đúng định dạng',
            'time_close.required' => 'Vui lòng nhập thời gian kết thúc',
            'time_close.after_or_equal' => 'Thời gian kết thúc không thể trước thời gian bắt đầu',
            'category_food_id.required' => 'Vui lòng chọn danh mục theo chương trình',
            'content.required' => 'Vui lòng nhập nội dung chương trình',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        if ($request->type_percent_or_money == 1) {
            $req['money'] = 0;
            if (!$request->percent) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập phần trăm giảm giá", null);
            }
            if ($request->percent > 100) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Phần trăm giảm giá không vượt quá 100%", null);
            }
        } else {
            $req['percent'] = 0;
            if (!$request->money) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập số tiền giảm giá", null);
            }
        }
        $req['user_id'] = $user->id;
        $check = CategoryFood::where([['status', 1], ['id', $request->category_food_id]])->firstorfail();
        $req['time_open'] = date('Y-m-d', strtotime($request->time_open));
        $req['time_close'] = date('Y-m-d', strtotime($request->time_close));
        if ($request->file('image')) {
            $check_image = DB::table('program')->where('id', $id)->first();
            if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
                unlink(ltrim($check_image->image, '/'));
            }
            $req['image'] = $this->uploadImage($request->file('image'));
        }
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteProgram($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Program::findorfail($id);
        $date_check = date('Y-m-d', time());
        $ls_store_id_close = ProgramDetail::where('program_id', $id)->pluck('store_id')->toArray();
        $update = ProgramStore::where([['status', 3], ['time_open', '<=', $date_check], ['time_close', '>=', $date_check]])
            ->whereIn('store_id', $ls_store_id_close)
            ->update([
                'status' => 1,
            ]);
        $update = ProgramStore::where([['status', 3], ['time_close', '<', $date_check]])
            ->whereIn('store_id', $ls_store_id_close)
            ->update([
                'status' => 2,
            ]);
        $check_image = DB::table('program')->where('id', $id)->first();
        if (file_exists(ltrim($check_image->image, '/')) && $check_image->image != "") {
            unlink(ltrim($check_image->image, '/'));
        }
        $delete = ProgramDetail::where('program_id', $id)->delete();
        $check->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check);
    }
    public function listProgramStore(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [];
        if($request->store_id){
            $where[] = ['store_id', $request->store_id];
        }
        $data = ProgramStore::where($where)->with('store')->orderby('id', 'desc')->paginate(24);
        $check = ProgramStore::where($where)->get();
        $array = array(
            'list' => $data,
            'count_program' => count($check),
            'sum_quantity_bought' => $check->SUM('quantity_bought')
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
    public function programStoreDetail($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = ProgramStore::findorfail($id)->load('store', 'food');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
}
