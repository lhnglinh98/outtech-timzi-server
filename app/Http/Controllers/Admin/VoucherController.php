<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OrderExport;
use App\Exports\VoucherShipperExport;
use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\OrderFood;
use App\Models\Store;
use App\Models\Voucher;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
class VoucherController extends Controller
{
    public function exportVoucherOrder($id)
    {
        // return User::all();
        $voucher = Voucher::findorfail($id)->load('store');
        // return $voucher;
        $array = [];
        if ($voucher->type == 1) {
            $total_money = 0;
            $total_money_shop_return = 0;
            $total_money_timzi_return = 0;
            $ls_store = OrderFood::where([['voucher_id', $id], ['status', SystemParam::status_order_finish]])->pluck('store_id')->toArray();
            $ls_store = array_unique($ls_store);
            foreach ($ls_store as $s) {
                // $ls_store_id = OrderFood::where([['store_id', $s], ['status', SystemParam::status_order_finish], ['voucher_id', $id]])->pluck('store_id')->toArray();
                // $ls_store_id = array_unique($ls_store_id);
                // foreach($ls_store_id as $s){
                $check = OrderFood::where([['store_id', $s], ['status', SystemParam::status_order_finish], ['voucher_id', $id]])->with('store')->get();
                $date_check = [];
                foreach ($check as $c) {
                    $created_at = date('Y-m-d', strtotime($c->created_at));
                    if (!in_array($created_at, $date_check)) {
                        $shipper_id_order = OrderFood::where([['store_id', $s], ['status', SystemParam::status_order_finish], ['voucher_id', $id], ['created_at', 'LIKE', '%' . $created_at . '%']])->pluck('shipper_id')->toArray();
                        $check_date = OrderFood::where([['store_id', $s], ['status', SystemParam::status_order_finish], ['voucher_id', $id], ['created_at', 'LIKE', '%' . $created_at . '%']])->get();
                        $shipper_id_order = array_unique($shipper_id_order);
                        if(count($shipper_id_order) > 0){
                            foreach($shipper_id_order as $sp){
                                $check_date_new = OrderFood::where([['store_id', $s], ['shipper_id', $sp], ['status', SystemParam::status_order_finish], ['voucher_id', $id], ['created_at', 'LIKE', '%' . $created_at . '%']])->get();
                                $shipper_name = User::find($sp);
                                $array[] = array(
                                    'store_name' => $c->store->name,
                                    'shipper_name' => $shipper_name != null ? $shipper_name->name : "",
                                    'date' => $created_at,
                                    'total_order' => count($check_date),
                                    'sum_money' => $check_date_new->SUM('discount'),
                                    'sum_money_shop_return' => $voucher->store_id == null ? 0 : $check_date_new->SUM('discount') / 2,
                                    'sum_money_timzi_return' => $voucher->store_id == null ? $check_date_new->SUM('discount') : $check_date_new->SUM('discount') / 2,
                                );
                                $total_money += $check_date->SUM('discount');
                                $total_money_shop_return += $voucher->store_id == null ? 0 : $check_date_new->SUM('discount') / 2;
                                $total_money_timzi_return += $voucher->store_id == null ? $check_date_new->SUM('discount') : $check_date->SUM('discount') / 2;
                            }
                        }
                        $date_check[] = $created_at;
                    }
                }
                // }
            }
            $data = array(
                'title' => $voucher->title,
                'code' => $voucher->code,
                'time_open' => date('d-m-Y', strtotime($voucher->time_open)),
                'time_close' => date('d-m-Y', strtotime($voucher->time_close)),
                'note' => $voucher->type_percent_or_money == 1 ? "Giảm mỗi đơn hàng " . $voucher->percent . '%' : "Giảm mỗi đơn hàng " . $voucher->money . ' VNĐ',
                'store_name' => $voucher->store_id != null ? $voucher->store->name : "",
                'list_data' => $array,
                'total_money' => number_format($total_money) . ' VNĐ',
                'total_money_shop_return' => number_format($total_money_shop_return) . ' VNĐ',
                'total_money_timzi_return' => number_format($total_money_timzi_return) . ' VNĐ',
            );
            $date = date("Y-m-d H:i:s");
            $name_excel = 'Báo cáo sử dụng voucher ' . $voucher->title . ' ngày ' . $date . '.xlsx';
            return Excel::download(new OrderExport($data), $name_excel);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::export_success, $data);
        } else {
            $total_money = 0;
            $ls_shipper = OrderFood::where([['voucher_id', $id], ['status', SystemParam::status_order_finish]])->pluck('shipper_id')->toArray();
            $ls_shipper = array_unique($ls_shipper);
            foreach ($ls_shipper as $u) {
                $ls_store_id = OrderFood::where([['shipper_id', $u], ['status', SystemParam::status_order_finish], ['voucher_id', $id]])->pluck('store_id')->toArray();
                $ls_store_id = array_unique($ls_store_id);
                foreach ($ls_store_id as $s) {
                    $check = OrderFood::where([['shipper_id', $u], ['store_id', $s], ['status', SystemParam::status_order_finish], ['voucher_id', $id]])->with('shipper', 'store')->get();
                    $date_check = [];
                    foreach ($check as $c) {
                        $created_at = date('Y-m-d', strtotime($c->created_at));
                        if (!in_array($created_at, $date_check)) {
                            $check_date = OrderFood::where([['shipper_id', $u], ['store_id', $s], ['status', SystemParam::status_order_finish], ['voucher_id', $id], ['created_at', 'LIKE', '%' . $created_at . '%']])->get();
                            $array[] = array(
                                'shipper_name' => $c->shipper->name,
                                'store_name' => $c->store->name,
                                'date' => $created_at,
                                'total_order' => count($check_date),
                                'sum_money' => $check_date->SUM('discount'),
                            );
                            $date_check[] = $created_at;
                            $total_money += $check_date->SUM('discount');
                        }
                    }
                }
            }
            $data = array(
                'title' => $voucher->title,
                'code' => $voucher->code,
                'time_open' => date('d-m-Y', strtotime($voucher->time_open)),
                'time_close' => date('d-m-Y', strtotime($voucher->time_close)),
                'note' => $voucher->type_percent_or_money == 1 ? "Giảm mỗi đơn hàng " . $voucher->percent . '%' : "Giảm mỗi đơn hàng " . $voucher->money . ' VNĐ',
                'list_data' => $array,
                'total_money' => number_format($total_money) . ' VNĐ',
            );
            $date = date("Y-m-d H:i:s");
            $name_excel = 'Báo cáo sử dụng voucher ' . $voucher->title . ' ngày ' . $date . '.xlsx';
            return Excel::download(new VoucherShipperExport($data), $name_excel);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::export_success, $data);
        }
    }
    public function listVoucher(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1  && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if($user->role_id == 1){
            if ($request->store_id) {
                $data = Voucher::where([['store_id', $request->store_id]])->orderby('id', 'desc')->with('store')->get();
            } else {
                $data = Voucher::orderby('id', 'desc')->with('store')->get();
            }
        }else{
            if ($request->store_id) {
                $data = Voucher::where([['store_id', $request->store_id], ['transfer_id', $user->id]])->orderby('id', 'desc')->with('store')->get();
            } else {
                $data = Voucher::where('transfer_id', $user->id)->orderby('id', 'desc')->with('store')->get();
            }
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function voucherDetail($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1  && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Voucher::findorfail($id)->load('user', 'store');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function createVoucher(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'code' => 'required',
            'description' => 'required',
            'type' => 'required',
            'proviso' => 'required',
            'quantity' => 'required',
            'time_open' => 'required|date',
            'time_close' => 'required|date|after_or_equal:time_open',
        ], [
            'title.required' => 'Vui lòng nhập tiêu đề voucher',
            'code.required' => 'Vui lòng nhập mã voucher',
            'description.required' => 'Vui lòng nhập mô tả voucher',
            'type.required' => 'Vui lòng chọn loại voucher',
            'proviso.required' => 'Vui lòng nhập điều kiện sử dụng voucher',
            'quantity.required' => 'Vui lòng nhập số lượng',
            'time_open.required' => 'Vui lòng nhập thời gian bắt đầu',
            'time_open.date' => 'Vui lòng nhập đúng định dạng',
            'time_open.after_or_equal' => 'Vui lòng nhập thời gian bắt đầu sau ngày hôm nay',
            'time_close.date' => 'Vui lòng nhập đúng định dạng',
            'time_close.required' => 'Vui lòng nhập thời gian kết thúc',
            'time_close.after_or_equal' => 'Thời gian kết thúc không thể trước thời gian bắt đầu',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if (date('Y-m-d', strtotime($request->time_open)) < date('Y-m-d')) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập thời gian bắt đầu sau ngày hôm nay", null);
        }
        if ($request->store_id) {
            if (!Store::find($request->store_id)) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $req = $request->all();
        if ($request->percent) {
            $req['money'] = 0;
            $req['type_percent_or_money'] = 1;
        } else if ($request->money) {
            $req['percent'] = 0;
            $req['type_percent_or_money'] = 2;
        }
        if ($request->type == 2 && $request->type_percent_or_money == 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng chọn loại giảm tiền cho voucher miễn phí ship.", null);
        }
        $req['time_open'] = date('Y-m-d', strtotime($request->time_open));
        $req['time_close'] = date('Y-m-d', strtotime($request->time_close));
        if (date('Y-m-d', strtotime($request->time_open)) == date('Y-m-d')) {
            $req['status'] = 2;
        }
        if ($user->role_id == 13 || $user->role_id == 14) {
            $req['is_transfer'] = 1;
            $req['transfer_id'] = $user->id;
        }
        $Voucher = Voucher::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $Voucher);
    }
    public function updateVoucher($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Voucher::findorfail($id);
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'code' => 'required',
            'description' => 'required',
            'type' => 'required',
            'proviso' => 'required',
            'quantity' => 'required',
            'time_open' => 'required|date',
            'time_close' => 'required|date|after_or_equal:time_open',
        ], [
            'title.required' => 'Vui lòng nhập tiêu đề voucher',
            'code.required' => 'Vui lòng nhập mã voucher',
            'description.required' => 'Vui lòng nhập mô tả voucher',
            'type.required' => 'Vui lòng chọn loại voucher',
            'proviso.required' => 'Vui lòng nhập điều kiện sử dụng voucher',
            'quantity.required' => 'Vui lòng nhập số lượng',
            'time_open.required' => 'Vui lòng nhập thời gian bắt đầu',
            'time_open.date' => 'Vui lòng nhập đúng định dạng',
            'time_open.after_or_equal' => 'Vui lòng nhập thời gian bắt đầu sau ngày hôm nay',
            'time_close.date' => 'Vui lòng nhập đúng định dạng',
            'time_close.required' => 'Vui lòng nhập thời gian kết thúc',
            'time_close.after_or_equal' => 'Thời gian kết thúc không thể trước thời gian bắt đầu',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        if (date('Y-m-d', strtotime($request->time_open)) < date('Y-m-d')) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng nhập thời gian bắt đầu sau ngày hôm nay", null);
        }
        if ($request->store_id) {
            if (!Store::find($request->store_id)) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::error_id_store_not_found, null);
            }
        }
        $req = $request->all();
        if ($request->percent) {
            $req['money'] = 0;
            $req['type_percent_or_money'] = 1;
        } else if ($request->money) {
            $req['percent'] = 0;
            $req['type_percent_or_money'] = 2;
        }
        if ($request->type == 2 && $request->type_percent_or_money == 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, "Vui lòng chọn loại giảm tiền cho voucher miễn phí ship.", null);
        }
        $req['time_open'] = date('Y-m-d', strtotime($request->time_open));
        $req['time_close'] = date('Y-m-d', strtotime($request->time_close));
        if (date('Y-m-d', strtotime($request->time_open)) == date('Y-m-d')) {
            $req['status'] = 2;
        }
        $check->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $check);
    }
    public function deleteVoucher($id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $check = Voucher::findorfail($id);
        $check->user()->detach();
        $check->update([
            'status' => 3,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $check);
    }
}
