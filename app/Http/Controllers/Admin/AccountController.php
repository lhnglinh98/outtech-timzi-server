<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\User;
use App\Models\HistoryMoneyShipper;
use App\Models\HistoryRechargeAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    //
    public function listChainOwner(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 9 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        if($user->role_id == 11 || $user->role_id == 13 || $user->role_id == 14){
            if(!$user->manage_district_id){
                $user = User::where([['role_id', 2], ['status', 1]])->where('province_id', $user->manage_province_id)->orderby('id', 'desc')->get();
            }else{
                $user = User::where([['role_id', 2], ['status', 1]])->where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->orderby('id', 'desc')->get();
            }
        }else{
            $user = User::where([['role_id', 2], ['status', 1]])->orderby('id', 'desc')->get();
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $user);
    }
    public function listAccount(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 7 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [['role_id', 5], ['status', '!=', 3]];
        if ($request->name) {
            $where[] = ['name', 'like', '%' . $request->name . '%'];
        }
        if ($request->phone) {
            $where[] = ['phone', 'like', '%' . $request->phone . '%'];
        }
        if ($request->status != null) {
            $where[] = ['status', $request->status];
        }
        if($user->role_id == 11 || $user->role_id == 13 || $user->role_id == 14){
            if(!$user->manage_district_id){
                $user2 = User::where($where)->where('province_id', $user->manage_province_id)->orderby('id', 'desc')->paginate(12);
                $user_count = User::where($where)->where('province_id', $user->manage_province_id)->orderby('id', 'desc')->get();
            }else{
                $user2 = User::where($where)->where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->orderby('id', 'desc')->paginate(12);
                $user_count = User::where($where)->where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->orderby('id', 'desc')->get();
            }
        }else{
            $user2 = User::where($where)->orderby('id', 'desc')->paginate(12);
            $user_count = User::where($where)->orderby('id', 'desc')->get();
        }
        $data = array(
            'user' => $user2,
            'user_count' => count($user_count)
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listShipper(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 8 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [['role_id', 4], ['status', '!=', 3]];
        if ($request->name) {
            $where[] = ['name', 'like', '%' . $request->name . '%'];
        }
        if ($request->phone) {
            $where[] = ['phone', 'like', '%' . $request->phone . '%'];
        }
        if ($request->status != null) {
            $where[] = ['status', $request->status];
        }
        if($request->province_id){
            $where[] = ['province_id', $request->province_id];
        }
        if($request->district_id){
            $where[] = ['district_id', $request->district_id];
        }
        if($user->role_id == 11 || $user->role_id == 13 || $user->role_id == 14){
            if(!$user->manage_district_id){
                $user2 = User::where($where)->where('province_id', $user->manage_province_id)->orderby('id', 'desc')->paginate(12);
                $user_count = User::where($where)->where('province_id', $user->manage_province_id)->orderby('id', 'desc')->get();
            }else{
                $user2 = User::where($where)->where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->orderby('id', 'desc')->paginate(12);
                $user_count = User::where($where)->where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->orderby('id', 'desc')->get();
            }
        }else{
            $user2 = User::where($where)->orderby('id', 'desc')->paginate(12);
            $user_count = User::where($where)->orderby('id', 'desc')->get();
        }
        $data = array(
            'user' => $user2,
            'user_count' => count($user_count)
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listStaffStore(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 9 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [['status', '!=', 3]];
        if ($request->name) {
            $where[] = ['name', 'like', '%' . $request->name . '%'];
        }
        if ($request->phone) {
            $where[] = ['phone', 'like', '%' . $request->phone . '%'];
        }
        if ($request->status != null) {
            $where[] = ['status', $request->status];
        }
        if($user->role_id == 11 || $user->role_id == 13 || $user->role_id == 14){
            if(!$user->manage_district_id){
                $user2 = User::where($where)->whereIn('role_id', [2, 3, 6])->where('province_id', $user->manage_province_id)->orderby('id', 'desc')->paginate(12);
                $user_count = User::where($where)->whereIn('role_id', [2, 3, 6])->where('province_id', $user->manage_province_id)->orderby('id', 'desc')->get();
            }else{
                $user2 = User::where($where)->whereIn('role_id', [2, 3, 6])->where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->orderby('id', 'desc')->paginate(12);
                $user_count = User::where($where)->whereIn('role_id', [2, 3, 6])->where([['province_id', $user->manage_province_id], ['district_id', $user->manage_district_id]])->orderby('id', 'desc')->get();
            }
        }else{
            $user2 = User::where($where)->whereIn('role_id', [2, 3, 6])->orderby('id', 'desc')->with('store', 'staffStore')->paginate(12);
            $user_count = User::where($where)->whereIn('role_id', [2, 3, 6])->orderby('id', 'desc')->get();
        }
        $data = array(
            'user' => $user2,
            'user_count' => count($user_count)
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function deleteAccount($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 7 && $user->role_id != 8 && $user->role_id != 9 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $update = User::where('id', $id)->update([
            'status' => 3,
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $update);
    }
    public function accountDetail($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($admin->role_id != 1) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        // }
        $user = User::findorfail($id)->load('role', 'store', 'staffStore');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $user);
    }
    public function lockAccount($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($admin->role_id != 1) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        // }
        $update = User::findorfail($id)->update(['status' => 2]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::lock_account_success, $update);
    }
    public function unlockAccount($id)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($admin->role_id != 1) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        // }
        $update = User::findorfail($id)->update(['status' => 1]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::unlock_account_success, $update);
    }
    public function createAccount(Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 7 && $admin->role_id != 8 && $admin->role_id != 9 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'role_id' => 'required',
            'phone' => 'required|regex:/^0([0-9]{9})+$/',
            // 'email' => 'sometimes|email|unique:users',
            'password' => 'required|min:6|max:100|confirmed',
        ], [
            'name.required' => 'Vui lòng nhập tên chủ chuỗi',
            'name.string' => 'Tên chủ chuỗi phải là kiểu chuỗi',
            'name.max' => 'Tên chủ chuỗi không quá :max kí tự',
            'role_id.required' => 'Vui lòng chọn quyền tài khoản',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.regex' => 'Định dạng số điện thoại không đúng',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.min' => "Mật khẩu không dưới :min kí tự!",
            'password.max' => "Mật khẩu không quá :max kí tự!",
            'password.confirmed' => "Xác nhận mật khẩu không chính xác!",
            // 'email.unique' => 'Email đã tồn tại',
            // 'email.email' => 'Vui lòng nhập email đúng định dạng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        if ($request->phone && User::where([['phone', $request->phone], ['status', '!=', 3]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_phone_already_exist, null);
        }
        do {
            $req['code'] = 'TZ' . $this->makeCodeRandom(8);
        } while (User::where('code', $req['code'])->exists());
        $req['status'] = 1;
        $user = User::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $user);
    }
    public function updateAccount($id, Request $request)
    {
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 7 && $admin->role_id != 8 && $admin->role_id != 9 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'sometimes|string|max:255',
            'role_id' => 'required',
        ], [
            'name.string' => 'Tên người dùng phải là kiểu chuỗi',
            'name.max' => 'Tên người dùng không quá :max kí tự',
            'role_id.required' => 'Vui lòng chọn quyền tài khoản',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $user = User::findorfail($id);
        $req = $request->all();
        if ($request->email && User::where([['id', '<>', $user->id], ['email', $request->email]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_email_already_exist, null);
        }
        if ($request->email && User::where([['id', '<>', $user->id], ['phone', $request->phone]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_invaild, SystemParam::error_phone_already_exist, null);
        }
        $user->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $user);
    }
    public function rechargeAccount(Request $request)
    {
        try {
            $admin = $this->getAuthenticatedUser();
            if (!$admin) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            if ($admin->role_id != 1 && $admin->role_id != 7 && $admin->role_id != 8 && $admin->role_id != 9 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
            }
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'money' => 'required',
            ], [
                'user_id.required' => 'Vui lòng chọn tài khoản cần nạp',
                'money.required' => 'Vui lòng nhập số tiền cần nạp',
            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }
            $check_user = User::findorfail($request->user_id);
            $money = $check_user->money + $request->money;
            $check_user->update([
                'money' => $money,
            ]);
            HistoryRechargeAccount::insert([
                'money' => $request->money,
                'shipper_id' => $request->user_id,
                'money_current' => $money,
                'content' => "Nạp thành công với số tiền " . number_format($request->money) . 'VNĐ.',
                'created_at' => date('Y-m-d H:i:s')
            ]);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::recharge_account_success, $check_user);
        } catch (\Exception $e) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::message_error_server, null);
        }
    }
    public function deleteAccountPhone($phone){
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $user = User::where('phone', $phone)->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $user);
    }
    public function resetPassword($id){
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($admin->role_id != 1 && $admin->role_id != 7 && $admin->role_id != 8 && $admin->role_id != 9 && $admin->role_id != 11 && $admin->role_id != 13 && $admin->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        }
        $check_user = User::findorfail($id);
        $check_user->update([
            'password' => "timzi123"
        ]);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::reset_password_success, $check_user);
    }
    public function listHistoryMoneyShipper($id){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 8 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = HistoryMoneyShipper::where('shipper_id', $id)->orderby('id', 'desc')->paginate(24);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listHistoryRechargeAccount($id, Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 8 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['created_at', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['created_at', '<=', $to_date];
        }
        $data = HistoryRechargeAccount::where('shipper_id', $id)->where($where)->orderby('id', 'desc')->paginate(24);
        $total_money = HistoryRechargeAccount::where('shipper_id', $id)->where($where)->get()->SUM('money');
        $array = array(
            'list' => $data,
            'total_money' => $total_money
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
    public function totalRechargeAccountTransfer(Request $request){
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1 && $user->role_id != 8 && $user->role_id != 11 && $user->role_id != 13 && $user->role_id != 14) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $where = [];
        if ($request->from_date) {
            $from_date = date('Y-m-d H:i:s', strtotime($request->from_date));
            $where[] = ['history_recharge_account.created_at', '>=', $from_date];
        }
        if ($request->to_date) {
            $to_date = date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 60 * 60 - 1);
            $where[] = ['history_recharge_account.created_at', '<=', $to_date];
        }

        if($user->role_id == 11 || $user->role_id == 13 || $user->role_id == 14){
            if(!$user->manage_district_id){
                $where[] = ['users.province_id', $user->manage_province_id];
                $data = HistoryRechargeAccount::select('history_recharge_account.*')->join('users', 'users.id', 'history_recharge_account.shipper_id')->where($where)->with('shipper')->orderby('history_recharge_account.id', 'desc')->paginate(24);
                $total_money = HistoryRechargeAccount::join('users', 'users.id', 'history_recharge_account.shipper_id')->where($where)->get()->SUM('money');
            }else{
                $where[] = ['users.province_id', $user->manage_province_id];
                $where[] = ['users.district_id', $user->manage_district_id];
                $data = HistoryRechargeAccount::select('history_recharge_account.*')->join('users', 'users.id', 'history_recharge_account.shipper_id')->where($where)->with('shipper')->orderby('history_recharge_account.id', 'desc')->paginate(24);
                $total_money = HistoryRechargeAccount::join('users', 'users.id', 'history_recharge_account.shipper_id')->where($where)->get()->SUM('money');
            }
        }else{
            if ($request->province_id) {
                $where[] = ['users.province_id', $request->province_id];
            }
            if ($request->district_id) {
                $where[] = ['users.district_id', $request->district_id];
            }
            $data = HistoryRechargeAccount::select('history_recharge_account.*')->join('users', 'users.id', 'history_recharge_account.shipper_id')->where($where)->with('shipper')->orderby('history_recharge_account.id', 'desc')->paginate(24);
            $total_money = HistoryRechargeAccount::join('users', 'users.id', 'history_recharge_account.shipper_id')->where($where)->get()->SUM('money');
        }
        $array = array(
            'list' => $data,
            'total_money' => $total_money
        );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $array);
    }
}
